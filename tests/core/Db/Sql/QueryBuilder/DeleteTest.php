<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;

class DeleteTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Delete', new Delete());
    } // testCanCreateInstance()


    public function testDeleteAll()
    {
        $instance = new Delete();
        $instance -> from('products');

        $result = $instance -> build();

        $this -> assertEquals('delete from products', $result[0]);
        $this -> assertEmpty($result[1]);
    } // testDeleteAll()


    public function testSetTableViaConstructor()
    {
        $instance = new Delete('products');

        $result = $instance -> build();

        $this -> assertEquals('delete from products', $result[0]);
        $this -> assertEmpty($result[1]);
    } // testSetTableViaConstructor()


    public function testWhere()
    {
        $instance = new Delete('products');
        $instance -> where()
            -> lt('screen', 12)
            -> andEqual('color', 'green')
            -> orIn('model', array('a-5', 'a-6', 'a-9'));

        $result = $instance -> build();

        $this -> assertEquals('delete from products where screen < :where_0 and color = :where_1 or model in (:where_in_2, :where_in_3, :where_in_4)', $result[0]);
        $this -> assertEquals(array(
            ':where_0' => 12,
            ':where_1' => 'green',
            ':where_in_2' => 'a-5',
            ':where_in_3' => 'a-6',
            ':where_in_4' => 'a-9'
        ), $result[1]);
    } // testWhere()

    public function testUndefinedTableException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Delete();
        $instance -> build();
    } // testUndefinedTableException()
} 