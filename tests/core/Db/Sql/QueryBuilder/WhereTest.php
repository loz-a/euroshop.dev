<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;
use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\QueryBuilder\Where\SubqueryBuilder;
use Faker;

class WhereTest extends \PHPUnit_Framework_TestCase
{        
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()


    public function testCanCreateInstance()
    {
        $instance = new Where(new QueryBuilder());
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Where', $instance);
    } // testCanCreateInstance()


    public function testAddBoundParam()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> params() -> set(array(':title' => 'test_title', ':Alias' => 'test_Alias'));
        $this -> assertEquals(array(':title' => 'test_title', ':alias' => 'test_Alias'), $instance -> params() -> toArray());

        $instance -> params() -> reset() -> set(array('title' => 'test_title', 'Alias' => 'test_alias'));
        $this -> assertEquals(array(':where_title_0' => 'test_title', ':where_alias_1' => 'test_alias'),  $instance -> params() -> toArray());
    } // testAddBoundParam()


    public function testEqual()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> equal('title', $title);
        $this -> assertEquals('where title = :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $title), $instance -> params() -> toArray());
    } // testEqual()


    public function testEqualNull()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> equal('title', null);
        $this -> assertEquals('where title is null', $instance -> build());
    } // testEqualNull()


    public function testNotEqual()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> notEqual('title', $title);
        $this -> assertEquals('where title != :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $title), $instance -> params() -> toArray());
    } // testEqual()


    public function testNotEqualNull()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> notEqual('title', null);
        $this -> assertEquals('where title is not null', $instance -> build());
    } // testNotEqualNull()


    public function testLikeAndNotLike()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> like('title', '%go') -> andNotLike('title', 'h_');

        $this -> assertEquals('where title like :where_0 and title not like :where_1', $instance -> build());
        $this -> assertEquals(array(':where_0' => '%go', ':where_1' => 'h_'), $instance -> params() -> toArray());
    } // testLike()


    public function testGt()
    {
        $time = Faker\Factory::create() -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> gt('create_date', $time);

        $this -> assertEquals('where create_date > :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $time), $instance -> params() -> toArray());
    }


    public function testGte()
    {
        $time = Faker\Factory::create() -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> gte('create_date', $time);

        $this -> assertEquals('where create_date >= :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $time), $instance -> params() -> toArray());
    }


    public function testLt()
    {
        $time = Faker\Factory::create() -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> lt('create_date', $time);

        $this -> assertEquals('where create_date < :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $time), $instance -> params() -> toArray());
    }


    public function testlte()
    {
        $time = Faker\Factory::create() -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> lte('create_date', $time);

        $this -> assertEquals('where create_date <= :where_0', $instance -> build());
        $this -> assertEquals(array(':where_0' => $time), $instance -> params() -> toArray());
    }


    public function testIn()
    {
        $faker = Faker\Factory::create();

        $time0 = $faker -> unixTime();
        $time1 = $faker -> unixTime();
        $time2 = $faker -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> in('create_date', array($time0, $time1, $time2));

        $this -> assertEquals('where create_date in (:where_in_0, :where_in_1, :where_in_2)', $instance -> build());
        $this -> assertEquals(array(
            ':where_in_0' => $time0,
            ':where_in_1' => $time1,
            ':where_in_2' => $time2
        ), $instance -> params() -> toArray());
    } // testIn()


    public function testNotIn()
    {
        $faker = Faker\Factory::create();

        $time0 = $faker -> unixTime();
        $time1 = $faker -> unixTime();
        $time2 = $faker -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> notIn('create_date', array($time0, $time1, $time2));

        $this -> assertEquals('where create_date not in (:where_in_0, :where_in_1, :where_in_2)', $instance -> build());
        $this -> assertEquals(array(
            ':where_in_0' => $time0,
            ':where_in_1' => $time1,
            ':where_in_2' => $time2
        ), $instance -> params() -> toArray());
    } // testNotIn()


    public function testBetween()
    {
        $faker = Faker\Factory::create();

        $min = $faker -> unixTime();
        $max = $faker -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> between('create_date', $min, $max);

        $this -> assertEquals('where create_date between :where_min_0 and :where_max_1', $instance -> build());
        $this -> assertEquals(array(
            ':where_min_0' => $min,
            ':where_max_1' => $max
        ), $instance -> params() -> toArray());
    } // testBetween()


    public function testNotBetween()
    {
        $faker = Faker\Factory::create();

        $min = $faker -> unixTime();
        $max = $faker -> unixTime();

        $instance = new Where(new QueryBuilder());
        $instance -> notBetween('create_date', $min, $max);

        $this -> assertEquals('where create_date not between :where_min_0 and :where_max_1', $instance -> build());
        $this -> assertEquals(array(
            ':where_min_0' => $min,
            ':where_max_1' => $max
        ), $instance -> params() -> toArray());
    } // testNotBetween()


    public function testIsNull()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> isNull('title');

        $this -> assertEquals('where title is null', $instance -> build());
    } // testIsNull()


    public function testIsNotNull()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> isNotNull('title');

        $this -> assertEquals('where title is not null', $instance -> build());
    } // testIsNotNull()


    public function testPredicate()
    {
        $id = 5;
        $instance = new Where(new QueryBuilder());
        $instance -> predicate('id = :id', array(':id' => $id));

        $this -> assertEquals('where id = :id', $instance -> build());
        $this -> assertEquals(array(':id' => $id), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> predicate('id is null');
        $this -> assertEquals('where id is null', $instance -> build());
        $this -> assertEquals(array(), $instance -> params() -> toArray());
    } // testPredicate()


    public function testAndOperators()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> equal('title', $title) -> andIsNull('alias');
        $this -> assertEquals('where title = :where_0 and alias is null', $instance -> build());
        $this -> assertEquals(array(':where_0' => $title), $instance -> params() -> toArray());
    } // testAnd()


    public function testOrOperator()
    {
        $faker    = Faker\Factory::create();
        $title    = $faker -> sentence();
        $instance = new Where(new QueryBuilder());

        $instance -> equal('title', $title) -> orIsNull('alias');
        $this -> assertEquals('where title = :where_0 or alias is null', $instance -> build());
        $this -> assertEquals(array(':where_0' => $title), $instance -> params() -> toArray());
    } // testOrOperator()


    public function testExistsPredicate()
    {
        $instance = new Where(new QueryBuilder());
        $instance
            -> equal('type', 'laptop')
            -> andSubquery(
                (new SubqueryBuilder())
                    -> exists()
                    -> select()
                    -> from('maker')
                    -> where() -> equal('type', 'Printer') -> andEqual('maker', 'HP')
            );

        $this -> assertEquals('where type = :where_0 and exists (select * from maker where type = :where_exists_1 and maker = :where_exists_2)', $instance -> build());
        $this -> assertEquals(array(':where_0' => 'laptop', ':where_exists_1' => 'Printer', ':where_exists_2' => 'HP'), $instance -> params() -> toArray());
    } // testExistsPredicate()


    public function testNotExistsPredicate()
    {
        $instance = new Where(new QueryBuilder());
        $instance
            -> andSubquery((new SubqueryBuilder()) -> notExists() -> select() -> from('maker') -> where() -> equal('type', 'Printer'));

        $this -> assertEquals('where not exists (select * from maker where type = :where_notexists_0)', $instance -> build());
        $this -> assertEquals(array(':where_notexists_0' => 'Printer'), $instance -> params() -> toArray());
    } // testNotExistsPredicate()


    public function testAll()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> andGt('price', (new SubqueryBuilder()) -> all() -> select() -> from('maker') -> where() -> equal('type', 'Printer'));

        $this -> assertEquals('where price > all (select * from maker where type = :where_all_0)', $instance -> build());
        $this -> assertEquals(array(':where_all_0' => 'Printer'), $instance -> params() -> toArray());
    } // testAll()


    public function testAny()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> andGt('price', (new SubqueryBuilder()) -> any() -> select() -> from('maker') -> where() -> equal('type', 'Printer'));

        $this -> assertEquals('where price > any (select * from maker where type = :where_any_0)', $instance -> build());
        $this -> assertEquals(array(':where_any_0' => 'Printer'), $instance -> params() -> toArray());
    } // testAny()


    public function testSome()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> andGt('price', (new SubqueryBuilder())
            -> some() -> select()
            -> from('maker')
            -> where() -> equal('type', 'Printer')
            -> group()
            -> by('alias')
            -> lte('date_create', 999999999));

        $this -> assertEquals('where price > some (select * from maker where type = :where_some_0 group by alias having date_create <= :group_some_1)', $instance -> build());
        $this -> assertEquals(array(':where_some_0' => 'Printer', ':group_some_1' => 999999999), $instance -> params() -> toArray());
    } // testAny()


    public function testSubquery()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> andGt('price', (new SubqueryBuilder()) -> select() -> min('price') -> from('maker') -> where() -> equal('type', 'Printer'));

        $this -> assertEquals('where price > (select min(price) from maker where type = :where__0)', $instance -> build());
        $this -> assertEquals(array(':where__0' => 'Printer'), $instance -> params() -> toArray());
    } // testSubquery()


    public function testAndOperatorBadMethodCallException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\BadMethodCallException');
        $instance = new Where(new QueryBuilder());
        $instance -> failMethod();
    } // testAndOperatorBadMethodCallException()


    public function testInvalidPredicate()
    {
        $instance = new Where(new QueryBuilder());
        $instance -> orIsNull('alias') -> build();
        $this -> assertEquals('where alias is null', $instance -> build());
    } // testInvalidClause()
}