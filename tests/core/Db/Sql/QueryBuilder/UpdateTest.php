<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;

class UpdateTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Update', new Update());
    } // testCanCreateInstance()


    public function testUpdate()
    {
        $instance = new Update();
        $instance
            -> table('products')
            -> set(array('model' => 'test_model', 'maker' => 'test_maker', 'type' => 'test_type'));

        $result = $instance -> build();

        $this -> assertEquals('update products set model = :update_0, maker = :update_1, type = :update_2', $result[0]);
        $this -> assertEquals(array(':update_0' => 'test_model', ':update_1' => 'test_maker', ':update_2' => 'test_type'), $result[1]);
    } // testUpdate()


    public function testSetTableViaConstructor()
    {
        $instance = new Update();
        $instance
            -> table('products')
            -> set(array('model' => 'test_model', 'maker' => 'test_maker', 'type' => 'test_type'))
            -> where() -> equal('id', 1);

        $result = $instance -> build();

        $this -> assertEquals('update products set model = :update_1, maker = :update_2, type = :update_3 where id = :where_0', $result[0]);
        $this -> assertEquals(array(':update_1' => 'test_model', ':update_2' => 'test_maker', ':update_3' => 'test_type', ':where_0' => 1), $result[1]);
    } // testSetTableViaConstructor()


    public function testUndefinedTableException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Update();
        $instance -> build();
    } // testUndefinedTableException()


    public function testUndefinedValuesExceptions()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Update('products');
        $instance -> build();
    } // testUndefinedValuesExceptions()


    public function testIntIndexForSetValueException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidArgumentException');
        $instance = new Update('products');
        $instance -> set(array('hello'));
        $instance -> build();
    } // testIntIndexForSetValueException()
}