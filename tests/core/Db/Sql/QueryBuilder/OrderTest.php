<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;

class OrderTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCreateInstance()
    {
        $instance = new Order(new QueryBuilder());
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Order', $instance);
    } // testCanCreateInstance()


    public function testAsc()
    {
        $instance = new Order(new QueryBuilder());
        $instance -> asc('date_create');
        $this -> assertEquals('order by date_create asc', $instance -> build());
    } // testAsc()


    public function testDesc()
    {
        $instance = new Order(new QueryBuilder());
        $instance -> desc('date_create');
        $this -> assertEquals('order by date_create desc', $instance -> build());
    } // testDesc()


    public function testAscAndDesc()
    {
        $instance = new Order(new QueryBuilder());
        $instance -> asc('date_create') -> desc('id');
        $this -> assertEquals('order by date_create asc, id desc', $instance -> build());
    } // testAscAndDesc()


    public function testEmptyColumnsException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Order(new QueryBuilder());
        $instance -> build();
    } // testEmptyColumnsException()
} 