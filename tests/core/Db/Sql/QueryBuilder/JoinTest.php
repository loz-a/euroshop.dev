<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;
use Core\Db\Sql\QueryBuilder;

class JoinTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()

    public function testCanCreateInstance()
    {
        $instance = new Join(new QueryBuilder());
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Join', $instance);
    } // testCanCreateInstance()


    public function testFullJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> full() -> with('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('full join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testFullJoin()


    public function testShortFullJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> full('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('full join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testShortFullJoin()


    public function testLeftJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> left() -> with('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('left join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testLeftJoin()


    public function testShortLeftJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> left('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('left join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testShortLeftJoin()


    public function testRightJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> right() -> with('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('right join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testRightJoin()


    public function testShortRightJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> right('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('right join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testShortRightJoin()


    public function testInnerJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> on('page.id = translate_page.page_id');
        $this -> assertEquals('inner join Pages on page.id = translate_page.page_id', $instance -> build());
    } // testInnerJoin()


    public function testTableAlias()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages', 'p') -> on('p.id = translate_page.page_id');
        $this -> assertEquals('inner join Pages as p on p.id = translate_page.page_id', $instance -> build());
    } // testTableAlias()


    public function testTableAliasWithShortRightJoin()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> right('Pages', 'p') -> on('p.id = translate_page.page_id');
        $this -> assertEquals('right join Pages as p on p.id = translate_page.page_id', $instance -> build());
    } // testTableAliasWithShortRightJoin()


    public function testTableUndefinedException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Join(new QueryBuilder());
        $instance -> build();
    } // testUndefinedTable()


    public function testPredicateUndefinedException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> build();
    } // testPredicateUndefinedException()


    public function testAndPredicates()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> on('page.id = translate_page.page_id') -> andEqual('translate_page.locale_id', 2);
        $this -> assertEquals('inner join Pages on page.id = translate_page.page_id and translate_page.locale_id = :join_0', $instance -> build());
        $this -> assertEquals(array(':join_0' => 2), $instance -> params() -> toArray());
    } // testAndPredicates()


    public function testOrPredicates()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> on('page.id = translate_page.page_id') -> orEqual('translate_page.locale_id', 2);
        $this -> assertEquals('inner join Pages on page.id = translate_page.page_id or translate_page.locale_id = :join_0', $instance -> build());
        $this -> assertEquals(array(':join_0' => 2), $instance -> params() -> toArray());
    } // testAndPredicates()


    public function testOrShortPredicate()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> on('page.id = translate_page.page_id') -> orPredicate('translate_page.locale_id') -> equal(2);
        $this -> assertEquals('inner join Pages on page.id = translate_page.page_id or translate_page.locale_id = :join_0', $instance -> build());
        $this -> assertEquals(array(':join_0' => 2), $instance -> params() -> toArray());
    } // testOrShortPredicate()


    public function testEmptyPredicate()
    {
        $instance = new Join(new QueryBuilder());
        $instance -> with('Pages') -> on('page.id = translate_page.page_id') -> orPredicate('') -> equal('translate_page.locale_id', 2);
        $this -> assertEquals('inner join Pages on page.id = translate_page.page_id or translate_page.locale_id = :join_0', $instance -> build());
        $this -> assertEquals(array(':join_0' => 2), $instance -> params() -> toArray());
    } // testOrShortPredicate()

} 