<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;

class SelectTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateInstance()
    {
        $instance = new Select(new QueryBuilder());
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Select', $instance);
    } // testCreateInstance()


    public function testColumnsAsArray()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> columns(array('title', 'alias', 'page_content' => 'content', 'page_create' => 'date_create'));
        $this -> assertEquals('select title, alias, content as page_content, date_create as page_create', $instance -> build());
    } // testColumnsAsArray()


    public function testColumnAsText()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> columns('title, alias, content as page_content, date_create as page_create');
        $this -> assertEquals('select title, alias, content as page_content, date_create as page_create', $instance -> build());
    } // testColumnAsText()


    public function testDistinct()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> columns('title, alias') -> distinct();
        $this -> assertEquals('select distinct title, alias', $instance -> build());
    } // testDistinct()


    public function testCountAll()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> count();
        $this -> assertEquals('select count(*)', $instance -> build());
    } // testCountAll()


    public function testDistinctCountAllException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Select(new QueryBuilder());
        $instance -> count() -> distinct();
        $instance -> build();
    } // testDistinctCountAll()


    public function testCountAndColumnsException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\InvalidClauseException');
        $instance = new Select(new QueryBuilder());
        $instance -> count() -> columns(array('title', 'alias'));
        $instance -> build();
    } // test()


    public function testCount()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> count('alias');
        $this -> assertEquals('select count(alias)', $instance -> build());
    } // testCount()


    public function testDistinctCount()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> count('alias', 'page_alias', true);
        $this -> assertEquals('select count(distinct alias) as page_alias', $instance -> build());
    } // testDistinctCount()


    public function testMax()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> max('date_create');
        $this -> assertEquals('select max(date_create)', $instance -> build());

        $instance1 = new Select(new QueryBuilder());
        $instance1 -> max('date_create', 'max_page_create');
        $this -> assertEquals('select max(date_create) as max_page_create', $instance1 -> build());
    } // testMax()


    public function testMin()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> min('date_create');
        $this -> assertEquals('select min(date_create)', $instance -> build());

        $instance1 = new Select(new QueryBuilder());
        $instance1 -> min('date_create', 'min_page_create');
        $this -> assertEquals('select min(date_create) as min_page_create', $instance1 -> build());
    } // testMin()


    public function testAvg()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> avg('date_create');
        $this -> assertEquals('select avg(date_create)', $instance -> build());

        $instance1 = new Select(new QueryBuilder());
        $instance1 -> avg('date_create', 'avg_page_create');
        $this -> assertEquals('select avg(date_create) as avg_page_create', $instance1 -> build());
    } // testAvg()


    public function testSum()
    {
        $instance = new Select(new QueryBuilder());
        $instance -> sum('date_create');
        $this -> assertEquals('select sum(date_create)', $instance -> build());

        $instance1 = new Select(new QueryBuilder());
        $instance1 -> sum('date_create', 'sum_page_create');
        $this -> assertEquals('select sum(date_create) as sum_page_create', $instance1 -> build());
    } // testSum()


    public function testBuild()
    {
        $instance = new Select(new QueryBuilder());
        $instance
            -> columns(array('title', 'alias', 'content'))
            -> count('date_create', 'page_create')
            -> distinct();

        $this -> assertEquals('select distinct title, alias, content, count(date_create) as page_create', $instance -> build());
    } // testBuild()


    public function testEmptyColumnsAndFunctionException()
    {
        $instance = new Select(new QueryBuilder());
        $this -> assertEquals('select *', $instance -> build());
    } // testEmptyColumnsAndFunctionException()


    public function testSetColumnsWithoutArray()
    {
        $instance = new Select(new QueryBuilder());
        $instance
            -> columns('title', 'alias', 'content')
            -> count('date_create', 'page_create')
            -> distinct();

        $this -> assertEquals('select distinct title, alias, content, count(date_create) as page_create', $instance -> build());
    } // testSetColumnsWithoutArray()
}
