<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\Params\Params;
use Core\Db\Sql\QueryBuilder;
use Faker;

class GroupTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        Params::resetVarIndex();
    } // setUp()


    public function testCanCreateInstance()
    {
        $instance = new Group(new QueryBuilder());
        $this -> assertInstanceOf('Core\Db\Sql\QueryBuilder\Group', $instance);
    } // testCanCreateInstance()


    public function testColumns()
    {
        $instance = new Group(new QueryBuilder());

        $instance -> by('alias');
        $this -> assertEquals('group by alias', $instance -> build());

        $instance -> by(array('alias', 'title'));
        $this -> assertEquals('group by alias, title', $instance -> build());
    } // testby()


    public function testRawHaving()
    {
        $time = Faker\Factory::create() -> unixTime();
        $instance = new Group(new QueryBuilder());
        $instance -> by(array('alias')) -> predicate('date_create > :group_date_create', array(':group_date_create' => $time));
        $this -> assertEquals('group by alias having date_create > :group_date_create', $instance -> build());
        $this -> assertEquals(array(':group_date_create' => $time), $instance -> params() -> toArray());
    } // test()


    public function testCount()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> count('views') -> gt(5);
        $this -> assertEquals('group by alias having count(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testCount()


    public function testMax()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> max('views') -> gt(5);
        $this -> assertEquals('group by alias having max(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testMax()


    public function testMin()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> min('views') -> gt(5);
        $this -> assertEquals('group by alias having min(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testMin()


    public function testAvg()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> avg('views') -> gt(5);
        $this -> assertEquals('group by alias having avg(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testAvg()


    public function testSum()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> gt(5);
        $this -> assertEquals('group by alias having sum(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testSum()


    public function testEqual()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> equal(5);
        $this -> assertEquals('group by alias having sum(views) = :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> equal('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) = :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testEqual()


    public function testNotEqual()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> notEqual(5);
        $this -> assertEquals('group by alias having sum(views) != :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> notEqual('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) != :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testNotEqual()


    public function testGt()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> gt(5);
        $this -> assertEquals('group by alias having sum(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> gt('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) > :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testGt()


    public function testGte()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> gte(5);
        $this -> assertEquals('group by alias having sum(views) >= :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> gte('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) >= :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testGte()


    public function testLt()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lt(5);
        $this -> assertEquals('group by alias having sum(views) < :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> lt('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) < :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testLt()


    public function testLte()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5);
        $this -> assertEquals('group by alias having sum(views) <= :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());

        $instance -> resetPredicates() -> lte('sum(views)', 5);
        $this -> assertEquals('group by alias having sum(views) <= :group_1', $instance -> build());
        $this -> assertEquals(array(':group_1' => 5), $instance -> params() -> toArray());
    } // testLte()


    public function testAnd()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5) -> andSum('views') -> lte(5);
        $this -> assertEquals('group by alias having sum(views) <= :group_0 and sum(views) <= :group_1', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5, ':group_1' => 5), $instance -> params() -> toArray());
    } // testAnd()


    public function testOr()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5) -> orSum('views') -> lte(5);
        $this -> assertEquals('group by alias having sum(views) <= :group_0 or sum(views) <= :group_1', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5, ':group_1' => 5), $instance -> params() -> toArray());
    } // testOr()


    public function testInvalidMethodException()
    {
        $this -> setExpectedException('Core\Db\Sql\QueryBuilder\Exception\BadMethodCallException');
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5) -> andHelloWorld();
    } // testInvalidMethodException()


    public function testResetPredicate()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5) -> orSum('views') -> lte(5) -> resetPredicates();
        $this -> assertEquals('group by alias', $instance -> build());
        $this -> assertEquals(array(), $instance -> params() -> toArray());
    } // testResetPredicate()


    public function testHasPredicates()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> sum('views') -> lte(5) -> orSum('views') -> lte(5);
        $this -> assertTrue($instance -> hasPredicates());
    } // testHasPredicates()


    public function testInvalidPredicate()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias') -> andLte('date_create', 999999999) -> orPredicate('');
        $this -> assertEquals('group by alias having date_create <= :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 999999999), $instance -> params() -> toArray());
    } // testInvalidPredicate()


    public function testAddColumnsWithoutArray()
    {
        $instance = new Group(new QueryBuilder());
        $instance -> by('alias', 'title', 'date_create') -> max('views') -> gt(5);
        $this -> assertEquals('group by alias, title, date_create having max(views) > :group_0', $instance -> build());
        $this -> assertEquals(array(':group_0' => 5), $instance -> params() -> toArray());
    } // testAddColumnsWithoutArray()
}