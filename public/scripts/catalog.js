(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
function formToObj(form) {
  var fields = formToArr(form);

  fields.sort(function (a, b) {
    return a.name.localeCompare(b.name);
  });

  return fields.reduce(function(obj, field) {
    addProp(obj, field.name, field.value);
    return obj;
  }, {});

  function formToArr(form) {
    var inputs = form.querySelectorAll('input, textarea, select, [contenteditable=true]');
    var arr = [];

    for (var i = 0; i < inputs.length; ++i) {
      var input = inputs[i],
          name = input.name || input.getAttribute('data-name'),
          val = input.value;

      if (!name ||
        ((input.type === 'checkbox' || input.type === 'radio') && !input.checked)) {
        continue;
      }

      if (input.getAttribute('contenteditable') === 'true') {
        val = input.innerHTML;
      }

      arr.push({
        name: name,
        value: val
      });
    }

    return arr;
  }

  function addProp(o, prop, val) {
    var props = prop.split('.');
    var lastProp = props.length - 1;

    props.reduce(function (obj, prop, i) {
      if (i === lastProp) {
        return setProp(obj, prop, val);
      } else {
        return setProp(obj, prop, {});
      }
    }, o);
  }

  function setProp(obj, name, val) {
    if (name.slice(-2) === '[]') {
      makeArr(obj, name).push(val);
    } else if (obj[name]) {
      return obj[name];
    } else if (name[name.length - 1] === ']') {
      var arr = makeArr(obj, name);

      if (arr.prevName === name) {
        return arr[arr.length - 1];
      }

      arr.push(val);
      arr.prevName = name;
    } else {
      obj[name] = val;
    }

    return val;
  }

  function makeArr(obj, name) {
    var arrName = name.replace(/\[\d*\]/, '');
    return (obj[arrName] || (obj[arrName] = []));
  }
}

if (typeof module !== 'undefined' && module.exports) {
  module.exports = formToObj;
}
},{}],2:[function(require,module,exports){
"use strict";
var window = require("global/window")
var once = require("once")
var isFunction = require("is-function")
var parseHeaders = require("parse-headers")
var xtend = require("xtend")

module.exports = createXHR
createXHR.XMLHttpRequest = window.XMLHttpRequest || noop
createXHR.XDomainRequest = "withCredentials" in (new createXHR.XMLHttpRequest()) ? createXHR.XMLHttpRequest : window.XDomainRequest

forEachArray(["get", "put", "post", "patch", "head", "delete"], function(method) {
    createXHR[method === "delete" ? "del" : method] = function(uri, options, callback) {
        options = initParams(uri, options, callback)
        options.method = method.toUpperCase()
        return _createXHR(options)
    }
})

function forEachArray(array, iterator) {
    for (var i = 0; i < array.length; i++) {
        iterator(array[i])
    }
}

function isEmpty(obj){
    for(var i in obj){
        if(obj.hasOwnProperty(i)) return false
    }
    return true
}

function initParams(uri, options, callback) {
    var params = uri

    if (isFunction(options)) {
        callback = options
        if (typeof uri === "string") {
            params = {uri:uri}
        }
    } else {
        params = xtend(options, {uri: uri})
    }

    params.callback = callback
    return params
}

function createXHR(uri, options, callback) {
    options = initParams(uri, options, callback)
    return _createXHR(options)
}

function _createXHR(options) {
    var callback = options.callback
    if(typeof callback === "undefined"){
        throw new Error("callback argument missing")
    }
    callback = once(callback)

    function readystatechange() {
        if (xhr.readyState === 4) {
            loadFunc()
        }
    }

    function getBody() {
        // Chrome with requestType=blob throws errors arround when even testing access to responseText
        var body = undefined

        if (xhr.response) {
            body = xhr.response
        } else if (xhr.responseType === "text" || !xhr.responseType) {
            body = xhr.responseText || xhr.responseXML
        }

        if (isJson) {
            try {
                body = JSON.parse(body)
            } catch (e) {}
        }

        return body
    }

    var failureResponse = {
                body: undefined,
                headers: {},
                statusCode: 0,
                method: method,
                url: uri,
                rawRequest: xhr
            }

    function errorFunc(evt) {
        clearTimeout(timeoutTimer)
        if(!(evt instanceof Error)){
            evt = new Error("" + (evt || "Unknown XMLHttpRequest Error") )
        }
        evt.statusCode = 0
        callback(evt, failureResponse)
    }

    // will load the data & process the response in a special response object
    function loadFunc() {
        if (aborted) return
        var status
        clearTimeout(timeoutTimer)
        if(options.useXDR && xhr.status===undefined) {
            //IE8 CORS GET successful response doesn't have a status field, but body is fine
            status = 200
        } else {
            status = (xhr.status === 1223 ? 204 : xhr.status)
        }
        var response = failureResponse
        var err = null

        if (status !== 0){
            response = {
                body: getBody(),
                statusCode: status,
                method: method,
                headers: {},
                url: uri,
                rawRequest: xhr
            }
            if(xhr.getAllResponseHeaders){ //remember xhr can in fact be XDR for CORS in IE
                response.headers = parseHeaders(xhr.getAllResponseHeaders())
            }
        } else {
            err = new Error("Internal XMLHttpRequest Error")
        }
        callback(err, response, response.body)

    }

    var xhr = options.xhr || null

    if (!xhr) {
        if (options.cors || options.useXDR) {
            xhr = new createXHR.XDomainRequest()
        }else{
            xhr = new createXHR.XMLHttpRequest()
        }
    }

    var key
    var aborted
    var uri = xhr.url = options.uri || options.url
    var method = xhr.method = options.method || "GET"
    var body = options.body || options.data || null
    var headers = xhr.headers = options.headers || {}
    var sync = !!options.sync
    var isJson = false
    var timeoutTimer

    if ("json" in options) {
        isJson = true
        headers["accept"] || headers["Accept"] || (headers["Accept"] = "application/json") //Don't override existing accept header declared by user
        if (method !== "GET" && method !== "HEAD") {
            headers["content-type"] || headers["Content-Type"] || (headers["Content-Type"] = "application/json") //Don't override existing accept header declared by user
            body = JSON.stringify(options.json)
        }
    }

    xhr.onreadystatechange = readystatechange
    xhr.onload = loadFunc
    xhr.onerror = errorFunc
    // IE9 must have onprogress be set to a unique function.
    xhr.onprogress = function () {
        // IE must die
    }
    xhr.ontimeout = errorFunc
    xhr.open(method, uri, !sync, options.username, options.password)
    //has to be after open
    if(!sync) {
        xhr.withCredentials = !!options.withCredentials
    }
    // Cannot set timeout with sync request
    // not setting timeout on the xhr object, because of old webkits etc. not handling that correctly
    // both npm's request and jquery 1.x use this kind of timeout, so this is being consistent
    if (!sync && options.timeout > 0 ) {
        timeoutTimer = setTimeout(function(){
            aborted=true//IE9 may still call readystatechange
            xhr.abort("timeout")
            var e = new Error("XMLHttpRequest timeout")
            e.code = "ETIMEDOUT"
            errorFunc(e)
        }, options.timeout )
    }

    if (xhr.setRequestHeader) {
        for(key in headers){
            if(headers.hasOwnProperty(key)){
                xhr.setRequestHeader(key, headers[key])
            }
        }
    } else if (options.headers && !isEmpty(options.headers)) {
        throw new Error("Headers cannot be set on an XDomainRequest object")
    }

    if ("responseType" in options) {
        xhr.responseType = options.responseType
    }

    if ("beforeSend" in options &&
        typeof options.beforeSend === "function"
    ) {
        options.beforeSend(xhr)
    }

    xhr.send(body)

    return xhr


}

function noop() {}

},{"global/window":3,"is-function":4,"once":5,"parse-headers":8,"xtend":9}],3:[function(require,module,exports){
(function (global){
if (typeof window !== "undefined") {
    module.exports = window;
} else if (typeof global !== "undefined") {
    module.exports = global;
} else if (typeof self !== "undefined"){
    module.exports = self;
} else {
    module.exports = {};
}

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],4:[function(require,module,exports){
module.exports = isFunction

var toString = Object.prototype.toString

function isFunction (fn) {
  var string = toString.call(fn)
  return string === '[object Function]' ||
    (typeof fn === 'function' && string !== '[object RegExp]') ||
    (typeof window !== 'undefined' &&
     // IE8 and below
     (fn === window.setTimeout ||
      fn === window.alert ||
      fn === window.confirm ||
      fn === window.prompt))
};

},{}],5:[function(require,module,exports){
module.exports = once

once.proto = once(function () {
  Object.defineProperty(Function.prototype, 'once', {
    value: function () {
      return once(this)
    },
    configurable: true
  })
})

function once (fn) {
  var called = false
  return function () {
    if (called) return
    called = true
    return fn.apply(this, arguments)
  }
}

},{}],6:[function(require,module,exports){
var isFunction = require('is-function')

module.exports = forEach

var toString = Object.prototype.toString
var hasOwnProperty = Object.prototype.hasOwnProperty

function forEach(list, iterator, context) {
    if (!isFunction(iterator)) {
        throw new TypeError('iterator must be a function')
    }

    if (arguments.length < 3) {
        context = this
    }
    
    if (toString.call(list) === '[object Array]')
        forEachArray(list, iterator, context)
    else if (typeof list === 'string')
        forEachString(list, iterator, context)
    else
        forEachObject(list, iterator, context)
}

function forEachArray(array, iterator, context) {
    for (var i = 0, len = array.length; i < len; i++) {
        if (hasOwnProperty.call(array, i)) {
            iterator.call(context, array[i], i, array)
        }
    }
}

function forEachString(string, iterator, context) {
    for (var i = 0, len = string.length; i < len; i++) {
        // no such thing as a sparse string.
        iterator.call(context, string.charAt(i), i, string)
    }
}

function forEachObject(object, iterator, context) {
    for (var k in object) {
        if (hasOwnProperty.call(object, k)) {
            iterator.call(context, object[k], k, object)
        }
    }
}

},{"is-function":4}],7:[function(require,module,exports){

exports = module.exports = trim;

function trim(str){
  return str.replace(/^\s*|\s*$/g, '');
}

exports.left = function(str){
  return str.replace(/^\s*/, '');
};

exports.right = function(str){
  return str.replace(/\s*$/, '');
};

},{}],8:[function(require,module,exports){
var trim = require('trim')
  , forEach = require('for-each')
  , isArray = function(arg) {
      return Object.prototype.toString.call(arg) === '[object Array]';
    }

module.exports = function (headers) {
  if (!headers)
    return {}

  var result = {}

  forEach(
      trim(headers).split('\n')
    , function (row) {
        var index = row.indexOf(':')
          , key = trim(row.slice(0, index)).toLowerCase()
          , value = trim(row.slice(index + 1))

        if (typeof(result[key]) === 'undefined') {
          result[key] = value
        } else if (isArray(result[key])) {
          result[key].push(value)
        } else {
          result[key] = [ result[key], value ]
        }
      }
  )

  return result
}
},{"for-each":6,"trim":7}],9:[function(require,module,exports){
module.exports = extend

var hasOwnProperty = Object.prototype.hasOwnProperty;

function extend() {
    var target = {}

    for (var i = 0; i < arguments.length; i++) {
        var source = arguments[i]

        for (var key in source) {
            if (hasOwnProperty.call(source, key)) {
                target[key] = source[key]
            }
        }
    }

    return target
}

},{}],10:[function(require,module,exports){
'use strict';

var _Catalog = require('./components/Catalog');

var _Catalog2 = _interopRequireDefault(_Catalog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.addEventListener('DOMContentLoaded', function () {
    _Catalog2.default.init('catalog');
});

},{"./components/Catalog":12}],11:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseCatalogWidget = (function (_BaseWidget) {
    _inherits(BaseCatalogWidget, _BaseWidget);

    function BaseCatalogWidget() {
        _classCallCheck(this, BaseCatalogWidget);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(BaseCatalogWidget).call(this));

        BaseCatalogWidget._data = new Map();
        return _this;
    }

    _createClass(BaseCatalogWidget, [{
        key: 'data',
        get: function get() {
            return BaseCatalogWidget._data;
        }
    }]);

    return BaseCatalogWidget;
})(_BaseWidget3.default);

exports.default = BaseCatalogWidget;

},{"./../lib/BaseWidget":46}],12:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseCatalogWidget = require('./BaseCatalogWidget');

var _BaseCatalogWidget2 = _interopRequireDefault(_BaseCatalogWidget);

var _Tree = require('./Tree');

var _Tree2 = _interopRequireDefault(_Tree);

var _Tabs = require('./catalog/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Model = require('./catalog/model/Model');

var _Model2 = _interopRequireDefault(_Model);

var _UploadModel = require('./catalog/model/UploadModel');

var _UploadModel2 = _interopRequireDefault(_UploadModel);

var _Overlay = require('./Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Catalog = (function (_BaseWidget) {
    _inherits(Catalog, _BaseWidget);

    function Catalog(container) {
        _classCallCheck(this, Catalog);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Catalog).call(this));

        _this.container = container;
        _this.model = new _Model2.default();
        _this.uploadModel = new _UploadModel2.default();

        _this.widgets.set(_Tree2.default.init('tree', _this.model)).set(_Tabs2.default.init(_this.container));

        _this.onsubmit = _this.submit.bind(_this);
        _this.onselect = _this.select.bind(_this);
        _this.ondelete = _this.remove.bind(_this);

        var tree = _this.get('tree');
        tree.makeActive(tree.root);

        _this.container.querySelector('.close-container a').onclick = function () {
            return _this.get('tabs').hide();
        };
        return _this;
    }

    _createClass(Catalog, [{
        key: 'select',
        value: function select(e) {
            var _this2 = this;

            var tabs = this.get('tabs');
            var id = e.selectedNode.dataset.catId;

            if (id === '0') {
                this.data.set('catalog', { id: id });
                tabs.showOnlyAddTab();
                return;
            }

            var tree = this.get('tree');
            var overlay = _Overlay2.default.instance;

            tabs.enable();
            overlay.setText('load data').enable();

            this.model.get(id).then(function (data) {
                _this2.data.set('catalog', data);
                if (data.children_count) return _this2.model.getChildren(data.id);
            }).then(function () {
                var catalogData = _this2.data.get('catalog');

                if (catalogData.img) {
                    tabs.tableView.viewImg(catalogData.img.path);
                    tabs.show();
                } else {
                    _this2.uploadModel.getByCatalog(_this2.data.get('catalog').id).then(function (data) {
                        _this2.data.get('catalog').img = data;
                        tabs.show();
                    }).catch(function (error) {
                        return console.log(error);
                    });
                }
            }).catch(function (error) {
                return console.log(error);
            });
        }
    }, {
        key: 'submit',
        value: function submit(e) {
            if (e.target.equals(this, 'tabs > tabs > form')) this.submitData(e);
            if (e.target.equals(this, 'tabs > tabs > uploadForm')) this.submitUploadData(e);
        }
    }, {
        key: 'submitUploadData',
        value: function submitUploadData(e) {
            var _this3 = this;

            var form = e.target;
            var data = e.data;
            var overlay = _Overlay2.default.instance;

            var _data$get = this.data.get('catalog');

            var id = _data$get.id;

            data.data.catalog_id = id;
            data.formData.append('catalog_id', id);

            form.img.onload = function () {
                return overlay.disable();
            };

            overlay.enable();

            this.uploadModel.set(data).then(function (data) {
                delete data.catalog_id;
                _this3.data.get('catalog').img = data;
                form.viewImg(data.path);
                form.reset();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    form.setErrors(errors);
                    overlay.disable();
                }
            });
        }
    }, {
        key: 'submitData',
        value: function submitData(e) {
            var _this4 = this;

            var form = e.target;
            var data = e.data;
            var overlay = _Overlay2.default.instance;

            overlay.enable();

            this.model.set(data).then(function (data) {
                if (_this4.data.has('catalog')) {
                    var catalogData = _this4.data.get('catalog');
                    var isResaveImg = catalogData.id === data.id && catalogData.img && !data.img;
                    if (isResaveImg) data.img = catalogData.img;
                }

                _this4.data.set('catalog', data);
                _this4.get('tabs').show();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    form.setErrors(errors);
                    overlay.disable();
                }
            });
        }
    }, {
        key: 'remove',
        value: function remove(e) {
            var target = e.target;
            if (target.equals(this, 'tabs > deleteView')) this.deleteData(e);
            if (target.equals(this, 'tabs > tabs > uploadForm')) this.deleteUploadData(e);
        }
    }, {
        key: 'deleteData',
        value: function deleteData(e) {
            var _this5 = this;

            var deleteView = e.target;
            var overlay = _Overlay2.default.instance;
            var data = this.data.get('catalog');

            overlay.enable();

            this.model.remove(data.id).then(function () {
                var imgId = data.img && data.img.id || null;
                if (imgId) return _this5.uploadModel.remove(imgId);
            }).then(function () {
                return overlay.disable();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    deleteView.disableOkBtn().showAlertMessage(errors[Object.keys(errors)[0]]);

                    overlay.disable();
                }
            });
        }
    }, {
        key: 'deleteUploadData',
        value: function deleteUploadData(e) {
            var overlay = _Overlay2.default.instance;
            var data = this.data.get('catalog');
            var uploadForm = e.target;

            var imgId = data.img && data.img.id || null;

            if (imgId) {
                overlay.enable();

                this.uploadModel.remove(imgId).then(function () {
                    uploadForm.resetImg();
                    overlay.disable();
                }).catch(function (errors) {
                    if (errors instanceof Error) overlay.setText(errors.message);else overlay.disable();
                });
            }
        }
    }, {
        key: 'onsubmit',
        set: function set(fn) {
            this.on('submit', fn);
        }
    }, {
        key: 'onshown',
        set: function set(fn) {
            this.on('shown', fn);
        }
    }, {
        key: 'onselect',
        set: function set(fn) {
            this.on('select', fn);
        }
    }, {
        key: 'ondelete',
        set: function set(fn) {
            this.on('delete', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new Catalog(containerNode);
        }
    }]);

    return Catalog;
})(_BaseCatalogWidget2.default);

exports.default = Catalog;

},{"./BaseCatalogWidget":11,"./Overlay":13,"./Tree":14,"./catalog/Tabs":15,"./catalog/model/Model":17,"./catalog/model/UploadModel":18}],13:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Overlay = (function () {
    function Overlay(container) {
        _classCallCheck(this, Overlay);

        this.container = container;
    }

    _createClass(Overlay, [{
        key: 'enable',
        value: function enable() {
            this.container.classList.remove('hidden');
            return this;
        }
    }, {
        key: 'disable',
        value: function disable() {
            var cl = this.container.classList;
            if (!cl.contains('hidden')) cl.add('hidden');
            this.resetText();
            return this;
        }
    }, {
        key: 'setText',
        value: function setText(text) {
            var translateText = _config2.default.i18n.uk[text];
            this.contentBox.innerHTML = '';
            this.contentBox.appendChild(document.createTextNode(translateText));
            return this;
        }
    }, {
        key: 'resetText',
        value: function resetText() {
            this.addPreloader();
            return this.setText('data processing');
        }
    }, {
        key: 'setHtml',
        value: function setHtml(html) {
            this.contentBox.innerHTML = html;
            return this;
        }
    }, {
        key: 'resetHtml',
        value: function resetHtml() {
            return this.setHtml('');
        }
    }, {
        key: 'setNode',
        value: function setNode(node) {
            this.resetHtml();
            this.contentBox.appendChild(node);
            return this;
        }
    }, {
        key: 'addPreloader',
        value: function addPreloader() {
            var cl = this.contentBox.classList;
            if (!cl.contains('preloader')) cl.add('preloader');
            return this;
        }
    }, {
        key: 'removePreloader',
        value: function removePreloader() {
            var cl = this.contentBox.classList;
            if (cl.contains('preloader')) cl.remove('preloader');
            return this;
        }
    }, {
        key: 'showCloseBtn',
        value: function showCloseBtn() {
            var cl = this.closeBtn.classList;
            if (cl.contains('hidden')) cl.remove('hidden');
            return this;
        }
    }, {
        key: 'hideCloseBtn',
        value: function hideCloseBtn() {
            var cl = this.closeBtn.classList;
            if (!cl.contains('hidden')) cl.add('hidden');
            return this;
        }
    }, {
        key: 'contentBox',
        get: function get() {
            if (this._contentBox === undefined) this._contentBox = this.container.querySelector('.overlay-content');
            return this._contentBox;
        }
    }, {
        key: 'closeBtn',
        get: function get() {
            var _this = this;

            if (this._closeBtn === undefined) {
                this._closeBtn = this.container.appendChild(document.createElement('a'));
                this._closeBtn.setAttribute('href', '#close');
                this._closeBtn.classList.add('close-btn');

                this._closeBtn.onclick = function (e) {
                    e.preventDefault();
                    _this.disable().hideCloseBtn().resetText();
                };

                var icon = this._closeBtn.appendChild(document.createElement('i'));
                icon.classList.add('fa', 'fa-times');
            }
            return this._closeBtn;
        }
    }, {
        key: 'closable',
        set: function set(flag) {
            this._closable = !!flag;
        },
        get: function get() {
            if (this._closable === undefined) this._closable = false;
            return this._closable;
        }
    }], [{
        key: 'createContainer',
        value: function createContainer() {
            var overlayText = document.createElement('div');
            overlayText.classList.add('overlay-content', 'preloader');
            overlayText.appendChild(document.createTextNode(_config2.default.i18n.uk['data processing']));

            var overlayContainer = document.createElement('div');
            overlayContainer.id = 'overlay';
            overlayContainer.classList.add('overlay', 'hidden');
            overlayContainer.appendChild(overlayText);

            document.body.appendChild(overlayContainer);

            return overlayContainer;
        }
    }, {
        key: 'instance',
        get: function get() {
            if (Overlay._instance === undefined) {
                Overlay._instance = new Overlay(Overlay.createContainer());
            }
            return Overlay._instance;
        }
    }]);

    return Overlay;
})();

exports.default = Overlay;

},{"../config.json":41}],14:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tree = (function (_BaseWidget) {
    _inherits(Tree, _BaseWidget);

    function Tree(container, model) {
        _classCallCheck(this, Tree);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Tree).call(this));

        _this.container = container;
        _this.model = model;
        _this.root = _this.container.querySelector('.root-container');
        _this.active = null;

        _this.container.onclick = _this.onClick.bind(_this);

        _this.model.ongetchildren = function (e) {
            return _this.addChildrenContainers(e.data);
        };
        _this.model.onset = function (e) {
            if (e.data.created === e.data.updated) _this.addContainer(e.data);else _this.updateContainer(e.data);
        };
        _this.model.onremove = _this.removeContainer.bind(_this);
        return _this;
    }

    _createClass(Tree, [{
        key: 'proxy',
        value: function proxy(node) {
            return !Object.is(this.active, node) && Tree.isExpand(node) ? this.makeActive(node) : Tree.isExpand(node) ? this.collapse(node) : this.expand(node);
        }
    }, {
        key: 'expand',
        value: function expand(node) {
            node.classList.add('expand');
            this.makeActive(node);
            return this;
        }
    }, {
        key: 'collapse',
        value: function collapse(node) {
            var cl = node.classList;
            if (cl.contains('expand')) cl.remove('expand');
            return this;
        }
    }, {
        key: 'makeActive',
        value: function makeActive(node) {
            this.active && this.active.classList.remove('active');
            node.classList.add('active');
            this.active = node;
            this.emit('select', { target: this, selectedNode: node });
            return this;
        }
    }, {
        key: 'addContainer',
        value: function addContainer(data) {
            var id = data.id;
            var title = data.title;

            var activeContainer = this.container.querySelector('.active>ul');

            var html = '<li class="node-container" data-cat-id="' + id + '">\n            <span class="container-title" title="' + title + '">' + title + '</span>\n            <ul></ul>\n        </li>';

            // if container has no element
            if (!activeContainer.childElementCount) {
                activeContainer.insertAdjacentHTML('beforeEnd', html);
            } else {

                var siblings = [].slice.call(activeContainer.children).filter(function (node) {
                    return node.classList.contains('node-container');
                });

                // if container has element only without node-cantainer css class
                if (!siblings.length) {
                    activeContainer.insertAdjacentHTML('afterBegin', html);
                } else {

                    var last = siblings.pop();
                    // if container has element with node-cantainer css class end element without this class
                    if (last.nextElementSibling) {
                        last.insertAdjacentHTML('afterEnd', html);
                    } else {
                        // if container has element only node-cantainer css class
                        activeContainer.insertAdjacentHTML('beforeEnd', html);
                    }
                }
            }

            var lastAdded = [].slice.call(this.container.querySelectorAll('.active>ul>.node-container')).pop();
            this.expand(lastAdded);
        }
    }, {
        key: 'updateContainer',
        value: function updateContainer(data) {
            var node = this.active.querySelector('.container-title');
            node.setAttribute('title', data.title);
            node.firstChild.nodeValue = data.title;
        }
    }, {
        key: 'addChildrenContainers',
        value: function addChildrenContainers(data) {
            if (!data.length) return;
            if (Tree.containsContainers(this.active)) return;

            var activeContainer = this.container.querySelector('.active>ul');
            var html = [];

            data.forEach(function (data) {
                var id = data.id;
                var title = data.title;

                html.push('<li class="node-container" data-cat-id="' + id + '">\n                <span class="container-title" title="' + title + '">' + title + '</span>\n            <ul></ul></li>');
            });

            activeContainer.innerHTML = html.join('');
        }
    }, {
        key: 'removeContainer',
        value: function removeContainer() {
            var node = this.active;
            if (node.classList.contains('root-container')) return;

            var nextActive = undefined,
                parent = node.parentNode;

            var next = node.nextElementSibling;
            if (next && next.classList.contains('node-container')) nextActive = next;else {
                var prev = node.previousElementSibling;
                if (prev && prev.classList.contains('node-container')) nextActive = prev;
            }

            this.parent.data.delete('catalog');
            this.makeActive(nextActive || node.parentNode.parentNode);
            parent.removeChild(node);
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            var target = e.target;
            var cl = target.classList;

            if (cl.contains('container-title') || cl.contains('root-title')) this.proxy(target.parentNode);
            if (cl.contains('node-container')) this.proxy(target);
        }
    }, {
        key: 'onselect',
        set: function set(fn) {
            this.on('select', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId, model) {
            var containerNode = document.getElementById(containerId);
            return new Tree(containerNode, model);
        }
    }, {
        key: 'isExpand',
        value: function isExpand(node) {
            return node.classList.contains('expand');
        }
    }, {
        key: 'containsContainers',
        value: function containsContainers(node) {
            return !!node.querySelector('.node-container');
        }
    }]);

    return Tree;
})(_BaseWidget3.default);

exports.default = Tree;

},{"./../lib/BaseWidget":46}],15:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseTabs2 = require('../../lib/BaseTabs');

var _BaseTabs3 = _interopRequireDefault(_BaseTabs2);

var _addCatalogTabs = require('./tabs/addCatalogTabs');

var _addCatalogTabs2 = _interopRequireDefault(_addCatalogTabs);

var _TableView = require('./tabs/TableView');

var _TableView2 = _interopRequireDefault(_TableView);

var _DeleteView = require('./tabs/DeleteView');

var _DeleteView2 = _interopRequireDefault(_DeleteView);

var _Goods = require('./tabs/Goods');

var _Goods2 = _interopRequireDefault(_Goods);

var _Overlay = require('../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tabs = (function (_BaseTabs) {
    _inherits(Tabs, _BaseTabs);

    function Tabs(container) {
        _classCallCheck(this, Tabs);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Tabs).call(this, container));

        _this.widgets.set('tabs', _addCatalogTabs2.default.init('catalog-manipulate-data')).set(_DeleteView2.default.init('catalog-delete')).set(_Goods2.default.init('goods'));

        _this.onshown = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(Tabs, [{
        key: 'proxy',
        value: function proxy(e) {
            var target = e.target;
            if (!this.equals(target)) return;

            switch (target.activeTab) {
                case this.viewTab:
                    this.onShowViewTab(e);break;
                case this.addTab:
                    this.onShowAddTab(e);break;
                case this.editTab:
                    this.onShowEditTab(e);break;
                case this.deleteTab:
                    this.onShowDeleteTab(e);break;
                case this.goodsTab:
                    this.onShowGoodsTab(e);break;
            }
        }
    }, {
        key: 'showOnlyAddTab',
        value: function showOnlyAddTab() {
            this.disable();
            this.enable(this.addTab);
            this.show(this.addTab);
            return this;
        }
    }, {
        key: 'onShowViewTab',
        value: function onShowViewTab(e) {
            var data = this.parent.data.get('catalog');
            this.tableView.populate(data).resetImg();
            if (data.img) this.tableView.viewImg(data.img.path);
            _Overlay2.default.instance.disable();
        }
    }, {
        key: 'onShowDeleteTab',
        value: function onShowDeleteTab(e) {
            this.get('deleteView').reset();
        }
    }, {
        key: 'onShowAddTab',
        value: function onShowAddTab(e) {
            var tabs = this.get('tabs');
            var form = tabs.get('form');

            var _parent$data$get = this.parent.data.get('catalog');

            var id = _parent$data$get.id;

            tabs.show();
            form.focus('title').action = 'add';
            form.data = { parent_id: id };

            tabs.disable(tabs.uploadTab);
        }
    }, {
        key: 'onShowEditTab',
        value: function onShowEditTab(e) {
            var tabs = this.get('tabs');
            var form = tabs.get('form');

            tabs.show();
            form.action = 'edit';
            form.data = this.parent.data.get('catalog');

            tabs.enable();
        }
    }, {
        key: 'onShowGoodsTab',
        value: function onShowGoodsTab(e) {
            var goods = this.get('goods');
            goods.get('tabs').show();
            this.hideNavigation();
        }
    }, {
        key: 'data',
        set: function set(data) {
            if (({}).toString.call(data).slice(8, -1) !== 'Object') throw new Error('Argument passed to data setter must be of the type object');
            this._data = data;
        },
        get: function get() {
            return this._data;
        }
    }, {
        key: 'viewTab',
        get: function get() {
            if (this._viewTab === undefined) this._viewTab = this.container.querySelector('a[data-action="catalog_view"]');
            return this._viewTab;
        }
    }, {
        key: 'addTab',
        get: function get() {
            if (this._addTab === undefined) this._addTab = this.container.querySelector('a[data-action="catalog_add"]');
            return this._addTab;
        }
    }, {
        key: 'editTab',
        get: function get() {
            if (this._editTab === undefined) this._editTab = this.container.querySelector('a[data-action="catalog_edit"]');
            return this._editTab;
        }
    }, {
        key: 'deleteTab',
        get: function get() {
            if (this._deleteTab === undefined) this._deleteTab = this.container.querySelector('a[data-action="catalog_delete"]');
            return this._deleteTab;
        }
    }, {
        key: 'goodsTab',
        get: function get() {
            if (this._goodsTab === undefined) this._goodsTab = this.container.querySelector('a[data-action="goods_list_view"]');
            return this._goodsTab;
        }
    }, {
        key: 'tableView',
        get: function get() {
            if (this._tableView === undefined) this._tableView = _TableView2.default.init(this.container);
            return this._tableView;
        }
    }], [{
        key: 'init',
        value: function init(container) {
            return new Tabs(container);
        }
    }]);

    return Tabs;
})(_BaseTabs3.default);

exports.default = Tabs;

},{"../../lib/BaseTabs":45,"../Overlay":13,"./tabs/DeleteView":19,"./tabs/Goods":20,"./tabs/TableView":21,"./tabs/addCatalogTabs":22}],16:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Cache = (function () {
    function Cache() {
        _classCallCheck(this, Cache);

        this.data = new Map();
        this.idMapper = new Map();
    }

    _createClass(Cache, [{
        key: 'set',
        value: function set(data) {
            if (({}).toString.call(data).slice(8, -1) === 'Array') {
                for (var i = 0; i < data.length; i++) {
                    this.set(data[i]);
                }
            } else {
                this.data.set(data.id, data);

                if (this.idMapper.has(data.parent_id)) this.idMapper.get(data.parent_id).add(data.id);else this.idMapper.set(data.parent_id, new Set([data.id]));
            }
            return this;
        }
    }, {
        key: 'get',
        value: function get(id) {
            return this.data.has(id) ? this.data.get(id) : null;
        }
    }, {
        key: 'has',
        value: function has(id) {
            return this.data.has(id);
        }
    }, {
        key: 'getChildren',
        value: function getChildren(pid) {
            var result = [];
            if (!this.idMapper.has(pid)) return result;

            var data = undefined;
            var idSet = this.idMapper.get(pid);
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = idSet[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var id = _step.value;

                    data = this.data.get(id);
                    if (data) result.push(data);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return result;
        }
    }, {
        key: 'hasChildren',
        value: function hasChildren(pid) {
            return this.idMapper.has(pid);
        }
    }, {
        key: 'remove',
        value: function remove(id) {
            if (this.data.has(id)) {
                var pid = this.data.get(id).parent_id;
                this.idMapper.has(pid) && this.idMapper.get(pid).delete(id);
                this.idMapper.has(id) && this.removeRecursiveFromMapper(id);
                this.data.delete(id);
            }
            return this;
        }
    }, {
        key: 'removeRecursiveFromMapper',
        value: function removeRecursiveFromMapper(id) {
            var _this = this;

            if (!this.idMapper.has(id)) return;

            var childrenSet = this.idMapper.get(id);
            if (childrenSet.size) {
                childrenSet.forEach(function (id) {
                    return _this.removeRecursiveFromMapper(id);
                });
            }
            this.idMapper.delete(id);
        }
    }]);

    return Cache;
})();

exports.default = Cache;

},{}],17:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _Cache = require('./Cache');

var _Cache2 = _interopRequireDefault(_Cache);

var _BaseModel2 = require('../../../lib/BaseModel');

var _BaseModel3 = _interopRequireDefault(_BaseModel2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Model = (function (_BaseModel) {
    _inherits(Model, _BaseModel);

    function Model() {
        _classCallCheck(this, Model);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Model).apply(this, arguments));
    }

    _createClass(Model, [{
        key: 'get',
        value: function get(id) {
            var _this2 = this;

            if (this.cache.has(id)) {
                var data = this.cache.get(id);
                this.emit('get', { target: this, data: data });
                return Promise.resolve(data);
            }

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = '' + options.uri + id;
            return this.xhr(options).then(function (data) {
                _this2.cache.set(data);
                _this2.emit('get', { target: _this2, data: data });
                return data;
            });
        }
    }, {
        key: 'getChildren',
        value: function getChildren(id) {
            var _this3 = this;

            if (this.cache.hasChildren(id)) {
                var children = this.cache.getChildren(id);
                this.emit('getChildren', { target: this, data: children });
                return Promise.resolve(children);
            }

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = options.uri + 'pid/' + id;
            return this.xhr(options).then(function (data) {
                _this3.cache.set(data);
                _this3.emit('getChildren', { target: _this3, data: data });
                return data;
            });
        }
    }, {
        key: 'set',
        value: function set(data) {
            var _this4 = this;

            var options = {
                body: JSON.stringify(data),
                method: data.id ? _BaseModel3.default.methods.PUT : _BaseModel3.default.methods.POST
            };

            options = Object.assign({}, this.config, options);

            if (options.method === _BaseModel3.default.methods.PUT) options.uri = '' + options.uri + data.id;

            return this.xhr(options).then(function (data) {
                _this4.cache.set(data);
                _this4.emit('set', { target: _this4, data: data });
                return data;
            });
        }
    }, {
        key: 'remove',
        value: function remove(id) {
            var _this5 = this;

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.DELETE });
            options.uri = '' + options.uri + id;

            return this.xhr(options).then(function (data) {
                _this5.cache.remove(data.id);
                _this5.emit('remove', { target: _this5, data: data });
                return data;
            });
        }
    }, {
        key: 'cache',
        get: function get() {
            if (this._cache === undefined) this._cache = new _Cache2.default();
            return this._cache;
        }
    }, {
        key: 'config',
        get: function get() {
            if (this._config === undefined) this._config = _BaseModel3.default.getConfig('catalog');
            return this._config;
        }
    }, {
        key: 'ongetchildren',
        set: function set(fn) {
            this.on('getChildren', fn);
        }
    }]);

    return Model;
})(_BaseModel3.default);

exports.default = Model;

},{"../../../config.json":41,"../../../lib/BaseModel":44,"./Cache":16}],18:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _BaseModel2 = require('../../../lib/BaseModel');

var _BaseModel3 = _interopRequireDefault(_BaseModel2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UploadModel = (function (_BaseModel) {
    _inherits(UploadModel, _BaseModel);

    function UploadModel() {
        _classCallCheck(this, UploadModel);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(UploadModel).apply(this, arguments));
    }

    _createClass(UploadModel, [{
        key: 'set',
        value: function set(data) {
            var _this2 = this;

            var options = {
                body: data.formData,
                method: _BaseModel3.default.methods.POST,
                headers: {}
            };

            options = Object.assign({}, this.config, options);
            options.uri = '' + options.uri + data.data.catalog_id;

            return this.xhr(options).then(function (data) {
                _this2.emit('set', { target: _this2, data: data.data });
                return data;
            });
        }
    }, {
        key: 'getByCatalog',
        value: function getByCatalog(catalogId) {
            var _this3 = this;

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = '' + options.uri + catalogId;

            return this.xhr(options).then(function (data) {
                _this3.emit('getByCatalog', { target: _this3, data: data });
                return data;
            });
        }
    }, {
        key: 'get',
        value: function get() {
            throw new Error('Method is not implemented');
        }
    }, {
        key: 'config',
        get: function get() {
            if (this._config === undefined) this._config = _BaseModel3.default.getConfig('catalogUpload');
            return this._config;
        }
    }]);

    return UploadModel;
})(_BaseModel3.default);

exports.default = UploadModel;

},{"../../../config.json":41,"../../../lib/BaseModel":44}],19:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseDelete2 = require('../../../lib/BaseDelete');

var _BaseDelete3 = _interopRequireDefault(_BaseDelete2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DeleteView = (function (_BaseDelete) {
    _inherits(DeleteView, _BaseDelete);

    function DeleteView(container) {
        _classCallCheck(this, DeleteView);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(DeleteView).call(this, container));
    }

    _createClass(DeleteView, null, [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new DeleteView(containerNode);
        }
    }]);

    return DeleteView;
})(_BaseDelete3.default);

exports.default = DeleteView;

},{"../../../lib/BaseDelete":42}],20:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseCatalogWidget = require('./../../BaseCatalogWidget');

var _BaseCatalogWidget2 = _interopRequireDefault(_BaseCatalogWidget);

var _Model = require('./goods/Model');

var _Model2 = _interopRequireDefault(_Model);

var _Tabs = require('./goods/Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _Overlay = require('./../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Goods = (function (_BaseWidget) {
    _inherits(Goods, _BaseWidget);

    function Goods(container) {
        _classCallCheck(this, Goods);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Goods).call(this));

        _this.container = container;
        _this.model = new _Model2.default();

        _this.widgets.set(_Tabs2.default.init(_this.container));

        _this.onsubmit = _this.submit.bind(_this);
        _this.ondelete = _this.submitDelete.bind(_this);
        return _this;
    }

    _createClass(Goods, [{
        key: 'populateList',
        value: function populateList() {
            var _this2 = this;

            var overlay = _Overlay2.default.instance;
            overlay.enable();

            this.model.getByCatalog(this.data.get('catalog').id).then(function (data) {
                var goodsGrid = _this2.get('tabs > goodsGrid');
                goodsGrid.populate(data);
                overlay.disable();
            }).catch(function (errors) {
                return overlay.setText("An error has occurred. Please, contact developer");
            });
        }
    }, {
        key: 'submit',
        value: function submit(e) {
            var _this3 = this;

            var form = e.target;
            if (!form.equals(this, 'tabs > addGoodsTabs > dataform')) return;

            var data = e.data;
            var overlay = _Overlay2.default.instance;

            form.resetErrors();
            overlay.enable();

            data.is_top = data.is_top && data.is_top === 'on' ? '1' : '0';
            data.is_index_carousel = data.is_index_carousel && data.is_index_carousel === 'on' ? '1' : '0';

            this.model.set(data).then(function (data) {
                _this3.data.set('goods', data);

                var tabs = _this3.get('tabs');
                tabs.show(tabs.viewTab);

                overlay.disable();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    form.setErrors(errors);
                    overlay.disable();
                }
            });
        }
    }, {
        key: 'submitDelete',
        value: function submitDelete(e) {
            var target = e.target;
            var goodsGrid = this.get('tabs > goodsGrid');
            if (!target.equals(goodsGrid.get('deleteView'))) return;

            var overlay = _Overlay2.default.instance;
            overlay.enable();

            this.model.remove(this.data.get('goods').id).then(function () {
                goodsGrid.deleteRow();
                overlay.disable();
            }).catch(function (error) {
                return overlay.setText(error.message);
            });
        }
    }, {
        key: 'ondelete',
        set: function set(fn) {
            this.on('delete', fn);
        }
    }, {
        key: 'onsubmit',
        set: function set(fn) {
            this.on('submit', fn);
        }
    }, {
        key: 'onshown',
        set: function set(fn) {
            this.on('shown', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new Goods(containerNode);
        }
    }]);

    return Goods;
})(_BaseCatalogWidget2.default);

exports.default = Goods;

},{"./../../BaseCatalogWidget":11,"./../../Overlay":13,"./goods/Model":25,"./goods/Tabs":26}],21:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TableView = (function () {
    function TableView(container) {
        _classCallCheck(this, TableView);

        this.container = container;
    }

    _createClass(TableView, [{
        key: 'populate',
        value: function populate(data) {
            var formatter = new Intl.DateTimeFormat('uk-UA');
            var langConfig = _config2.default.i18n.uk;

            var translData = {};
            translData[langConfig.title] = data.title;
            translData[langConfig.slug] = data.slug;
            translData[langConfig.created] = formatter.format(new Date(parseInt(data.created)));
            translData[langConfig.updated] = formatter.format(new Date(parseInt(data.updated)));

            var html = [];
            for (var key in translData) {
                if (translData.hasOwnProperty(key)) {
                    html.push('<tr><td>' + key + '</td><td>' + translData[key] + '</td></tr>');
                }
            }this.container.innerHTML = html.join('');
            return this;
        }
    }, {
        key: 'viewImg',
        value: function viewImg(src) {
            var alt = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
            var title = arguments.length <= 2 || arguments[2] === undefined ? '' : arguments[2];

            if (!src) this.resetImg();else {
                this.img.src = src;
                this.img.alt = alt;
                this.img.title = title;
                this.img.classList.remove('hidden');
            }
            return this;
        }
    }, {
        key: 'resetImg',
        value: function resetImg() {
            this.img.src = this.img.alt = this.img.title = '';
            this.img.classList.add('hidden');
            return this;
        }
    }, {
        key: 'img',
        get: function get() {
            if (this._img === undefined) this._img = document.getElementById('catalog-view').querySelector('.img-container img');
            return this._img;
        }
    }], [{
        key: 'init',
        value: function init(tabsContainer) {
            var containerNode = tabsContainer.querySelector('#catalog-view table tbody');
            return new TableView(containerNode);
        }
    }]);

    return TableView;
})();

exports.default = TableView;

},{"../../../config.json":41}],22:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseTabs2 = require('../../../lib/BaseTabs');

var _BaseTabs3 = _interopRequireDefault(_BaseTabs2);

var _Form = require('../tabs/addCatalogTabs/Form');

var _Form2 = _interopRequireDefault(_Form);

var _UploadForm = require('../tabs/addCatalogTabs/UploadForm');

var _UploadForm2 = _interopRequireDefault(_UploadForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AddCatalogTabs = (function (_BaseTabs) {
    _inherits(AddCatalogTabs, _BaseTabs);

    function AddCatalogTabs(container) {
        _classCallCheck(this, AddCatalogTabs);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(AddCatalogTabs).call(this, container));

        _this.widgets.set(_Form2.default.init('catalog-form-form')).set('uploadForm', _UploadForm2.default.init('catalog-upload-form'));

        _this.onshown = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(AddCatalogTabs, [{
        key: 'proxy',
        value: function proxy(e) {
            var target = e.target;
            if (!this.equals(target)) return;

            switch (target.activeTab) {
                case this.dataTab:
                    this.onShowDataTab(e);break;
                case this.uploadTab:
                    this.onShowUploadTab(e);break;
            }
        }
    }, {
        key: 'onShowDataTab',
        value: function onShowDataTab(e) {}
    }, {
        key: 'onShowUploadTab',
        value: function onShowUploadTab(e) {
            var data = this.parent.parent.data;
            this.get('uploadForm').data = data.get('catalog');
        }
    }, {
        key: 'onWidgetShow',
        value: function onWidgetShow(e) {
            if (this.equals(e.target)) this.adjustTabs(true);
        }
    }, {
        key: 'dataTab',
        get: function get() {
            if (this._dataTab === undefined) this._dataTab = this.container.querySelector('a[data-action="catalog_data"]');
            return this._dataTab;
        }
    }, {
        key: 'uploadTab',
        get: function get() {
            if (this._galleryTab === undefined) this._galleryTab = this.container.querySelector('a[data-action="catalog_gallery"]');
            return this._galleryTab;
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new AddCatalogTabs(containerNode);
        }
    }]);

    return AddCatalogTabs;
})(_BaseTabs3.default);

exports.default = AddCatalogTabs;

},{"../../../lib/BaseTabs":45,"../tabs/addCatalogTabs/Form":23,"../tabs/addCatalogTabs/UploadForm":24}],23:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _set = function set(object, property, value, receiver) { var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent !== null) { set(parent, property, value, receiver); } } else if ("value" in desc && desc.writable) { desc.value = value; } else { var setter = desc.set; if (setter !== undefined) { setter.call(receiver, value); } } return value; };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseForm2 = require('../../../../lib/BaseForm');

var _BaseForm3 = _interopRequireDefault(_BaseForm2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ACTION_ADD = 'add';
var ACTION_EDIT = 'edit';

var Form = (function (_BaseForm) {
    _inherits(Form, _BaseForm);

    function Form(container) {
        _classCallCheck(this, Form);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Form).call(this, container));
    }

    _createClass(Form, [{
        key: 'isAdd',
        value: function isAdd() {
            return this.action === ACTION_ADD;
        }
    }, {
        key: 'isEdit',
        value: function isEdit() {
            return this.action === ACTION_EDIT;
        }
    }, {
        key: 'title',
        get: function get() {
            if (this._title === undefined) this._title = document.getElementById('inputTitle');
            return this._title;
        }
    }, {
        key: 'data',
        set: function set(data) {
            var filteredData = {};

            if (data.id) filteredData.id = data.id;
            if (data.parent_id) filteredData.parent_id = data.parent_id;
            if (data.title) filteredData.title = data.title;

            _set(Object.getPrototypeOf(Form.prototype), 'data', Form.normalizeData(filteredData), this);
        },
        get: function get() {
            return _get(Object.getPrototypeOf(Form.prototype), 'data', this);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new Form(containerNode);
        }
    }, {
        key: 'normalizeData',
        value: function normalizeData(data) {
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key] === null) data[key] = '0';
                }
            }return data;
        }
    }]);

    return Form;
})(_BaseForm3.default);

exports.default = Form;

},{"../../../../lib/BaseForm":43}],24:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseForm2 = require('../../../../lib/BaseForm');

var _BaseForm3 = _interopRequireDefault(_BaseForm2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UploadForm = (function (_BaseForm) {
    _inherits(UploadForm, _BaseForm);

    function UploadForm(container) {
        _classCallCheck(this, UploadForm);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(UploadForm).call(this, container));

        _this.deleteSubmitBtn.onclick = _this.onDeleteSubmit.bind(_this);
        return _this;
    }

    _createClass(UploadForm, [{
        key: 'viewImg',
        value: function viewImg(src) {
            var alt = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];
            var title = arguments.length <= 2 || arguments[2] === undefined ? '' : arguments[2];

            if (!src) this.resetImg();else {
                this.img.src = src;
                this.img.alt = alt;
                this.img.title = title;
                this.img.classList.remove('hidden');
                this.deleteSubmitBtn.classList.remove('disabled');
            }
            return this;
        }
    }, {
        key: 'resetImg',
        value: function resetImg() {
            this.img.src = this.img.alt = this.img.title = '';
            this.img.classList.add('hidden');
            this.deleteSubmitBtn.classList.add('disabled');
            return this;
        }
    }, {
        key: 'onDeleteSubmit',
        value: function onDeleteSubmit() {
            var catalog_id = this.data.data.catalog_id;

            this.emit('delete', {
                target: this,
                data: { catalog_id: catalog_id }
            });
        }
    }, {
        key: 'data',
        set: function set(data) {
            this.reset();

            if (data.img) this.viewImg(data.img.path);else this.resetImg();

            if (data.id) this.container.elements['catalog_id'].value = data.id;
        },
        get: function get() {
            var formData = new FormData(this.container);

            return {
                data: _get(Object.getPrototypeOf(UploadForm.prototype), 'data', this),
                formData: formData
            };
        }
    }, {
        key: 'img',
        get: function get() {
            if (this._img === undefined) this._img = this.container.querySelector('img');
            return this._img;
        }
    }, {
        key: 'deleteSubmitBtn',
        get: function get() {
            if (this._deleteSbmt === undefined) this._deleteSbmt = this.container.querySelector('.delete-btn-box button');
            return this._deleteSbmt;
        }
    }, {
        key: 'ondelete',
        set: function set(fn) {
            this.on('delete', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new UploadForm(containerNode);
        }
    }]);

    return UploadForm;
})(_BaseForm3.default);

exports.default = UploadForm;

},{"../../../../lib/BaseForm":43}],25:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _BaseModel2 = require('../../../../lib/BaseModel');

var _BaseModel3 = _interopRequireDefault(_BaseModel2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Model = (function (_BaseModel) {
    _inherits(Model, _BaseModel);

    function Model() {
        _classCallCheck(this, Model);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Model).call(this));

        _this.onset = function (e) {
            _this.cache.set(e.data.id, e.data);
        };
        _this.onremove = function (e) {
            if (_this.cache.has(e.data.id)) _this.cache.delete(e.data.id);
        };
        return _this;
    }

    _createClass(Model, [{
        key: 'get',
        value: function get(id) {
            id = parseInt(id);
            if (this.cache.has(id)) return Promise.resolve(this.cache.get(id));
            return _get(Object.getPrototypeOf(Model.prototype), 'get', this).call(this, id);
        }
    }, {
        key: 'getByCatalog',
        value: function getByCatalog(catalogId) {
            var _this2 = this;

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = options.uri + 'cid/' + catalogId;
            return this.xhr(options).then(function (data) {
                for (var i = 0; i < data.length; i++) {
                    _this2.cache.set(data[i].id, data[i]);
                }
                _this2.emit('getByCatalog', { target: _this2, data: data });
                return data;
            });
        }
    }, {
        key: 'cache',
        get: function get() {
            if (this._cache === undefined) this._cache = new Map();
            return this._cache;
        }
    }, {
        key: 'config',
        get: function get() {
            if (this._config === undefined) this._config = _BaseModel3.default.getConfig('goods');
            return this._config;
        }
    }, {
        key: 'ongetbycatalog',
        set: function set(fn) {
            this.on('getByCatalog', fn);
        }
    }]);

    return Model;
})(_BaseModel3.default);

exports.default = Model;

},{"../../../../config.json":41,"../../../../lib/BaseModel":44}],26:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseTabs2 = require('../../../../lib/BaseTabs');

var _BaseTabs3 = _interopRequireDefault(_BaseTabs2);

var _AddGoodsTabs = require('./tabs/AddGoodsTabs');

var _AddGoodsTabs2 = _interopRequireDefault(_AddGoodsTabs);

var _GoodsGrid = require('./tabs/GoodsGrid');

var _GoodsGrid2 = _interopRequireDefault(_GoodsGrid);

var _GoodsView = require('./tabs/GoodsView');

var _GoodsView2 = _interopRequireDefault(_GoodsView);

var _DataForm = require('./tabs/addGoodsTabs/DataForm');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Tabs = (function (_BaseTabs) {
    _inherits(Tabs, _BaseTabs);

    function Tabs(container) {
        _classCallCheck(this, Tabs);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Tabs).call(this, container));

        _this.widgets.set('addGoodsTabs', _AddGoodsTabs2.default.init('goods-manipulate-data')).set('goodsGrid', _GoodsGrid2.default.init('goods-grid')).set('goodsView', _GoodsView2.default.init('goods-view'));

        _this.onshown = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(Tabs, [{
        key: 'proxy',
        value: function proxy(e) {
            if (!this.equals(e.target)) return;

            switch (this.activeTab) {
                case this.gridTab:
                    this.onShowGridTab(e);break;
                case this.addTab:
                    this.onShowAddTab(e);break;
                case this.editTab:
                    this.onShowEditTab(e);break;
                case this.viewTab:
                    this.onShowViewTab(e);break;
                //case this.catalogReturnTab :this.onCatalogReturnTab(e); break
            }
        }
    }, {
        key: 'onShowGridTab',
        value: function onShowGridTab(e) {
            this.resetHiddenTabs();
            this.parent.populateList();
            this.get('goodsGrid').showGrid();
        }
    }, {
        key: 'onShowViewTab',
        value: function onShowViewTab(e) {
            this.editTab.parentNode.classList.add('hidden');
            this.viewTab.parentNode.classList.remove('hidden');

            this.get('goodsView').populate(this.parent.data.get('goods'));
        }
    }, {
        key: 'onShowAddTab',
        value: function onShowAddTab(e) {
            var tabs = this.get('addGoodsTabs');

            this.resetHiddenTabs();
            tabs.get('dataForm').action = _DataForm.ACTION_ADD;
            tabs.disable().enable(tabs.dataTab).show();
        }
    }, {
        key: 'onShowEditTab',
        value: function onShowEditTab(e) {
            var tabs = this.get('addGoodsTabs');

            this.viewTab.parentNode.classList.add('hidden');
            this.editTab.parentNode.classList.remove('hidden');

            tabs.get('dataForm').action = _DataForm.ACTION_EDIT;
            tabs.enable().show();
        }
    }, {
        key: 'onCatalogReturnTab',
        value: function onCatalogReturnTab(e) {
            this.parent.parent.show();
        }
    }, {
        key: 'resetHiddenTabs',
        value: function resetHiddenTabs() {
            this.viewTab.parentNode.classList.add('hidden');
            this.editTab.parentNode.classList.add('hidden');
            return this;
        }
    }, {
        key: 'gridTab',
        get: function get() {
            if (this._gridTab === undefined) this._gridTab = this.dom.select('a[data-action="goods_list"]');
            return this._gridTab;
        }
    }, {
        key: 'viewTab',
        get: function get() {
            if (this._viewTab === undefined) this._viewTab = this.dom.select('a[data-action="goods_view"]');
            return this._viewTab;
        }
    }, {
        key: 'addTab',
        get: function get() {
            if (this._addTab === undefined) this._addTab = this.dom.select('a[data-action="goods_add"]');
            return this._addTab;
        }
    }, {
        key: 'editTab',
        get: function get() {
            if (this._editTab === undefined) this._editTab = this.dom.select('a[data-action="goods_edit"]');
            return this._editTab;
        }
    }, {
        key: 'catalogReturnTab',
        get: function get() {
            if (this._catalogReturnTab === undefined) this._catalogReturnTab = this.dom.select('a[data-action="catalog_view"]');
            return this._catalogReturnTab;
        }
    }], [{
        key: 'init',
        value: function init(container) {
            return new Tabs(container);
        }
    }]);

    return Tabs;
})(_BaseTabs3.default);

exports.default = Tabs;

},{"../../../../lib/BaseTabs":45,"./tabs/AddGoodsTabs":27,"./tabs/GoodsGrid":28,"./tabs/GoodsView":30,"./tabs/addGoodsTabs/DataForm":31}],27:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseTabs2 = require('../../../../../lib/BaseTabs');

var _BaseTabs3 = _interopRequireDefault(_BaseTabs2);

var _DataForm = require('./addGoodsTabs/DataForm');

var _GoodsDetailed = require('./addGoodsTabs/GoodsDetailed');

var _GoodsDetailed2 = _interopRequireDefault(_GoodsDetailed);

var _GoodsGallery = require('./addGoodsTabs/GoodsGallery');

var _GoodsGallery2 = _interopRequireDefault(_GoodsGallery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var AddGoodsTabs = (function (_BaseTabs) {
    _inherits(AddGoodsTabs, _BaseTabs);

    function AddGoodsTabs(container) {
        _classCallCheck(this, AddGoodsTabs);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(AddGoodsTabs).call(this, container));

        _this.widgets.set(_DataForm.DataForm.init('goods-form')).set(_GoodsDetailed2.default.init('goods-detailed-data')).set(_GoodsGallery2.default.init('goods-gallery'));

        _this.onshown = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(AddGoodsTabs, [{
        key: 'proxy',
        value: function proxy(e) {
            if (!this.equals(e.target)) return;

            switch (this.activeTab) {
                case this.dataTab:
                    this.onShowDataTab(e);break;
                case this.dataDetailedTab:
                    this.onShowDetailedDataTab(e);break;
                case this.galleryTab:
                    this.onShowGalleryTab(e);break;
            }
        }
    }, {
        key: 'onShowDataTab',
        value: function onShowDataTab(e) {
            var parentContainer = this.parent.container;
            parentContainer.parentNode.querySelector('#' + parentContainer.id + '>.nav-tabs').classList.remove('hidden');
            this.adjustTabs(true);

            this.get('dataForm').data = {
                catalog_id: this.data.get('catalog').id
            };
        }
    }, {
        key: 'onShowDetailedDataTab',
        value: function onShowDetailedDataTab(e) {
            var goodsDetailed = this.get('goodsDetailed');
            var detailedGrid = goodsDetailed.get('detailedGrid');
            var form = detailedGrid.get('form');

            goodsDetailed.populateList();
            detailedGrid.showGrid();
            form.reset().hide();
        }
    }, {
        key: 'onShowGalleryTab',
        value: function onShowGalleryTab(e) {
            var gallery = this.get('goodsGallery');

            gallery.get('uploadForm').data = {
                goods_id: this.data.get('goods').id
            };

            gallery.get('list').disableActionsBtns();
            gallery.viewImagesList();
        }
    }, {
        key: 'dataTab',
        get: function get() {
            if (this._dataTab === undefined) this._dataTab = this.container.querySelector('a[data-action="goods_data"]');
            return this._dataTab;
        }
    }, {
        key: 'dataDetailedTab',
        get: function get() {
            if (this._dataDetailedTab === undefined) this._dataDetailedTab = this.container.querySelector('a[data-action="goods_detailed_data"]');
            return this._dataDetailedTab;
        }
    }, {
        key: 'galleryTab',
        get: function get() {
            if (this._galleryTab === undefined) this._galleryTab = this.container.querySelector('a[data-action="goods_gallery"]');
            return this._galleryTab;
        }
    }, {
        key: 'data',
        get: function get() {
            return this.parent.parent.data;
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new AddGoodsTabs(containerNode);
        }
    }]);

    return AddGoodsTabs;
})(_BaseTabs3.default);

exports.default = AddGoodsTabs;

},{"../../../../../lib/BaseTabs":45,"./addGoodsTabs/DataForm":31,"./addGoodsTabs/GoodsDetailed":32,"./addGoodsTabs/GoodsGallery":33}],28:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./../../../../../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

var _DataForm = require('./addGoodsTabs/DataForm');

var _DeleteView = require('./GoodsGrid/DeleteView');

var _DeleteView2 = _interopRequireDefault(_DeleteView);

var _Overlay = require('./../../../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

var _config = require('./../../../../../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ACTION_VIEW = 'view';

var GoodsGrid = (function (_BaseWidget) {
    _inherits(GoodsGrid, _BaseWidget);

    function GoodsGrid(container) {
        _classCallCheck(this, GoodsGrid);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(GoodsGrid).call(this));

        _this.container = container;
        _this.grid = _this.dom.select('.table tbody');
        _this.selected = null;

        _this.widgets.set(_DeleteView2.default.init('goods-delete'));

        _this.oncancel = _this.onCancelDelete.bind(_this);
        _this.grid.onclick = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(GoodsGrid, [{
        key: 'proxy',
        value: function proxy(e) {
            var _this2 = this;

            e.preventDefault();
            var target = e.target;

            var selected = target.closest('tr');
            if (selected) this.selected = selected;

            if (target.parentNode.hasAttribute('href')) target = target.parentNode;
            if (!target.hasAttribute('href')) return;

            var parsedUri = target.getAttribute('href').slice(1).split('/');
            var id = parsedUri.pop();
            var action = parsedUri.pop();
            var overlay = _Overlay2.default.instance;

            var switchAction = function switchAction(action) {
                switch (action) {
                    case _DataForm.ACTION_EDIT:
                        _this2.onEditBtnClick(e);break;
                    case _DataForm.ACTION_DELETE:
                        _this2.onDeleteBtnClick(e);break;
                    case ACTION_VIEW:
                        _this2.onViewClick(e);break;
                }
            };

            var isAlreadyCached = this.data.has('goods') && this.data.get('goods').id == id;

            if (isAlreadyCached) {
                switchAction(action);
            } else {
                overlay.setText('load data').enable();
                this.model.get(id).then(function (data) {
                    _this2.data.set('goods', data);
                    overlay.disable();
                    switchAction(action);
                });
            }
        }
    }, {
        key: 'onEditBtnClick',
        value: function onEditBtnClick(e) {
            var parentTabs = this.parent;
            var overlay = _Overlay2.default.instance;
            var form = parentTabs.get('addGoodsTabs > dataForm');

            form.reset().show().focus('title');

            overlay.setText('load data').enable();

            parentTabs.show(parentTabs.editTab);

            this.model.get(this.data.get('goods').id).then(function (data) {
                form.data = data;
                overlay.disable();
            });
        }
    }, {
        key: 'onDeleteBtnClick',
        value: function onDeleteBtnClick(e) {
            var deleteView = this.get('deleteView');
            this.hideGrid();
            deleteView.reset().show();
        }
    }, {
        key: 'onCancelDelete',
        value: function onCancelDelete(e) {
            if (!e.target.equals(this, 'deleteView')) return;
            this.showGrid();
        }
    }, {
        key: 'onViewClick',
        value: function onViewClick(e) {
            var parent = this.parent;
            parent.show(parent.viewTab);
        }
    }, {
        key: 'populate',
        value: function populate(data) {
            this.get('deleteView').hide();

            this.grid.innerHTML = '';

            for (var i = 0; i < data.length; i++) {
                this.addRow(data[i]);
            }
            return this;
        }
    }, {
        key: 'addRow',
        value: function addRow(data) {
            var text = function text(_text) {
                return document.createElement('td').appendChild(document.createTextNode(_text)).parentNode;
            };
            var row = this.grid.insertRow();

            row.dataset.id = data.id;
            row.appendChild(text(row.rowIndex));

            var title = document.createElement('td');
            row.appendChild(title);
            title.innerHTML = '<a href="#view/' + data.id + '">' + data.title + '</a>';

            row.appendChild(text(data.price));
            row.appendChild(text(data.quantity));

            var actions = document.createElement('td');
            row.appendChild(actions);

            actions.innerHTML = '<a href="#edit/' + data.id + '" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i> ' + _config2.default.i18n.uk['edit'] + '</a>\n            <a href="#delete/' + data.id + '" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ' + _config2.default.i18n.uk['delete'] + '</a>';
        }
    }, {
        key: 'deleteRow',
        value: function deleteRow() {
            this.grid.parentNode.deleteRow(this.selected.rowIndex);
            this.selected = null;
            this.rebuildGridIndexes();
            this.showGrid();
        }
    }, {
        key: 'rebuildGridIndexes',
        value: function rebuildGridIndexes() {
            var rows = this.grid.rows;
            for (var i = 0; i < rows.length; i++) {
                rows[i].cells[0].textContent = rows[i].rowIndex;
            }
        }
    }, {
        key: 'hideGrid',
        value: function hideGrid() {
            this.grid.parentNode.classList.add('hidden');
            return this;
        }
    }, {
        key: 'showGrid',
        value: function showGrid() {
            this.grid.parentNode.classList.remove('hidden');
            return this;
        }
    }, {
        key: 'data',
        get: function get() {
            return this.parent.parent.data;
        }
    }, {
        key: 'model',
        get: function get() {
            return this.parent.parent.model;
        }
    }, {
        key: 'oncancel',
        set: function set(fn) {
            this.on('cancel', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new GoodsGrid(containerNode);
        }
    }]);

    return GoodsGrid;
})(_BaseWidget3.default);

exports.default = GoodsGrid;

},{"./../../../../../config.json":41,"./../../../../../lib/BaseWidget":46,"./../../../../Overlay":13,"./GoodsGrid/DeleteView":29,"./addGoodsTabs/DataForm":31}],29:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseDelete2 = require('../../../../../../lib/BaseDelete');

var _BaseDelete3 = _interopRequireDefault(_BaseDelete2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DeleteView = (function (_BaseDelete) {
    _inherits(DeleteView, _BaseDelete);

    function DeleteView(container) {
        _classCallCheck(this, DeleteView);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DeleteView).call(this, container));

        _this.cancelBtn = _this.container.querySelector('a[href^="#cancel"]');

        _this.cancelBtn.onclick = _this.onCancelBtnClick.bind(_this);
        return _this;
    }

    _createClass(DeleteView, [{
        key: 'onCancelBtnClick',
        value: function onCancelBtnClick(e) {
            e.preventDefault();
            this.hide();
            this.reset();
            this.emit('cancel', { target: this });
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            _get(Object.getPrototypeOf(DeleteView.prototype), 'onClick', this).call(this, e);
            this.hide();
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new DeleteView(containerNode);
        }
    }]);

    return DeleteView;
})(_BaseDelete3.default);

exports.default = DeleteView;

},{"../../../../../../lib/BaseDelete":42}],30:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _BaseWidget2 = require('../../../../../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GoodsView = (function (_BaseWidget) {
    _inherits(GoodsView, _BaseWidget);

    function GoodsView(container) {
        _classCallCheck(this, GoodsView);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(GoodsView).call(this));

        _this.container = container;
        _this.tableBody = _this.dom.select('table tbody');
        _this.editBtn = _this.dom.select('.close-box .edit-btn');

        _this.editBtn.onclick = _this.onEdit.bind(_this);
        return _this;
    }

    _createClass(GoodsView, [{
        key: 'populate',
        value: function populate(data) {
            var formatter = new Intl.DateTimeFormat('uk-UA');
            var langConfig = _config2.default.i18n.uk;

            var translData = {};
            translData[langConfig.title] = data.title;
            translData[langConfig.code] = data.code;
            translData[langConfig.slug] = data.slug;
            translData[langConfig.description] = data.description;
            translData[langConfig.short_description] = data.short_description;
            translData[langConfig.producer] = data.producer;
            translData[langConfig.price] = data.price;
            translData[langConfig.quantity] = data.quantity;
            translData[langConfig.created] = formatter.format(new Date(parseInt(data.created)));
            translData[langConfig.updated] = formatter.format(new Date(parseInt(data.updated)));
            translData[langConfig.is_top] = data.is_top == 0 ? langConfig.no : langConfig.yes;
            translData[langConfig.is_index_carousel] = data.is_index_carousel == 0 ? langConfig.no : langConfig.yes;

            var html = [];
            for (var key in translData) {
                if (translData.hasOwnProperty(key)) {
                    html.push('<tr><td>' + key + '</td><td>' + translData[key] + '</td></tr>');
                }
            }this.tableBody.innerHTML = html.join('');
            return this;
        }
    }, {
        key: 'onEdit',
        value: function onEdit(e) {
            e.preventDefault();
            this.parent.get('goodsGrid').onEditBtnClick();
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = _BaseWidget3.default.byId(containerId);
            return new GoodsView(containerNode);
        }
    }]);

    return GoodsView;
})(_BaseWidget3.default);

exports.default = GoodsView;

},{"../../../../../config.json":41,"../../../../../lib/BaseWidget":46}],31:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _set = function set(object, property, value, receiver) { var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent !== null) { set(parent, property, value, receiver); } } else if ("value" in desc && desc.writable) { desc.value = value; } else { var setter = desc.set; if (setter !== undefined) { setter.call(receiver, value); } } return value; };

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DataForm = exports.ACTION_DELETE = exports.ACTION_EDIT = exports.ACTION_ADD = undefined;

var _BaseForm2 = require('../../../../../../lib/BaseForm');

var _BaseForm3 = _interopRequireDefault(_BaseForm2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ACTION_ADD = exports.ACTION_ADD = 'add';
var ACTION_EDIT = exports.ACTION_EDIT = 'edit';
var ACTION_DELETE = exports.ACTION_DELETE = 'delete';

var DataForm = exports.DataForm = (function (_BaseForm) {
    _inherits(DataForm, _BaseForm);

    function DataForm(container) {
        _classCallCheck(this, DataForm);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(DataForm).call(this, container));
    }

    _createClass(DataForm, [{
        key: 'isAdd',
        value: function isAdd() {
            return this.action === ACTION_ADD;
        }
    }, {
        key: 'isEdit',
        value: function isEdit() {
            return this.action === ACTION_EDIT;
        }
    }, {
        key: 'data',
        set: function set(data) {
            if (data.price) data.price = data.price.toFixed(2);
            _set(Object.getPrototypeOf(DataForm.prototype), 'data', data, this);
        },
        get: function get() {
            return _get(Object.getPrototypeOf(DataForm.prototype), 'data', this);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new DataForm(containerNode);
        }
    }]);

    return DataForm;
})(_BaseForm3.default);

},{"../../../../../../lib/BaseForm":43}],32:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseCatalogWidget = require('../../../../../BaseCatalogWidget');

var _BaseCatalogWidget2 = _interopRequireDefault(_BaseCatalogWidget);

var _Model = require('./goodsDetailed/Model');

var _Model2 = _interopRequireDefault(_Model);

var _DetailedGrid = require('./goodsDetailed/DetailedGrid');

var _DetailedGrid2 = _interopRequireDefault(_DetailedGrid);

var _Overlay = require('./../../../../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GoodsDetailed = (function (_BaseWidget) {
    _inherits(GoodsDetailed, _BaseWidget);

    function GoodsDetailed(container) {
        _classCallCheck(this, GoodsDetailed);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(GoodsDetailed).call(this));

        _this.container = container;
        _this.model = new _Model2.default();

        _this.widgets.set(_DetailedGrid2.default.init('detailed-grid'));

        _this.onsubmit = _this.submit.bind(_this);
        _this.ondelete = _this.submitDelete.bind(_this);
        return _this;
    }

    _createClass(GoodsDetailed, [{
        key: 'populateList',
        value: function populateList() {
            var _this2 = this;

            var overlay = _Overlay2.default.instance;
            overlay.enable();

            this.model.getByGoods(this.data.get('goods').id).then(function (data) {
                var detailedGrid = _this2.get('detailedGrid');
                detailedGrid.populate(data);
                overlay.disable();
            }).catch(function (errors) {
                return overlay.setText("An error has occurred. Please, contact developer");
            });
        }
    }, {
        key: 'submit',
        value: function submit(e) {
            var form = e.target;
            var grid = this.get('detailedGrid');

            if (!form.equals(grid, 'form')) return;

            var data = e.data;
            var overlay = _Overlay2.default.instance;

            overlay.enable();

            this.model.set(data).then(function (data) {
                form.hide().reset();

                if (form.isAdd()) grid.addRow(data);else grid.editRow(data);

                grid.submitForm();
                overlay.disable();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    form.setErrors(errors);
                    overlay.disable();
                }
            });
        }
    }, {
        key: 'submitDelete',
        value: function submitDelete(e) {
            var target = e.target;
            var detailedGrid = this.get('detailedGrid');
            if (!target.equals(detailedGrid.get('deleteView'))) return;

            var overlay = _Overlay2.default.instance;
            var id = detailedGrid.selected ? detailedGrid.selected.data.id : null;

            overlay.enable();
            if (!id) {
                // TODO handle when id not exists
                return;
            }

            this.model.remove(id).then(function () {
                detailedGrid.submitDelete();
                overlay.disable();
            }).catch(function (error) {
                return overlay.setText(error.message);
            });
        }
    }, {
        key: 'onsubmit',
        set: function set(fn) {
            this.on('submit', fn);
        }
    }, {
        key: 'ondelete',
        set: function set(fn) {
            this.on('delete', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new GoodsDetailed(containerNode);
        }
    }]);

    return GoodsDetailed;
})(_BaseCatalogWidget2.default);

exports.default = GoodsDetailed;

},{"../../../../../BaseCatalogWidget":11,"./../../../../../Overlay":13,"./goodsDetailed/DetailedGrid":34,"./goodsDetailed/Model":35}],33:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseCatalogWidget = require('../../../../../BaseCatalogWidget');

var _BaseCatalogWidget2 = _interopRequireDefault(_BaseCatalogWidget);

var _Overlay = require('../../../../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

var _Model = require('./goodsGallery/Model');

var _Model2 = _interopRequireDefault(_Model);

var _List = require('./goodsGallery/List');

var _List2 = _interopRequireDefault(_List);

var _UploadForm = require('./goodsGallery/UploadForm');

var _UploadForm2 = _interopRequireDefault(_UploadForm);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GoodsGallery = (function (_BaseWidget) {
    _inherits(GoodsGallery, _BaseWidget);

    function GoodsGallery(container) {
        _classCallCheck(this, GoodsGallery);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(GoodsGallery).call(this));

        _this.container = container;
        _this.model = new _Model2.default();

        _this.widgets.set(_List2.default.init('goods-gallery-list')).set(_UploadForm2.default.init('goods-gallery-form'));

        _this.onsubmit = _this.submit.bind(_this);
        _this.ondelete = _this.remove.bind(_this);
        return _this;
    }

    _createClass(GoodsGallery, [{
        key: 'viewImagesList',
        value: function viewImagesList() {
            var goodsId = this.data.get('goods').id;
            var overlay = _Overlay2.default.instance;
            var list = this.get('list');

            overlay.enable();

            this.model.getByGoods(goodsId).then(function (data) {
                list.populate(data);
                overlay.disable();
            });
        }
    }, {
        key: 'submit',
        value: function submit(e) {
            var form = this.get('uploadForm');
            if (!form.equals(e.target)) return;

            var list = this.get('list');
            var overlay = _Overlay2.default.instance;
            overlay.enable();

            this.model.set(e.data).then(function (data) {
                list.add(data);
                form.container.reset();
                overlay.disable();
            }).catch(function (errors) {
                if (errors instanceof Error) overlay.setText(errors.message);else {
                    form.setErrors(errors);
                    overlay.disable();
                }
            });
        }
    }, {
        key: 'remove',
        value: function remove(e) {
            var list = this.get('list');
            var target = e.target;
            if (!list.equals(target)) return;

            var overlay = _Overlay2.default.instance;
            overlay.enable();

            this.model.remove(e.imageId).then(function (data) {
                list.deleteActive();
                overlay.disable();
            });
        }
    }, {
        key: 'onsubmit',
        set: function set(fn) {
            this.on('submit', fn);
        }
    }, {
        key: 'ondelete',
        set: function set(fn) {
            this.on('delete', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            return new GoodsGallery(_BaseCatalogWidget2.default.byId(containerId));
        }
    }]);

    return GoodsGallery;
})(_BaseCatalogWidget2.default);

exports.default = GoodsGallery;

},{"../../../../../BaseCatalogWidget":11,"../../../../../Overlay":13,"./goodsGallery/List":38,"./goodsGallery/Model":39,"./goodsGallery/UploadForm":40}],34:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../../../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _Overlay = require('../../../../../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

var _BaseWidget2 = require('./../../../../../../../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

var _Form = require('./detailedGrid/Form');

var _DeleteView = require('./detailedGrid/DeleteView');

var _DeleteView2 = _interopRequireDefault(_DeleteView);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DetailedGrid = (function (_BaseWidget) {
    _inherits(DetailedGrid, _BaseWidget);

    function DetailedGrid(container) {
        _classCallCheck(this, DetailedGrid);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DetailedGrid).call(this));

        _this.container = container;
        _this.grid = _this.container.querySelector('.detailed-table tbody');
        _this.addBtn = _this.container.querySelector('.actions .add-btn');

        _this.selected = null;

        _this.widgets.set(_Form.Form.init('detailed-form')).set(_DeleteView2.default.init('detailed-delete'));

        _this.oncancel = _this.onCancel.bind(_this);
        _this.addBtn.onclick = _this.onAddBtnClick.bind(_this);
        _this.grid.onclick = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(DetailedGrid, [{
        key: 'proxy',
        value: function proxy(e) {
            e.preventDefault();
            var target = e.target;

            var selectedRow = target.closest('tr');
            if (selectedRow) this.selected = { data: this.getRowData(selectedRow), node: selectedRow };

            if (target.parentNode.hasAttribute('href')) target = target.parentNode;
            if (!target.hasAttribute('href')) return;

            var parsedUri = e.target.getAttribute('href').slice(1).split('/');
            e.id = parsedUri.pop();
            var action = parsedUri.pop();

            switch (action) {
                case _Form.ACTION_EDIT:
                    this.onEditBtnClick(e);break;
                case _Form.ACTION_REMOVE:
                    this.onDeleteBtnClick(e);break;
            }
        }
    }, {
        key: 'onAddBtnClick',
        value: function onAddBtnClick(e) {
            e.preventDefault();
            var form = this.get('form');
            form.reset().show().focus('title');
            form.action = _Form.ACTION_ADD;
            form.data = { goods_id: this.parent.data.get('goods').id };

            this.hideAddBtn();
        }
    }, {
        key: 'onEditBtnClick',
        value: function onEditBtnClick(e) {
            var form = this.get('form');
            var row = e.target.closest('tr');
            form.action = _Form.ACTION_EDIT;
            form.reset().show().focus('title');
            form.data = this.getRowData(row);

            this.hideAddBtn();
        }
    }, {
        key: 'onDeleteBtnClick',
        value: function onDeleteBtnClick(e) {
            var deleteView = this.get('deleteView');
            this.hideGrid();
            this.get('form').hide();
            deleteView.reset().show();
        }
    }, {
        key: 'onCancel',
        value: function onCancel(e) {
            var target = e.target;
            if (target.equals(this, 'deleteView')) this.showGrid();
            if (target.equals(this, 'form')) this.showGrid();
        }
    }, {
        key: 'submitForm',
        value: function submitForm() {
            this.showGrid();
        }
    }, {
        key: 'submitDelete',
        value: function submitDelete() {
            this.deleteSelectedRow();
            this.showGrid();
        }
    }, {
        key: 'populate',
        value: function populate(data) {
            this.grid.innerHTML = '';

            for (var i = 0; i < data.length; i++) {
                this.addRow(data[i]);
            }
            return this;
        }
    }, {
        key: 'addRow',
        value: function addRow(data) {
            var text = function text(_text) {
                return document.createElement('td').appendChild(document.createTextNode(_text)).parentNode;
            };

            var tr = this.grid.insertRow();
            tr.dataset.id = data.id;

            tr.appendChild(text(tr.rowIndex));
            tr.appendChild(text(data.title));
            tr.appendChild(text(data.value));

            var actions = document.createElement('td');
            actions.innerHTML = '<a href="#edit/' + data.id + '" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i> ' + _config2.default.i18n.uk['edit'] + '</a>\n            <a href="#remove/' + data.id + '" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ' + _config2.default.i18n.uk['delete'] + '</a>';

            tr.appendChild(actions);

            this.selected = {
                data: data,
                node: tr
            };
        }
    }, {
        key: 'editRow',
        value: function editRow(data) {
            var cells = this.selected.node.cells;
            cells[1].textContent = this.selected.data.title = data.title;
            cells[2].textContent = this.selected.data.value = data.value;
            return this;
        }
    }, {
        key: 'deleteSelectedRow',
        value: function deleteSelectedRow() {
            this.grid.parentNode.deleteRow(this.selected.node.rowIndex);
            this.selected = null;
            this.rebuildGridIndexes();
        }
    }, {
        key: 'rebuildGridIndexes',
        value: function rebuildGridIndexes() {
            var rows = this.grid.rows;
            for (var i = 0; i < rows.length; i++) {
                rows[i].cells[0].textContent = rows[i].rowIndex;
            }
        }
    }, {
        key: 'getRowData',
        value: function getRowData(row) {
            var cells = row.cells;
            return {
                id: row.dataset.id,
                title: cells[1].textContent,
                value: cells[2].textContent
            };
        }
    }, {
        key: 'hideGrid',
        value: function hideGrid() {
            this.grid.parentNode.classList.add('hidden');
            this.hideAddBtn();
            return this;
        }
    }, {
        key: 'showGrid',
        value: function showGrid() {
            this.grid.parentNode.classList.remove('hidden');
            this.addBtn.classList.remove('hidden');
            return this;
        }
    }, {
        key: 'hideAddBtn',
        value: function hideAddBtn() {
            this.addBtn.classList.add('hidden');
            return this;
        }
    }, {
        key: 'oncancel',
        set: function set(fn) {
            this.on('cancel', fn);
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new DetailedGrid(containerNode);
        }
    }]);

    return DetailedGrid;
})(_BaseWidget3.default);

exports.default = DetailedGrid;

},{"../../../../../../../config.json":41,"../../../../../../Overlay":13,"./../../../../../../../lib/BaseWidget":46,"./detailedGrid/DeleteView":36,"./detailedGrid/Form":37}],35:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../../../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _BaseModel2 = require('../../../../../../../lib/BaseModel');

var _BaseModel3 = _interopRequireDefault(_BaseModel2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Model = (function (_BaseModel) {
    _inherits(Model, _BaseModel);

    function Model() {
        _classCallCheck(this, Model);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Model).apply(this, arguments));
    }

    _createClass(Model, [{
        key: 'getByGoods',
        value: function getByGoods(goodsId) {
            var _this2 = this;

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = options.uri + 'gid/' + goodsId;
            return this.xhr(options).then(function (data) {
                _this2.emit('getByGoods', { target: _this2, data: data });
                return data;
            });
        }
    }, {
        key: 'config',
        get: function get() {
            if (this._config === undefined) this._config = _BaseModel3.default.getConfig('goodsDetailed');
            return this._config;
        }
    }]);

    return Model;
})(_BaseModel3.default);

exports.default = Model;

},{"../../../../../../../config.json":41,"../../../../../../../lib/BaseModel":44}],36:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseDelete2 = require('../../../../../../../../lib/BaseDelete');

var _BaseDelete3 = _interopRequireDefault(_BaseDelete2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DeleteView = (function (_BaseDelete) {
    _inherits(DeleteView, _BaseDelete);

    function DeleteView(container) {
        _classCallCheck(this, DeleteView);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(DeleteView).call(this, container));

        _this.cancelBtn = _this.container.querySelector('a[href^="#cancel"]');

        _this.cancelBtn.onclick = _this.onCancelBtnClick.bind(_this);
        return _this;
    }

    _createClass(DeleteView, [{
        key: 'onCancelBtnClick',
        value: function onCancelBtnClick(e) {
            e.preventDefault();
            this.hide();
            this.reset();
            this.emit('cancel', { target: this });
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            _get(Object.getPrototypeOf(DeleteView.prototype), 'onClick', this).call(this, e);
            this.hide();
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new DeleteView(containerNode);
        }
    }]);

    return DeleteView;
})(_BaseDelete3.default);

exports.default = DeleteView;

},{"../../../../../../../../lib/BaseDelete":42}],37:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Form = exports.ACTION_REMOVE = exports.ACTION_EDIT = exports.ACTION_ADD = undefined;

var _BaseForm2 = require('../../../../../../../../lib/BaseForm');

var _BaseForm3 = _interopRequireDefault(_BaseForm2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var ACTION_ADD = exports.ACTION_ADD = 'add';
var ACTION_EDIT = exports.ACTION_EDIT = 'edit';
var ACTION_REMOVE = exports.ACTION_REMOVE = 'remove';

var Form = exports.Form = (function (_BaseForm) {
    _inherits(Form, _BaseForm);

    function Form(container) {
        _classCallCheck(this, Form);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Form).call(this, container));

        _this.cancelBtn = _this.container.querySelector('button[type="submit"]+button[type="button"]');

        _this.cancelBtn.onclick = _this.onCancelForm.bind(_this);
        return _this;
    }

    _createClass(Form, [{
        key: 'isAdd',
        value: function isAdd() {
            return this.action === ACTION_ADD;
        }
    }, {
        key: 'isEdit',
        value: function isEdit() {
            return this.action === ACTION_EDIT;
        }
    }, {
        key: 'onCancelForm',
        value: function onCancelForm(e) {
            this.reset();
            this.hide();
            this.emit('cancel', { target: this });
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new Form(containerNode);
        }
    }]);

    return Form;
})(_BaseForm3.default);

},{"../../../../../../../../lib/BaseForm":43}],38:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('../../../../../../../lib/BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

var _Overlay = require('../../../../../../Overlay');

var _Overlay2 = _interopRequireDefault(_Overlay);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var List = (function (_BaseWidget) {
    _inherits(List, _BaseWidget);

    function List(container) {
        _classCallCheck(this, List);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(List).call(this));

        _this.container = container;

        _this.container.onclick = _this.proxy.bind(_this);
        return _this;
    }

    _createClass(List, [{
        key: 'proxy',
        value: function proxy(e) {
            e.preventDefault();
            var target = e.target;

            if (target.classList.contains('disabled')) return;
            if (target === this.deleteBtn) return this.onDeleteClick(e);
            if (target === this.expandBtn) return this.onExpand(e);

            if (target.tagName.toLowerCase() === 'img') target = target.parentNode;
            if (target.tagName.toLowerCase() === 'a') return this.activeItem(target);
        }
    }, {
        key: 'populate',
        value: function populate(data) {
            this.list.innerHTML = '';

            for (var i = 0; i < data.length; i++) {
                this.add(data[i]);
            }
            this.preloadImages();
            return this;
        }
    }, {
        key: 'add',
        value: function add(data) {
            var li = document.createElement('li');
            var a = li.appendChild(document.createElement('a'));
            var img = a.appendChild(document.createElement('img'));

            a.href = data.path;
            a.dataset.id = data.id;
            a.classList.add('thumbnail');

            img.src = data.thumbs && data.thumbs.min || data.min;
            img.alt = '';

            this.list.appendChild(li);
        }
    }, {
        key: 'deleteActive',
        value: function deleteActive() {
            var activeNode = this.dom.node(this.list).select('.active');
            if (!activeNode) return;

            var parent = activeNode.parentNode;
            parent.parentNode.removeChild(parent);

            this.disableActionsBtns();

            return this;
        }
    }, {
        key: 'activeItem',
        value: function activeItem(node) {
            this.enableActionsBtns();

            this.dom.node(this.list).selectAll('.active').forEach(function (node) {
                return node.classList.remove('active');
            });
            node.classList.add('active');
            return this;
        }
    }, {
        key: 'onDeleteClick',
        value: function onDeleteClick(e) {
            var active = this.dom.node(this.list).select('.active');
            if (!active) return;

            this.emit('delete', {
                target: this,
                imageId: active.dataset.id
            });
        }
    }, {
        key: 'onExpand',
        value: function onExpand(e) {
            var active = this.dom.node(this.list).select('.active');
            if (!active) return;

            var overlay = _Overlay2.default.instance;
            var img = this.expandImg;
            img.src = active.getAttribute('href');

            overlay.setNode(img).removePreloader().showCloseBtn().enable();
        }
    }, {
        key: 'disableActionsBtns',
        value: function disableActionsBtns() {
            var delCl = this.deleteBtn.classList;
            var expCl = this.expandBtn.classList;

            if (!delCl.contains('disabled')) delCl.add('disabled');
            if (!expCl.contains('disabled')) expCl.add('disabled');
        }
    }, {
        key: 'enableActionsBtns',
        value: function enableActionsBtns() {
            var delCl = this.deleteBtn.classList;
            var expCl = this.expandBtn.classList;

            if (delCl.contains('disabled')) delCl.remove('disabled');
            if (expCl.contains('disabled')) expCl.remove('disabled');
        }
    }, {
        key: 'preloadImages',
        value: function preloadImages() {
            var links = this.dom.node(this.list).selectAll('a');
            if (!links) return;

            var imgNode = this.expandImg;
            links.forEach(function (link) {
                return imgNode.src = link.getAttribute('href');
            });
            imgNode.src = '';

            return this;
        }
    }, {
        key: 'list',
        get: function get() {
            if (this._list === undefined) this._list = this.dom.select('.gallery-list');
            return this._list;
        }
    }, {
        key: 'deleteBtn',
        get: function get() {
            if (this._deleteBtn === undefined) this._deleteBtn = this.dom.select('.delete-btn');
            return this._deleteBtn;
        }
    }, {
        key: 'expandBtn',
        get: function get() {
            if (this._expandBtn === undefined) this._expandBtn = this.dom.select('.expand-btn');
            return this._expandBtn;
        }
    }, {
        key: 'expandImg',
        get: function get() {
            if (this._expandImg === undefined) this._expandImg = document.createElement('img');
            return this._expandImg;
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            return new List(_BaseWidget3.default.byId(containerId));
        }
    }]);

    return List;
})(_BaseWidget3.default);

exports.default = List;

},{"../../../../../../../lib/BaseWidget":46,"../../../../../../Overlay":13}],39:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../../../../../../config.json');

var _config2 = _interopRequireDefault(_config);

var _BaseModel2 = require('../../../../../../../lib/BaseModel');

var _BaseModel3 = _interopRequireDefault(_BaseModel2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Model = (function (_BaseModel) {
    _inherits(Model, _BaseModel);

    function Model() {
        _classCallCheck(this, Model);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(Model).apply(this, arguments));
    }

    _createClass(Model, [{
        key: 'set',
        value: function set(data) {
            var _this2 = this;

            var options = {
                body: data.formData,
                method: _BaseModel3.default.methods.POST,
                headers: {}
            };

            options = Object.assign({}, this.config, options);
            options.uri = '' + options.uri + data.data.goods_id;

            return this.xhr(options).then(function (data) {
                _this2.emit('set', { target: _this2, data: data.data });
                return data;
            });
        }
    }, {
        key: 'getByGoods',
        value: function getByGoods(goodsId) {
            var _this3 = this;

            var options = Object.assign({}, this.config, { method: _BaseModel3.default.methods.GET });
            options.uri = '' + options.uri + goodsId;

            return this.xhr(options).then(function (data) {
                _this3.emit('getByGoods', { target: _this3, data: data });
                return data;
            });
        }
    }, {
        key: 'get',
        value: function get() {
            throw new Error('Method is not implemented');
        }
    }, {
        key: 'config',
        get: function get() {
            if (this._config === undefined) this._config = _BaseModel3.default.getConfig('goodsImages');
            return this._config;
        }
    }]);

    return Model;
})(_BaseModel3.default);

exports.default = Model;

},{"../../../../../../../config.json":41,"../../../../../../../lib/BaseModel":44}],40:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseForm2 = require('../../../../../../../lib/BaseForm');

var _BaseForm3 = _interopRequireDefault(_BaseForm2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var UploadForm = (function (_BaseForm) {
    _inherits(UploadForm, _BaseForm);

    function UploadForm(container) {
        _classCallCheck(this, UploadForm);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(UploadForm).call(this, container));
    }

    _createClass(UploadForm, [{
        key: 'data',
        set: function set(data) {
            this.reset();
            if (data.goods_id) this.container.elements['goods_id'].value = data.goods_id;
        },
        get: function get() {
            var formData = new FormData(this.container);

            return {
                data: _get(Object.getPrototypeOf(UploadForm.prototype), 'data', this),
                formData: formData
            };
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            return new UploadForm(_BaseForm3.default.byId(containerId));
        }
    }]);

    return UploadForm;
})(_BaseForm3.default);

exports.default = UploadForm;

},{"../../../../../../../lib/BaseForm":43}],41:[function(require,module,exports){
module.exports={
  "model": {
    "catalog": {
      "uri": "/api/catalogs/"
    },
    "catalogUpload": {
      "uri": "/api/catalog-upload/"
    },
    "goods": {
      "uri": "/api/goods/"
    },
    "goodsDetailed": {
      "uri": "/api/goods-detailed/"
    },
    "goodsImages": {
      "uri": "/api/goods-images/"
    }
  },

  "i18n": {
    "uk": {
      "yes": "Так",
      "no": "Ні",
      "edit": "Редагувати",
      "delete": "Видалити",
      "data processing": "Дані обробляються",
      "load data": "Завантаження даних",
      "title": "Назва",
      "alias": "Псевдонім",
      "code": "Код",
      "description": "Опис",
      "short_description": "Короткий опис",
      "producer": "Виробник",
      "price": "Ціна",
      "quantity": "Кількість",
      "is_top": "Лідери продаж",
      "is_index_carousel": "Відображається в каруселі",
      "slug": "Назва посилання",
      "created" : "Створено",
      "updated" : "Обновлено",
      "alert!": "Увага!",
      "Are you sure you want to delete this element?": "Ви дійсно хочете видалити цей елемент?",
      "Element is not empty. First, you have to remove content.": "Елемент не порожній. Спочатку, ви повинні видалити вміст елемента",
      "An error has occurred. Please, contact developer": "Сталася помилка. Будь ласка, зв'яжіться з розробником",
      "No catalog found": "Каталог не знайдено",
      "No goods found": "Товар не знайдено",
      "No goods detail found": "Елемент не знайдено",
      "The input is more than 100 characters long": "Довжина запису не повинна перевищувати 100 символів",
      "The input is more than 255 characters long": "Довжина запису не повинна перевищувати 255 символів",
      "The input is more than 120 characters long": "Довжина запису не повинна перевищувати 120 символів",
      "Catalog with this name already exists": "Каталог з такою назвою вже існує",
      "Goods with this name already exists" : "Товар з такою назвою вже існує",
      "Record with this title already exists": "Запис з такою назвою вже існує",
      "Value is required and can't be empty": "Поле обов'язкове для заповнення і не може бути пустим",
      "The input does not appear to be an integer": "Введене значення повинне бути цілим числом",
      "Invalid input. Input mask: 999 999 999.99": "Введене значення не вірне. Маска значення: 999 999 999.99",
      "Catalog has goods. First clear catalog": "Каталог містить товари. Спочатку очистіть каталог",
      "Catalog has children catalogs. First clear catalog": "Каталог містить інші каталоги. Спочатку очистіть каталог",
      "File was not uploaded": "Файл не завантажено",
      "File is no image": "Файл, який ви пробуєте завантажити не є зображенням",
      "Maximum allowed size for file is 2 MB": "Максимальний розмір файлу, який можна завантажити - 2 Mb"
    }
  }
}
},{}],42:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseDelete = (function (_BaseWidget) {
    _inherits(BaseDelete, _BaseWidget);

    function BaseDelete(container) {
        var deleteBtn = arguments.length <= 1 || arguments[1] === undefined ? null : arguments[1];

        _classCallCheck(this, BaseDelete);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(BaseDelete).call(this));

        _this.container = container;
        _this.deleteBtn = deleteBtn ? deleteBtn : _this.container.querySelector('a[href^="#delete"]');

        _this.deleteBtn.onclick = _this.onClick.bind(_this);
        return _this;
    }

    _createClass(BaseDelete, [{
        key: 'onClick',
        value: function onClick(e) {
            e.preventDefault();

            if (this.deleteBtn.classList.contains('disabled')) {
                e.stopPropagation();
                e.stopImmediatePropagation();
                return;
            }
            this.emit('delete', { target: this });
        }
    }, {
        key: 'disableOkBtn',
        value: function disableOkBtn() {
            this.showAlertMessage('Element is not empty. First, you have to remove content.');
            this.deleteBtn.classList.add('disabled');
            return this;
        }
    }, {
        key: 'showAlertMessage',
        value: function showAlertMessage(msg) {
            msg = _config2.default.i18n.uk[msg];
            var alertText = _config2.default.i18n.uk['alert!'];
            this.alertBox.innerHTML = '<strong>' + alertText + '</strong> ' + msg;
            return this;
        }
    }, {
        key: 'reset',
        value: function reset() {
            this.showAlertMessage('Are you sure you want to delete this element?');
            var cl = this.deleteBtn.classList;
            if (cl.contains('disabled')) cl.remove('disabled');
            return this;
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.classList.remove('hidden');
            return this;
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.container.classList.add('hidden');
            return this;
        }
    }, {
        key: 'alertBox',
        get: function get() {
            if (this._alertBox === undefined) this._alertBox = this.container.querySelector('.msg-text');
            return this._alertBox;
        }
    }]);

    return BaseDelete;
})(_BaseWidget3.default);

exports.default = BaseDelete;

},{"../config.json":41,"./BaseWidget":46}],43:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

var _formToObj = require('form-to-obj');

var _formToObj2 = _interopRequireDefault(_formToObj);

var _populate = require('./populate');

var _populate2 = _interopRequireDefault(_populate);

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseForm = (function (_BaseWidget) {
    _inherits(BaseForm, _BaseWidget);

    function BaseForm(container) {
        _classCallCheck(this, BaseForm);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(BaseForm).call(this));

        _this.container = container;
        _this.container.onsubmit = _this.onSubmitForm.bind(_this);
        return _this;
    }

    _createClass(BaseForm, [{
        key: 'reset',
        value: function reset() {
            var hiddenInputs = this.container.querySelectorAll('input[type="hidden"]');
            var each = Array.prototype.forEach;

            each.call(hiddenInputs, function (input) {
                return input.value = '';
            });

            this.container.reset();
            this.resetErrors();
            return this;
        }
    }, {
        key: 'focus',
        value: function focus(name) {
            this.container.elements[name].focus();
            return this;
        }
    }, {
        key: 'setErrors',
        value: function setErrors(errors) {
            var container = this.container;
            var translate = _config2.default.i18n.uk;

            this.resetErrors();

            for (var inputName in errors) {
                if (errors.hasOwnProperty(inputName)) {
                    var input = container.querySelector('[name="' + inputName + '"]');
                    if (!input) continue;

                    var msgs = errors[inputName];
                    for (var key in msgs) {
                        if (msgs.hasOwnProperty(key)) {
                            var icon = document.createElement('i');
                            icon.classList.add('fa', 'fa-exclamation-triangle');

                            var msgBox = document.createElement('span');
                            msgBox.classList.add('help-block');

                            var text = translate[msgs[key]] ? translate[msgs[key]] : msgs[key];
                            msgBox.appendChild(icon);
                            msgBox.appendChild(document.createTextNode(text));
                            input.parentNode.appendChild(msgBox);
                        }
                    }var fgNode = input.closest('.form-group');
                    if (fgNode) fgNode.classList.add('has-error');
                }
            }return this;
        }
    }, {
        key: 'resetErrors',
        value: function resetErrors() {
            var each = Array.prototype.forEach;
            var groupBoxes = this.container.querySelectorAll('.form-group.has-error');

            each.call(groupBoxes, function (group) {
                var msgsBoxes = group.querySelectorAll('.help-block');
                each.call(msgsBoxes, function (msg) {
                    return msg.parentNode.removeChild(msg);
                });
                group.classList.remove('has-error');
            });
            return this;
        }
    }, {
        key: 'onSubmitForm',
        value: function onSubmitForm(e) {
            e.preventDefault();

            this.resetErrors();

            this.emit('submit', {
                target: this,
                data: this.data
            });
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.classList.remove('hidden');
            return this;
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.container.classList.add('hidden');
            return this;
        }
    }, {
        key: 'action',
        set: function set(action) {
            this.container.setAttribute('action', action);
        },
        get: function get() {
            return this.container.getAttribute('action');
        }
    }, {
        key: 'data',
        set: function set(data) {
            this.reset();
            (0, _populate2.default)(this.container, data);
        },
        get: function get() {
            return (0, _formToObj2.default)(this.container);
        }
    }, {
        key: 'onsubmit',
        set: function set(fn) {
            this.on('submit', fn);
        }
    }]);

    return BaseForm;
})(_BaseWidget3.default);

exports.default = BaseForm;

},{"../config.json":41,"./BaseWidget":46,"./populate":48,"form-to-obj":1}],44:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../config.json');

var _config2 = _interopRequireDefault(_config);

var _xhr2 = require('xhr');

var _xhr3 = _interopRequireDefault(_xhr2);

var _Emitter2 = require('./Emitter');

var _Emitter3 = _interopRequireDefault(_Emitter2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var methodsConsts = Object.freeze({
    'GET': 'GET',
    'POST': 'POST',
    'PUT': 'PUT',
    'DELETE': 'DELETE'
});

var BaseModel = (function (_Emitter) {
    _inherits(BaseModel, _Emitter);

    function BaseModel() {
        _classCallCheck(this, BaseModel);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(BaseModel).apply(this, arguments));
    }

    _createClass(BaseModel, [{
        key: 'xhr',
        value: function xhr(options) {
            return BaseModel.xhr(options);
        }
    }, {
        key: 'set',
        value: function set(data) {
            var _this2 = this;

            var options = {
                body: JSON.stringify(data),
                method: data.id ? BaseModel.methods.PUT : BaseModel.methods.POST
            };

            options = Object.assign({}, this.config, options);

            if (options.method === BaseModel.methods.PUT) options.uri = '' + options.uri + data.id;

            return this.xhr(options).then(function (data) {
                _this2.emit('set', { target: _this2, data: data });
                return data;
            });
        }
    }, {
        key: 'get',
        value: function get(id) {
            var _this3 = this;

            var options = Object.assign({}, this.config, { method: BaseModel.methods.GET });
            options.uri = '' + options.uri + id;
            return this.xhr(options).then(function (data) {
                _this3.emit('get', { target: _this3, data: data });
                return data;
            });
        }
    }, {
        key: 'remove',
        value: function remove(id) {
            var _this4 = this;

            var options = Object.assign({}, this.config, { method: BaseModel.methods.DELETE });
            options.uri = '' + options.uri + id;

            return this.xhr(options).then(function (data) {
                _this4.emit('remove', { target: _this4, data: data });
                return data;
            });
        }
    }, {
        key: 'onset',
        set: function set(fn) {
            this.on('set', fn);
        }
    }, {
        key: 'onget',
        set: function set(fn) {
            this.on('get', fn);
        }
    }, {
        key: 'onremove',
        set: function set(fn) {
            this.on('remove', fn);
        }
    }], [{
        key: 'getDefaultOptions',
        value: function getDefaultOptions() {
            return {
                headers: {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                }
                //,timeout: 30000
            };
        }
    }, {
        key: 'getConfig',
        value: function getConfig(key) {
            var globalConfig = _config2.default.model && _config2.default.model[key] ? _config2.default.model[key] : null;

            return globalConfig ? Object.assign(BaseModel.getDefaultOptions(), globalConfig) : BaseModel.getDefaultOptions();
        }
    }, {
        key: 'xhr',
        value: function xhr(options) {
            return new Promise(function (resolve, reject) {
                (0, _xhr3.default)(options, function (error, response) {
                    var parsed = undefined;

                    switch (response.statusCode) {
                        case 200: // OK
                        case 201: // Created
                        case 204: // No Content
                        case 206:
                            // Partial Content
                            parsed = JSON.parse(response.body);
                            resolve(parsed.data ? parsed.data : parsed);
                            break;
                        case 400:
                            // Bad Request
                            parsed = JSON.parse(response.body);
                            reject(parsed.messages ? parsed.messages : parsed);
                            break;
                        default:
                            reject(new Error('An error has occurred. Please, contact developer'));
                    }
                });
            });
        }
    }, {
        key: 'methods',
        get: function get() {
            return methodsConsts;
        }
    }]);

    return BaseModel;
})(_Emitter3.default);

exports.default = BaseModel;

},{"../config.json":41,"./Emitter":47,"xhr":2}],45:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _BaseWidget2 = require('./BaseWidget');

var _BaseWidget3 = _interopRequireDefault(_BaseWidget2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var BaseTabs = (function (_BaseWidget) {
    _inherits(BaseTabs, _BaseWidget);

    function BaseTabs(container) {
        _classCallCheck(this, BaseTabs);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(BaseTabs).call(this));

        _this.container = container;
        _this.activeTab = null;
        _this.activeContent = null;

        _this.onwidgetshow = _this.onWidgetShow.bind(_this);
        _this.onwidgethide = _this.onWidgetHide.bind(_this);
        _this.onshown = _this.onShown.bind(_this);
        _this.onclick = _this.onClick.bind(_this);
        return _this;
    }

    _createClass(BaseTabs, [{
        key: 'getTabByAction',
        value: function getTabByAction(action) {
            return this.nav.querySelector('a[data-action="' + action + '"]');
        }
    }, {
        key: 'show',
        value: function show() {
            var tab = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

            if (!tab) tab = this.nav.querySelector('a[data-action]');

            if (typeof tab == 'string') tab = this.nav.querySelector('a[data-action="${tab}"]');

            if (tab.parentNode.classList.contains('disabled')) return;

            var action = tab.dataset.action;
            var contentBox = this.container.parentNode.querySelector('#' + this.container.id + '>.tab-content>' + tab.getAttribute('href'));

            // show container if hidden
            var cl = this.container.classList;
            //let isWidgetShowEmited = false
            if (cl.contains('hidden')) {
                //isWidgetShowEmited = true
                this.emit('widget-show', { target: this });
                cl.remove('hidden');
            }

            cl = this.nav.classList;
            if (cl.contains('hidden')) {
                //if (!isWidgetShowEmited) {
                //    isWidgetShowEmited = true
                //    this.emit('widget-show', {target: this})
                //}
                cl.remove('hidden');
                this.hideChildren();
            }

            var evt = {
                target: this,
                willBeShownTab: tab,
                action: action
            };

            this.emit('show', evt);
            this.inactivateAll();

            tab.parentNode.classList.add('active');
            this.activeTab = tab;

            if (contentBox) contentBox.classList.add('active');
            this.activeContent = contentBox;

            delete evt.willBeShownTab;
            this.emit('shown', evt);
            return this;
        }
    }, {
        key: 'hide',
        value: function hide() {
            this.container.classList.add('hidden');
            this.emit('widget-hide', { target: this });
            return this;
        }
    }, {
        key: 'hideChildren',
        value: function hideChildren() {
            var registry = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

            if (!registry) registry = this.widgets.registry;
            if (!registry.size) return;

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = registry.values()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var widget = _step.value;

                    this.hideChildren(widget.widgets.registry);
                    if (widget instanceof BaseTabs) widget.hide();
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return this;
        }
    }, {
        key: 'hideNavigation',
        value: function hideNavigation() {
            this.nav.classList.add('hidden');
        }
    }, {
        key: 'disable',
        value: function disable() {
            var tab = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

            if (!tab) {
                var tabs = this.nav.querySelectorAll('a');
                var slice = Array.prototype.slice;

                slice.call(tabs).forEach(function (node) {
                    node.parentNode.classList.add('disabled');
                });
            } else {
                tab.parentNode.classList.add('disabled');
            }
            return this;
        }
    }, {
        key: 'enable',
        value: function enable() {
            var tab = arguments.length <= 0 || arguments[0] === undefined ? null : arguments[0];

            if (!tab) {
                var tabs = this.nav.querySelectorAll('.disabled');
                var slice = Array.prototype.slice;

                slice.call(tabs).forEach(function (node) {
                    node.classList.remove('disabled');
                });
            } else {
                if (tab && tab.parentNode.classList.contains('disabled')) tab.parentNode.classList.remove('disabled');
            }
            return this;
        }
    }, {
        key: 'inactivateAll',
        value: function inactivateAll() {
            var activeElements = this.container.querySelectorAll('.active');
            for (var idx = 0; idx < activeElements.length; idx++) {
                activeElements[idx].classList.remove('active');
            }
            return this;
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            e.preventDefault();

            var target = e.target;
            if (target.tagName.toLowerCase() != 'a') return;
            if (target.closest('.close-container')) return;
            this.show(target);
        }
    }, {
        key: 'onWidgetShow',
        value: function onWidgetShow(e) {
            if (this.equals(e.target)) this.adjustTabs();
        }
    }, {
        key: 'onWidgetHide',
        value: function onWidgetHide(e) {
            if (this.equals(e.target)) this.adjustTabs(true);
        }
    }, {
        key: 'onShown',
        value: function onShown(e) {
            if (this.equals(e.target)) return;
            if (e.target.activeContent) return;

            var tab = this.getTabByAction(e.action);
            if (tab) this.show(tab);
        }
    }, {
        key: 'adjustTabs',
        value: function adjustTabs() {
            var clear = arguments.length <= 0 || arguments[0] === undefined ? false : arguments[0];

            var pane = this.container.closest('.tab-pane');
            if (pane) {
                var ps = pane.style,
                    ns = this.nav.style;
                ns.marginLeft = ps.paddingTop = !!clear ? '' : '0px';
            }
            return this;
        }
    }, {
        key: 'nav',
        get: function get() {
            if (this._nav === undefined) this._nav = this.container.parentNode.querySelector('#' + this.container.id + '>.nav');
            return this._nav;
        }
    }, {
        key: 'onclick',
        set: function set(fn) {
            return this.nav.onclick = fn;
        }
    }, {
        key: 'onshow',
        set: function set(fn) {
            this.on('show', fn);
        }
    }, {
        key: 'onshown',
        set: function set(fn) {
            this.on('shown', fn);
        }
    }, {
        key: 'onwidgetshow',
        set: function set(fn) {
            this.on('widget-show', fn);
        }
    }, {
        key: 'onwidgethide',
        set: function set(fn) {
            this.on('widget-hide', fn);
        }
    }]);

    return BaseTabs;
})(_BaseWidget3.default);

exports.default = BaseTabs;

},{"./BaseWidget":46}],46:[function(require,module,exports){
'use strict';

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _Emitter2 = require('./Emitter');

var _Emitter3 = _interopRequireDefault(_Emitter2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Registry = (function () {
    function Registry(widgetsContainer) {
        _classCallCheck(this, Registry);

        this.widgetsContainer = widgetsContainer;
    }

    _createClass(Registry, [{
        key: 'set',
        value: function set(widget) {
            var alias = undefined;
            if (typeof widget === 'string') {
                alias = widget.toLowerCase();
                widget = arguments[1];
            } else alias = widget.constructor.name.toLowerCase();

            if (this.registry.has(alias)) throw new Error('Widget with this alias already exists');

            this.registry.set(alias, widget);
            Registry.tree.set(widget, this.widgetsContainer);
            return this;
        }
    }, {
        key: 'get',
        value: function get(alias) {
            alias = alias.toLowerCase();
            return this.registry.has(alias) ? this.registry.get(alias) : null;
        }
    }, {
        key: 'has',
        value: function has(alias) {
            return this.registry.has(alias);
        }
    }, {
        key: 'registry',
        get: function get() {
            if (this._registry === undefined) this._registry = new Map();
            return this._registry;
        }
    }], [{
        key: 'tree',
        get: function get() {
            if (Registry._tree === undefined) Registry._tree = new WeakMap();
            return Registry._tree;
        }
    }]);

    return Registry;
})();

var Dom = (function () {
    function Dom() {
        _classCallCheck(this, Dom);
    }

    _createClass(Dom, [{
        key: 'node',
        value: function node(_node) {
            this._node = _node;
            return this;
        }
    }, {
        key: 'select',
        value: function select(selector) {
            var result = (this._node || this.canonical).querySelector(selector);
            this._node = null;
            return result;
        }
    }, {
        key: 'selectAll',
        value: function selectAll(selector) {
            var result = (this._node || this.canonical).querySelectorAll(selector) || [];
            this._node = null;
            return Array.prototype.slice.call(result);
        }
    }, {
        key: 'matches',
        value: function matches(selector) {
            var _this = this;

            var node = this._node || this.canonical;
            var p = Element.prototype;
            var polyfill = function polyfill(s) {
                return Array.prototype.indexOf.call(node.querySelectorAll(s), _this) !== -1;
            };
            var f = p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || polyfill;
            var result = f.call(node, selector);
            this._node = null;
            return result;
        }
    }, {
        key: 'canonical',
        set: function set(node) {
            this._canonical = node;
        },
        get: function get() {
            return this._canonical || document;
        }
    }], [{
        key: 'byId',
        value: function byId(nodeId) {
            return document.getElementById(nodeId);
        }
    }]);

    return Dom;
})();

var BaseWidget = (function (_Emitter) {
    _inherits(BaseWidget, _Emitter);

    function BaseWidget() {
        _classCallCheck(this, BaseWidget);

        return _possibleConstructorReturn(this, Object.getPrototypeOf(BaseWidget).apply(this, arguments));
    }

    _createClass(BaseWidget, [{
        key: 'equals',
        value: function equals(widget) {
            var selector = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

            if (selector.length) widget = widget.get(selector);
            if (!widget) throw new Error('Widget for comparison is undefined');

            if (!this.container || !widget.container) return false;
            if (!this.container.id || !widget.container.id) return false;

            return this.container.id === widget.container.id;
        }
    }, {
        key: 'get',
        value: function get(widgetName) {
            if (~widgetName.indexOf('>')) {
                var widget = this,
                    names = widgetName.split('>'),
                    len = names.length,
                    name = '';

                for (var i = 0; i < len; i++) {
                    name = names[i].trim();
                    if (i === len - 1) return widget.widgets.get(name);else widget = widget.get(name);
                }
            }

            return this.widgets.get(widgetName);
        }
    }, {
        key: 'emit',
        value: function emit(event) {
            var args = [].slice.call(arguments);

            if (args[1] !== undefined) {
                if (({}).toString.call(args[1]).slice(8, -1) === 'Object') {
                    if (!args[1].type) args[1].type = event;
                }
            }

            _get(Object.getPrototypeOf(BaseWidget.prototype), 'emit', this).apply(this, args);

            var parent = this.parent;
            while (parent !== null) {
                if (parent.hasListeners && parent.hasListeners(event)) _get(Object.getPrototypeOf(BaseWidget.prototype), 'emit', this).apply(parent, args);
                parent = parent.parent;
            }
        }
    }, {
        key: 'widgets',
        get: function get() {
            if (this._widgets === undefined) this._widgets = new Registry(this);
            return this._widgets;
        }
    }, {
        key: 'parent',
        get: function get() {
            return Registry.tree.has(this) ? Registry.tree.get(this) : null;
        }
    }, {
        key: 'dom',
        get: function get() {
            if (this._dom === undefined) this._dom = new Dom();
            if (this.container !== undefined) this._dom.canonical = this.container;
            return this._dom;
        }
    }], [{
        key: 'byId',
        value: function byId(nodeId) {
            return Dom.byId(nodeId);
        }
    }]);

    return BaseWidget;
})(_Emitter3.default);

exports.default = BaseWidget;

},{"./Emitter":47}],47:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

/**
 * Expose `Emitter`.
 */

//module.exports = Emitter;

/**
 * Initialize a new `Emitter`.
 *
 * @api public
 */

function Emitter(obj) {
    if (obj) return mixin(obj);
}

/**
 * Mixin the emitter properties.
 *
 * @param {Object} obj
 * @return {Object}
 * @api private
 */

function mixin(obj) {
    for (var key in Emitter.prototype) {
        obj[key] = Emitter.prototype[key];
    }
    return obj;
}

/**
 * Listen on the given `event` with `fn`.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.on = Emitter.prototype.addEventListener = function (event, fn) {
    this._callbacks = this._callbacks || {};
    (this._callbacks['$' + event] = this._callbacks['$' + event] || []).push(fn);
    return this;
};

/**
 * Adds an `event` listener that will be invoked a single
 * time then automatically removed.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.once = function (event, fn) {
    function on() {
        this.off(event, on);
        fn.apply(this, arguments);
    }

    on.fn = fn;
    this.on(event, on);
    return this;
};

/**
 * Remove the given callback for `event` or all
 * registered callbacks.
 *
 * @param {String} event
 * @param {Function} fn
 * @return {Emitter}
 * @api public
 */

Emitter.prototype.off = Emitter.prototype.removeListener = Emitter.prototype.removeAllListeners = Emitter.prototype.removeEventListener = function (event, fn) {
    this._callbacks = this._callbacks || {};

    // all
    if (0 == arguments.length) {
        this._callbacks = {};
        return this;
    }

    // specific event
    var callbacks = this._callbacks['$' + event];
    if (!callbacks) return this;

    // remove all handlers
    if (1 == arguments.length) {
        delete this._callbacks['$' + event];
        return this;
    }

    // remove specific handler
    var cb;
    for (var i = 0; i < callbacks.length; i++) {
        cb = callbacks[i];
        if (cb === fn || cb.fn === fn) {
            callbacks.splice(i, 1);
            break;
        }
    }
    return this;
};

/**
 * Emit `event` with the given args.
 *
 * @param {String} event
 * @param {Mixed} ...
 * @return {Emitter}
 */

Emitter.prototype.emit = function (event) {
    this._callbacks = this._callbacks || {};
    var args = [].slice.call(arguments, 1),
        callbacks = this._callbacks['$' + event];

    if (callbacks) {
        callbacks = callbacks.slice(0);
        for (var i = 0, len = callbacks.length; i < len; ++i) {
            callbacks[i].apply(this, args);
        }
    }

    return this;
};

/**
 * Return array of callbacks for `event`.
 *
 * @param {String} event
 * @return {Array}
 * @api public
 */

Emitter.prototype.listeners = function (event) {
    this._callbacks = this._callbacks || {};
    return this._callbacks['$' + event] || [];
};

/**
 * Check if this emitter has `event` handlers.
 *
 * @param {String} event
 * @return {Boolean}
 * @api public
 */

Emitter.prototype.hasListeners = function (event) {
    return !!this.listeners(event).length;
};

exports.default = Emitter;

},{}],48:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _typeof(obj) { return obj && typeof Symbol !== "undefined" && obj.constructor === Symbol ? "symbol" : typeof obj; }

function trigger(elem) {
    var event = new Event('change', {
        'bubbles': true,
        'cancelable': true
    });

    elem.dispatchEvent(event);
}

/**
 * Populate form fields from a JSON object.
 *
 * @param form object The form element containing your input fields.
 * @param data array JSON data to populate the fields with.
 * @param basename string Optional basename which is added to `name` attributes
 */
var populate = function populate(form, data, basename) {

    for (var key in data) {
        if (data.hasOwnProperty(key)) {

            var name = key;
            var value = data[key];

            // handle array name attributes
            if (typeof basename !== "undefined") name = basename + "[" + key + "]";

            if (value.constructor === Array) {
                name += '[]';
            } else if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) == "object") {
                populate(form, value, name);
                continue;
            }

            // only proceed if element is set
            var element = form.elements.namedItem(name);
            if (!element) continue;

            var type = element.type || element[0].type;

            switch (type) {
                default:
                    element.value = value;
                    break;

                case 'radio':
                case 'checkbox':
                    var len = element.length;
                    if (!len) {
                        element.checked = !!value;
                    } else {
                        for (var j = 0; j < len; j++) {
                            element[j].checked = value.indexOf(element[j].value) > -1;
                        }
                    }

                    break;

                case 'select-multiple':
                    var values = value.constructor == Array ? value : [value];

                    for (var k = 0; k < element.options.length; k++) {
                        element.options[k].selected |= values.indexOf(element.options[k].value) > -1;
                    }
                    break;

                case 'select':
                case 'select-one':
                    element.value = value.toString() || value;
                    break;

            }
            trigger(element);
        }
    }
};

exports.default = populate;

},{}]},{},[10])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvZm9ybS10by1vYmovZm9ybS10by1vYmouanMiLCJub2RlX21vZHVsZXMveGhyL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL3hoci9ub2RlX21vZHVsZXMvZ2xvYmFsL3dpbmRvdy5qcyIsIm5vZGVfbW9kdWxlcy94aHIvbm9kZV9tb2R1bGVzL2lzLWZ1bmN0aW9uL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL3hoci9ub2RlX21vZHVsZXMvb25jZS9vbmNlLmpzIiwibm9kZV9tb2R1bGVzL3hoci9ub2RlX21vZHVsZXMvcGFyc2UtaGVhZGVycy9ub2RlX21vZHVsZXMvZm9yLWVhY2gvaW5kZXguanMiLCJub2RlX21vZHVsZXMveGhyL25vZGVfbW9kdWxlcy9wYXJzZS1oZWFkZXJzL25vZGVfbW9kdWxlcy90cmltL2luZGV4LmpzIiwibm9kZV9tb2R1bGVzL3hoci9ub2RlX21vZHVsZXMvcGFyc2UtaGVhZGVycy9wYXJzZS1oZWFkZXJzLmpzIiwibm9kZV9tb2R1bGVzL3hoci9ub2RlX21vZHVsZXMveHRlbmQvaW1tdXRhYmxlLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjYXRhbG9nLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxCYXNlQ2F0YWxvZ1dpZGdldC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcQ2F0YWxvZy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcT3ZlcmxheS5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcVHJlZS5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcVGFicy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcbW9kZWxcXENhY2hlLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxjYXRhbG9nXFxtb2RlbFxcTW9kZWwuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXG1vZGVsXFxVcGxvYWRNb2RlbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcRGVsZXRlVmlldy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcR29vZHMuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXHRhYnNcXFRhYmxlVmlldy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcYWRkQ2F0YWxvZ1RhYnMuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXHRhYnNcXGFkZENhdGFsb2dUYWJzXFxGb3JtLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxjYXRhbG9nXFx0YWJzXFxhZGRDYXRhbG9nVGFic1xcVXBsb2FkRm9ybS5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXE1vZGVsLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxjYXRhbG9nXFx0YWJzXFxnb29kc1xcVGFicy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXEFkZEdvb2RzVGFicy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXEdvb2RzR3JpZC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXEdvb2RzR3JpZFxcRGVsZXRlVmlldy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXEdvb2RzVmlldy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXGFkZEdvb2RzVGFic1xcRGF0YUZvcm0uanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXHRhYnNcXGdvb2RzXFx0YWJzXFxhZGRHb29kc1RhYnNcXEdvb2RzRGV0YWlsZWQuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXHRhYnNcXGdvb2RzXFx0YWJzXFxhZGRHb29kc1RhYnNcXEdvb2RzR2FsbGVyeS5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXGFkZEdvb2RzVGFic1xcZ29vZHNEZXRhaWxlZFxcRGV0YWlsZWRHcmlkLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxjYXRhbG9nXFx0YWJzXFxnb29kc1xcdGFic1xcYWRkR29vZHNUYWJzXFxnb29kc0RldGFpbGVkXFxNb2RlbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXGFkZEdvb2RzVGFic1xcZ29vZHNEZXRhaWxlZFxcZGV0YWlsZWRHcmlkXFxEZWxldGVWaWV3LmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxjb21wb25lbnRzXFxjYXRhbG9nXFx0YWJzXFxnb29kc1xcdGFic1xcYWRkR29vZHNUYWJzXFxnb29kc0RldGFpbGVkXFxkZXRhaWxlZEdyaWRcXEZvcm0uanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXGNhdGFsb2dcXHRhYnNcXGdvb2RzXFx0YWJzXFxhZGRHb29kc1RhYnNcXGdvb2RzR2FsbGVyeVxcTGlzdC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXGFkZEdvb2RzVGFic1xcZ29vZHNHYWxsZXJ5XFxNb2RlbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcY2F0YWxvZ1xcdGFic1xcZ29vZHNcXHRhYnNcXGFkZEdvb2RzVGFic1xcZ29vZHNHYWxsZXJ5XFxVcGxvYWRGb3JtLmpzIiwic3JjL3NjcmlwdHMvZXMtNi9jb25maWcuanNvbiIsInNyY1xcc2NyaXB0c1xcZXMtNlxcbGliXFxCYXNlRGVsZXRlLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxsaWJcXEJhc2VGb3JtLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxsaWJcXEJhc2VNb2RlbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcbGliXFxCYXNlVGFicy5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcbGliXFxCYXNlV2lkZ2V0LmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxsaWJcXEVtaXR0ZXIuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGxpYlxccG9wdWxhdGUuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNqRkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQzNOQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7OztBQ1RBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ2ZBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDOUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNkQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUM5QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7Ozs7OztBQ2pCQSxRQUFRLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsWUFBTTtBQUNoRCxzQkFBUSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7Q0FDMUIsQ0FBQyxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0ZrQixpQkFBaUI7Y0FBakIsaUJBQWlCOztBQUVsQyxhQUZpQixpQkFBaUIsR0FFcEI7OEJBRkcsaUJBQWlCOzsyRUFBakIsaUJBQWlCOztBQUk5Qix5QkFBaUIsQ0FBQyxLQUFLLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQTs7S0FDdEM7O2lCQUxnQixpQkFBaUI7OzRCQU92QjtBQUNQLG1CQUFPLGlCQUFpQixDQUFDLEtBQUssQ0FBQTtTQUNqQzs7O1dBVGdCLGlCQUFpQjs7O2tCQUFqQixpQkFBaUI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNLakIsT0FBTztjQUFQLE9BQU87O0FBRXhCLGFBRmlCLE9BQU8sQ0FFWixTQUFTLEVBQUU7OEJBRk4sT0FBTzs7MkVBQVAsT0FBTzs7QUFJcEIsY0FBSyxTQUFTLEdBQUssU0FBUyxDQUFBO0FBQzVCLGNBQUssS0FBSyxHQUFTLHFCQUFXLENBQUE7QUFDOUIsY0FBSyxXQUFXLEdBQUcsMkJBQWlCLENBQUE7O0FBRXBDLGNBQUssT0FBTyxDQUNQLEdBQUcsQ0FBQyxlQUFLLElBQUksQ0FBQyxNQUFNLEVBQUUsTUFBSyxLQUFLLENBQUMsQ0FBQyxDQUNsQyxHQUFHLENBQUMsZUFBSyxJQUFJLENBQUMsTUFBSyxTQUFTLENBQUMsQ0FBQyxDQUFBOztBQUVuQyxjQUFLLFFBQVEsR0FBRyxNQUFLLE1BQU0sQ0FBQyxJQUFJLE9BQU0sQ0FBQTtBQUN0QyxjQUFLLFFBQVEsR0FBRyxNQUFLLE1BQU0sQ0FBQyxJQUFJLE9BQU0sQ0FBQTtBQUN0QyxjQUFLLFFBQVEsR0FBRyxNQUFLLE1BQU0sQ0FBQyxJQUFJLE9BQU0sQ0FBQTs7QUFFdEMsWUFBSSxJQUFJLEdBQUcsTUFBSyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDM0IsWUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7O0FBRTFCLGNBQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE9BQU8sR0FBRzttQkFBTSxNQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUU7U0FBQSxDQUFBOztLQUM3Rjs7aUJBcEJnQixPQUFPOzsrQkEyQmpCLENBQUMsRUFBRTs7O0FBQ04sZ0JBQUksSUFBSSxHQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDNUIsZ0JBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQTs7QUFFckMsZ0JBQUksRUFBRSxLQUFLLEdBQUcsRUFBRTtBQUNaLG9CQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsRUFBQyxFQUFFLEVBQUYsRUFBRSxFQUFDLENBQUMsQ0FBQTtBQUM5QixvQkFBSSxDQUFDLGNBQWMsRUFBRSxDQUFBO0FBQ3JCLHVCQUFNO2FBQ1Q7O0FBRUQsZ0JBQUksSUFBSSxHQUFNLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDOUIsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTs7QUFFOUIsZ0JBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtBQUNiLG1CQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVyQyxnQkFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQ2IsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDOUIsb0JBQUksSUFBSSxDQUFDLGNBQWMsRUFBRSxPQUFPLE9BQUssS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUE7YUFDbEUsQ0FBQyxDQUNELElBQUksQ0FBQyxZQUFNO0FBQ1Isb0JBQUksV0FBVyxHQUFHLE9BQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTs7QUFFMUMsb0JBQUcsV0FBVyxDQUFDLEdBQUcsRUFBRTtBQUNoQix3QkFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUM1Qyx3QkFBSSxDQUFDLElBQUksRUFBRSxDQUFBO2lCQUNkLE1BQU07QUFDSCwyQkFBSyxXQUFXLENBQ1gsWUFBWSxDQUFDLE9BQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FDekMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osK0JBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFBO0FBQ25DLDRCQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7cUJBQ2QsQ0FBQyxDQUNELEtBQUssQ0FBQyxVQUFDLEtBQUs7K0JBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7cUJBQUEsQ0FBQyxDQUFBO2lCQUM1QzthQUNKLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBQyxLQUFLO3VCQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO2FBQUEsQ0FBQyxDQUFBO1NBQzVDOzs7K0JBRU0sQ0FBQyxFQUFFO0FBQ04sZ0JBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLG9CQUFvQixDQUFDLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUNuRSxnQkFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsMEJBQTBCLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDbEY7Ozt5Q0FFZ0IsQ0FBQyxFQUFFOzs7QUFDaEIsZ0JBQUksSUFBSSxHQUFNLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDdEIsZ0JBQUksSUFBSSxHQUFNLENBQUMsQ0FBQyxJQUFJLENBQUE7QUFDcEIsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTs7NEJBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQzs7Z0JBQWpDLEVBQUUsYUFBRixFQUFFOztBQUVQLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUE7QUFDekIsZ0JBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQTs7QUFFdEMsZ0JBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHO3VCQUFNLE9BQU8sQ0FBQyxPQUFPLEVBQUU7YUFBQSxDQUFBOztBQUV6QyxtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVoQixnQkFBSSxDQUFDLFdBQVcsQ0FDWCxHQUFHLENBQUMsSUFBSSxDQUFDLENBQ1QsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQTtBQUN0Qix1QkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUE7QUFDbkMsb0JBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ3ZCLG9CQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7YUFDZixDQUFDLENBQ0QsS0FBSyxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ2Ysb0JBQUksTUFBTSxZQUFZLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQSxLQUN2RDtBQUNELHdCQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ3RCLDJCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7aUJBQ3BCO2FBQ0osQ0FBQyxDQUFBO1NBQ1Q7OzttQ0FFVSxDQUFDLEVBQUU7OztBQUNWLGdCQUFJLElBQUksR0FBTSxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ3RCLGdCQUFJLElBQUksR0FBTSxDQUFDLENBQUMsSUFBSSxDQUFBO0FBQ3BCLGdCQUFJLE9BQU8sR0FBRyxrQkFBUSxRQUFRLENBQUE7O0FBRTlCLG1CQUFPLENBQUMsTUFBTSxFQUFFLENBQUE7O0FBRWhCLGdCQUFJLENBQUMsS0FBSyxDQUNMLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FDVCxJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDWixvQkFBSSxPQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEVBQUU7QUFDMUIsd0JBQUksV0FBVyxHQUFHLE9BQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUMxQyx3QkFBSSxXQUFXLEdBQUcsV0FBVyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxJQUFJLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFBO0FBQzVFLHdCQUFJLFdBQVcsRUFBRSxJQUFJLENBQUMsR0FBRyxHQUFHLFdBQVcsQ0FBQyxHQUFHLENBQUE7aUJBQzlDOztBQUVELHVCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFBO0FBQzlCLHVCQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTthQUMxQixDQUFDLENBQ0QsS0FBSyxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ2Ysb0JBQUksTUFBTSxZQUFZLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQSxLQUN2RDtBQUNELHdCQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQ3RCLDJCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7aUJBQ3BCO2FBQ0osQ0FBQyxDQUFBO1NBQ1Q7OzsrQkFFTSxDQUFDLEVBQUU7QUFDTixnQkFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtBQUNyQixnQkFBSSxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDaEUsZ0JBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsMEJBQTBCLENBQUMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDaEY7OzttQ0FFVSxDQUFDLEVBQUU7OztBQUNWLGdCQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ3pCLGdCQUFJLE9BQU8sR0FBTSxrQkFBUSxRQUFRLENBQUE7QUFDakMsZ0JBQUksSUFBSSxHQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUV6QyxtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVoQixnQkFBSSxDQUFDLEtBQUssQ0FDTCxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUNmLElBQUksQ0FBQyxZQUFNO0FBQ1Isb0JBQUksS0FBSyxHQUFHLEFBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSyxJQUFJLENBQUE7QUFDN0Msb0JBQUksS0FBSyxFQUFFLE9BQU8sT0FBSyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFBO2FBQ25ELENBQUMsQ0FDRCxJQUFJLENBQUM7dUJBQU0sT0FBTyxDQUFDLE9BQU8sRUFBRTthQUFBLENBQUMsQ0FDN0IsS0FBSyxDQUFDLFVBQUMsTUFBTSxFQUFLO0FBQ2Ysb0JBQUksTUFBTSxZQUFZLEtBQUssRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQSxLQUN2RDtBQUNELDhCQUFVLENBQ0wsWUFBWSxFQUFFLENBQ2QsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUVyRCwyQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2lCQUNwQjthQUNKLENBQUMsQ0FBQTtTQUNUOzs7eUNBRWdCLENBQUMsRUFBRTtBQUNoQixnQkFBSSxPQUFPLEdBQU0sa0JBQVEsUUFBUSxDQUFBO0FBQ2pDLGdCQUFJLElBQUksR0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUN6QyxnQkFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTs7QUFFekIsZ0JBQUksS0FBSyxHQUFHLEFBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsSUFBSyxJQUFJLENBQUM7O0FBRTlDLGdCQUFJLEtBQUssRUFBRTtBQUNQLHVCQUFPLENBQUMsTUFBTSxFQUFFLENBQUE7O0FBRWhCLG9CQUFJLENBQUMsV0FBVyxDQUNYLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FDYixJQUFJLENBQUMsWUFBTTtBQUNSLDhCQUFVLENBQUMsUUFBUSxFQUFFLENBQUE7QUFDckIsMkJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtpQkFDcEIsQ0FBQyxDQUNELEtBQUssQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUNmLHdCQUFJLE1BQU0sWUFBWSxLQUFLLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUEsS0FDdkQsT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2lCQUN6QixDQUFDLENBQUE7YUFDVDtTQUNKOzs7MEJBRVksRUFBRSxFQUFFO0FBQUUsZ0JBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQUM7OzswQkFDN0IsRUFBRSxFQUFHO0FBQUUsZ0JBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQUU7OzswQkFDNUIsRUFBRSxFQUFFO0FBQUUsZ0JBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQUU7OzswQkFDN0IsRUFBRSxFQUFFO0FBQUUsZ0JBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQUU7Ozs2QkF0SzlCLFdBQVcsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4RCxtQkFBTyxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUNwQzs7O1dBekJnQixPQUFPOzs7a0JBQVAsT0FBTzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0xQLE9BQU87QUFFeEIsYUFGaUIsT0FBTyxDQUVaLFNBQVMsRUFBRTs4QkFGTixPQUFPOztBQUdwQixZQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQTtLQUM3Qjs7aUJBSmdCLE9BQU87O2lDQThEZjtBQUNMLGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDekMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztrQ0FFUztBQUNOLGdCQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQTtBQUNqQyxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUM1QyxnQkFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO0FBQ2hCLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7Z0NBRU8sSUFBSSxFQUFFO0FBQ1YsZ0JBQUksYUFBYSxHQUFHLGlCQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDeEMsZ0JBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQTtBQUM5QixnQkFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFBO0FBQ25FLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7b0NBRVc7QUFDUixnQkFBSSxDQUFDLFlBQVksRUFBRSxDQUFBO0FBQ25CLG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQTtTQUN6Qzs7O2dDQUVPLElBQUksRUFBRTtBQUNWLGdCQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUE7QUFDaEMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztvQ0FFVztBQUNSLG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUE7U0FDMUI7OztnQ0FFTyxJQUFJLEVBQUU7QUFDVixnQkFBSSxDQUFDLFNBQVMsRUFBRSxDQUFBO0FBQ2hCLGdCQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQyxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3VDQUVjO0FBQ1gsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFBO0FBQ2xDLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ2xELG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7MENBRWlCO0FBQ2QsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFBO0FBQ2xDLGdCQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUNwRCxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3VDQUVjO0FBQ1gsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFBO0FBQ2hDLGdCQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUM5QyxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3VDQUVjO0FBQ1gsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFBO0FBQ2hDLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQzVDLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7NEJBL0ZnQjtBQUNiLGdCQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtBQUN2RyxtQkFBTyxJQUFJLENBQUMsV0FBVyxDQUFBO1NBQzFCOzs7NEJBRWM7OztBQUNYLGdCQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO0FBQzlCLG9CQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtBQUN4RSxvQkFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFBO0FBQzdDLG9CQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRXpDLG9CQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxVQUFDLENBQUMsRUFBSztBQUM1QixxQkFBQyxDQUFDLGNBQWMsRUFBRSxDQUFBO0FBQ2xCLDBCQUFLLE9BQU8sRUFBRSxDQUNULFlBQVksRUFBRSxDQUNkLFNBQVMsRUFBRSxDQUFBO2lCQUNuQixDQUFBOztBQUVELG9CQUFJLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUE7QUFDbEUsb0JBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQTthQUN2QztBQUNELG1CQUFPLElBQUksQ0FBQyxTQUFTLENBQUE7U0FDeEI7OzswQkFFWSxJQUFJLEVBQUU7QUFDZixnQkFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFBO1NBRTFCOzRCQUVjO0FBQ1gsZ0JBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7QUFDeEQsbUJBQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQTtTQUN4Qjs7OzBDQXREd0I7QUFDckIsZ0JBQUksV0FBVyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDL0MsdUJBQVcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLFdBQVcsQ0FBQyxDQUFBO0FBQ3pELHVCQUFXLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsaUJBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQTs7QUFFbkYsZ0JBQUksZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQTtBQUNwRCw0QkFBZ0IsQ0FBQyxFQUFFLEdBQUcsU0FBUyxDQUFBO0FBQy9CLDRCQUFnQixDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFBO0FBQ25ELDRCQUFnQixDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs7QUFFekMsb0JBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUE7O0FBRTNDLG1CQUFPLGdCQUFnQixDQUFBO1NBQzFCOzs7NEJBRXFCO0FBQ2xCLGdCQUFJLE9BQU8sQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO0FBQ2pDLHVCQUFPLENBQUMsU0FBUyxHQUFHLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFBO2FBQzdEO0FBQ0QsbUJBQU8sT0FBTyxDQUFDLFNBQVMsQ0FBQTtTQUMzQjs7O1dBMUJnQixPQUFPOzs7a0JBQVAsT0FBTzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNBUCxJQUFJO2NBQUosSUFBSTs7QUFFckIsYUFGaUIsSUFBSSxDQUVULFNBQVMsRUFBRSxLQUFLLEVBQUU7OEJBRmIsSUFBSTs7MkVBQUosSUFBSTs7QUFJakIsY0FBSyxTQUFTLEdBQUcsU0FBUyxDQUFDO0FBQzNCLGNBQUssS0FBSyxHQUFPLEtBQUssQ0FBQztBQUN2QixjQUFLLElBQUksR0FBUSxNQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsQ0FBQztBQUNqRSxjQUFLLE1BQU0sR0FBTSxJQUFJLENBQUM7O0FBRXRCLGNBQUssU0FBUyxDQUFDLE9BQU8sR0FBRyxNQUFLLE9BQU8sQ0FBQyxJQUFJLE9BQU0sQ0FBQzs7QUFFakQsY0FBSyxLQUFLLENBQUMsYUFBYSxHQUFHLFVBQUMsQ0FBQzttQkFBSyxNQUFLLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FBQSxDQUFDO0FBQ3JFLGNBQUssS0FBSyxDQUFDLEtBQUssR0FBVyxVQUFDLENBQUMsRUFBSztBQUM5QixnQkFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxNQUFLLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUEsS0FDM0QsTUFBSyxlQUFlLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3JDLENBQUE7QUFDRCxjQUFLLEtBQUssQ0FBQyxRQUFRLEdBQUcsTUFBSyxlQUFlLENBQUMsSUFBSSxPQUFNLENBQUM7O0tBQ3pEOztpQkFqQmdCLElBQUk7OzhCQXdCZixJQUFJLEVBQUU7QUFDUixtQkFBTyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUNyRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUNwQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQUFBQyxDQUFDO1NBQ3pFOzs7K0JBTU0sSUFBSSxFQUFFO0FBQ1QsZ0JBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzdCLGdCQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3RCLG1CQUFPLElBQUksQ0FBQztTQUNmOzs7aUNBRVEsSUFBSSxFQUFFO0FBQ1gsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7QUFDeEIsZ0JBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQzlDLG1CQUFPLElBQUksQ0FBQztTQUNmOzs7bUNBRVUsSUFBSSxFQUFFO0FBQ2IsZ0JBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ3RELGdCQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztBQUM3QixnQkFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7QUFDbkIsZ0JBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztBQUN4RCxtQkFBTyxJQUFJLENBQUM7U0FDZjs7O3FDQU1ZLElBQUksRUFBRTtnQkFDVixFQUFFLEdBQVcsSUFBSSxDQUFqQixFQUFFO2dCQUFFLEtBQUssR0FBSSxJQUFJLENBQWIsS0FBSzs7QUFDZCxnQkFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7O0FBRWpFLGdCQUFJLElBQUksZ0RBQThDLEVBQUUsNkRBQ2IsS0FBSyxVQUFLLEtBQUssa0RBRXBEOzs7QUFBQyxBQUdQLGdCQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixFQUFFO0FBQ3BDLCtCQUFlLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3pELE1BQU07O0FBRUgsb0JBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FDakQsTUFBTSxDQUFDLFVBQUMsSUFBSTsyQkFBSyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztpQkFBQSxDQUFDOzs7QUFBQyxBQUdqRSxvQkFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUU7QUFDbEIsbUNBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQzFELE1BQU07O0FBRUgsd0JBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxHQUFHLEVBQUU7O0FBQUMsQUFFMUIsd0JBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO0FBQ3pCLDRCQUFJLENBQUMsa0JBQWtCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO3FCQUM3QyxNQUFNOztBQUVILHVDQUFlLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO3FCQUN6RDtpQkFDSjthQUNKOztBQUVELGdCQUFJLFNBQVMsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLDRCQUE0QixDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztBQUNuRyxnQkFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUMxQjs7O3dDQUVlLElBQUksRUFBRTtBQUNsQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQztBQUN6RCxnQkFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0FBQ3ZDLGdCQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQzFDOzs7OENBRXFCLElBQUksRUFBRTtBQUN4QixnQkFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTztBQUN6QixnQkFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU87O0FBRWpELGdCQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQztBQUNqRSxnQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDOztBQUVkLGdCQUFJLENBQUMsT0FBTyxDQUFDLFVBQVMsSUFBSSxFQUFFO29CQUNuQixFQUFFLEdBQVcsSUFBSSxDQUFqQixFQUFFO29CQUFFLEtBQUssR0FBSSxJQUFJLENBQWIsS0FBSzs7QUFDZCxvQkFBSSxDQUFDLElBQUksOENBQTRDLEVBQUUsaUVBQ1osS0FBSyxVQUFLLEtBQUsseUNBQzFDLENBQUM7YUFDcEIsQ0FBQyxDQUFDOztBQUVILDJCQUFlLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDN0M7OzswQ0FFaUI7QUFDZCxnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztBQUN2QixnQkFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLE9BQU87O0FBRXRELGdCQUFJLFVBQVUsWUFBQTtnQkFBRSxNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzs7QUFFekMsZ0JBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztBQUNuQyxnQkFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQ3BFO0FBQ0Qsb0JBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQztBQUN2QyxvQkFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxVQUFVLEdBQUcsSUFBSSxDQUFDO2FBQzVFOztBQUVELGdCQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDbkMsZ0JBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDMUQsa0JBQU0sQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUI7OztnQ0FFTyxDQUFDLEVBQUU7QUFDUCxnQkFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQztBQUN0QixnQkFBSSxFQUFFLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQzs7QUFFMUIsZ0JBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDL0YsZ0JBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDMUQ7OzswQkFFWSxFQUFFLEVBQUk7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzZCQTdIaEMsV0FBVyxFQUFFLEtBQUssRUFBRTtBQUM1QixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN6RCxtQkFBTyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDekM7OztpQ0FRZSxJQUFJLEVBQUU7QUFDbEIsbUJBQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDNUM7OzsyQ0FzQnlCLElBQUksRUFBRTtBQUM1QixtQkFBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ2xEOzs7V0F4RGdCLElBQUk7OztrQkFBSixJQUFJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDS0osSUFBSTtjQUFKLElBQUk7O0FBRXJCLGFBRmlCLElBQUksQ0FFVCxTQUFTLEVBQUU7OEJBRk4sSUFBSTs7MkVBQUosSUFBSSxhQUdYLFNBQVM7O0FBRWYsY0FBSyxPQUFPLENBQ1AsR0FBRyxDQUFDLE1BQU0sRUFBRSx5QkFBZSxJQUFJLENBQUMseUJBQXlCLENBQUMsQ0FBQyxDQUMzRCxHQUFHLENBQUMscUJBQVcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FDdEMsR0FBRyxDQUFDLGdCQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFBOztBQUU3QixjQUFLLE9BQU8sR0FBRyxNQUFLLEtBQUssQ0FBQyxJQUFJLE9BQU0sQ0FBQTs7S0FDdkM7O2lCQVhnQixJQUFJOzs4QkFzRGYsQ0FBQyxFQUFFO0FBQ0wsZ0JBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDckIsZ0JBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU07O0FBRWhDLG9CQUFRLE1BQU0sQ0FBQyxTQUFTO0FBQ3BCLHFCQUFLLElBQUksQ0FBQyxPQUFPO0FBQUksd0JBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsQUFDakQscUJBQUssSUFBSSxDQUFDLE1BQU07QUFBSyx3QkFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxBQUNoRCxxQkFBSyxJQUFJLENBQUMsT0FBTztBQUFJLHdCQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEFBQUMsTUFBSztBQUFBLEFBQ2pELHFCQUFLLElBQUksQ0FBQyxTQUFTO0FBQUUsd0JBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsQUFDbkQscUJBQUssSUFBSSxDQUFDLFFBQVE7QUFBRyx3QkFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxhQUNyRDtTQUNKOzs7eUNBRWdCO0FBQ2IsZ0JBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQTtBQUNkLGdCQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUN4QixnQkFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDdEIsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztzQ0FFYSxDQUFDLEVBQUU7QUFDYixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFBO0FBQzFDLGdCQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQTtBQUN4QyxnQkFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDbkQsOEJBQVEsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFBO1NBQzdCOzs7d0NBRWUsQ0FBQyxFQUFFO0FBQ2YsZ0JBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUE7U0FDakM7OztxQ0FFWSxDQUFDLEVBQUU7QUFDWixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUMzQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7bUNBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUM7O2dCQUFyQyxFQUFFLG9CQUFGLEVBQUU7O0FBRVAsZ0JBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQTtBQUNYLGdCQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUE7QUFDbEMsZ0JBQUksQ0FBQyxJQUFJLEdBQUcsRUFBQyxTQUFTLEVBQUUsRUFBRSxFQUFDLENBQUE7O0FBRTNCLGdCQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUMvQjs7O3NDQUVhLENBQUMsRUFBRTtBQUNiLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQzNCLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBOztBQUUzQixnQkFBSSxDQUFDLElBQUksRUFBRSxDQUFBO0FBQ1gsZ0JBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFBO0FBQ3BCLGdCQUFJLENBQUMsSUFBSSxHQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTs7QUFFN0MsZ0JBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQTtTQUNoQjs7O3VDQUVjLENBQUMsRUFBRTtBQUNkLGdCQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQzdCLGlCQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO0FBQ3hCLGdCQUFJLENBQUMsY0FBYyxFQUFFLENBQUE7U0FDeEI7OzswQkEvRlEsSUFBSSxFQUFFO0FBQ1gsZ0JBQUksQ0FBQSxHQUFFLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssUUFBUSxFQUFFLE1BQU0sSUFBSSxLQUFLLENBQUMsMkRBQTJELENBQUMsQ0FBQTtBQUNsSSxnQkFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUE7U0FDcEI7NEJBRVU7QUFBRSxtQkFBTyxJQUFJLENBQUMsS0FBSyxDQUFBO1NBQUU7Ozs0QkFFbEI7QUFDVixnQkFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLCtCQUErQixDQUFDLENBQUE7QUFDOUcsbUJBQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQTtTQUN2Qjs7OzRCQUVZO0FBQ1QsZ0JBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFBO0FBQzNHLG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUE7U0FDdEI7Ozs0QkFFYTtBQUNWLGdCQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsK0JBQStCLENBQUMsQ0FBQTtBQUM5RyxtQkFBTyxJQUFJLENBQUMsUUFBUSxDQUFBO1NBQ3ZCOzs7NEJBRWU7QUFDWixnQkFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLGlDQUFpQyxDQUFDLENBQUE7QUFDcEgsbUJBQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQTtTQUN6Qjs7OzRCQUVjO0FBQ1gsZ0JBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFBO0FBQ25ILG1CQUFPLElBQUksQ0FBQyxTQUFTLENBQUE7U0FDeEI7Ozs0QkFFZTtBQUNaLGdCQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsb0JBQVUsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUNuRixtQkFBTyxJQUFJLENBQUMsVUFBVSxDQUFBO1NBQ3pCOzs7NkJBdkNXLFNBQVMsRUFBRTtBQUNuQixtQkFBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtTQUM3Qjs7O1dBZmdCLElBQUk7OztrQkFBSixJQUFJOzs7Ozs7Ozs7Ozs7O0lDUEosS0FBSztBQUV0QixhQUZpQixLQUFLLEdBRVI7OEJBRkcsS0FBSzs7QUFHbEIsWUFBSSxDQUFDLElBQUksR0FBTyxJQUFJLEdBQUcsRUFBRSxDQUFDO0FBQzFCLFlBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztLQUM3Qjs7aUJBTGdCLEtBQUs7OzRCQU9sQixJQUFJLEVBQUU7QUFDTixnQkFBSSxDQUFBLEdBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxPQUFPLEVBQUU7QUFDakQscUJBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtBQUFFLHdCQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUFBO2FBQzFELE1BQU07QUFDSCxvQkFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQzs7QUFFN0Isb0JBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQ2pGLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQzlEO0FBQ0QsbUJBQU8sSUFBSSxDQUFDO1NBQ2Y7Ozs0QkFFRyxFQUFFLEVBQUU7QUFDSixtQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7U0FDdkQ7Ozs0QkFFRyxFQUFFLEVBQUU7QUFDSixtQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM1Qjs7O29DQUVXLEdBQUcsRUFBRTtBQUNiLGdCQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7QUFDaEIsZ0JBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxPQUFPLE1BQU0sQ0FBQzs7QUFFM0MsZ0JBQUksSUFBSSxZQUFBLENBQUM7QUFDVCxnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7Ozs7OztBQUNuQyxxQ0FBZSxLQUFLLDhIQUFFO3dCQUFiLEVBQUU7O0FBQ1Asd0JBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUN6Qix3QkFBSSxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDL0I7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDRCxtQkFBTyxNQUFNLENBQUM7U0FDakI7OztvQ0FFVyxHQUFHLEVBQUU7QUFDYixtQkFBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNqQzs7OytCQUVNLEVBQUUsRUFBRTtBQUNQLGdCQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQ25CLG9CQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUM7QUFDdEMsb0JBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM1RCxvQkFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLHlCQUF5QixDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQzVELG9CQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN4QjtBQUNELG1CQUFPLElBQUksQ0FBQztTQUNmOzs7a0RBRXlCLEVBQUUsRUFBRTs7O0FBQzFCLGdCQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEVBQUUsT0FBTzs7QUFFbkMsZ0JBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ3hDLGdCQUFJLFdBQVcsQ0FBQyxJQUFJLEVBQUU7QUFDbEIsMkJBQVcsQ0FBQyxPQUFPLENBQUMsVUFBQyxFQUFFOzJCQUFLLE1BQUsseUJBQXlCLENBQUMsRUFBRSxDQUFDO2lCQUFBLENBQUMsQ0FBQzthQUNuRTtBQUNELGdCQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM1Qjs7O1dBOURnQixLQUFLOzs7a0JBQUwsS0FBSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0lMLEtBQUs7Y0FBTCxLQUFLOzthQUFMLEtBQUs7OEJBQUwsS0FBSzs7c0VBQUwsS0FBSzs7O2lCQUFMLEtBQUs7OzRCQVlsQixFQUFFLEVBQUU7OztBQUNKLGdCQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQ3BCLG9CQUFJLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztBQUM5QixvQkFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0FBQzdDLHVCQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEM7O0FBRUQsZ0JBQUksT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxNQUFNLEVBQUUsb0JBQVUsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7QUFDaEYsbUJBQU8sQ0FBQyxHQUFHLFFBQU0sT0FBTyxDQUFDLEdBQUcsR0FBRyxFQUFFLEFBQUUsQ0FBQztBQUNwQyxtQkFBTyxJQUFJLENBQ04sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUNaLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLHVCQUFLLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDckIsdUJBQUssSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0FBQzdDLHVCQUFPLElBQUksQ0FBQzthQUNmLENBQUMsQ0FBQztTQUNWOzs7b0NBRVcsRUFBRSxFQUFFOzs7QUFDWixnQkFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsRUFBRTtBQUM1QixvQkFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDMUMsb0JBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FBQztBQUN6RCx1QkFBTyxPQUFPLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3BDOztBQUVELGdCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLG9CQUFVLE9BQU8sQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO0FBQ2hGLG1CQUFPLENBQUMsR0FBRyxHQUFNLE9BQU8sQ0FBQyxHQUFHLFlBQU8sRUFBRSxBQUFFLENBQUM7QUFDeEMsbUJBQU8sSUFBSSxDQUNOLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FDWixJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDWix1QkFBSyxLQUFLLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0FBQ3JCLHVCQUFLLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLFFBQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztBQUNyRCx1QkFBTyxJQUFJLENBQUM7YUFDZixDQUFDLENBQUM7U0FDVjs7OzRCQUVHLElBQUksRUFBRTs7O0FBQ04sZ0JBQUksT0FBTyxHQUFHO0FBQ1Ysb0JBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztBQUMxQixzQkFBTSxFQUFFLElBQUksQ0FBQyxFQUFFLEdBQUcsb0JBQVUsT0FBTyxDQUFDLEdBQUcsR0FBRyxvQkFBVSxPQUFPLENBQUMsSUFBSTthQUNuRSxDQUFDOztBQUVGLG1CQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQzs7QUFFbEQsZ0JBQUksT0FBTyxDQUFDLE1BQU0sS0FBSyxvQkFBVSxPQUFPLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLFFBQU0sT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsRUFBRSxBQUFFLENBQUM7O0FBRXZGLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNyQix1QkFBSyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUMsTUFBTSxRQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7QUFDN0MsdUJBQU8sSUFBSSxDQUFDO2FBQ2YsQ0FBQyxDQUFDO1NBQ1Y7OzsrQkFFTSxFQUFFLEVBQUU7OztBQUNQLGdCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUMsTUFBTSxFQUFFLG9CQUFVLE9BQU8sQ0FBQyxNQUFNLEVBQUMsQ0FBQyxDQUFDO0FBQ2pGLG1CQUFPLENBQUMsR0FBRyxRQUFNLE9BQU8sQ0FBQyxHQUFHLEdBQUcsRUFBRSxBQUFFLENBQUM7O0FBRXBDLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7QUFDM0IsdUJBQUssSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0FBQ2hELHVCQUFPLElBQUksQ0FBQzthQUNmLENBQUMsQ0FBQztTQUNWOzs7NEJBNUVXO0FBQ1IsZ0JBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLE1BQU0sR0FBRyxxQkFBVyxDQUFDO0FBQ3pELG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDdEI7Ozs0QkFFWTtBQUNULGdCQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQVUsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQzlFLG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7U0FDdkI7OzswQkFzRWlCLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7V0FoRm5DLEtBQUs7OztrQkFBTCxLQUFLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNETCxXQUFXO2NBQVgsV0FBVzs7YUFBWCxXQUFXOzhCQUFYLFdBQVc7O3NFQUFYLFdBQVc7OztpQkFBWCxXQUFXOzs0QkFPeEIsSUFBSSxFQUFFOzs7QUFDTixnQkFBSSxPQUFPLEdBQUc7QUFDVixvQkFBSSxFQUFJLElBQUksQ0FBQyxRQUFRO0FBQ3JCLHNCQUFNLEVBQUUsb0JBQVUsT0FBTyxDQUFDLElBQUk7QUFDOUIsdUJBQU8sRUFBRSxFQUNSO2FBQ0osQ0FBQTs7QUFFRCxtQkFBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7QUFDakQsbUJBQU8sQ0FBQyxHQUFHLFFBQU0sT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQUFBRSxDQUFBOztBQUVyRCxtQkFBTyxJQUFJLENBQ04sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUNaLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLHVCQUFLLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBQyxNQUFNLFFBQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLENBQUE7QUFDakQsdUJBQU8sSUFBSSxDQUFBO2FBQ2QsQ0FBQyxDQUFDO1NBQ1Y7OztxQ0FFWSxTQUFTLEVBQUU7OztBQUNwQixnQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLE1BQU0sRUFBRSxvQkFBVSxPQUFPLENBQUMsR0FBRyxFQUFDLENBQUMsQ0FBQTtBQUM3RSxtQkFBTyxDQUFDLEdBQUcsUUFBTSxPQUFPLENBQUMsR0FBRyxHQUFHLFNBQVMsQUFBRSxDQUFBOztBQUUxQyxtQkFBTyxJQUFJLENBQ04sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUNaLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLHVCQUFLLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBQyxNQUFNLFFBQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtBQUNyRCx1QkFBTyxJQUFJLENBQUE7YUFDZCxDQUFDLENBQUM7U0FDVjs7OzhCQUVLO0FBQ0Ysa0JBQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtTQUMvQzs7OzRCQXRDWTtBQUNULGdCQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQVUsU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO0FBQ3BGLG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7U0FDdkI7OztXQUxnQixXQUFXOzs7a0JBQVgsV0FBVzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNEWCxVQUFVO2NBQVYsVUFBVTs7QUFFM0IsYUFGaUIsVUFBVSxDQUVmLFNBQVMsRUFBRTs4QkFGTixVQUFVOztzRUFBVixVQUFVLGFBR2pCLFNBQVM7S0FDbEI7O2lCQUpnQixVQUFVOzs2QkFNZixXQUFXLEVBQUU7QUFDckIsZ0JBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7QUFDekQsbUJBQU8sSUFBSSxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDeEM7OztXQVRnQixVQUFVOzs7a0JBQVYsVUFBVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNJVixLQUFLO2NBQUwsS0FBSzs7QUFFdEIsYUFGaUIsS0FBSyxDQUVWLFNBQVMsRUFBRTs4QkFGTixLQUFLOzsyRUFBTCxLQUFLOztBQUlsQixjQUFLLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDMUIsY0FBSyxLQUFLLEdBQUcscUJBQVcsQ0FBQTs7QUFFeEIsY0FBSyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQUssSUFBSSxDQUFDLE1BQUssU0FBUyxDQUFDLENBQUMsQ0FBQTs7QUFFM0MsY0FBSyxRQUFRLEdBQUcsTUFBSyxNQUFNLENBQUMsSUFBSSxPQUFNLENBQUE7QUFDdEMsY0FBSyxRQUFRLEdBQUcsTUFBSyxZQUFZLENBQUMsSUFBSSxPQUFNLENBQUE7O0tBQy9DOztpQkFYZ0IsS0FBSzs7dUNBa0JQOzs7QUFDWCxnQkFBSSxPQUFPLEdBQUcsa0JBQVEsUUFBUSxDQUFBO0FBQzlCLG1CQUFPLENBQUMsTUFBTSxFQUFFLENBQUE7O0FBRWhCLGdCQUFJLENBQUMsS0FBSyxDQUNMLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FDekMsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osb0JBQUksU0FBUyxHQUFHLE9BQUssR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUE7QUFDNUMseUJBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDeEIsdUJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTthQUNwQixDQUFDLENBQ0QsS0FBSyxDQUFDLFVBQUMsTUFBTTt1QkFBSyxPQUFPLENBQUMsT0FBTyxDQUFDLGtEQUFrRCxDQUFDO2FBQUEsQ0FBQyxDQUFBO1NBQzlGOzs7K0JBRU0sQ0FBQyxFQUFFOzs7QUFDTixnQkFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtBQUNuQixnQkFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLGdDQUFnQyxDQUFDLEVBQUUsT0FBTTs7QUFFaEUsZ0JBQUksSUFBSSxHQUFNLENBQUMsQ0FBQyxJQUFJLENBQUE7QUFDcEIsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTs7QUFFOUIsZ0JBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTtBQUNsQixtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVoQixnQkFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUE7QUFDN0QsZ0JBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixLQUFLLElBQUksR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFBOztBQUU5RixnQkFBSSxDQUFDLEtBQUssQ0FDTCxHQUFHLENBQUMsSUFBSSxDQUFDLENBQ1QsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUE7O0FBRTVCLG9CQUFJLElBQUksR0FBRyxPQUFLLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUMzQixvQkFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRXZCLHVCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7YUFDcEIsQ0FBQyxDQUNELEtBQUssQ0FBQyxVQUFDLE1BQU0sRUFBSztBQUNmLG9CQUFJLE1BQU0sWUFBWSxLQUFLLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUEsS0FDdkQ7QUFDRCx3QkFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUN0QiwyQkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2lCQUNwQjthQUNKLENBQUMsQ0FBQTtTQUNUOzs7cUNBRVksQ0FBQyxFQUFFO0FBQ1osZ0JBQUksTUFBTSxHQUFNLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDeEIsZ0JBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtBQUM1QyxnQkFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxFQUFFLE9BQU07O0FBRXZELGdCQUFJLE9BQU8sR0FBRyxrQkFBUSxRQUFRLENBQUE7QUFDOUIsbUJBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQTs7QUFFaEIsZ0JBQUksQ0FBQyxLQUFLLENBQ0wsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUNqQyxJQUFJLENBQUMsWUFBTTtBQUNSLHlCQUFTLENBQUMsU0FBUyxFQUFFLENBQUE7QUFDckIsdUJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTthQUNwQixDQUFDLENBQ0QsS0FBSyxDQUFDLFVBQUMsS0FBSzt1QkFBSyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7YUFBQSxDQUFDLENBQUE7U0FDeEQ7OzswQkFFWSxFQUFFLEVBQUU7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzBCQUM3QixFQUFFLEVBQUU7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzBCQUM5QixFQUFFLEVBQUc7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRzs7OzZCQXRFOUIsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hELG1CQUFPLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQ2xDOzs7V0FoQmdCLEtBQUs7OztrQkFBTCxLQUFLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDSkwsU0FBUztBQUUxQixhQUZpQixTQUFTLENBRWQsU0FBUyxFQUFFOzhCQUZOLFNBQVM7O0FBR3RCLFlBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO0tBQzdCOztpQkFKZ0IsU0FBUzs7aUNBZ0JqQixJQUFJLEVBQUU7QUFDWCxnQkFBSSxTQUFTLEdBQUksSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ2pELGdCQUFJLFVBQVUsR0FBRyxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFBOztBQUUvQixnQkFBSSxVQUFVLEdBQUcsRUFBRSxDQUFBO0FBQ25CLHNCQUFVLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7QUFDekMsc0JBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtBQUN2QyxzQkFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO0FBQ25GLHNCQUFVLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7O0FBRW5GLGdCQUFJLElBQUksR0FBRyxFQUFFLENBQUE7QUFDYixpQkFBSSxJQUFJLEdBQUcsSUFBSSxVQUFVO0FBQUUsb0JBQUksVUFBVSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUMzRCx3QkFBSSxDQUFDLElBQUksY0FBWSxHQUFHLGlCQUFZLFVBQVUsQ0FBQyxHQUFHLENBQUMsZ0JBQWEsQ0FBQTtpQkFDbkU7YUFBQSxBQUNELElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUE7QUFDeEMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztnQ0FFTyxHQUFHLEVBQXdCO2dCQUF0QixHQUFHLHlEQUFHLEVBQUU7Z0JBQUUsS0FBSyx5REFBRyxFQUFFOztBQUM3QixnQkFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUEsS0FDcEI7QUFDRCxvQkFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUssR0FBRyxDQUFBO0FBQ3BCLG9CQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBSyxHQUFHLENBQUE7QUFDcEIsb0JBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQTtBQUN0QixvQkFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2FBQ3RDO0FBQ0QsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzttQ0FFVTtBQUNQLGdCQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUE7QUFDakQsZ0JBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNoQyxtQkFBTyxJQUFJLENBQUE7U0FDZDs7OzRCQXRDUztBQUNOLGdCQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsQ0FBQTtBQUNwSCxtQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFBO1NBQ25COzs7NkJBUlcsYUFBYSxFQUFFO0FBQ3ZCLGdCQUFJLGFBQWEsR0FBRyxhQUFhLENBQUMsYUFBYSxDQUFDLDJCQUEyQixDQUFDLENBQUE7QUFDNUUsbUJBQU8sSUFBSSxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDdEM7OztXQVRnQixTQUFTOzs7a0JBQVQsU0FBUzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0VULGNBQWM7Y0FBZCxjQUFjOztBQUUvQixhQUZpQixjQUFjLENBRW5CLFNBQVMsRUFBRTs4QkFGTixjQUFjOzsyRUFBZCxjQUFjLGFBR3JCLFNBQVM7O0FBRWYsY0FBSyxPQUFPLENBQ1AsR0FBRyxDQUFDLGVBQUssSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FDbkMsR0FBRyxDQUFDLFlBQVksRUFBRSxxQkFBVyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFBOztBQUU5RCxjQUFLLE9BQU8sR0FBRyxNQUFLLEtBQUssQ0FBQyxJQUFJLE9BQU0sQ0FBQTs7S0FDdkM7O2lCQVZnQixjQUFjOzs4QkEyQnpCLENBQUMsRUFBRTtBQUNMLGdCQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ3JCLGdCQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxPQUFNOztBQUVoQyxvQkFBUSxNQUFNLENBQUMsU0FBUztBQUNwQixxQkFBSyxJQUFJLENBQUMsT0FBTztBQUFLLHdCQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEFBQUMsTUFBSztBQUFBLEFBQ2xELHFCQUFLLElBQUksQ0FBQyxTQUFTO0FBQUcsd0JBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsYUFDdkQ7U0FDSjs7O3NDQUVhLENBQUMsRUFBRSxFQUVoQjs7O3dDQUVlLENBQUMsRUFBRTtBQUNmLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7QUFDbEMsZ0JBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7U0FDcEQ7OztxQ0FFWSxDQUFDLEVBQUU7QUFDWixnQkFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBO1NBQ25EOzs7NEJBL0JhO0FBQ1YsZ0JBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQywrQkFBK0IsQ0FBQyxDQUFBO0FBQzlHLG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7U0FDdkI7Ozs0QkFFZTtBQUNaLGdCQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsa0NBQWtDLENBQUMsQ0FBQTtBQUN2SCxtQkFBTyxJQUFJLENBQUMsV0FBVyxDQUFBO1NBQzFCOzs7NkJBYlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hELG1CQUFPLElBQUksY0FBYyxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQzNDOzs7V0FmZ0IsY0FBYzs7O2tCQUFkLGNBQWM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0ZuQyxJQUFNLFVBQVUsR0FBSSxLQUFLLENBQUE7QUFDekIsSUFBTSxXQUFXLEdBQUcsTUFBTSxDQUFBOztJQUVMLElBQUk7Y0FBSixJQUFJOztBQUVyQixhQUZpQixJQUFJLENBRVQsU0FBUyxFQUFFOzhCQUZOLElBQUk7O3NFQUFKLElBQUksYUFHWCxTQUFTO0tBQ2xCOztpQkFKZ0IsSUFBSTs7Z0NBZ0JiO0FBQUUsbUJBQU8sSUFBSSxDQUFDLE1BQU0sS0FBSyxVQUFVLENBQUE7U0FBRTs7O2lDQUVwQztBQUFFLG1CQUFPLElBQUksQ0FBQyxNQUFNLEtBQUssV0FBVyxDQUFBO1NBQUU7Ozs0QkFQbkM7QUFDUixnQkFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUE7QUFDbEYsbUJBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQTtTQUNyQjs7OzBCQWFRLElBQUksRUFBRTtBQUNYLGdCQUFJLFlBQVksR0FBRyxFQUFFLENBQUE7O0FBRXJCLGdCQUFJLElBQUksQ0FBQyxFQUFFLEVBQVMsWUFBWSxDQUFDLEVBQUUsR0FBVSxJQUFJLENBQUMsRUFBRSxDQUFBO0FBQ3BELGdCQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO0FBQzNELGdCQUFJLElBQUksQ0FBQyxLQUFLLEVBQU0sWUFBWSxDQUFDLEtBQUssR0FBTyxJQUFJLENBQUMsS0FBSyxDQUFBOztBQUV2RCx1Q0FsQ2EsSUFBSSxxQkFrQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsUUFBQTtTQUNoRDs0QkFFVTtBQUFFLDhDQXJDSSxJQUFJLDJCQXFDUztTQUFFOzs7NkJBL0JwQixXQUFXLEVBQUU7QUFDckIsZ0JBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEQsbUJBQU8sSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDakM7OztzQ0FXb0IsSUFBSSxFQUFFO0FBQ3ZCLGlCQUFLLElBQUksR0FBRyxJQUFJLElBQUk7QUFBRSxvQkFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxFQUFFO0FBQ2hELHdCQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxJQUFJLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQTtpQkFDMUM7YUFBQSxBQUNELE9BQU8sSUFBSSxDQUFBO1NBQ2Q7OztXQXpCZ0IsSUFBSTs7O2tCQUFKLElBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNISixVQUFVO2NBQVYsVUFBVTs7QUFFM0IsYUFGaUIsVUFBVSxDQUVmLFNBQVMsRUFBRTs4QkFGTixVQUFVOzsyRUFBVixVQUFVLGFBR2pCLFNBQVM7O0FBRWYsY0FBSyxlQUFlLENBQUMsT0FBTyxHQUFHLE1BQUssY0FBYyxDQUFDLElBQUksT0FBTSxDQUFBOztLQUNoRTs7aUJBTmdCLFVBQVU7O2dDQXlDbkIsR0FBRyxFQUF3QjtnQkFBdEIsR0FBRyx5REFBRyxFQUFFO2dCQUFFLEtBQUsseURBQUcsRUFBRTs7QUFDN0IsZ0JBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBLEtBQ3BCO0FBQ0Qsb0JBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFLLEdBQUcsQ0FBQTtBQUNwQixvQkFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUssR0FBRyxDQUFBO0FBQ3BCLG9CQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUE7QUFDdEIsb0JBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNuQyxvQkFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2FBQ3BEO0FBQ0QsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzttQ0FFVTtBQUNQLGdCQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUE7QUFDakQsZ0JBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNoQyxnQkFBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBO0FBQzlDLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7eUNBRWdCO2dCQUNSLFVBQVUsR0FBSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBNUIsVUFBVTs7QUFFZixnQkFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7QUFDaEIsc0JBQU0sRUFBRSxJQUFJO0FBQ1osb0JBQUksRUFBRSxFQUFDLFVBQVUsRUFBVixVQUFVLEVBQUM7YUFDckIsQ0FBQyxDQUFBO1NBQ0w7OzswQkF0RFEsSUFBSSxFQUFFO0FBQ1gsZ0JBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTs7QUFFWixnQkFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQSxLQUNwQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7O0FBRXBCLGdCQUFJLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUE7U0FDckU7NEJBRVU7QUFDUCxnQkFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUUzQyxtQkFBTztBQUNILG9CQUFJLDZCQTFCSyxVQUFVLDBCQTBCSDtBQUNoQix3QkFBUSxFQUFFLFFBQVE7YUFDckIsQ0FBQTtTQUNKOzs7NEJBRVM7QUFDTixnQkFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQzVFLG1CQUFPLElBQUksQ0FBQyxJQUFJLENBQUE7U0FDbkI7Ozs0QkFFcUI7QUFDbEIsZ0JBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO0FBQzdHLG1CQUFPLElBQUksQ0FBQyxXQUFXLENBQUE7U0FDMUI7OzswQkE4QlksRUFBRSxFQUFFO0FBQ2IsZ0JBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQ3hCOzs7NkJBL0RXLFdBQVcsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4RCxtQkFBTyxJQUFJLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUN2Qzs7O1dBWGdCLFVBQVU7OztrQkFBVixVQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0NWLEtBQUs7Y0FBTCxLQUFLOztBQUV0QixhQUZpQixLQUFLLEdBRVI7OEJBRkcsS0FBSzs7MkVBQUwsS0FBSzs7QUFLbEIsY0FBSyxLQUFLLEdBQU0sVUFBQyxDQUFDLEVBQUs7QUFBRSxrQkFBSyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQTtTQUFFLENBQUM7QUFDN0QsY0FBSyxRQUFRLEdBQUcsVUFBQyxDQUFDLEVBQUs7QUFBRSxnQkFBSSxNQUFLLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRSxNQUFLLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQTtTQUFFLENBQUM7O0tBQzFGOztpQkFQZ0IsS0FBSzs7NEJBbUJsQixFQUFFLEVBQUU7QUFDSixjQUFFLEdBQUcsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2xCLGdCQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxFQUFFLE9BQU8sT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ25FLDhDQXRCYSxLQUFLLHFDQXNCRCxFQUFFLEVBQUU7U0FDeEI7OztxQ0FFWSxTQUFTLEVBQUU7OztBQUNwQixnQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLE1BQU0sRUFBRSxvQkFBVSxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztBQUNoRixtQkFBTyxDQUFDLEdBQUcsR0FBTSxPQUFPLENBQUMsR0FBRyxZQUFPLFNBQVMsQUFBRSxDQUFDO0FBQy9DLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1oscUJBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ2pDLDJCQUFLLEtBQUssQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdkM7QUFDRCx1QkFBSyxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxRQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7QUFDdEQsdUJBQU8sSUFBSSxDQUFDO2FBQ2YsQ0FBQyxDQUFDO1NBQ1Y7Ozs0QkE1Qlc7QUFDUixnQkFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7QUFDdkQsbUJBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztTQUN0Qjs7OzRCQUVZO0FBQ1QsZ0JBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBVSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7QUFDNUUsbUJBQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUN2Qjs7OzBCQXNCa0IsRUFBRSxFQUFFO0FBQUUsZ0JBQUksQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQUU7OztXQXZDckMsS0FBSzs7O2tCQUFMLEtBQUs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNHTCxJQUFJO2NBQUosSUFBSTs7QUFFckIsYUFGaUIsSUFBSSxDQUVULFNBQVMsRUFBRTs4QkFGTixJQUFJOzsyRUFBSixJQUFJLGFBR1gsU0FBUzs7QUFFZixjQUFLLE9BQU8sQ0FDUCxHQUFHLENBQUMsY0FBYyxFQUFFLHVCQUFhLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQy9ELEdBQUcsQ0FBQyxXQUFXLEVBQUUsb0JBQVUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQzlDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsb0JBQVUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUE7O0FBRW5ELGNBQUssT0FBTyxHQUFHLE1BQUssS0FBSyxDQUFDLElBQUksT0FBTSxDQUFBOztLQUN2Qzs7aUJBWGdCLElBQUk7OzhCQTBDZixDQUFDLEVBQUU7QUFDTCxnQkFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU07O0FBRWxDLG9CQUFRLElBQUksQ0FBQyxTQUFTO0FBQ2xCLHFCQUFLLElBQUksQ0FBQyxPQUFPO0FBQUUsd0JBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsQUFDL0MscUJBQUssSUFBSSxDQUFDLE1BQU07QUFBRyx3QkFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxBQUM5QyxxQkFBSyxJQUFJLENBQUMsT0FBTztBQUFFLHdCQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEFBQUMsTUFBSztBQUFBLEFBQy9DLHFCQUFLLElBQUksQ0FBQyxPQUFPO0FBQUUsd0JBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLOztBQUFBLGFBRWxEO1NBQ0o7OztzQ0FFYSxDQUFDLEVBQUU7QUFDYixnQkFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO0FBQ3RCLGdCQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRSxDQUFBO0FBQzFCLGdCQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFBO1NBQ25DOzs7c0NBRWEsQ0FBQyxFQUFFO0FBQ2IsZ0JBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDL0MsZ0JBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7O0FBRWxELGdCQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQTtTQUNoRTs7O3FDQUVZLENBQUMsRUFBRTtBQUNaLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFBOztBQUVuQyxnQkFBSSxDQUFDLGVBQWUsRUFBRSxDQUFBO0FBQ3RCLGdCQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU0sYUF6RTNCLFVBQVUsQUF5RThCLENBQUE7QUFDeEMsZ0JBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFBO1NBQzdDOzs7c0NBRWEsQ0FBQyxFQUFFO0FBQ2IsZ0JBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUE7O0FBRW5DLGdCQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQy9DLGdCQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBOztBQUVsRCxnQkFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNLGFBbkZmLFdBQVcsQUFtRmtCLENBQUE7QUFDekMsZ0JBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtTQUN2Qjs7OzJDQUVrQixDQUFDLEVBQUU7QUFDbEIsZ0JBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFBO1NBQzVCOzs7MENBRWlCO0FBQ2QsZ0JBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDL0MsZ0JBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDL0MsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7Ozs0QkE1RWE7QUFDVixnQkFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLDZCQUE2QixDQUFDLENBQUE7QUFDL0YsbUJBQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQTtTQUN2Qjs7OzRCQUVhO0FBQ1YsZ0JBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyw2QkFBNkIsQ0FBQyxDQUFBO0FBQy9GLG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7U0FDdkI7Ozs0QkFFWTtBQUNULGdCQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsNEJBQTRCLENBQUMsQ0FBQTtBQUM1RixtQkFBTyxJQUFJLENBQUMsT0FBTyxDQUFBO1NBQ3RCOzs7NEJBRWE7QUFDVixnQkFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLDZCQUE2QixDQUFDLENBQUE7QUFDL0YsbUJBQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQTtTQUN2Qjs7OzRCQUVzQjtBQUNuQixnQkFBSSxJQUFJLENBQUMsaUJBQWlCLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQywrQkFBK0IsQ0FBQyxDQUFBO0FBQ25ILG1CQUFPLElBQUksQ0FBQyxpQkFBaUIsQ0FBQTtTQUNoQzs7OzZCQTNCVyxTQUFTLEVBQUU7QUFDbkIsbUJBQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7U0FDN0I7OztXQWZnQixJQUFJOzs7a0JBQUosSUFBSTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDREosWUFBWTtjQUFaLFlBQVk7O0FBRTdCLGFBRmlCLFlBQVksQ0FFakIsU0FBUyxFQUFFOzhCQUZOLFlBQVk7OzJFQUFaLFlBQVksYUFHbkIsU0FBUzs7QUFFZixjQUFLLE9BQU8sQ0FDUCxHQUFHLENBQUMsVUFWVCxRQUFRLENBVVUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQ2hDLEdBQUcsQ0FBQyx3QkFBYyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUM5QyxHQUFHLENBQUMsdUJBQWEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUE7O0FBRTVDLGNBQUssT0FBTyxHQUFHLE1BQUssS0FBSyxDQUFDLElBQUksT0FBTSxDQUFBOztLQUN2Qzs7aUJBWGdCLFlBQVk7OzhCQXFDdkIsQ0FBQyxFQUFFO0FBQ0wsZ0JBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxPQUFNOztBQUVsQyxvQkFBUSxJQUFJLENBQUMsU0FBUztBQUNsQixxQkFBSyxJQUFJLENBQUMsT0FBTztBQUFVLHdCQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEFBQUMsTUFBSztBQUFBLEFBQ3ZELHFCQUFLLElBQUksQ0FBQyxlQUFlO0FBQUUsd0JBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxBQUMvRCxxQkFBSyxJQUFJLENBQUMsVUFBVTtBQUFPLHdCQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsYUFDN0Q7U0FDSjs7O3NDQUVhLENBQUMsRUFBRTtBQUNiLGdCQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQTtBQUMzQywyQkFBZSxDQUFDLFVBQVUsQ0FBQyxhQUFhLE9BQUssZUFBZSxDQUFDLEVBQUUsZ0JBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQ3ZHLGdCQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBOztBQUVyQixnQkFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FDZixJQUFJLEdBQUc7QUFDSiwwQkFBVSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7YUFDMUMsQ0FBQTtTQUNSOzs7OENBRXFCLENBQUMsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQTtBQUM3QyxnQkFBSSxZQUFZLEdBQUksYUFBYSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtBQUNyRCxnQkFBSSxJQUFJLEdBQVksWUFBWSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFNUMseUJBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQTtBQUM1Qix3QkFBWSxDQUFDLFFBQVEsRUFBRSxDQUFBO0FBQ3ZCLGdCQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDdEI7Ozt5Q0FFZ0IsQ0FBQyxFQUFFO0FBQ2hCLGdCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFBOztBQUV0QyxtQkFBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FDcEIsSUFBSSxHQUFHO0FBQ0osd0JBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFO2FBQ3RDLENBQUE7O0FBRUwsbUJBQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtBQUN4QyxtQkFBTyxDQUFDLGNBQWMsRUFBRSxDQUFBO1NBRTNCOzs7NEJBN0RhO0FBQ1YsZ0JBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFBO0FBQzVHLG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7U0FDdkI7Ozs0QkFFcUI7QUFDbEIsZ0JBQUksSUFBSSxDQUFDLGdCQUFnQixLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsc0NBQXNDLENBQUMsQ0FBQTtBQUNySSxtQkFBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7U0FDL0I7Ozs0QkFFZ0I7QUFDYixnQkFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLGdDQUFnQyxDQUFDLENBQUE7QUFDckgsbUJBQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQTtTQUMxQjs7OzRCQUVVO0FBQ1AsbUJBQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFBO1NBQ2pDOzs7NkJBdEJXLFdBQVcsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUN4RCxtQkFBTyxJQUFJLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUN6Qzs7O1dBaEJnQixZQUFZOzs7a0JBQVosWUFBWTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0NqQyxJQUFNLFdBQVcsR0FBRyxNQUFNLENBQUE7O0lBRUwsU0FBUztjQUFULFNBQVM7O0FBRTFCLGFBRmlCLFNBQVMsQ0FFZCxTQUFTLEVBQUU7OEJBRk4sU0FBUzs7MkVBQVQsU0FBUzs7QUFJdEIsY0FBSyxTQUFTLEdBQUcsU0FBUyxDQUFBO0FBQzFCLGNBQUssSUFBSSxHQUFRLE1BQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTtBQUNoRCxjQUFLLFFBQVEsR0FBSSxJQUFJLENBQUE7O0FBRXJCLGNBQUssT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBVyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQTs7QUFFakQsY0FBSyxRQUFRLEdBQU8sTUFBSyxjQUFjLENBQUMsSUFBSSxPQUFNLENBQUE7QUFDbEQsY0FBSyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQUssS0FBSyxDQUFDLElBQUksT0FBTSxDQUFBOztLQUM1Qzs7aUJBWmdCLFNBQVM7OzhCQXNCcEIsQ0FBQyxFQUFFOzs7QUFDTCxhQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7QUFDbEIsZ0JBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUE7O0FBRXJCLGdCQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ25DLGdCQUFJLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQTs7QUFFdEMsZ0JBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUE7QUFDdEUsZ0JBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU07O0FBRXhDLGdCQUFJLFNBQVMsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDL0QsZ0JBQUksRUFBRSxHQUFVLFNBQVMsQ0FBQyxHQUFHLEVBQUUsQ0FBQTtBQUMvQixnQkFBSSxNQUFNLEdBQU0sU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFBO0FBQy9CLGdCQUFJLE9BQU8sR0FBSyxrQkFBUSxRQUFRLENBQUE7O0FBRWhDLGdCQUFJLFlBQVksR0FBRyxTQUFmLFlBQVksQ0FBSSxNQUFNLEVBQUs7QUFDM0Isd0JBQVEsTUFBTTtBQUNWLG1DQTlDUixXQUFXO0FBOENpQiwrQkFBSyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsQUFDakQsbUNBL0NLLGFBQWE7QUErQ0UsK0JBQUssZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsQUFDbkQseUJBQUssV0FBVztBQUFJLCtCQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxpQkFDakQ7YUFDSixDQUFBOztBQUVELGdCQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFBOztBQUUvRSxnQkFBSSxlQUFlLEVBQUU7QUFDakIsNEJBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQTthQUN2QixNQUFNO0FBQ0gsdUJBQU8sQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUE7QUFDckMsb0JBQUksQ0FBQyxLQUFLLENBQ0wsR0FBRyxDQUFDLEVBQUUsQ0FBQyxDQUNQLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLDJCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFBO0FBQzVCLDJCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7QUFDakIsZ0NBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQTtpQkFDdkIsQ0FBQyxDQUFBO2FBQ1Q7U0FDSjs7O3VDQUVjLENBQUMsRUFBRTtBQUNkLGdCQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO0FBQzVCLGdCQUFJLE9BQU8sR0FBTSxrQkFBUSxRQUFRLENBQUE7QUFDakMsZ0JBQUksSUFBSSxHQUFTLFVBQVUsQ0FBQyxHQUFHLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7QUFFMUQsZ0JBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRWxDLG1CQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVyQyxzQkFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRW5DLGdCQUFJLENBQUMsS0FBSyxDQUNMLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FDOUIsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osb0JBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFBO0FBQ2hCLHVCQUFPLENBQUMsT0FBTyxFQUFFLENBQUE7YUFDcEIsQ0FBQyxDQUFBO1NBQ1Q7Ozt5Q0FFZ0IsQ0FBQyxFQUFFO0FBQ2hCLGdCQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBQ3ZDLGdCQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7QUFDZixzQkFBVSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksRUFBRSxDQUFBO1NBQzVCOzs7dUNBRWMsQ0FBQyxFQUFFO0FBQ2QsZ0JBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLEVBQUUsT0FBTTtBQUNoRCxnQkFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO1NBQ2xCOzs7b0NBRVcsQ0FBQyxFQUFFO0FBQ1gsZ0JBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUE7QUFDeEIsa0JBQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQzlCOzs7aUNBRVEsSUFBSSxFQUFFO0FBQ1gsZ0JBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUE7O0FBRTdCLGdCQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUE7O0FBRXhCLGlCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNsQyxvQkFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUN2QjtBQUNELG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7K0JBRU0sSUFBSSxFQUFFO0FBQ1QsZ0JBQUksSUFBSSxHQUFHLGNBQUMsS0FBSTt1QkFBSyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUMsVUFBVTthQUFBLENBQUE7QUFDdkcsZ0JBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUE7O0FBRS9CLGVBQUcsQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUE7QUFDeEIsZUFBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7O0FBRW5DLGdCQUFJLEtBQUssR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ3hDLGVBQUcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7QUFDdEIsaUJBQUssQ0FBQyxTQUFTLHVCQUFxQixJQUFJLENBQUMsRUFBRSxVQUFLLElBQUksQ0FBQyxLQUFLLFNBQU0sQ0FBQTs7QUFFaEUsZUFBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUE7QUFDakMsZUFBRyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUE7O0FBR3BDLGdCQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQzFDLGVBQUcsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUE7O0FBRXhCLG1CQUFPLENBQUMsU0FBUyx1QkFDSyxJQUFJLENBQUMsRUFBRSwrRUFBMEUsaUJBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsMkNBQ3RHLElBQUksQ0FBQyxFQUFFLHNFQUFpRSxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFNLENBQUE7U0FDaEk7OztvQ0FFVztBQUNSLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUN0RCxnQkFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUE7QUFDcEIsZ0JBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFBO0FBQ3pCLGdCQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7U0FDbEI7Ozs2Q0FFb0I7QUFDakIsZ0JBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFBO0FBQ3pCLGlCQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUNoQyxvQkFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQTthQUNsRDtTQUNKOzs7bUNBRVU7QUFDUCxnQkFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUM1QyxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O21DQUVVO0FBQ1AsZ0JBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDL0MsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7Ozs0QkFySVc7QUFBRSxtQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUE7U0FBRTs7OzRCQUNsQztBQUFFLG1CQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQTtTQUFFOzs7MEJBc0lsQyxFQUFFLEVBQUU7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzZCQTVJOUIsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hELG1CQUFPLElBQUksU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQ3RDOzs7V0FqQmdCLFNBQVM7OztrQkFBVCxTQUFTOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDTlQsVUFBVTtjQUFWLFVBQVU7O0FBRTNCLGFBRmlCLFVBQVUsQ0FFZixTQUFTLEVBQUU7OEJBRk4sVUFBVTs7MkVBQVYsVUFBVSxhQUdqQixTQUFTOztBQUNmLGNBQUssU0FBUyxHQUFHLE1BQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOztBQUVwRSxjQUFLLFNBQVMsQ0FBQyxPQUFPLEdBQUcsTUFBSyxnQkFBZ0IsQ0FBQyxJQUFJLE9BQU0sQ0FBQzs7S0FDN0Q7O2lCQVBnQixVQUFVOzt5Q0FjVixDQUFDLEVBQUU7QUFDaEIsYUFBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO0FBQ25CLGdCQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7QUFDWixnQkFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQ2IsZ0JBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7U0FDdkM7OztnQ0FFTyxDQUFDLEVBQUU7QUFDUCx1Q0F0QmEsVUFBVSx5Q0FzQlQsQ0FBQyxFQUFFO0FBQ2pCLGdCQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZjs7OzZCQWZXLFdBQVcsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN6RCxtQkFBTyxJQUFJLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUN4Qzs7O1dBWmdCLFVBQVU7OztrQkFBVixVQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNDVixTQUFTO2NBQVQsU0FBUzs7QUFFMUIsYUFGaUIsU0FBUyxDQUVkLFNBQVMsRUFBRTs4QkFGTixTQUFTOzsyRUFBVCxTQUFTOztBQUl0QixjQUFLLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDMUIsY0FBSyxTQUFTLEdBQUcsTUFBSyxHQUFHLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBQy9DLGNBQUssT0FBTyxHQUFLLE1BQUssR0FBRyxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBOztBQUV4RCxjQUFLLE9BQU8sQ0FBQyxPQUFPLEdBQUksTUFBSyxNQUFNLENBQUMsSUFBSSxPQUFNLENBQUE7O0tBQ2pEOztpQkFUZ0IsU0FBUzs7aUNBZ0JqQixJQUFJLEVBQUU7QUFDWCxnQkFBSSxTQUFTLEdBQUksSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ2pELGdCQUFJLFVBQVUsR0FBRyxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFBOztBQUUvQixnQkFBSSxVQUFVLEdBQUcsRUFBRSxDQUFBO0FBQ25CLHNCQUFVLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUE7QUFDekMsc0JBQVUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQTtBQUN2QyxzQkFBVSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO0FBQ3ZDLHNCQUFVLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUE7QUFDckQsc0JBQVUsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUE7QUFDakUsc0JBQVUsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQTtBQUMvQyxzQkFBVSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFBO0FBQ3pDLHNCQUFVLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUE7QUFDL0Msc0JBQVUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQTtBQUNuRixzQkFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFBO0FBQ25GLHNCQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxFQUFFLEdBQUcsVUFBVSxDQUFDLEdBQUcsQ0FBQTtBQUNqRixzQkFBVSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFBOztBQUV2RyxnQkFBSSxJQUFJLEdBQUcsRUFBRSxDQUFBO0FBQ2IsaUJBQUksSUFBSSxHQUFHLElBQUksVUFBVTtBQUFFLG9CQUFJLFVBQVUsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7QUFDM0Qsd0JBQUksQ0FBQyxJQUFJLGNBQVksR0FBRyxpQkFBWSxVQUFVLENBQUMsR0FBRyxDQUFDLGdCQUFhLENBQUE7aUJBQ25FO2FBQUEsQUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFBO0FBQ3hDLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7K0JBRU0sQ0FBQyxFQUFFO0FBQ04sYUFBQyxDQUFDLGNBQWMsRUFBRSxDQUFBO0FBQ2xCLGdCQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtTQUNoRDs7OzZCQWxDVyxXQUFXLEVBQUU7QUFDckIsZ0JBQUksYUFBYSxHQUFHLHFCQUFXLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUNoRCxtQkFBTyxJQUFJLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQTtTQUN0Qzs7O1dBZGdCLFNBQVM7OztrQkFBVCxTQUFTOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRHZCLElBQU0sVUFBVSxXQUFWLFVBQVUsR0FBTSxLQUFLLENBQUM7QUFDNUIsSUFBTSxXQUFXLFdBQVgsV0FBVyxHQUFLLE1BQU0sQ0FBQztBQUM3QixJQUFNLGFBQWEsV0FBYixhQUFhLEdBQUcsUUFBUSxDQUFDOztJQUV6QixRQUFRLFdBQVIsUUFBUTtjQUFSLFFBQVE7O0FBRWpCLGFBRlMsUUFBUSxDQUVMLFNBQVMsRUFBRTs4QkFGZCxRQUFROztzRUFBUixRQUFRLGFBR1AsU0FBUztLQUNsQjs7aUJBSlEsUUFBUTs7Z0NBV1Q7QUFBRSxtQkFBTyxJQUFJLENBQUMsTUFBTSxLQUFLLFVBQVUsQ0FBQTtTQUFFOzs7aUNBRXBDO0FBQUUsbUJBQU8sSUFBSSxDQUFDLE1BQU0sS0FBSyxXQUFXLENBQUE7U0FBRTs7OzBCQUV0QyxJQUFJLEVBQUU7QUFDWCxnQkFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDbEQsdUNBakJLLFFBQVEscUJBaUJBLElBQUksUUFBQTtTQUNwQjs0QkFDVTtBQUFFLDhDQW5CSixRQUFRLDJCQW1CYTtTQUFFOzs7NkJBYnBCLFdBQVcsRUFBRTtBQUNyQixnQkFBSSxhQUFhLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUN6RCxtQkFBTyxJQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUN0Qzs7O1dBVFEsUUFBUTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDREEsYUFBYTtjQUFiLGFBQWE7O0FBRTlCLGFBRmlCLGFBQWEsQ0FFbEIsU0FBUyxFQUFFOzhCQUZOLGFBQWE7OzJFQUFiLGFBQWE7O0FBSTFCLGNBQUssU0FBUyxHQUFHLFNBQVMsQ0FBQTtBQUMxQixjQUFLLEtBQUssR0FBTyxxQkFBVyxDQUFBOztBQUU1QixjQUFLLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQWEsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUE7O0FBRXBELGNBQUssUUFBUSxHQUFHLE1BQUssTUFBTSxDQUFDLElBQUksT0FBTSxDQUFBO0FBQ3RDLGNBQUssUUFBUSxHQUFHLE1BQUssWUFBWSxDQUFDLElBQUksT0FBTSxDQUFBOztLQUMvQzs7aUJBWGdCLGFBQWE7O3VDQWtCZjs7O0FBQ1gsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTtBQUM5QixtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVoQixnQkFBSSxDQUFDLEtBQUssQ0FDTCxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQ3JDLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLG9CQUFJLFlBQVksR0FBRyxPQUFLLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQTtBQUMzQyw0QkFBWSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUMzQix1QkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2FBQ3BCLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBQyxNQUFNO3VCQUFLLE9BQU8sQ0FBQyxPQUFPLENBQUMsa0RBQWtELENBQUM7YUFBQSxDQUFDLENBQUE7U0FDOUY7OzsrQkFFTSxDQUFDLEVBQUU7QUFDTixnQkFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtBQUNuQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQTs7QUFFbkMsZ0JBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsRUFBRSxPQUFNOztBQUV0QyxnQkFBSSxJQUFJLEdBQU0sQ0FBQyxDQUFDLElBQUksQ0FBQTtBQUNwQixnQkFBSSxPQUFPLEdBQUcsa0JBQVEsUUFBUSxDQUFBOztBQUU5QixtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBOztBQUVoQixnQkFBSSxDQUFDLEtBQUssQ0FDTCxHQUFHLENBQUMsSUFBSSxDQUFDLENBQ1QsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osb0JBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsQ0FBQTs7QUFFbkIsb0JBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUEsS0FDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTs7QUFFdkIsb0JBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTtBQUNqQix1QkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2FBQ3BCLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDZixvQkFBSSxNQUFNLFlBQVksS0FBSyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBLEtBQ3ZEO0FBQ0Qsd0JBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDdEIsMkJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtpQkFDcEI7YUFDSixDQUFDLENBQUE7U0FDVDs7O3FDQUVZLENBQUMsRUFBRTtBQUNaLGdCQUFJLE1BQU0sR0FBUyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQzNCLGdCQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFBO0FBQzNDLGdCQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLEVBQUUsT0FBTTs7QUFFMUQsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTtBQUM5QixnQkFBSSxFQUFFLEdBQVEsWUFBWSxDQUFDLFFBQVEsR0FBRyxZQUFZLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFBOztBQUUxRSxtQkFBTyxDQUFDLE1BQU0sRUFBRSxDQUFBO0FBQ2hCLGdCQUFJLENBQUMsRUFBRSxFQUFFOztBQUVMLHVCQUFNO2FBQ1Q7O0FBRUQsZ0JBQUksQ0FBQyxLQUFLLENBQ0wsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUNWLElBQUksQ0FBQyxZQUFNO0FBQ1IsNEJBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQTtBQUMzQix1QkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2FBQ3BCLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBQyxLQUFLO3VCQUFLLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQzthQUFBLENBQUMsQ0FBQTtTQUN4RDs7OzBCQUVZLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7MEJBQzdCLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7NkJBMUU5QixXQUFXLEVBQUU7QUFDckIsZ0JBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDeEQsbUJBQU8sSUFBSSxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUE7U0FDMUM7OztXQWhCZ0IsYUFBYTs7O2tCQUFiLGFBQWE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0NiLFlBQVk7Y0FBWixZQUFZOztBQUU3QixhQUZpQixZQUFZLENBRWpCLFNBQVMsRUFBRTs4QkFGTixZQUFZOzsyRUFBWixZQUFZOztBQUl6QixjQUFLLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDMUIsY0FBSyxLQUFLLEdBQU8scUJBQVcsQ0FBQTs7QUFFNUIsY0FBSyxPQUFPLENBQ1AsR0FBRyxDQUFDLGVBQUssSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FDcEMsR0FBRyxDQUFDLHFCQUFXLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUE7O0FBRS9DLGNBQUssUUFBUSxHQUFHLE1BQUssTUFBTSxDQUFDLElBQUksT0FBTSxDQUFBO0FBQ3RDLGNBQUssUUFBUSxHQUFHLE1BQUssTUFBTSxDQUFDLElBQUksT0FBTSxDQUFBOztLQUN6Qzs7aUJBYmdCLFlBQVk7O3lDQW1CWjtBQUNiLGdCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLENBQUE7QUFDdkMsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTtBQUM5QixnQkFBSSxJQUFJLEdBQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTs7QUFFOUIsbUJBQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQTs7QUFFaEIsZ0JBQUksQ0FBQyxLQUFLLENBQ0wsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUNuQixJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDWixvQkFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNuQix1QkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2FBQ3BCLENBQUMsQ0FBQTtTQUNUOzs7K0JBRU0sQ0FBQyxFQUFFO0FBQ04sZ0JBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUE7QUFDakMsZ0JBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxPQUFNOztBQUVsQyxnQkFBSSxJQUFJLEdBQU0sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUM5QixnQkFBSSxPQUFPLEdBQUcsa0JBQVEsUUFBUSxDQUFBO0FBQzlCLG1CQUFPLENBQUMsTUFBTSxFQUFFLENBQUE7O0FBRWhCLGdCQUFJLENBQUMsS0FBSyxDQUNMLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQ1gsSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osb0JBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDZCxvQkFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQTtBQUN0Qix1QkFBTyxDQUFDLE9BQU8sRUFBRSxDQUFBO2FBQ3BCLENBQUMsQ0FDRCxLQUFLLENBQUMsVUFBQyxNQUFNLEVBQUs7QUFDZixvQkFBSSxNQUFNLFlBQVksS0FBSyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFBLEtBQ3ZEO0FBQ0Qsd0JBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDdEIsMkJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTtpQkFDcEI7YUFDSixDQUFDLENBQUE7U0FDVDs7OytCQUVNLENBQUMsRUFBRTtBQUNOLGdCQUFJLElBQUksR0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQzdCLGdCQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ3JCLGdCQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxPQUFNOztBQUVoQyxnQkFBSSxPQUFPLEdBQUcsa0JBQVEsUUFBUSxDQUFBO0FBQzlCLG1CQUFPLENBQUMsTUFBTSxFQUFFLENBQUE7O0FBRWhCLGdCQUFJLENBQUMsS0FBSyxDQUNMLE1BQU0sQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQ2pCLElBQUksQ0FBQyxVQUFDLElBQUksRUFBSztBQUNaLG9CQUFJLENBQUMsWUFBWSxFQUFFLENBQUE7QUFDbkIsdUJBQU8sQ0FBQyxPQUFPLEVBQUUsQ0FBQTthQUNwQixDQUFDLENBQUE7U0FDVDs7OzBCQUVZLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7MEJBQzdCLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7NkJBNUQ5QixXQUFXLEVBQUU7QUFDckIsbUJBQU8sSUFBSSxZQUFZLENBQUMsNEJBQVcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUE7U0FDeEQ7OztXQWpCZ0IsWUFBWTs7O2tCQUFaLFlBQVk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNBWixZQUFZO2NBQVosWUFBWTs7QUFFN0IsYUFGaUIsWUFBWSxDQUVqQixTQUFTLEVBQUU7OEJBRk4sWUFBWTs7MkVBQVosWUFBWTs7QUFJekIsY0FBSyxTQUFTLEdBQUcsU0FBUyxDQUFBO0FBQzFCLGNBQUssSUFBSSxHQUFRLE1BQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFBO0FBQ3RFLGNBQUssTUFBTSxHQUFNLE1BQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFBOztBQUVsRSxjQUFLLFFBQVEsR0FBRyxJQUFJLENBQUE7O0FBRXBCLGNBQUssT0FBTyxDQUNQLEdBQUcsQ0FBQyxNQWRULElBQUksQ0FjVSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FDL0IsR0FBRyxDQUFDLHFCQUFXLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUE7O0FBRTVDLGNBQUssUUFBUSxHQUFTLE1BQUssUUFBUSxDQUFDLElBQUksT0FBTSxDQUFBO0FBQzlDLGNBQUssTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFLLGFBQWEsQ0FBQyxJQUFJLE9BQU0sQ0FBQTtBQUNuRCxjQUFLLElBQUksQ0FBQyxPQUFPLEdBQUssTUFBSyxLQUFLLENBQUMsSUFBSSxPQUFNLENBQUE7O0tBQzlDOztpQkFqQmdCLFlBQVk7OzhCQXdCdkIsQ0FBQyxFQUFFO0FBQ0wsYUFBQyxDQUFDLGNBQWMsRUFBRSxDQUFBO0FBQ2xCLGdCQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFBOztBQUVyQixnQkFBSSxXQUFXLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUN0QyxnQkFBSSxXQUFXLEVBQUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQTs7QUFFMUYsZ0JBQUksTUFBTSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEVBQUUsTUFBTSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUE7QUFDdEUsZ0JBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxFQUFFLE9BQU07O0FBRXhDLGdCQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2pFLGFBQUMsQ0FBQyxFQUFFLEdBQVksU0FBUyxDQUFDLEdBQUcsRUFBRSxDQUFBO0FBQy9CLGdCQUFJLE1BQU0sR0FBTSxTQUFTLENBQUMsR0FBRyxFQUFFLENBQUE7O0FBRS9CLG9CQUFRLE1BQU07QUFDViwyQkExQ2MsV0FBVztBQTBDTCx3QkFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxBQUFDLE1BQUs7QUFBQSxBQUNqRCwyQkEzQzJCLGFBQWE7QUEyQ3BCLHdCQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsQUFBQyxNQUFLO0FBQUEsYUFDdEQ7U0FDSjs7O3NDQUVhLENBQUMsRUFBRTtBQUNiLGFBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtBQUNsQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQTtBQUMzQixnQkFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQTtBQUNsQyxnQkFBSSxDQUFDLE1BQU0sU0FuREwsVUFBVSxBQW1EUSxDQUFBO0FBQ3hCLGdCQUFJLENBQUMsSUFBSSxHQUFLLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQTs7QUFFNUQsZ0JBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQTtTQUNwQjs7O3VDQUVjLENBQUMsRUFBRTtBQUNkLGdCQUFJLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQzNCLGdCQUFJLEdBQUcsR0FBSSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQTtBQUNqQyxnQkFBSSxDQUFDLE1BQU0sU0E1RE8sV0FBVyxBQTRESixDQUFBO0FBQ3pCLGdCQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFBO0FBQ2xDLGdCQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUE7O0FBRWhDLGdCQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7U0FDcEI7Ozt5Q0FFZ0IsQ0FBQyxFQUFFO0FBQ2hCLGdCQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFBO0FBQ3ZDLGdCQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7QUFDZixnQkFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtBQUN2QixzQkFBVSxDQUFDLEtBQUssRUFBRSxDQUFDLElBQUksRUFBRSxDQUFBO1NBQzVCOzs7aUNBRVEsQ0FBQyxFQUFFO0FBQ1IsZ0JBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUE7QUFDckIsZ0JBQUksTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLEVBQUUsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0FBQ3RELGdCQUFJLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtTQUNuRDs7O3FDQUVZO0FBQ1QsZ0JBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQTtTQUNsQjs7O3VDQUVjO0FBQ1gsZ0JBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFBO0FBQ3hCLGdCQUFJLENBQUMsUUFBUSxFQUFFLENBQUE7U0FDbEI7OztpQ0FFUSxJQUFJLEVBQUU7QUFDWCxnQkFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFBOztBQUV4QixpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDbEMsb0JBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDdkI7QUFDRCxtQkFBTyxJQUFJLENBQUE7U0FDZDs7OytCQUVNLElBQUksRUFBRTtBQUNULGdCQUFJLElBQUksR0FBRyxjQUFDLEtBQUk7dUJBQUssUUFBUSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxLQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVU7YUFBQSxDQUFBOztBQUV2RyxnQkFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQTtBQUM5QixjQUFFLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFBOztBQUV2QixjQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQTtBQUNqQyxjQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTtBQUNoQyxjQUFFLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTs7QUFFaEMsZ0JBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDMUMsbUJBQU8sQ0FBQyxTQUFTLHVCQUNLLElBQUksQ0FBQyxFQUFFLCtFQUEwRSxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sQ0FBQywyQ0FDdEcsSUFBSSxDQUFDLEVBQUUsc0VBQWlFLGlCQUFPLElBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQU0sQ0FBQTs7QUFFN0gsY0FBRSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQTs7QUFFdkIsZ0JBQUksQ0FBQyxRQUFRLEdBQUc7QUFDWixvQkFBSSxFQUFFLElBQUk7QUFDVixvQkFBSSxFQUFFLEVBQUU7YUFDWCxDQUFBO1NBQ0o7OztnQ0FFTyxJQUFJLEVBQUU7QUFDVixnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFBO0FBQ3BDLGlCQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFBO0FBQzVELGlCQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFBO0FBQzVELG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7NENBRW1CO0FBQ2hCLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDM0QsZ0JBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFBO0FBQ3BCLGdCQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTtTQUM1Qjs7OzZDQUVvQjtBQUNqQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUE7QUFDekIsaUJBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQ2hDLG9CQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFBO2FBQ2xEO1NBQ0o7OzttQ0FFVSxHQUFHLEVBQUU7QUFDWixnQkFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQTtBQUNyQixtQkFBTztBQUNILGtCQUFFLEVBQUssR0FBRyxDQUFDLE9BQU8sQ0FBQyxFQUFFO0FBQ3JCLHFCQUFLLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVc7QUFDM0IscUJBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVzthQUM5QixDQUFBO1NBQ0o7OzttQ0FFVTtBQUNQLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQzVDLGdCQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7QUFDakIsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzttQ0FFVTtBQUNQLGdCQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQy9DLGdCQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDdEMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztxQ0FFWTtBQUNULGdCQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDbkMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzswQkFFWSxFQUFFLEVBQUU7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzZCQWpKOUIsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQ3hELG1CQUFPLElBQUksWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQ3pDOzs7V0F0QmdCLFlBQVk7OztrQkFBWixZQUFZOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNIWixLQUFLO2NBQUwsS0FBSzs7YUFBTCxLQUFLOzhCQUFMLEtBQUs7O3NFQUFMLEtBQUs7OztpQkFBTCxLQUFLOzttQ0FPWCxPQUFPLEVBQUU7OztBQUNoQixnQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLE1BQU0sRUFBRSxvQkFBVSxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQTtBQUMvRSxtQkFBTyxDQUFDLEdBQUcsR0FBTSxPQUFPLENBQUMsR0FBRyxZQUFPLE9BQU8sQUFBRSxDQUFBO0FBQzVDLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO0FBQ25ELHVCQUFPLElBQUksQ0FBQTthQUNkLENBQUMsQ0FBQztTQUNWOzs7NEJBZFk7QUFDVCxnQkFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxHQUFHLG9CQUFVLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQTtBQUNuRixtQkFBTyxJQUFJLENBQUMsT0FBTyxDQUFBO1NBQ3RCOzs7V0FMZ0IsS0FBSzs7O2tCQUFMLEtBQUs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNETCxVQUFVO2NBQVYsVUFBVTs7QUFFM0IsYUFGaUIsVUFBVSxDQUVmLFNBQVMsRUFBRTs4QkFGTixVQUFVOzsyRUFBVixVQUFVLGFBR2pCLFNBQVM7O0FBQ2YsY0FBSyxTQUFTLEdBQUcsTUFBSyxTQUFTLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUM7O0FBRXBFLGNBQUssU0FBUyxDQUFDLE9BQU8sR0FBRyxNQUFLLGdCQUFnQixDQUFDLElBQUksT0FBTSxDQUFDOztLQUM3RDs7aUJBUGdCLFVBQVU7O3lDQWNWLENBQUMsRUFBRTtBQUNoQixhQUFDLENBQUMsY0FBYyxFQUFFLENBQUM7QUFDbkIsZ0JBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUNaLGdCQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7QUFDYixnQkFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztTQUN2Qzs7O2dDQUVPLENBQUMsRUFBRTtBQUNQLHVDQXRCYSxVQUFVLHlDQXNCVCxDQUFDLEVBQUU7QUFDakIsZ0JBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmOzs7NkJBZlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pELG1CQUFPLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ3hDOzs7V0FaZ0IsVUFBVTs7O2tCQUFWLFVBQVU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0F4QixJQUFNLFVBQVUsV0FBVixVQUFVLEdBQUksS0FBSyxDQUFDO0FBQzFCLElBQU0sV0FBVyxXQUFYLFdBQVcsR0FBRyxNQUFNLENBQUM7QUFDM0IsSUFBTSxhQUFhLFdBQWIsYUFBYSxHQUFHLFFBQVEsQ0FBQzs7SUFFekIsSUFBSSxXQUFKLElBQUk7Y0FBSixJQUFJOztBQUViLGFBRlMsSUFBSSxDQUVELFNBQVMsRUFBRTs4QkFGZCxJQUFJOzsyRUFBSixJQUFJLGFBR0gsU0FBUzs7QUFFZixjQUFLLFNBQVMsR0FBRyxNQUFLLFNBQVMsQ0FBQyxhQUFhLENBQUMsNkNBQTZDLENBQUMsQ0FBQzs7QUFFN0YsY0FBSyxTQUFTLENBQUMsT0FBTyxHQUFHLE1BQUssWUFBWSxDQUFDLElBQUksT0FBTSxDQUFDOztLQUN6RDs7aUJBUlEsSUFBSTs7Z0NBZUw7QUFBRSxtQkFBTyxJQUFJLENBQUMsTUFBTSxLQUFLLFVBQVUsQ0FBQTtTQUFFOzs7aUNBRXBDO0FBQUUsbUJBQU8sSUFBSSxDQUFDLE1BQU0sS0FBSyxXQUFXLENBQUE7U0FBRTs7O3FDQUVsQyxDQUFDLEVBQUU7QUFDWixnQkFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQ2IsZ0JBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztBQUNaLGdCQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO1NBQ3ZDOzs7NkJBYlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pELG1CQUFPLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQ2xDOzs7V0FiUSxJQUFJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDSEksSUFBSTtjQUFKLElBQUk7O0FBRXJCLGFBRmlCLElBQUksQ0FFVCxTQUFTLEVBQUU7OEJBRk4sSUFBSTs7MkVBQUosSUFBSTs7QUFJakIsY0FBSyxTQUFTLEdBQUcsU0FBUyxDQUFBOztBQUUxQixjQUFLLFNBQVMsQ0FBQyxPQUFPLEdBQUcsTUFBSyxLQUFLLENBQUMsSUFBSSxPQUFNLENBQUE7O0tBQ2pEOztpQkFQZ0IsSUFBSTs7OEJBaUNmLENBQUMsRUFBRTtBQUNMLGFBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtBQUNsQixnQkFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTs7QUFFckIsZ0JBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUUsT0FBTTtBQUNqRCxnQkFBSSxNQUFNLEtBQUssSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUE7QUFDM0QsZ0JBQUksTUFBTSxLQUFLLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFBOztBQUV0RCxnQkFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLEtBQUssRUFBRSxNQUFNLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQTtBQUN0RSxnQkFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLEdBQUcsRUFBRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDM0U7OztpQ0FFUSxJQUFJLEVBQUU7QUFDWCxnQkFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFBOztBQUV4QixpQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDbEMsb0JBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7YUFDcEI7QUFDRCxnQkFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBO0FBQ3BCLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7NEJBRUcsSUFBSSxFQUFFO0FBQ04sZ0JBQUksRUFBRSxHQUFJLFFBQVEsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUE7QUFDdEMsZ0JBQUksQ0FBQyxHQUFLLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFBO0FBQ3JELGdCQUFJLEdBQUcsR0FBRyxDQUFDLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQTs7QUFFdEQsYUFBQyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFBO0FBQ2xCLGFBQUMsQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUE7QUFDdEIsYUFBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7O0FBRTVCLGVBQUcsQ0FBQyxHQUFHLEdBQUcsQUFBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFLLElBQUksQ0FBQyxHQUFHLENBQUE7QUFDdEQsZUFBRyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUE7O0FBRVosZ0JBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFBO1NBQzVCOzs7dUNBRWM7QUFDWCxnQkFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUMzRCxnQkFBSSxDQUFDLFVBQVUsRUFBRSxPQUFNOztBQUV2QixnQkFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQTtBQUNsQyxrQkFBTSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7O0FBRXJDLGdCQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQTs7QUFFekIsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzttQ0FFVSxJQUFJLEVBQUU7QUFDYixnQkFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUE7O0FBRXhCLGdCQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQ25CLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FDcEIsT0FBTyxDQUFDLFVBQUMsSUFBSTt1QkFBSyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7YUFBQSxDQUFDLENBQUE7QUFDdkQsZ0JBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQzVCLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7c0NBRWEsQ0FBQyxFQUFFO0FBQ2IsZ0JBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDdkQsZ0JBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTTs7QUFFbkIsZ0JBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO0FBQ2hCLHNCQUFNLEVBQUUsSUFBSTtBQUNaLHVCQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxFQUFFO2FBQzdCLENBQUMsQ0FBQTtTQUNMOzs7aUNBRVEsQ0FBQyxFQUFFO0FBQ1IsZ0JBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUE7QUFDdkQsZ0JBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTTs7QUFFbkIsZ0JBQUksT0FBTyxHQUFHLGtCQUFRLFFBQVEsQ0FBQTtBQUM5QixnQkFBSSxHQUFHLEdBQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQTtBQUM1QixlQUFHLENBQUMsR0FBRyxHQUFPLE1BQU0sQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUE7O0FBRXpDLG1CQUFPLENBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUNaLGVBQWUsRUFBRSxDQUNqQixZQUFZLEVBQUUsQ0FDZCxNQUFNLEVBQUUsQ0FBQTtTQUNoQjs7OzZDQUVvQjtBQUNqQixnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUE7QUFDcEMsZ0JBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFBOztBQUVwQyxnQkFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtBQUN0RCxnQkFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtTQUN6RDs7OzRDQUVtQjtBQUNoQixnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUE7QUFDcEMsZ0JBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFBOztBQUVwQyxnQkFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7QUFDeEQsZ0JBQUksS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRSxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO1NBQzNEOzs7d0NBRWU7QUFDWixnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUNuRCxnQkFBSSxDQUFDLEtBQUssRUFBRSxPQUFNOztBQUVsQixnQkFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQTtBQUM1QixpQkFBSyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7dUJBQUssT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQzthQUFBLENBQUMsQ0FBQTtBQUNoRSxtQkFBTyxDQUFDLEdBQUcsR0FBRyxFQUFFLENBQUE7O0FBRWhCLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7NEJBaklVO0FBQ1AsZ0JBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsQ0FBQTtBQUMzRSxtQkFBTyxJQUFJLENBQUMsS0FBSyxDQUFBO1NBQ3BCOzs7NEJBRWU7QUFDWixnQkFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFBO0FBQ25GLG1CQUFPLElBQUksQ0FBQyxVQUFVLENBQUE7U0FDekI7Ozs0QkFFZTtBQUNaLGdCQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUE7QUFDbkYsbUJBQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQTtTQUN6Qjs7OzRCQUVlO0FBQ1osZ0JBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFBO0FBQ2xGLG1CQUFPLElBQUksQ0FBQyxVQUFVLENBQUE7U0FDekI7Ozs2QkF0QlcsV0FBVyxFQUFFO0FBQ3JCLG1CQUFPLElBQUksSUFBSSxDQUFDLHFCQUFXLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFBO1NBQ2hEOzs7V0FYZ0IsSUFBSTs7O2tCQUFKLElBQUk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ0FKLEtBQUs7Y0FBTCxLQUFLOzthQUFMLEtBQUs7OEJBQUwsS0FBSzs7c0VBQUwsS0FBSzs7O2lCQUFMLEtBQUs7OzRCQU9sQixJQUFJLEVBQUU7OztBQUNOLGdCQUFJLE9BQU8sR0FBRztBQUNWLG9CQUFJLEVBQUksSUFBSSxDQUFDLFFBQVE7QUFDckIsc0JBQU0sRUFBRSxvQkFBVSxPQUFPLENBQUMsSUFBSTtBQUM5Qix1QkFBTyxFQUFFLEVBQ1I7YUFDSixDQUFBOztBQUVELG1CQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQTtBQUNqRCxtQkFBTyxDQUFDLEdBQUcsUUFBTSxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxBQUFFLENBQUE7O0FBRW5ELG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQTtBQUNqRCx1QkFBTyxJQUFJLENBQUE7YUFDZCxDQUFDLENBQUE7U0FDVDs7O21DQUVVLE9BQU8sRUFBRTs7O0FBQ2hCLGdCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUMsTUFBTSxFQUFFLG9CQUFVLE9BQU8sQ0FBQyxHQUFHLEVBQUMsQ0FBQyxDQUFBO0FBQzdFLG1CQUFPLENBQUMsR0FBRyxRQUFNLE9BQU8sQ0FBQyxHQUFHLEdBQUcsT0FBTyxBQUFFLENBQUE7O0FBRXhDLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFBO0FBQ25ELHVCQUFPLElBQUksQ0FBQTthQUNkLENBQUMsQ0FBQTtTQUNUOzs7OEJBRUs7QUFDRixrQkFBTSxJQUFJLEtBQUssQ0FBQywyQkFBMkIsQ0FBQyxDQUFBO1NBQy9DOzs7NEJBdENZO0FBQ1QsZ0JBQUksSUFBSSxDQUFDLE9BQU8sS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxvQkFBVSxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUE7QUFDakYsbUJBQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQTtTQUN0Qjs7O1dBTGdCLEtBQUs7OztrQkFBTCxLQUFLOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDREwsVUFBVTtjQUFWLFVBQVU7O0FBRTNCLGFBRmlCLFVBQVUsQ0FFZixTQUFTLEVBQUU7OEJBRk4sVUFBVTs7c0VBQVYsVUFBVSxhQUdqQixTQUFTO0tBQ2xCOztpQkFKZ0IsVUFBVTs7MEJBVWxCLElBQUksRUFBRTtBQUNYLGdCQUFJLENBQUMsS0FBSyxFQUFFLENBQUE7QUFDWixnQkFBSSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFBO1NBQy9FOzRCQUVVO0FBQ1AsZ0JBQUksUUFBUSxHQUFHLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTs7QUFFM0MsbUJBQU87QUFDSCxvQkFBSSw2QkFuQkssVUFBVSwwQkFtQkg7QUFDaEIsd0JBQVEsRUFBRSxRQUFRO2FBQ3JCLENBQUE7U0FDSjs7OzZCQWhCVyxXQUFXLEVBQUU7QUFDckIsbUJBQU8sSUFBSSxVQUFVLENBQUMsbUJBQVMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUE7U0FDcEQ7OztXQVJnQixVQUFVOzs7a0JBQVYsVUFBVTs7O0FDRi9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQzVEcUIsVUFBVTtjQUFWLFVBQVU7O0FBRTNCLGFBRmlCLFVBQVUsQ0FFZixTQUFTLEVBQW9CO1lBQWxCLFNBQVMseURBQUcsSUFBSTs7OEJBRnRCLFVBQVU7OzJFQUFWLFVBQVU7O0FBSXZCLGNBQUssU0FBUyxHQUFHLFNBQVMsQ0FBQztBQUMzQixjQUFLLFNBQVMsR0FBRyxTQUFTLEdBQUcsU0FBUyxHQUFHLE1BQUssU0FBUyxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDOztBQUU1RixjQUFLLFNBQVMsQ0FBQyxPQUFPLEdBQUcsTUFBSyxPQUFPLENBQUMsSUFBSSxPQUFNLENBQUM7O0tBQ3BEOztpQkFSZ0IsVUFBVTs7Z0NBZW5CLENBQUMsRUFBRTtBQUNQLGFBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs7QUFFbkIsZ0JBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFO0FBQy9DLGlCQUFDLENBQUMsZUFBZSxFQUFFLENBQUM7QUFDcEIsaUJBQUMsQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO0FBQzdCLHVCQUFPO2FBQ1Y7QUFDRCxnQkFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztTQUN2Qzs7O3VDQUVjO0FBQ1gsZ0JBQUksQ0FBQyxnQkFBZ0IsQ0FBQywwREFBMEQsQ0FBQyxDQUFDO0FBQ2xGLGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDekMsbUJBQU8sSUFBSSxDQUFDO1NBQ2Y7Ozt5Q0FFZ0IsR0FBRyxFQUFFO0FBQ2xCLGVBQUcsR0FBYSxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQ3BDLGdCQUFJLFNBQVMsR0FBRyxpQkFBTyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0FBQ3pDLGdCQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsZ0JBQWMsU0FBUyxrQkFBYSxHQUFHLEFBQUUsQ0FBQztBQUNqRSxtQkFBTyxJQUFJLENBQUM7U0FDZjs7O2dDQUVPO0FBQ0osZ0JBQUksQ0FBQyxnQkFBZ0IsQ0FBQywrQ0FBK0MsQ0FBQyxDQUFDO0FBQ3ZFLGdCQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQztBQUNsQyxnQkFBSSxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUM7QUFDbkQsbUJBQU8sSUFBSSxDQUFDO1NBQ2Y7OzsrQkFFTTtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDMUMsbUJBQU8sSUFBSSxDQUFDO1NBQ2Y7OzsrQkFFTTtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7QUFDdkMsbUJBQU8sSUFBSSxDQUFDO1NBQ2Y7Ozs0QkE1Q2M7QUFDWCxnQkFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQzdGLG1CQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDekI7OztXQWJnQixVQUFVOzs7a0JBQVYsVUFBVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SUNFVixRQUFRO2NBQVIsUUFBUTs7QUFFekIsYUFGaUIsUUFBUSxDQUViLFNBQVMsRUFBRTs4QkFGTixRQUFROzsyRUFBUixRQUFROztBQUlyQixjQUFLLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDMUIsY0FBSyxTQUFTLENBQUMsUUFBUSxHQUFHLE1BQUssWUFBWSxDQUFDLElBQUksT0FBTSxDQUFBOztLQUN6RDs7aUJBTmdCLFFBQVE7O2dDQWlCakI7QUFDSixnQkFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFBO0FBQzFFLGdCQUFJLElBQUksR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQTs7QUFFbEMsZ0JBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLFVBQUMsS0FBSzt1QkFBSyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUU7YUFBQSxDQUFDLENBQUM7O0FBRXJELGdCQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFBO0FBQ3RCLGdCQUFJLENBQUMsV0FBVyxFQUFFLENBQUE7QUFDbEIsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7Ozs4QkFFSyxJQUFJLEVBQUU7QUFDUixnQkFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUE7QUFDckMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztrQ0FFUyxNQUFNLEVBQUU7QUFDZCxnQkFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQTtBQUM5QixnQkFBSSxTQUFTLEdBQUcsaUJBQU8sSUFBSSxDQUFDLEVBQUUsQ0FBQTs7QUFFOUIsZ0JBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7QUFFbEIsaUJBQUksSUFBSSxTQUFTLElBQUksTUFBTTtBQUFFLG9CQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEVBQUU7QUFDL0Qsd0JBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxhQUFhLGFBQVcsU0FBUyxRQUFLLENBQUE7QUFDNUQsd0JBQUksQ0FBQyxLQUFLLEVBQUUsU0FBUTs7QUFFcEIsd0JBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUM1Qix5QkFBSyxJQUFJLEdBQUcsSUFBSSxJQUFJO0FBQUUsNEJBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUNoRCxnQ0FBSSxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQTtBQUN0QyxnQ0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLHlCQUF5QixDQUFDLENBQUE7O0FBRW5ELGdDQUFJLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFBO0FBQzNDLGtDQUFNLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQTs7QUFFbEMsZ0NBQUksSUFBSSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO0FBQ2xFLGtDQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ3hCLGtDQUFNLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQTtBQUNqRCxpQ0FBSyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUE7eUJBQ3ZDO3FCQUFBLEFBRUQsSUFBSSxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQTtBQUN6Qyx3QkFBSSxNQUFNLEVBQUUsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7aUJBQ2hEO2FBQUEsQUFDRCxPQUFPLElBQUksQ0FBQTtTQUNkOzs7c0NBRWE7QUFDVixnQkFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUE7QUFDbEMsZ0JBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsQ0FBQTs7QUFFekUsZ0JBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLFVBQUMsS0FBSyxFQUFLO0FBQzdCLG9CQUFJLFNBQVMsR0FBRyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUE7QUFDckQsb0JBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQUMsR0FBRzsyQkFBSyxHQUFHLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUM7aUJBQUEsQ0FBQyxDQUFBO0FBQzlELHFCQUFLLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQTthQUN0QyxDQUFDLENBQUE7QUFDRixtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3FDQUVZLENBQUMsRUFBRTtBQUNaLGFBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTs7QUFFbEIsZ0JBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7QUFFbEIsZ0JBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO0FBQ2hCLHNCQUFNLEVBQUUsSUFBSTtBQUNaLG9CQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7YUFDbEIsQ0FBQyxDQUFBO1NBQ0w7OzsrQkFFTTtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDekMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzsrQkFFTTtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDdEMsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OzswQkF0RlUsTUFBTSxFQUFFO0FBQUUsZ0JBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQTtTQUFFOzRCQUNqRDtBQUFFLG1CQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1NBQUU7OzswQkFFMUQsSUFBSSxFQUFFO0FBQ1gsZ0JBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQTtBQUNaLG9DQUFTLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUE7U0FDakM7NEJBQ1U7QUFBRSxtQkFBTyx5QkFBVSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7U0FBRTs7OzBCQWlGbEMsRUFBRSxFQUFFO0FBQ2IsZ0JBQUksQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFBO1NBQ3hCOzs7V0FsR2dCLFFBQVE7OztrQkFBUixRQUFROzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDRDdCLElBQUksYUFBYSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7QUFDOUIsU0FBSyxFQUFLLEtBQUs7QUFDZixVQUFNLEVBQUksTUFBTTtBQUNoQixTQUFLLEVBQUssS0FBSztBQUNmLFlBQVEsRUFBRSxRQUFRO0NBQ3JCLENBQUMsQ0FBQzs7SUFFa0IsU0FBUztjQUFULFNBQVM7O2FBQVQsU0FBUzs4QkFBVCxTQUFTOztzRUFBVCxTQUFTOzs7aUJBQVQsU0FBUzs7NEJBaUR0QixPQUFPLEVBQUU7QUFDVCxtQkFBTyxTQUFTLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2pDOzs7NEJBRUcsSUFBSSxFQUFFOzs7QUFDTixnQkFBSSxPQUFPLEdBQUc7QUFDVixvQkFBSSxFQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO0FBQzVCLHNCQUFNLEVBQUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUk7YUFDbkUsQ0FBQzs7QUFFRixtQkFBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7O0FBRWxELGdCQUFJLE9BQU8sQ0FBQyxNQUFNLEtBQUssU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsT0FBTyxDQUFDLEdBQUcsUUFBTSxPQUFPLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLEFBQUUsQ0FBQzs7QUFFdkYsbUJBQU8sSUFBSSxDQUNOLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FDWixJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDWix1QkFBSyxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUMsTUFBTSxRQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7QUFDN0MsdUJBQU8sSUFBSSxDQUFDO2FBQ2YsQ0FBQyxDQUFDO1NBQ1Y7Ozs0QkFFRyxFQUFFLEVBQUU7OztBQUNKLGdCQUFJLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUUsTUFBTSxFQUFFLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztBQUNoRixtQkFBTyxDQUFDLEdBQUcsUUFBTSxPQUFPLENBQUMsR0FBRyxHQUFHLEVBQUUsQUFBRSxDQUFDO0FBQ3BDLG1CQUFPLElBQUksQ0FDTixHQUFHLENBQUMsT0FBTyxDQUFDLENBQ1osSUFBSSxDQUFDLFVBQUMsSUFBSSxFQUFLO0FBQ1osdUJBQUssSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFDLE1BQU0sUUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO0FBQzdDLHVCQUFPLElBQUksQ0FBQzthQUNmLENBQUMsQ0FBQztTQUNWOzs7K0JBRU0sRUFBRSxFQUFFOzs7QUFDUCxnQkFBSSxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBQyxDQUFDLENBQUM7QUFDakYsbUJBQU8sQ0FBQyxHQUFHLFFBQU0sT0FBTyxDQUFDLEdBQUcsR0FBRyxFQUFFLEFBQUUsQ0FBQzs7QUFFcEMsbUJBQU8sSUFBSSxDQUNOLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FDWixJQUFJLENBQUMsVUFBQyxJQUFJLEVBQUs7QUFDWix1QkFBSyxJQUFJLENBQUMsUUFBUSxFQUFFLEVBQUMsTUFBTSxRQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7QUFDaEQsdUJBQU8sSUFBSSxDQUFDO2FBQ2YsQ0FBQyxDQUFDO1NBQ1Y7OzswQkFFUyxFQUFFLEVBQUs7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzBCQUM3QixFQUFFLEVBQUs7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzBCQUMxQixFQUFFLEVBQUU7QUFBRSxnQkFBSSxDQUFDLEVBQUUsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUE7U0FBRTs7OzRDQTFGZjtBQUN2QixtQkFBTztBQUNILHVCQUFPLEVBQUU7QUFDTCxzQ0FBa0IsRUFBRSxnQkFBZ0I7QUFDcEMsa0NBQWMsRUFBRSxrQkFBa0I7QUFDbEMsNEJBQVEsRUFBQyxrQkFBa0I7aUJBQzlCOztBQUFBLGFBRUosQ0FBQztTQUNMOzs7a0NBRWdCLEdBQUcsRUFBRTtBQUNsQixnQkFBSSxZQUFZLEdBQUcsQUFBQyxpQkFBTyxLQUFLLElBQUksaUJBQU8sS0FBSyxDQUFDLEdBQUcsQ0FBQyxHQUFJLGlCQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUM7O0FBRWxGLG1CQUFPLFlBQVksR0FDYixNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFlBQVksQ0FBQyxHQUMxRCxTQUFTLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUN2Qzs7OzRCQUVVLE9BQU8sRUFBRTtBQUNoQixtQkFBTyxJQUFJLE9BQU8sQ0FBQyxVQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUs7QUFDcEMsbUNBQUksT0FBTyxFQUFFLFVBQUMsS0FBSyxFQUFFLFFBQVEsRUFBSztBQUM5Qix3QkFBSSxNQUFNLFlBQUEsQ0FBQzs7QUFFWCw0QkFBUSxRQUFRLENBQUMsVUFBVTtBQUN2Qiw2QkFBSyxHQUFHO0FBQUMsQUFDVCw2QkFBSyxHQUFHO0FBQUMsQUFDVCw2QkFBSyxHQUFHO0FBQUMsQUFDVCw2QkFBSyxHQUFHOztBQUNKLGtDQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7QUFDbkMsbUNBQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLENBQUM7QUFDNUMsa0NBQU07QUFBQSxBQUNWLDZCQUFLLEdBQUc7O0FBQ0osa0NBQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztBQUNuQyxrQ0FBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsQ0FBQztBQUNuRCxrQ0FBTTtBQUFBLEFBQ1Y7QUFDSSxrQ0FBTSxDQUFDLElBQUksS0FBSyxDQUFDLGtEQUFrRCxDQUFDLENBQUMsQ0FBQztBQUFBLHFCQUM3RTtpQkFDSixDQUFDLENBQUM7YUFDTixDQUFDLENBQUM7U0FDTjs7OzRCQTdDb0I7QUFDakIsbUJBQU8sYUFBYSxDQUFDO1NBQ3hCOzs7V0FKZ0IsU0FBUzs7O2tCQUFULFNBQVM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDVFQsUUFBUTtjQUFSLFFBQVE7O0FBRXpCLGFBRmlCLFFBQVEsQ0FFYixTQUFTLEVBQUU7OEJBRk4sUUFBUTs7MkVBQVIsUUFBUTs7QUFJckIsY0FBSyxTQUFTLEdBQU8sU0FBUyxDQUFBO0FBQzlCLGNBQUssU0FBUyxHQUFPLElBQUksQ0FBQTtBQUN6QixjQUFLLGFBQWEsR0FBRyxJQUFJLENBQUE7O0FBRXpCLGNBQUssWUFBWSxHQUFHLE1BQUssWUFBWSxDQUFDLElBQUksT0FBTSxDQUFBO0FBQ2hELGNBQUssWUFBWSxHQUFHLE1BQUssWUFBWSxDQUFDLElBQUksT0FBTSxDQUFBO0FBQ2hELGNBQUssT0FBTyxHQUFRLE1BQUssT0FBTyxDQUFDLElBQUksT0FBTSxDQUFBO0FBQzNDLGNBQUssT0FBTyxHQUFRLE1BQUssT0FBTyxDQUFDLElBQUksT0FBTSxDQUFBOztLQUM5Qzs7aUJBWmdCLFFBQVE7O3VDQXVCVixNQUFNLEVBQUU7QUFDbkIsbUJBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLHFCQUFtQixNQUFNLFFBQUssQ0FBQTtTQUM5RDs7OytCQUVnQjtnQkFBWixHQUFHLHlEQUFHLElBQUk7O0FBQ1gsZ0JBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLENBQUE7O0FBRXhELGdCQUFJLE9BQU8sR0FBRyxJQUFJLFFBQVEsRUFBRSxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMseUJBQXlCLENBQUMsQ0FBQTs7QUFFbkYsZ0JBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFLE9BQU07O0FBRXpELGdCQUFJLE1BQU0sR0FBTyxHQUFHLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQTtBQUNuQyxnQkFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsYUFBYSxPQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxzQkFBaUIsR0FBRyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBRzs7O0FBQUEsQUFHMUgsZ0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUzs7QUFBQSxBQUVqQyxnQkFBSSxFQUFFLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxFQUFFOztBQUV2QixvQkFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtBQUN4QyxrQkFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQTthQUN0Qjs7QUFFRCxjQUFFLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUE7QUFDdkIsZ0JBQUksRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTs7Ozs7QUFLdkIsa0JBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDbkIsb0JBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQTthQUN0Qjs7QUFFRCxnQkFBSSxHQUFHLEdBQUc7QUFDTixzQkFBTSxFQUFVLElBQUk7QUFDcEIsOEJBQWMsRUFBRSxHQUFHO0FBQ25CLHNCQUFNLEVBQVUsTUFBTTthQUN6QixDQUFBOztBQUVELGdCQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQTtBQUN0QixnQkFBSSxDQUFDLGFBQWEsRUFBRSxDQUFBOztBQUVwQixlQUFHLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDdEMsZ0JBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFBOztBQUVwQixnQkFBSSxVQUFVLEVBQUUsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7QUFDbEQsZ0JBQUksQ0FBQyxhQUFhLEdBQUcsVUFBVSxDQUFBOztBQUUvQixtQkFBTyxHQUFHLENBQUMsY0FBYyxDQUFBO0FBQ3pCLGdCQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQTtBQUN2QixtQkFBTyxJQUFJLENBQUE7U0FDZDs7OytCQUVNO0FBQ0gsZ0JBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUN0QyxnQkFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtBQUN4QyxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3VDQUU2QjtnQkFBakIsUUFBUSx5REFBRyxJQUFJOztBQUN4QixnQkFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUE7QUFDL0MsZ0JBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE9BQU07Ozs7Ozs7QUFFMUIscUNBQW1CLFFBQVEsQ0FBQyxNQUFNLEVBQUUsOEhBQUU7d0JBQTdCLE1BQU07O0FBQ1gsd0JBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUMxQyx3QkFBSSxNQUFNLFlBQVksUUFBUSxFQUFFLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQTtpQkFDaEQ7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDRCxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O3lDQUVnQjtBQUNiLGdCQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUE7U0FDbkM7OztrQ0FFbUI7Z0JBQVosR0FBRyx5REFBRyxJQUFJOztBQUNkLGdCQUFJLENBQUMsR0FBRyxFQUFFO0FBQ04sb0JBQUksSUFBSSxHQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUE7QUFDMUMsb0JBQUksS0FBSyxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFBOztBQUVqQyxxQkFBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBUyxJQUFJLEVBQUU7QUFDcEMsd0JBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQTtpQkFDNUMsQ0FBQyxDQUFBO2FBQ0wsTUFBTTtBQUNILG1CQUFHLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUE7YUFDM0M7QUFDRCxtQkFBTyxJQUFJLENBQUE7U0FDZDs7O2lDQUVrQjtnQkFBWixHQUFHLHlEQUFHLElBQUk7O0FBQ2IsZ0JBQUksQ0FBQyxHQUFHLEVBQUU7QUFDTixvQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQTtBQUNqRCxvQkFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUE7O0FBRWpDLHFCQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFTLElBQUksRUFBRTtBQUNwQyx3QkFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLENBQUE7aUJBQ3BDLENBQUMsQ0FBQTthQUNMLE1BQU07QUFDSCxvQkFBSSxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQTthQUN4RztBQUNELG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7d0NBRWU7QUFDWixnQkFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUMsQ0FBQTtBQUMvRCxpQkFBSSxJQUFJLEdBQUcsR0FBRyxDQUFDLEVBQUUsR0FBRyxHQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEVBQUU7QUFDL0MsOEJBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFBO2FBQ2pEO0FBQ0QsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztnQ0FFTyxDQUFDLEVBQUU7QUFDUCxhQUFDLENBQUMsY0FBYyxFQUFFLENBQUE7O0FBRWxCLGdCQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFBO0FBQ3JCLGdCQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLElBQUksR0FBRyxFQUFFLE9BQU07QUFDL0MsZ0JBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLE9BQU07QUFDOUMsZ0JBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDcEI7OztxQ0FFWSxDQUFDLEVBQUU7QUFDWixnQkFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUE7U0FDL0M7OztxQ0FFWSxDQUFDLEVBQUU7QUFDWixnQkFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFBO1NBQ25EOzs7Z0NBRU8sQ0FBQyxFQUFFO0FBQ1AsZ0JBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEVBQUcsT0FBTTtBQUNsQyxnQkFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFNOztBQUVsQyxnQkFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUE7QUFDdkMsZ0JBQUksR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUE7U0FDMUI7OztxQ0FFeUI7Z0JBQWYsS0FBSyx5REFBRyxLQUFLOztBQUNwQixnQkFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUE7QUFDOUMsZ0JBQUksSUFBSSxFQUFFO0FBQ04sb0JBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLO29CQUFFLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQTtBQUN4QyxrQkFBRSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxHQUFHLEtBQUssQ0FBQTthQUN2RDtBQUNELG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7NEJBdkpTO0FBQ04sZ0JBQUksSUFBSSxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxhQUFhLE9BQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLFdBQVEsQ0FBQTtBQUM5RyxtQkFBTyxJQUFJLENBQUMsSUFBSSxDQUFBO1NBQ25COzs7MEJBRVcsRUFBRSxFQUFFO0FBQ1osbUJBQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFBO1NBQy9COzs7MEJBa0pVLEVBQUUsRUFBUTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7MEJBQ2hDLEVBQUUsRUFBTztBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7MEJBQzVCLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7MEJBQ2xDLEVBQUUsRUFBRTtBQUFFLGdCQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFFLENBQUMsQ0FBQTtTQUFFOzs7V0ExS2xDLFFBQVE7OztrQkFBUixRQUFROzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lDQXZCLFFBQVE7QUFFVixhQUZFLFFBQVEsQ0FFRSxnQkFBZ0IsRUFBRTs4QkFGNUIsUUFBUTs7QUFHTixZQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUE7S0FDM0M7O2lCQUpDLFFBQVE7OzRCQWdCTixNQUFNLEVBQUU7QUFDUixnQkFBSSxLQUFLLFlBQUEsQ0FBQTtBQUNULGdCQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsRUFBRTtBQUM1QixxQkFBSyxHQUFHLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQTtBQUM1QixzQkFBTSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQTthQUN4QixNQUNJLEtBQUssR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQTs7QUFHbEQsZ0JBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEVBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFBOztBQUV0RixnQkFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFBO0FBQ2hDLG9CQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUE7QUFDaEQsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7Ozs0QkFFRyxLQUFLLEVBQUU7QUFDUCxpQkFBSyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQTtBQUMzQixtQkFBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUE7U0FDcEU7Ozs0QkFFRyxLQUFLLEVBQUU7QUFDUCxtQkFBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtTQUNsQzs7OzRCQTVCYztBQUNYLGdCQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQTtBQUM1RCxtQkFBTyxJQUFJLENBQUMsU0FBUyxDQUFBO1NBQ3hCOzs7NEJBUmlCO0FBQ2QsZ0JBQUksUUFBUSxDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUUsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFBO0FBQ2hFLG1CQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUE7U0FDeEI7OztXQVRDLFFBQVE7OztJQTJDUixHQUFHO2FBQUgsR0FBRzs4QkFBSCxHQUFHOzs7aUJBQUgsR0FBRzs7NkJBVUEsS0FBSSxFQUFFO0FBQ1AsZ0JBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFBO0FBQ2pCLG1CQUFPLElBQUksQ0FBQTtTQUNkOzs7K0JBTU0sUUFBUSxFQUFFO0FBQ2IsZ0JBQUksTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFBLENBQUUsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFBO0FBQ25FLGdCQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtBQUNqQixtQkFBTyxNQUFNLENBQUE7U0FDaEI7OztrQ0FFUyxRQUFRLEVBQUU7QUFDaEIsZ0JBQUksTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFBLENBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFBO0FBQzVFLGdCQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQTtBQUNqQixtQkFBTyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDNUM7OztnQ0FFTyxRQUFRLEVBQUU7OztBQUNkLGdCQUFJLElBQUksR0FBTyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUE7QUFDM0MsZ0JBQUksQ0FBQyxHQUFVLE9BQU8sQ0FBQyxTQUFTLENBQUM7QUFDakMsZ0JBQUksUUFBUSxHQUFHLFNBQVgsUUFBUSxDQUFJLENBQUM7dUJBQUssS0FBSyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsUUFBTyxLQUFLLENBQUMsQ0FBQzthQUFBLENBQUE7QUFDekYsZ0JBQUksQ0FBQyxHQUFVLENBQUMsQ0FBQyxPQUFPLElBQUksQ0FBQyxDQUFDLHFCQUFxQixJQUFJLENBQUMsQ0FBQyxrQkFBa0IsSUFBSSxDQUFDLENBQUMsaUJBQWlCLElBQUksUUFBUSxDQUFBO0FBQzlHLGdCQUFJLE1BQU0sR0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQTtBQUNyQyxnQkFBSSxDQUFDLEtBQUssR0FBSyxJQUFJLENBQUE7QUFDbkIsbUJBQU8sTUFBTSxDQUFBO1NBQ2hCOzs7MEJBckNhLElBQUksRUFBRTtBQUNoQixnQkFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUE7U0FDekI7NEJBRWU7QUFDWixtQkFBUSxJQUFJLENBQUMsVUFBVSxJQUFJLFFBQVEsQ0FBQztTQUN2Qzs7OzZCQU9XLE1BQU0sRUFBRTtBQUNoQixtQkFBTyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQ3pDOzs7V0FqQkMsR0FBRzs7O0lBMkNZLFVBQVU7Y0FBVixVQUFVOzthQUFWLFVBQVU7OEJBQVYsVUFBVTs7c0VBQVYsVUFBVTs7O2lCQUFWLFVBQVU7OytCQVNwQixNQUFNLEVBQWlCO2dCQUFmLFFBQVEseURBQUcsRUFBRTs7QUFDeEIsZ0JBQUksUUFBUSxDQUFDLE1BQU0sRUFBRSxNQUFNLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtBQUNsRCxnQkFBSSxDQUFDLE1BQU0sRUFBRSxNQUFNLElBQUksS0FBSyxDQUFDLG9DQUFvQyxDQUFDLENBQUE7O0FBRWxFLGdCQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsT0FBTyxLQUFLLENBQUE7QUFDdEQsZ0JBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFFLE9BQU8sS0FBSyxDQUFBOztBQUU1RCxtQkFBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsS0FBSyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQTtTQUNuRDs7OzRCQUVHLFVBQVUsRUFBRTtBQUNaLGdCQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtBQUMxQixvQkFBSSxNQUFNLEdBQUcsSUFBSTtvQkFDYixLQUFLLEdBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQzlCLEdBQUcsR0FBTSxLQUFLLENBQUMsTUFBTTtvQkFDckIsSUFBSSxHQUFLLEVBQUUsQ0FBQTs7QUFFZixxQkFBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEdBQUcsRUFBRSxDQUFDLEVBQUUsRUFBRTtBQUMxQix3QkFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQTtBQUN0Qix3QkFBSSxDQUFDLEtBQUssR0FBRyxHQUFDLENBQUMsRUFBRSxPQUFPLE1BQU0sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBLEtBQzNDLE1BQU0sR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFBO2lCQUNqQzthQUNKOztBQUVELG1CQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFBO1NBQ3RDOzs7NkJBRUksS0FBSyxFQUFFO0FBQ1IsZ0JBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBOztBQUVuQyxnQkFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssU0FBUyxFQUFFO0FBQ3ZCLG9CQUFJLENBQUEsR0FBRSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLFFBQVEsRUFBRTtBQUNyRCx3QkFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxLQUFLLENBQUE7aUJBQzFDO2FBQ0o7O0FBRUQsdUNBN0NhLFVBQVUsMkJBNkNaLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUE7O0FBRTVCLGdCQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFBO0FBQ3hCLG1CQUFNLE1BQU0sS0FBSyxJQUFJLEVBQUU7QUFDbkIsb0JBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxFQUFFLDJCQWpEOUMsVUFBVSwyQkFpRCtDLEtBQUssQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUE7QUFDckYsc0JBQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFBO2FBQ3pCO1NBQ0o7Ozs0QkFsRGE7QUFDVixnQkFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFBO0FBQ25FLG1CQUFPLElBQUksQ0FBQyxRQUFRLENBQUE7U0FDdkI7Ozs0QkFFWTtBQUFFLG1CQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQTtTQUFFOzs7NEJBK0N0RTtBQUNOLGdCQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQTtBQUNsRCxnQkFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFBO0FBQ3RFLG1CQUFPLElBQUksQ0FBQyxJQUFJLENBQUE7U0FDbkI7Ozs2QkFFVyxNQUFNLEVBQUU7QUFDaEIsbUJBQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUMxQjs7O1dBOURnQixVQUFVOzs7a0JBQVYsVUFBVTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0UvQixTQUFTLE9BQU8sQ0FBQyxHQUFHLEVBQUU7QUFDbEIsUUFBSSxHQUFHLEVBQUUsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Q0FDOUI7Ozs7Ozs7Ozs7QUFBQSxBQVVELFNBQVMsS0FBSyxDQUFDLEdBQUcsRUFBRTtBQUNoQixTQUFLLElBQUksR0FBRyxJQUFJLE9BQU8sQ0FBQyxTQUFTLEVBQUU7QUFDL0IsV0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDckM7QUFDRCxXQUFPLEdBQUcsQ0FBQztDQUNkOzs7Ozs7Ozs7OztBQUFBLEFBV0QsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLEdBQ2hCLE9BQU8sQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEdBQUcsVUFBUyxLQUFLLEVBQUUsRUFBRSxFQUFDO0FBQ3BELFFBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUM7QUFDeEMsS0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUEsQ0FDN0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0FBQ2QsV0FBTyxJQUFJLENBQUM7Q0FDZjs7Ozs7Ozs7Ozs7O0FBQUMsQUFZTixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksR0FBRyxVQUFTLEtBQUssRUFBRSxFQUFFLEVBQUM7QUFDeEMsYUFBUyxFQUFFLEdBQUc7QUFDVixZQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztBQUNwQixVQUFFLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztLQUM3Qjs7QUFFRCxNQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQztBQUNYLFFBQUksQ0FBQyxFQUFFLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0FBQ25CLFdBQU8sSUFBSSxDQUFDO0NBQ2Y7Ozs7Ozs7Ozs7OztBQUFDLEFBWUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEdBQ2pCLE9BQU8sQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUM1QixPQUFPLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUNoQyxPQUFPLENBQUMsU0FBUyxDQUFDLG1CQUFtQixHQUFHLFVBQVMsS0FBSyxFQUFFLEVBQUUsRUFBQztBQUN2RCxRQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRTs7O0FBQUMsQUFHeEMsUUFBSSxDQUFDLElBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtBQUN2QixZQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztBQUNyQixlQUFPLElBQUksQ0FBQztLQUNmOzs7QUFBQSxBQUdELFFBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDO0FBQzdDLFFBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxJQUFJLENBQUM7OztBQUFBLEFBRzVCLFFBQUksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7QUFDdkIsZUFBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQztBQUNwQyxlQUFPLElBQUksQ0FBQztLQUNmOzs7QUFBQSxBQUdELFFBQUksRUFBRSxDQUFDO0FBQ1AsU0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDdkMsVUFBRSxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQUNsQixZQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUU7QUFDM0IscUJBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0FBQ3ZCLGtCQUFNO1NBQ1Q7S0FDSjtBQUNELFdBQU8sSUFBSSxDQUFDO0NBQ2Y7Ozs7Ozs7Ozs7QUFBQyxBQVVkLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLFVBQVMsS0FBSyxFQUFDO0FBQ3BDLFFBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsSUFBSSxFQUFFLENBQUM7QUFDeEMsUUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQztRQUNoQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUM7O0FBRS9DLFFBQUksU0FBUyxFQUFFO0FBQ1gsaUJBQVMsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQy9CLGFBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsR0FBRyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUU7QUFDbEQscUJBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2xDO0tBQ0o7O0FBRUQsV0FBTyxJQUFJLENBQUM7Q0FDZjs7Ozs7Ozs7OztBQUFDLEFBVUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxTQUFTLEdBQUcsVUFBUyxLQUFLLEVBQUM7QUFDekMsUUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztBQUN4QyxXQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztDQUM3Qzs7Ozs7Ozs7OztBQUFDLEFBVUYsT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEdBQUcsVUFBUyxLQUFLLEVBQUM7QUFDNUMsV0FBTyxDQUFDLENBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7Q0FDMUMsQ0FBQzs7a0JBRWEsT0FBTzs7Ozs7Ozs7Ozs7QUNqS3RCLFNBQVMsT0FBTyxDQUFDLElBQUksRUFBRTtBQUNuQixRQUFJLEtBQUssR0FBRyxJQUFJLEtBQUssQ0FBQyxRQUFRLEVBQUU7QUFDNUIsaUJBQVMsRUFBRSxJQUFJO0FBQ2Ysb0JBQVksRUFBRSxJQUFJO0tBQ3JCLENBQUMsQ0FBQzs7QUFFSCxRQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0NBQzdCOzs7Ozs7Ozs7QUFBQSxBQVNELElBQUksUUFBUSxHQUFHLFNBQVgsUUFBUSxDQUFhLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFOztBQUUzQyxTQUFJLElBQUksR0FBRyxJQUFJLElBQUk7QUFBRSxZQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7O0FBRS9DLGdCQUFJLElBQUksR0FBRyxHQUFHLENBQUM7QUFDZixnQkFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQzs7O0FBQUMsQUFHdEIsZ0JBQUcsT0FBTyxRQUFRLEFBQUMsS0FBSyxXQUFXLEVBQUUsSUFBSSxHQUFHLFFBQVEsR0FBRyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQzs7QUFFdkUsZ0JBQUcsS0FBSyxDQUFDLFdBQVcsS0FBSyxLQUFLLEVBQUU7QUFDNUIsb0JBQUksSUFBSSxJQUFJLENBQUM7YUFDaEIsTUFBTSxJQUFHLFFBQU8sS0FBSyx5Q0FBTCxLQUFLLE1BQUksUUFBUSxFQUFFO0FBQ2hDLHdCQUFRLENBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztBQUM3Qix5QkFBUzthQUNaOzs7QUFBQSxBQUdELGdCQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBRSxJQUFJLENBQUUsQ0FBQztBQUM5QyxnQkFBRyxDQUFDLE9BQU8sRUFBRSxTQUFTOztBQUV0QixnQkFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDOztBQUUzQyxvQkFBTyxJQUFJO0FBQ1A7QUFDSSwyQkFBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7QUFDdEIsMEJBQU07O0FBQUEsQUFFVixxQkFBSyxPQUFPLENBQUM7QUFDYixxQkFBSyxVQUFVO0FBQ1gsd0JBQUksR0FBRyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7QUFDekIsd0JBQUksQ0FBQyxHQUFHLEVBQUU7QUFDTiwrQkFBTyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO3FCQUM3QixNQUFNO0FBQ0gsNkJBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQyxFQUFFLEVBQUU7QUFDdkIsbUNBQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLEdBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEFBQUMsQ0FBQzt5QkFDL0Q7cUJBQ0o7O0FBRUQsMEJBQU07O0FBQUEsQUFFVixxQkFBSyxpQkFBaUI7QUFDbEIsd0JBQUksTUFBTSxHQUFHLEtBQUssQ0FBQyxXQUFXLElBQUksS0FBSyxHQUFHLEtBQUssR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDOztBQUUxRCx5QkFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO0FBQzVDLCtCQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSyxNQUFNLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEFBQUUsQ0FBQztxQkFDbkY7QUFDRCwwQkFBTTs7QUFBQSxBQUVWLHFCQUFLLFFBQVEsQ0FBQztBQUNkLHFCQUFLLFlBQVk7QUFDYiwyQkFBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksS0FBSyxDQUFDO0FBQzFDLDBCQUFNOztBQUFBLGFBRWI7QUFDRCxtQkFBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3BCO0tBQUE7Q0FFSixDQUFDOztrQkFFYSxRQUFRIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsImZ1bmN0aW9uIGZvcm1Ub09iaihmb3JtKSB7XG4gIHZhciBmaWVsZHMgPSBmb3JtVG9BcnIoZm9ybSk7XG5cbiAgZmllbGRzLnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcbiAgICByZXR1cm4gYS5uYW1lLmxvY2FsZUNvbXBhcmUoYi5uYW1lKTtcbiAgfSk7XG5cbiAgcmV0dXJuIGZpZWxkcy5yZWR1Y2UoZnVuY3Rpb24ob2JqLCBmaWVsZCkge1xuICAgIGFkZFByb3Aob2JqLCBmaWVsZC5uYW1lLCBmaWVsZC52YWx1ZSk7XG4gICAgcmV0dXJuIG9iajtcbiAgfSwge30pO1xuXG4gIGZ1bmN0aW9uIGZvcm1Ub0Fycihmb3JtKSB7XG4gICAgdmFyIGlucHV0cyA9IGZvcm0ucXVlcnlTZWxlY3RvckFsbCgnaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QsIFtjb250ZW50ZWRpdGFibGU9dHJ1ZV0nKTtcbiAgICB2YXIgYXJyID0gW107XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGlucHV0cy5sZW5ndGg7ICsraSkge1xuICAgICAgdmFyIGlucHV0ID0gaW5wdXRzW2ldLFxuICAgICAgICAgIG5hbWUgPSBpbnB1dC5uYW1lIHx8IGlucHV0LmdldEF0dHJpYnV0ZSgnZGF0YS1uYW1lJyksXG4gICAgICAgICAgdmFsID0gaW5wdXQudmFsdWU7XG5cbiAgICAgIGlmICghbmFtZSB8fFxuICAgICAgICAoKGlucHV0LnR5cGUgPT09ICdjaGVja2JveCcgfHwgaW5wdXQudHlwZSA9PT0gJ3JhZGlvJykgJiYgIWlucHV0LmNoZWNrZWQpKSB7XG4gICAgICAgIGNvbnRpbnVlO1xuICAgICAgfVxuXG4gICAgICBpZiAoaW5wdXQuZ2V0QXR0cmlidXRlKCdjb250ZW50ZWRpdGFibGUnKSA9PT0gJ3RydWUnKSB7XG4gICAgICAgIHZhbCA9IGlucHV0LmlubmVySFRNTDtcbiAgICAgIH1cblxuICAgICAgYXJyLnB1c2goe1xuICAgICAgICBuYW1lOiBuYW1lLFxuICAgICAgICB2YWx1ZTogdmFsXG4gICAgICB9KTtcbiAgICB9XG5cbiAgICByZXR1cm4gYXJyO1xuICB9XG5cbiAgZnVuY3Rpb24gYWRkUHJvcChvLCBwcm9wLCB2YWwpIHtcbiAgICB2YXIgcHJvcHMgPSBwcm9wLnNwbGl0KCcuJyk7XG4gICAgdmFyIGxhc3RQcm9wID0gcHJvcHMubGVuZ3RoIC0gMTtcblxuICAgIHByb3BzLnJlZHVjZShmdW5jdGlvbiAob2JqLCBwcm9wLCBpKSB7XG4gICAgICBpZiAoaSA9PT0gbGFzdFByb3ApIHtcbiAgICAgICAgcmV0dXJuIHNldFByb3Aob2JqLCBwcm9wLCB2YWwpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgcmV0dXJuIHNldFByb3Aob2JqLCBwcm9wLCB7fSk7XG4gICAgICB9XG4gICAgfSwgbyk7XG4gIH1cblxuICBmdW5jdGlvbiBzZXRQcm9wKG9iaiwgbmFtZSwgdmFsKSB7XG4gICAgaWYgKG5hbWUuc2xpY2UoLTIpID09PSAnW10nKSB7XG4gICAgICBtYWtlQXJyKG9iaiwgbmFtZSkucHVzaCh2YWwpO1xuICAgIH0gZWxzZSBpZiAob2JqW25hbWVdKSB7XG4gICAgICByZXR1cm4gb2JqW25hbWVdO1xuICAgIH0gZWxzZSBpZiAobmFtZVtuYW1lLmxlbmd0aCAtIDFdID09PSAnXScpIHtcbiAgICAgIHZhciBhcnIgPSBtYWtlQXJyKG9iaiwgbmFtZSk7XG5cbiAgICAgIGlmIChhcnIucHJldk5hbWUgPT09IG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIGFyclthcnIubGVuZ3RoIC0gMV07XG4gICAgICB9XG5cbiAgICAgIGFyci5wdXNoKHZhbCk7XG4gICAgICBhcnIucHJldk5hbWUgPSBuYW1lO1xuICAgIH0gZWxzZSB7XG4gICAgICBvYmpbbmFtZV0gPSB2YWw7XG4gICAgfVxuXG4gICAgcmV0dXJuIHZhbDtcbiAgfVxuXG4gIGZ1bmN0aW9uIG1ha2VBcnIob2JqLCBuYW1lKSB7XG4gICAgdmFyIGFyck5hbWUgPSBuYW1lLnJlcGxhY2UoL1xcW1xcZCpcXF0vLCAnJyk7XG4gICAgcmV0dXJuIChvYmpbYXJyTmFtZV0gfHwgKG9ialthcnJOYW1lXSA9IFtdKSk7XG4gIH1cbn1cblxuaWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIG1vZHVsZS5leHBvcnRzKSB7XG4gIG1vZHVsZS5leHBvcnRzID0gZm9ybVRvT2JqO1xufSIsIlwidXNlIHN0cmljdFwiO1xudmFyIHdpbmRvdyA9IHJlcXVpcmUoXCJnbG9iYWwvd2luZG93XCIpXG52YXIgb25jZSA9IHJlcXVpcmUoXCJvbmNlXCIpXG52YXIgaXNGdW5jdGlvbiA9IHJlcXVpcmUoXCJpcy1mdW5jdGlvblwiKVxudmFyIHBhcnNlSGVhZGVycyA9IHJlcXVpcmUoXCJwYXJzZS1oZWFkZXJzXCIpXG52YXIgeHRlbmQgPSByZXF1aXJlKFwieHRlbmRcIilcblxubW9kdWxlLmV4cG9ydHMgPSBjcmVhdGVYSFJcbmNyZWF0ZVhIUi5YTUxIdHRwUmVxdWVzdCA9IHdpbmRvdy5YTUxIdHRwUmVxdWVzdCB8fCBub29wXG5jcmVhdGVYSFIuWERvbWFpblJlcXVlc3QgPSBcIndpdGhDcmVkZW50aWFsc1wiIGluIChuZXcgY3JlYXRlWEhSLlhNTEh0dHBSZXF1ZXN0KCkpID8gY3JlYXRlWEhSLlhNTEh0dHBSZXF1ZXN0IDogd2luZG93LlhEb21haW5SZXF1ZXN0XG5cbmZvckVhY2hBcnJheShbXCJnZXRcIiwgXCJwdXRcIiwgXCJwb3N0XCIsIFwicGF0Y2hcIiwgXCJoZWFkXCIsIFwiZGVsZXRlXCJdLCBmdW5jdGlvbihtZXRob2QpIHtcbiAgICBjcmVhdGVYSFJbbWV0aG9kID09PSBcImRlbGV0ZVwiID8gXCJkZWxcIiA6IG1ldGhvZF0gPSBmdW5jdGlvbih1cmksIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gICAgICAgIG9wdGlvbnMgPSBpbml0UGFyYW1zKHVyaSwgb3B0aW9ucywgY2FsbGJhY2spXG4gICAgICAgIG9wdGlvbnMubWV0aG9kID0gbWV0aG9kLnRvVXBwZXJDYXNlKClcbiAgICAgICAgcmV0dXJuIF9jcmVhdGVYSFIob3B0aW9ucylcbiAgICB9XG59KVxuXG5mdW5jdGlvbiBmb3JFYWNoQXJyYXkoYXJyYXksIGl0ZXJhdG9yKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBhcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICBpdGVyYXRvcihhcnJheVtpXSlcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGlzRW1wdHkob2JqKXtcbiAgICBmb3IodmFyIGkgaW4gb2JqKXtcbiAgICAgICAgaWYob2JqLmhhc093blByb3BlcnR5KGkpKSByZXR1cm4gZmFsc2VcbiAgICB9XG4gICAgcmV0dXJuIHRydWVcbn1cblxuZnVuY3Rpb24gaW5pdFBhcmFtcyh1cmksIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gICAgdmFyIHBhcmFtcyA9IHVyaVxuXG4gICAgaWYgKGlzRnVuY3Rpb24ob3B0aW9ucykpIHtcbiAgICAgICAgY2FsbGJhY2sgPSBvcHRpb25zXG4gICAgICAgIGlmICh0eXBlb2YgdXJpID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgICAgICBwYXJhbXMgPSB7dXJpOnVyaX1cbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIHBhcmFtcyA9IHh0ZW5kKG9wdGlvbnMsIHt1cmk6IHVyaX0pXG4gICAgfVxuXG4gICAgcGFyYW1zLmNhbGxiYWNrID0gY2FsbGJhY2tcbiAgICByZXR1cm4gcGFyYW1zXG59XG5cbmZ1bmN0aW9uIGNyZWF0ZVhIUih1cmksIG9wdGlvbnMsIGNhbGxiYWNrKSB7XG4gICAgb3B0aW9ucyA9IGluaXRQYXJhbXModXJpLCBvcHRpb25zLCBjYWxsYmFjaylcbiAgICByZXR1cm4gX2NyZWF0ZVhIUihvcHRpb25zKVxufVxuXG5mdW5jdGlvbiBfY3JlYXRlWEhSKG9wdGlvbnMpIHtcbiAgICB2YXIgY2FsbGJhY2sgPSBvcHRpb25zLmNhbGxiYWNrXG4gICAgaWYodHlwZW9mIGNhbGxiYWNrID09PSBcInVuZGVmaW5lZFwiKXtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiY2FsbGJhY2sgYXJndW1lbnQgbWlzc2luZ1wiKVxuICAgIH1cbiAgICBjYWxsYmFjayA9IG9uY2UoY2FsbGJhY2spXG5cbiAgICBmdW5jdGlvbiByZWFkeXN0YXRlY2hhbmdlKCkge1xuICAgICAgICBpZiAoeGhyLnJlYWR5U3RhdGUgPT09IDQpIHtcbiAgICAgICAgICAgIGxvYWRGdW5jKClcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGZ1bmN0aW9uIGdldEJvZHkoKSB7XG4gICAgICAgIC8vIENocm9tZSB3aXRoIHJlcXVlc3RUeXBlPWJsb2IgdGhyb3dzIGVycm9ycyBhcnJvdW5kIHdoZW4gZXZlbiB0ZXN0aW5nIGFjY2VzcyB0byByZXNwb25zZVRleHRcbiAgICAgICAgdmFyIGJvZHkgPSB1bmRlZmluZWRcblxuICAgICAgICBpZiAoeGhyLnJlc3BvbnNlKSB7XG4gICAgICAgICAgICBib2R5ID0geGhyLnJlc3BvbnNlXG4gICAgICAgIH0gZWxzZSBpZiAoeGhyLnJlc3BvbnNlVHlwZSA9PT0gXCJ0ZXh0XCIgfHwgIXhoci5yZXNwb25zZVR5cGUpIHtcbiAgICAgICAgICAgIGJvZHkgPSB4aHIucmVzcG9uc2VUZXh0IHx8IHhoci5yZXNwb25zZVhNTFxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGlzSnNvbikge1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBib2R5ID0gSlNPTi5wYXJzZShib2R5KVxuICAgICAgICAgICAgfSBjYXRjaCAoZSkge31cbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBib2R5XG4gICAgfVxuXG4gICAgdmFyIGZhaWx1cmVSZXNwb25zZSA9IHtcbiAgICAgICAgICAgICAgICBib2R5OiB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogMCxcbiAgICAgICAgICAgICAgICBtZXRob2Q6IG1ldGhvZCxcbiAgICAgICAgICAgICAgICB1cmw6IHVyaSxcbiAgICAgICAgICAgICAgICByYXdSZXF1ZXN0OiB4aHJcbiAgICAgICAgICAgIH1cblxuICAgIGZ1bmN0aW9uIGVycm9yRnVuYyhldnQpIHtcbiAgICAgICAgY2xlYXJUaW1lb3V0KHRpbWVvdXRUaW1lcilcbiAgICAgICAgaWYoIShldnQgaW5zdGFuY2VvZiBFcnJvcikpe1xuICAgICAgICAgICAgZXZ0ID0gbmV3IEVycm9yKFwiXCIgKyAoZXZ0IHx8IFwiVW5rbm93biBYTUxIdHRwUmVxdWVzdCBFcnJvclwiKSApXG4gICAgICAgIH1cbiAgICAgICAgZXZ0LnN0YXR1c0NvZGUgPSAwXG4gICAgICAgIGNhbGxiYWNrKGV2dCwgZmFpbHVyZVJlc3BvbnNlKVxuICAgIH1cblxuICAgIC8vIHdpbGwgbG9hZCB0aGUgZGF0YSAmIHByb2Nlc3MgdGhlIHJlc3BvbnNlIGluIGEgc3BlY2lhbCByZXNwb25zZSBvYmplY3RcbiAgICBmdW5jdGlvbiBsb2FkRnVuYygpIHtcbiAgICAgICAgaWYgKGFib3J0ZWQpIHJldHVyblxuICAgICAgICB2YXIgc3RhdHVzXG4gICAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0VGltZXIpXG4gICAgICAgIGlmKG9wdGlvbnMudXNlWERSICYmIHhoci5zdGF0dXM9PT11bmRlZmluZWQpIHtcbiAgICAgICAgICAgIC8vSUU4IENPUlMgR0VUIHN1Y2Nlc3NmdWwgcmVzcG9uc2UgZG9lc24ndCBoYXZlIGEgc3RhdHVzIGZpZWxkLCBidXQgYm9keSBpcyBmaW5lXG4gICAgICAgICAgICBzdGF0dXMgPSAyMDBcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHN0YXR1cyA9ICh4aHIuc3RhdHVzID09PSAxMjIzID8gMjA0IDogeGhyLnN0YXR1cylcbiAgICAgICAgfVxuICAgICAgICB2YXIgcmVzcG9uc2UgPSBmYWlsdXJlUmVzcG9uc2VcbiAgICAgICAgdmFyIGVyciA9IG51bGxcblxuICAgICAgICBpZiAoc3RhdHVzICE9PSAwKXtcbiAgICAgICAgICAgIHJlc3BvbnNlID0ge1xuICAgICAgICAgICAgICAgIGJvZHk6IGdldEJvZHkoKSxcbiAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiBzdGF0dXMsXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBtZXRob2QsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge30sXG4gICAgICAgICAgICAgICAgdXJsOiB1cmksXG4gICAgICAgICAgICAgICAgcmF3UmVxdWVzdDogeGhyXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZih4aHIuZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKXsgLy9yZW1lbWJlciB4aHIgY2FuIGluIGZhY3QgYmUgWERSIGZvciBDT1JTIGluIElFXG4gICAgICAgICAgICAgICAgcmVzcG9uc2UuaGVhZGVycyA9IHBhcnNlSGVhZGVycyh4aHIuZ2V0QWxsUmVzcG9uc2VIZWFkZXJzKCkpXG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBlcnIgPSBuZXcgRXJyb3IoXCJJbnRlcm5hbCBYTUxIdHRwUmVxdWVzdCBFcnJvclwiKVxuICAgICAgICB9XG4gICAgICAgIGNhbGxiYWNrKGVyciwgcmVzcG9uc2UsIHJlc3BvbnNlLmJvZHkpXG5cbiAgICB9XG5cbiAgICB2YXIgeGhyID0gb3B0aW9ucy54aHIgfHwgbnVsbFxuXG4gICAgaWYgKCF4aHIpIHtcbiAgICAgICAgaWYgKG9wdGlvbnMuY29ycyB8fCBvcHRpb25zLnVzZVhEUikge1xuICAgICAgICAgICAgeGhyID0gbmV3IGNyZWF0ZVhIUi5YRG9tYWluUmVxdWVzdCgpXG4gICAgICAgIH1lbHNle1xuICAgICAgICAgICAgeGhyID0gbmV3IGNyZWF0ZVhIUi5YTUxIdHRwUmVxdWVzdCgpXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIga2V5XG4gICAgdmFyIGFib3J0ZWRcbiAgICB2YXIgdXJpID0geGhyLnVybCA9IG9wdGlvbnMudXJpIHx8IG9wdGlvbnMudXJsXG4gICAgdmFyIG1ldGhvZCA9IHhoci5tZXRob2QgPSBvcHRpb25zLm1ldGhvZCB8fCBcIkdFVFwiXG4gICAgdmFyIGJvZHkgPSBvcHRpb25zLmJvZHkgfHwgb3B0aW9ucy5kYXRhIHx8IG51bGxcbiAgICB2YXIgaGVhZGVycyA9IHhoci5oZWFkZXJzID0gb3B0aW9ucy5oZWFkZXJzIHx8IHt9XG4gICAgdmFyIHN5bmMgPSAhIW9wdGlvbnMuc3luY1xuICAgIHZhciBpc0pzb24gPSBmYWxzZVxuICAgIHZhciB0aW1lb3V0VGltZXJcblxuICAgIGlmIChcImpzb25cIiBpbiBvcHRpb25zKSB7XG4gICAgICAgIGlzSnNvbiA9IHRydWVcbiAgICAgICAgaGVhZGVyc1tcImFjY2VwdFwiXSB8fCBoZWFkZXJzW1wiQWNjZXB0XCJdIHx8IChoZWFkZXJzW1wiQWNjZXB0XCJdID0gXCJhcHBsaWNhdGlvbi9qc29uXCIpIC8vRG9uJ3Qgb3ZlcnJpZGUgZXhpc3RpbmcgYWNjZXB0IGhlYWRlciBkZWNsYXJlZCBieSB1c2VyXG4gICAgICAgIGlmIChtZXRob2QgIT09IFwiR0VUXCIgJiYgbWV0aG9kICE9PSBcIkhFQURcIikge1xuICAgICAgICAgICAgaGVhZGVyc1tcImNvbnRlbnQtdHlwZVwiXSB8fCBoZWFkZXJzW1wiQ29udGVudC1UeXBlXCJdIHx8IChoZWFkZXJzW1wiQ29udGVudC1UeXBlXCJdID0gXCJhcHBsaWNhdGlvbi9qc29uXCIpIC8vRG9uJ3Qgb3ZlcnJpZGUgZXhpc3RpbmcgYWNjZXB0IGhlYWRlciBkZWNsYXJlZCBieSB1c2VyXG4gICAgICAgICAgICBib2R5ID0gSlNPTi5zdHJpbmdpZnkob3B0aW9ucy5qc29uKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgeGhyLm9ucmVhZHlzdGF0ZWNoYW5nZSA9IHJlYWR5c3RhdGVjaGFuZ2VcbiAgICB4aHIub25sb2FkID0gbG9hZEZ1bmNcbiAgICB4aHIub25lcnJvciA9IGVycm9yRnVuY1xuICAgIC8vIElFOSBtdXN0IGhhdmUgb25wcm9ncmVzcyBiZSBzZXQgdG8gYSB1bmlxdWUgZnVuY3Rpb24uXG4gICAgeGhyLm9ucHJvZ3Jlc3MgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIC8vIElFIG11c3QgZGllXG4gICAgfVxuICAgIHhoci5vbnRpbWVvdXQgPSBlcnJvckZ1bmNcbiAgICB4aHIub3BlbihtZXRob2QsIHVyaSwgIXN5bmMsIG9wdGlvbnMudXNlcm5hbWUsIG9wdGlvbnMucGFzc3dvcmQpXG4gICAgLy9oYXMgdG8gYmUgYWZ0ZXIgb3BlblxuICAgIGlmKCFzeW5jKSB7XG4gICAgICAgIHhoci53aXRoQ3JlZGVudGlhbHMgPSAhIW9wdGlvbnMud2l0aENyZWRlbnRpYWxzXG4gICAgfVxuICAgIC8vIENhbm5vdCBzZXQgdGltZW91dCB3aXRoIHN5bmMgcmVxdWVzdFxuICAgIC8vIG5vdCBzZXR0aW5nIHRpbWVvdXQgb24gdGhlIHhociBvYmplY3QsIGJlY2F1c2Ugb2Ygb2xkIHdlYmtpdHMgZXRjLiBub3QgaGFuZGxpbmcgdGhhdCBjb3JyZWN0bHlcbiAgICAvLyBib3RoIG5wbSdzIHJlcXVlc3QgYW5kIGpxdWVyeSAxLnggdXNlIHRoaXMga2luZCBvZiB0aW1lb3V0LCBzbyB0aGlzIGlzIGJlaW5nIGNvbnNpc3RlbnRcbiAgICBpZiAoIXN5bmMgJiYgb3B0aW9ucy50aW1lb3V0ID4gMCApIHtcbiAgICAgICAgdGltZW91dFRpbWVyID0gc2V0VGltZW91dChmdW5jdGlvbigpe1xuICAgICAgICAgICAgYWJvcnRlZD10cnVlLy9JRTkgbWF5IHN0aWxsIGNhbGwgcmVhZHlzdGF0ZWNoYW5nZVxuICAgICAgICAgICAgeGhyLmFib3J0KFwidGltZW91dFwiKVxuICAgICAgICAgICAgdmFyIGUgPSBuZXcgRXJyb3IoXCJYTUxIdHRwUmVxdWVzdCB0aW1lb3V0XCIpXG4gICAgICAgICAgICBlLmNvZGUgPSBcIkVUSU1FRE9VVFwiXG4gICAgICAgICAgICBlcnJvckZ1bmMoZSlcbiAgICAgICAgfSwgb3B0aW9ucy50aW1lb3V0IClcbiAgICB9XG5cbiAgICBpZiAoeGhyLnNldFJlcXVlc3RIZWFkZXIpIHtcbiAgICAgICAgZm9yKGtleSBpbiBoZWFkZXJzKXtcbiAgICAgICAgICAgIGlmKGhlYWRlcnMuaGFzT3duUHJvcGVydHkoa2V5KSl7XG4gICAgICAgICAgICAgICAgeGhyLnNldFJlcXVlc3RIZWFkZXIoa2V5LCBoZWFkZXJzW2tleV0pXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG9wdGlvbnMuaGVhZGVycyAmJiAhaXNFbXB0eShvcHRpb25zLmhlYWRlcnMpKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIkhlYWRlcnMgY2Fubm90IGJlIHNldCBvbiBhbiBYRG9tYWluUmVxdWVzdCBvYmplY3RcIilcbiAgICB9XG5cbiAgICBpZiAoXCJyZXNwb25zZVR5cGVcIiBpbiBvcHRpb25zKSB7XG4gICAgICAgIHhoci5yZXNwb25zZVR5cGUgPSBvcHRpb25zLnJlc3BvbnNlVHlwZVxuICAgIH1cblxuICAgIGlmIChcImJlZm9yZVNlbmRcIiBpbiBvcHRpb25zICYmXG4gICAgICAgIHR5cGVvZiBvcHRpb25zLmJlZm9yZVNlbmQgPT09IFwiZnVuY3Rpb25cIlxuICAgICkge1xuICAgICAgICBvcHRpb25zLmJlZm9yZVNlbmQoeGhyKVxuICAgIH1cblxuICAgIHhoci5zZW5kKGJvZHkpXG5cbiAgICByZXR1cm4geGhyXG5cblxufVxuXG5mdW5jdGlvbiBub29wKCkge31cbiIsImlmICh0eXBlb2Ygd2luZG93ICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSB3aW5kb3c7XG59IGVsc2UgaWYgKHR5cGVvZiBnbG9iYWwgIT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IGdsb2JhbDtcbn0gZWxzZSBpZiAodHlwZW9mIHNlbGYgIT09IFwidW5kZWZpbmVkXCIpe1xuICAgIG1vZHVsZS5leHBvcnRzID0gc2VsZjtcbn0gZWxzZSB7XG4gICAgbW9kdWxlLmV4cG9ydHMgPSB7fTtcbn1cbiIsIm1vZHVsZS5leHBvcnRzID0gaXNGdW5jdGlvblxuXG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nXG5cbmZ1bmN0aW9uIGlzRnVuY3Rpb24gKGZuKSB7XG4gIHZhciBzdHJpbmcgPSB0b1N0cmluZy5jYWxsKGZuKVxuICByZXR1cm4gc3RyaW5nID09PSAnW29iamVjdCBGdW5jdGlvbl0nIHx8XG4gICAgKHR5cGVvZiBmbiA9PT0gJ2Z1bmN0aW9uJyAmJiBzdHJpbmcgIT09ICdbb2JqZWN0IFJlZ0V4cF0nKSB8fFxuICAgICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJyAmJlxuICAgICAvLyBJRTggYW5kIGJlbG93XG4gICAgIChmbiA9PT0gd2luZG93LnNldFRpbWVvdXQgfHxcbiAgICAgIGZuID09PSB3aW5kb3cuYWxlcnQgfHxcbiAgICAgIGZuID09PSB3aW5kb3cuY29uZmlybSB8fFxuICAgICAgZm4gPT09IHdpbmRvdy5wcm9tcHQpKVxufTtcbiIsIm1vZHVsZS5leHBvcnRzID0gb25jZVxuXG5vbmNlLnByb3RvID0gb25jZShmdW5jdGlvbiAoKSB7XG4gIE9iamVjdC5kZWZpbmVQcm9wZXJ0eShGdW5jdGlvbi5wcm90b3R5cGUsICdvbmNlJywge1xuICAgIHZhbHVlOiBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gb25jZSh0aGlzKVxuICAgIH0sXG4gICAgY29uZmlndXJhYmxlOiB0cnVlXG4gIH0pXG59KVxuXG5mdW5jdGlvbiBvbmNlIChmbikge1xuICB2YXIgY2FsbGVkID0gZmFsc2VcbiAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICBpZiAoY2FsbGVkKSByZXR1cm5cbiAgICBjYWxsZWQgPSB0cnVlXG4gICAgcmV0dXJuIGZuLmFwcGx5KHRoaXMsIGFyZ3VtZW50cylcbiAgfVxufVxuIiwidmFyIGlzRnVuY3Rpb24gPSByZXF1aXJlKCdpcy1mdW5jdGlvbicpXG5cbm1vZHVsZS5leHBvcnRzID0gZm9yRWFjaFxuXG52YXIgdG9TdHJpbmcgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nXG52YXIgaGFzT3duUHJvcGVydHkgPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5XG5cbmZ1bmN0aW9uIGZvckVhY2gobGlzdCwgaXRlcmF0b3IsIGNvbnRleHQpIHtcbiAgICBpZiAoIWlzRnVuY3Rpb24oaXRlcmF0b3IpKSB7XG4gICAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ2l0ZXJhdG9yIG11c3QgYmUgYSBmdW5jdGlvbicpXG4gICAgfVxuXG4gICAgaWYgKGFyZ3VtZW50cy5sZW5ndGggPCAzKSB7XG4gICAgICAgIGNvbnRleHQgPSB0aGlzXG4gICAgfVxuICAgIFxuICAgIGlmICh0b1N0cmluZy5jYWxsKGxpc3QpID09PSAnW29iamVjdCBBcnJheV0nKVxuICAgICAgICBmb3JFYWNoQXJyYXkobGlzdCwgaXRlcmF0b3IsIGNvbnRleHQpXG4gICAgZWxzZSBpZiAodHlwZW9mIGxpc3QgPT09ICdzdHJpbmcnKVxuICAgICAgICBmb3JFYWNoU3RyaW5nKGxpc3QsIGl0ZXJhdG9yLCBjb250ZXh0KVxuICAgIGVsc2VcbiAgICAgICAgZm9yRWFjaE9iamVjdChsaXN0LCBpdGVyYXRvciwgY29udGV4dClcbn1cblxuZnVuY3Rpb24gZm9yRWFjaEFycmF5KGFycmF5LCBpdGVyYXRvciwgY29udGV4dCkge1xuICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBhcnJheS5sZW5ndGg7IGkgPCBsZW47IGkrKykge1xuICAgICAgICBpZiAoaGFzT3duUHJvcGVydHkuY2FsbChhcnJheSwgaSkpIHtcbiAgICAgICAgICAgIGl0ZXJhdG9yLmNhbGwoY29udGV4dCwgYXJyYXlbaV0sIGksIGFycmF5KVxuICAgICAgICB9XG4gICAgfVxufVxuXG5mdW5jdGlvbiBmb3JFYWNoU3RyaW5nKHN0cmluZywgaXRlcmF0b3IsIGNvbnRleHQpIHtcbiAgICBmb3IgKHZhciBpID0gMCwgbGVuID0gc3RyaW5nLmxlbmd0aDsgaSA8IGxlbjsgaSsrKSB7XG4gICAgICAgIC8vIG5vIHN1Y2ggdGhpbmcgYXMgYSBzcGFyc2Ugc3RyaW5nLlxuICAgICAgICBpdGVyYXRvci5jYWxsKGNvbnRleHQsIHN0cmluZy5jaGFyQXQoaSksIGksIHN0cmluZylcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGZvckVhY2hPYmplY3Qob2JqZWN0LCBpdGVyYXRvciwgY29udGV4dCkge1xuICAgIGZvciAodmFyIGsgaW4gb2JqZWN0KSB7XG4gICAgICAgIGlmIChoYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgaykpIHtcbiAgICAgICAgICAgIGl0ZXJhdG9yLmNhbGwoY29udGV4dCwgb2JqZWN0W2tdLCBrLCBvYmplY3QpXG4gICAgICAgIH1cbiAgICB9XG59XG4iLCJcbmV4cG9ydHMgPSBtb2R1bGUuZXhwb3J0cyA9IHRyaW07XG5cbmZ1bmN0aW9uIHRyaW0oc3RyKXtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC9eXFxzKnxcXHMqJC9nLCAnJyk7XG59XG5cbmV4cG9ydHMubGVmdCA9IGZ1bmN0aW9uKHN0cil7XG4gIHJldHVybiBzdHIucmVwbGFjZSgvXlxccyovLCAnJyk7XG59O1xuXG5leHBvcnRzLnJpZ2h0ID0gZnVuY3Rpb24oc3RyKXtcbiAgcmV0dXJuIHN0ci5yZXBsYWNlKC9cXHMqJC8sICcnKTtcbn07XG4iLCJ2YXIgdHJpbSA9IHJlcXVpcmUoJ3RyaW0nKVxuICAsIGZvckVhY2ggPSByZXF1aXJlKCdmb3ItZWFjaCcpXG4gICwgaXNBcnJheSA9IGZ1bmN0aW9uKGFyZykge1xuICAgICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcmcpID09PSAnW29iamVjdCBBcnJheV0nO1xuICAgIH1cblxubW9kdWxlLmV4cG9ydHMgPSBmdW5jdGlvbiAoaGVhZGVycykge1xuICBpZiAoIWhlYWRlcnMpXG4gICAgcmV0dXJuIHt9XG5cbiAgdmFyIHJlc3VsdCA9IHt9XG5cbiAgZm9yRWFjaChcbiAgICAgIHRyaW0oaGVhZGVycykuc3BsaXQoJ1xcbicpXG4gICAgLCBmdW5jdGlvbiAocm93KSB7XG4gICAgICAgIHZhciBpbmRleCA9IHJvdy5pbmRleE9mKCc6JylcbiAgICAgICAgICAsIGtleSA9IHRyaW0ocm93LnNsaWNlKDAsIGluZGV4KSkudG9Mb3dlckNhc2UoKVxuICAgICAgICAgICwgdmFsdWUgPSB0cmltKHJvdy5zbGljZShpbmRleCArIDEpKVxuXG4gICAgICAgIGlmICh0eXBlb2YocmVzdWx0W2tleV0pID09PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgIHJlc3VsdFtrZXldID0gdmFsdWVcbiAgICAgICAgfSBlbHNlIGlmIChpc0FycmF5KHJlc3VsdFtrZXldKSkge1xuICAgICAgICAgIHJlc3VsdFtrZXldLnB1c2godmFsdWUpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVzdWx0W2tleV0gPSBbIHJlc3VsdFtrZXldLCB2YWx1ZSBdXG4gICAgICAgIH1cbiAgICAgIH1cbiAgKVxuXG4gIHJldHVybiByZXN1bHRcbn0iLCJtb2R1bGUuZXhwb3J0cyA9IGV4dGVuZFxuXG52YXIgaGFzT3duUHJvcGVydHkgPSBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5O1xuXG5mdW5jdGlvbiBleHRlbmQoKSB7XG4gICAgdmFyIHRhcmdldCA9IHt9XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICB2YXIgc291cmNlID0gYXJndW1lbnRzW2ldXG5cbiAgICAgICAgZm9yICh2YXIga2V5IGluIHNvdXJjZSkge1xuICAgICAgICAgICAgaWYgKGhhc093blByb3BlcnR5LmNhbGwoc291cmNlLCBrZXkpKSB7XG4gICAgICAgICAgICAgICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHRhcmdldFxufVxuIiwiaW1wb3J0IENhdGFsb2cgICAgICBmcm9tICcuL2NvbXBvbmVudHMvQ2F0YWxvZydcclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCAoKSA9PiB7XHJcbiAgICBDYXRhbG9nLmluaXQoJ2NhdGFsb2cnKVxyXG59KTsiLCJpbXBvcnQgQmFzZVdpZGdldCBmcm9tICcuLy4uL2xpYi9CYXNlV2lkZ2V0J1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZUNhdGFsb2dXaWRnZXQgZXh0ZW5kcyBCYXNlV2lkZ2V0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgQmFzZUNhdGFsb2dXaWRnZXQuX2RhdGEgPSBuZXcgTWFwKClcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4gQmFzZUNhdGFsb2dXaWRnZXQuX2RhdGFcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZVdpZGdldCBmcm9tICcuL0Jhc2VDYXRhbG9nV2lkZ2V0J1xyXG5pbXBvcnQgVHJlZSBmcm9tICcuL1RyZWUnXHJcbmltcG9ydCBUYWJzIGZyb20gJy4vY2F0YWxvZy9UYWJzJ1xyXG5pbXBvcnQgTW9kZWwgZnJvbSAnLi9jYXRhbG9nL21vZGVsL01vZGVsJ1xyXG5pbXBvcnQgVXBsb2FkTW9kZWwgZnJvbSAnLi9jYXRhbG9nL21vZGVsL1VwbG9hZE1vZGVsJ1xyXG5pbXBvcnQgT3ZlcmxheSBmcm9tICcuL092ZXJsYXknXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXRhbG9nIGV4dGVuZHMgQmFzZVdpZGdldCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyICAgPSBjb250YWluZXJcclxuICAgICAgICB0aGlzLm1vZGVsICAgICAgID0gbmV3IE1vZGVsKClcclxuICAgICAgICB0aGlzLnVwbG9hZE1vZGVsID0gbmV3IFVwbG9hZE1vZGVsKClcclxuXHJcbiAgICAgICAgdGhpcy53aWRnZXRzXHJcbiAgICAgICAgICAgIC5zZXQoVHJlZS5pbml0KCd0cmVlJywgdGhpcy5tb2RlbCkpXHJcbiAgICAgICAgICAgIC5zZXQoVGFicy5pbml0KHRoaXMuY29udGFpbmVyKSlcclxuXHJcbiAgICAgICAgdGhpcy5vbnN1Ym1pdCA9IHRoaXMuc3VibWl0LmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLm9uc2VsZWN0ID0gdGhpcy5zZWxlY3QuYmluZCh0aGlzKVxyXG4gICAgICAgIHRoaXMub25kZWxldGUgPSB0aGlzLnJlbW92ZS5iaW5kKHRoaXMpXHJcblxyXG4gICAgICAgIGxldCB0cmVlID0gdGhpcy5nZXQoJ3RyZWUnKVxyXG4gICAgICAgIHRyZWUubWFrZUFjdGl2ZSh0cmVlLnJvb3QpXHJcblxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJy5jbG9zZS1jb250YWluZXIgYScpLm9uY2xpY2sgPSAoKSA9PiB0aGlzLmdldCgndGFicycpLmhpZGUoKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZClcclxuICAgICAgICByZXR1cm4gbmV3IENhdGFsb2coY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3QoZSkge1xyXG4gICAgICAgIGxldCB0YWJzICA9IHRoaXMuZ2V0KCd0YWJzJylcclxuICAgICAgICBsZXQgaWQgPSBlLnNlbGVjdGVkTm9kZS5kYXRhc2V0LmNhdElkXHJcblxyXG4gICAgICAgIGlmIChpZCA9PT0gJzAnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YS5zZXQoJ2NhdGFsb2cnLCB7aWR9KVxyXG4gICAgICAgICAgICB0YWJzLnNob3dPbmx5QWRkVGFiKClcclxuICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgdHJlZSAgICA9IHRoaXMuZ2V0KCd0cmVlJylcclxuICAgICAgICBsZXQgb3ZlcmxheSA9IE92ZXJsYXkuaW5zdGFuY2VcclxuXHJcbiAgICAgICAgdGFicy5lbmFibGUoKVxyXG4gICAgICAgIG92ZXJsYXkuc2V0VGV4dCgnbG9hZCBkYXRhJykuZW5hYmxlKClcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbC5nZXQoaWQpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGEuc2V0KCdjYXRhbG9nJywgZGF0YSlcclxuICAgICAgICAgICAgICAgIGlmIChkYXRhLmNoaWxkcmVuX2NvdW50KSByZXR1cm4gdGhpcy5tb2RlbC5nZXRDaGlsZHJlbihkYXRhLmlkKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY2F0YWxvZ0RhdGEgPSB0aGlzLmRhdGEuZ2V0KCdjYXRhbG9nJylcclxuXHJcbiAgICAgICAgICAgICAgICBpZihjYXRhbG9nRGF0YS5pbWcpIHtcclxuICAgICAgICAgICAgICAgICAgICB0YWJzLnRhYmxlVmlldy52aWV3SW1nKGNhdGFsb2dEYXRhLmltZy5wYXRoKVxyXG4gICAgICAgICAgICAgICAgICAgIHRhYnMuc2hvdygpXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkTW9kZWxcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmdldEJ5Q2F0YWxvZyh0aGlzLmRhdGEuZ2V0KCdjYXRhbG9nJykuaWQpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEuZ2V0KCdjYXRhbG9nJykuaW1nID0gZGF0YVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFicy5zaG93KClcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4gY29uc29sZS5sb2coZXJyb3IpKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiBjb25zb2xlLmxvZyhlcnJvcikpXHJcbiAgICB9XHJcblxyXG4gICAgc3VibWl0KGUpIHtcclxuICAgICAgICBpZiAoZS50YXJnZXQuZXF1YWxzKHRoaXMsICd0YWJzID4gdGFicyA+IGZvcm0nKSkgdGhpcy5zdWJtaXREYXRhKGUpXHJcbiAgICAgICAgaWYgKGUudGFyZ2V0LmVxdWFscyh0aGlzLCAndGFicyA+IHRhYnMgPiB1cGxvYWRGb3JtJykpIHRoaXMuc3VibWl0VXBsb2FkRGF0YShlKVxyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdFVwbG9hZERhdGEoZSkge1xyXG4gICAgICAgIGxldCBmb3JtICAgID0gZS50YXJnZXRcclxuICAgICAgICBsZXQgZGF0YSAgICA9IGUuZGF0YVxyXG4gICAgICAgIGxldCBvdmVybGF5ID0gT3ZlcmxheS5pbnN0YW5jZVxyXG4gICAgICAgIGxldCB7aWR9ICAgID0gdGhpcy5kYXRhLmdldCgnY2F0YWxvZycpXHJcblxyXG4gICAgICAgIGRhdGEuZGF0YS5jYXRhbG9nX2lkID0gaWRcclxuICAgICAgICBkYXRhLmZvcm1EYXRhLmFwcGVuZCgnY2F0YWxvZ19pZCcsIGlkKVxyXG5cclxuICAgICAgICBmb3JtLmltZy5vbmxvYWQgPSAoKSA9PiBvdmVybGF5LmRpc2FibGUoKVxyXG5cclxuICAgICAgICBvdmVybGF5LmVuYWJsZSgpXHJcblxyXG4gICAgICAgIHRoaXMudXBsb2FkTW9kZWxcclxuICAgICAgICAgICAgLnNldChkYXRhKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIGRhdGEuY2F0YWxvZ19pZFxyXG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhLmdldCgnY2F0YWxvZycpLmltZyA9IGRhdGFcclxuICAgICAgICAgICAgICAgIGZvcm0udmlld0ltZyhkYXRhLnBhdGgpXHJcbiAgICAgICAgICAgICAgICBmb3JtLnJlc2V0KClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcnMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlcnJvcnMgaW5zdGFuY2VvZiBFcnJvcikgb3ZlcmxheS5zZXRUZXh0KGVycm9ycy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ybS5zZXRFcnJvcnMoZXJyb3JzKVxyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgc3VibWl0RGF0YShlKSB7XHJcbiAgICAgICAgbGV0IGZvcm0gICAgPSBlLnRhcmdldFxyXG4gICAgICAgIGxldCBkYXRhICAgID0gZS5kYXRhXHJcbiAgICAgICAgbGV0IG92ZXJsYXkgPSBPdmVybGF5Lmluc3RhbmNlXHJcblxyXG4gICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbFxyXG4gICAgICAgICAgICAuc2V0KGRhdGEpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5kYXRhLmhhcygnY2F0YWxvZycpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNhdGFsb2dEYXRhID0gdGhpcy5kYXRhLmdldCgnY2F0YWxvZycpXHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGlzUmVzYXZlSW1nID0gY2F0YWxvZ0RhdGEuaWQgPT09IGRhdGEuaWQgJiYgY2F0YWxvZ0RhdGEuaW1nICYmICFkYXRhLmltZ1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChpc1Jlc2F2ZUltZykgZGF0YS5pbWcgPSBjYXRhbG9nRGF0YS5pbWdcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGEuc2V0KCdjYXRhbG9nJywgZGF0YSlcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0KCd0YWJzJykuc2hvdygpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyb3JzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3JzIGluc3RhbmNlb2YgRXJyb3IpIG92ZXJsYXkuc2V0VGV4dChlcnJvcnMubWVzc2FnZSlcclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc2V0RXJyb3JzKGVycm9ycylcclxuICAgICAgICAgICAgICAgICAgICBvdmVybGF5LmRpc2FibGUoKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZShlKSB7XHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUudGFyZ2V0XHJcbiAgICAgICAgaWYgKHRhcmdldC5lcXVhbHModGhpcywgJ3RhYnMgPiBkZWxldGVWaWV3JykpIHRoaXMuZGVsZXRlRGF0YShlKVxyXG4gICAgICAgIGlmICh0YXJnZXQuZXF1YWxzKHRoaXMsICd0YWJzID4gdGFicyA+IHVwbG9hZEZvcm0nKSkgdGhpcy5kZWxldGVVcGxvYWREYXRhKGUpXHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlRGF0YShlKSB7XHJcbiAgICAgICAgbGV0IGRlbGV0ZVZpZXcgPSBlLnRhcmdldFxyXG4gICAgICAgIGxldCBvdmVybGF5ICAgID0gT3ZlcmxheS5pbnN0YW5jZVxyXG4gICAgICAgIGxldCBkYXRhICAgICAgID0gdGhpcy5kYXRhLmdldCgnY2F0YWxvZycpXHJcblxyXG4gICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbFxyXG4gICAgICAgICAgICAucmVtb3ZlKGRhdGEuaWQpXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBpbWdJZCA9IChkYXRhLmltZyAmJiBkYXRhLmltZy5pZCkgfHwgbnVsbFxyXG4gICAgICAgICAgICAgICAgaWYgKGltZ0lkKSByZXR1cm4gdGhpcy51cGxvYWRNb2RlbC5yZW1vdmUoaW1nSWQpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IG92ZXJsYXkuZGlzYWJsZSgpKVxyXG4gICAgICAgICAgICAuY2F0Y2goKGVycm9ycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGVycm9ycyBpbnN0YW5jZW9mIEVycm9yKSBvdmVybGF5LnNldFRleHQoZXJyb3JzLm1lc3NhZ2UpXHJcbiAgICAgICAgICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxldGVWaWV3XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5kaXNhYmxlT2tCdG4oKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAuc2hvd0FsZXJ0TWVzc2FnZShlcnJvcnNbT2JqZWN0LmtleXMoZXJyb3JzKVswXV0pXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZGVsZXRlVXBsb2FkRGF0YShlKSB7XHJcbiAgICAgICAgbGV0IG92ZXJsYXkgICAgPSBPdmVybGF5Lmluc3RhbmNlXHJcbiAgICAgICAgbGV0IGRhdGEgICAgICAgPSB0aGlzLmRhdGEuZ2V0KCdjYXRhbG9nJylcclxuICAgICAgICBsZXQgdXBsb2FkRm9ybSA9IGUudGFyZ2V0XHJcblxyXG4gICAgICAgIGxldCBpbWdJZCA9IChkYXRhLmltZyAmJiBkYXRhLmltZy5pZCkgfHwgbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKGltZ0lkKSB7XHJcbiAgICAgICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgICAgIHRoaXMudXBsb2FkTW9kZWxcclxuICAgICAgICAgICAgICAgIC5yZW1vdmUoaW1nSWQpXHJcbiAgICAgICAgICAgICAgICAudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXBsb2FkRm9ybS5yZXNldEltZygpXHJcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAuY2F0Y2goKGVycm9ycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvcnMgaW5zdGFuY2VvZiBFcnJvcikgb3ZlcmxheS5zZXRUZXh0KGVycm9ycy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgICAgIGVsc2Ugb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbnN1Ym1pdChmbikgeyB0aGlzLm9uKCdzdWJtaXQnLCBmbil9XHJcbiAgICBzZXQgb25zaG93bihmbikgIHsgdGhpcy5vbignc2hvd24nLCBmbikgfVxyXG4gICAgc2V0IG9uc2VsZWN0KGZuKSB7IHRoaXMub24oJ3NlbGVjdCcsIGZuKSB9XHJcbiAgICBzZXQgb25kZWxldGUoZm4pIHsgdGhpcy5vbignZGVsZXRlJywgZm4pIH1cclxufVxyXG4iLCJpbXBvcnQgY29uZmlnIGZyb20gJy4uL2NvbmZpZy5qc29uJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgT3ZlcmxheSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXJcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgY3JlYXRlQ29udGFpbmVyKCkge1xyXG4gICAgICAgIGxldCBvdmVybGF5VGV4dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpXHJcbiAgICAgICAgb3ZlcmxheVRleHQuY2xhc3NMaXN0LmFkZCgnb3ZlcmxheS1jb250ZW50JywgJ3ByZWxvYWRlcicpXHJcbiAgICAgICAgb3ZlcmxheVRleHQuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUoY29uZmlnLmkxOG4udWtbJ2RhdGEgcHJvY2Vzc2luZyddKSlcclxuXHJcbiAgICAgICAgbGV0IG92ZXJsYXlDb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKVxyXG4gICAgICAgIG92ZXJsYXlDb250YWluZXIuaWQgPSAnb3ZlcmxheSdcclxuICAgICAgICBvdmVybGF5Q29udGFpbmVyLmNsYXNzTGlzdC5hZGQoJ292ZXJsYXknLCAnaGlkZGVuJylcclxuICAgICAgICBvdmVybGF5Q29udGFpbmVyLmFwcGVuZENoaWxkKG92ZXJsYXlUZXh0KVxyXG5cclxuICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKG92ZXJsYXlDb250YWluZXIpXHJcblxyXG4gICAgICAgIHJldHVybiBvdmVybGF5Q29udGFpbmVyXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGdldCBpbnN0YW5jZSgpIHtcclxuICAgICAgICBpZiAoT3ZlcmxheS5faW5zdGFuY2UgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBPdmVybGF5Ll9pbnN0YW5jZSA9IG5ldyBPdmVybGF5KE92ZXJsYXkuY3JlYXRlQ29udGFpbmVyKCkpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBPdmVybGF5Ll9pbnN0YW5jZVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb250ZW50Qm94KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jb250ZW50Qm94ID09PSB1bmRlZmluZWQpIHRoaXMuX2NvbnRlbnRCb3ggPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCcub3ZlcmxheS1jb250ZW50JylcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29udGVudEJveFxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjbG9zZUJ0bigpIHtcclxuICAgICAgICBpZiAodGhpcy5fY2xvc2VCdG4gPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLl9jbG9zZUJ0biA9IHRoaXMuY29udGFpbmVyLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKSlcclxuICAgICAgICAgICAgdGhpcy5fY2xvc2VCdG4uc2V0QXR0cmlidXRlKCdocmVmJywgJyNjbG9zZScpXHJcbiAgICAgICAgICAgIHRoaXMuX2Nsb3NlQnRuLmNsYXNzTGlzdC5hZGQoJ2Nsb3NlLWJ0bicpXHJcblxyXG4gICAgICAgICAgICB0aGlzLl9jbG9zZUJ0bi5vbmNsaWNrID0gKGUpID0+IHtcclxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5kaXNhYmxlKClcclxuICAgICAgICAgICAgICAgICAgICAuaGlkZUNsb3NlQnRuKClcclxuICAgICAgICAgICAgICAgICAgICAucmVzZXRUZXh0KClcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgbGV0IGljb24gPSB0aGlzLl9jbG9zZUJ0bi5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpJykpXHJcbiAgICAgICAgICAgIGljb24uY2xhc3NMaXN0LmFkZCgnZmEnLCAnZmEtdGltZXMnKVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fY2xvc2VCdG5cclxuICAgIH1cclxuXHJcbiAgICBzZXQgY2xvc2FibGUoZmxhZykge1xyXG4gICAgICAgIHRoaXMuX2Nsb3NhYmxlID0gISFmbGFnXHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjbG9zYWJsZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5fY2xvc2FibGUgPT09IHVuZGVmaW5lZCkgdGhpcy5fY2xvc2FibGUgPSBmYWxzZVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9jbG9zYWJsZVxyXG4gICAgfVxyXG5cclxuICAgIGVuYWJsZSgpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgZGlzYWJsZSgpIHtcclxuICAgICAgICBsZXQgY2wgPSB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3RcclxuICAgICAgICBpZiAoIWNsLmNvbnRhaW5zKCdoaWRkZW4nKSkgY2wuYWRkKCdoaWRkZW4nKVxyXG4gICAgICAgIHRoaXMucmVzZXRUZXh0KClcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIHNldFRleHQodGV4dCkge1xyXG4gICAgICAgIGxldCB0cmFuc2xhdGVUZXh0ID0gY29uZmlnLmkxOG4udWtbdGV4dF1cclxuICAgICAgICB0aGlzLmNvbnRlbnRCb3guaW5uZXJIVE1MID0gJydcclxuICAgICAgICB0aGlzLmNvbnRlbnRCb3guYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodHJhbnNsYXRlVGV4dCkpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICByZXNldFRleHQoKSB7XHJcbiAgICAgICAgdGhpcy5hZGRQcmVsb2FkZXIoKVxyXG4gICAgICAgIHJldHVybiB0aGlzLnNldFRleHQoJ2RhdGEgcHJvY2Vzc2luZycpXHJcbiAgICB9XHJcblxyXG4gICAgc2V0SHRtbChodG1sKSB7XHJcbiAgICAgICAgdGhpcy5jb250ZW50Qm94LmlubmVySFRNTCA9IGh0bWxcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0SHRtbCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zZXRIdG1sKCcnKVxyXG4gICAgfVxyXG5cclxuICAgIHNldE5vZGUobm9kZSkge1xyXG4gICAgICAgIHRoaXMucmVzZXRIdG1sKClcclxuICAgICAgICB0aGlzLmNvbnRlbnRCb3guYXBwZW5kQ2hpbGQobm9kZSlcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIGFkZFByZWxvYWRlcigpIHtcclxuICAgICAgICBsZXQgY2wgPSB0aGlzLmNvbnRlbnRCb3guY2xhc3NMaXN0XHJcbiAgICAgICAgaWYgKCFjbC5jb250YWlucygncHJlbG9hZGVyJykpIGNsLmFkZCgncHJlbG9hZGVyJylcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZVByZWxvYWRlcigpIHtcclxuICAgICAgICBsZXQgY2wgPSB0aGlzLmNvbnRlbnRCb3guY2xhc3NMaXN0XHJcbiAgICAgICAgaWYgKGNsLmNvbnRhaW5zKCdwcmVsb2FkZXInKSkgY2wucmVtb3ZlKCdwcmVsb2FkZXInKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0Nsb3NlQnRuKCkge1xyXG4gICAgICAgIGxldCBjbCA9IHRoaXMuY2xvc2VCdG4uY2xhc3NMaXN0XHJcbiAgICAgICAgaWYgKGNsLmNvbnRhaW5zKCdoaWRkZW4nKSkgY2wucmVtb3ZlKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUNsb3NlQnRuKCkge1xyXG4gICAgICAgIGxldCBjbCA9IHRoaXMuY2xvc2VCdG4uY2xhc3NMaXN0XHJcbiAgICAgICAgaWYgKCFjbC5jb250YWlucygnaGlkZGVuJykpIGNsLmFkZCgnaGlkZGVuJylcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IEJhc2VXaWRnZXQgZnJvbSAnLi8uLi9saWIvQmFzZVdpZGdldCc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUcmVlIGV4dGVuZHMgQmFzZVdpZGdldCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyLCBtb2RlbCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXI7XHJcbiAgICAgICAgdGhpcy5tb2RlbCAgICAgPSBtb2RlbDtcclxuICAgICAgICB0aGlzLnJvb3QgICAgICA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJy5yb290LWNvbnRhaW5lcicpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlICAgID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIub25jbGljayA9IHRoaXMub25DbGljay5iaW5kKHRoaXMpO1xyXG5cclxuICAgICAgICB0aGlzLm1vZGVsLm9uZ2V0Y2hpbGRyZW4gPSAoZSkgPT4gdGhpcy5hZGRDaGlsZHJlbkNvbnRhaW5lcnMoZS5kYXRhKTtcclxuICAgICAgICB0aGlzLm1vZGVsLm9uc2V0ICAgICAgICAgPSAoZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZS5kYXRhLmNyZWF0ZWQgPT09IGUuZGF0YS51cGRhdGVkKSB0aGlzLmFkZENvbnRhaW5lcihlLmRhdGEpXHJcbiAgICAgICAgICAgIGVsc2UgdGhpcy51cGRhdGVDb250YWluZXIoZS5kYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tb2RlbC5vbnJlbW92ZSA9IHRoaXMucmVtb3ZlQ29udGFpbmVyLmJpbmQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVySWQsIG1vZGVsKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBUcmVlKGNvbnRhaW5lck5vZGUsIG1vZGVsKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm94eShub2RlKSB7XHJcbiAgICAgICAgcmV0dXJuICFPYmplY3QuaXModGhpcy5hY3RpdmUsIG5vZGUpICYmIFRyZWUuaXNFeHBhbmQobm9kZSlcclxuICAgICAgICAgICAgPyB0aGlzLm1ha2VBY3RpdmUobm9kZSlcclxuICAgICAgICAgICAgOiAoVHJlZS5pc0V4cGFuZChub2RlKSA/IHRoaXMuY29sbGFwc2Uobm9kZSkgOiB0aGlzLmV4cGFuZChub2RlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGlzRXhwYW5kKG5vZGUpIHtcclxuICAgICAgICByZXR1cm4gbm9kZS5jbGFzc0xpc3QuY29udGFpbnMoJ2V4cGFuZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGV4cGFuZChub2RlKSB7XHJcbiAgICAgICAgbm9kZS5jbGFzc0xpc3QuYWRkKCdleHBhbmQnKTtcclxuICAgICAgICB0aGlzLm1ha2VBY3RpdmUobm9kZSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgY29sbGFwc2Uobm9kZSkge1xyXG4gICAgICAgIGxldCBjbCA9IG5vZGUuY2xhc3NMaXN0O1xyXG4gICAgICAgIGlmKGNsLmNvbnRhaW5zKCdleHBhbmQnKSkgY2wucmVtb3ZlKCdleHBhbmQnKTtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBtYWtlQWN0aXZlKG5vZGUpIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZSAmJiB0aGlzLmFjdGl2ZS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKTtcclxuICAgICAgICBub2RlLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlID0gbm9kZTtcclxuICAgICAgICB0aGlzLmVtaXQoJ3NlbGVjdCcsIHt0YXJnZXQ6IHRoaXMsIHNlbGVjdGVkTm9kZTogbm9kZX0pO1xyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBjb250YWluc0NvbnRhaW5lcnMobm9kZSkge1xyXG4gICAgICAgIHJldHVybiAhIW5vZGUucXVlcnlTZWxlY3RvcignLm5vZGUtY29udGFpbmVyJyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ29udGFpbmVyKGRhdGEpIHtcclxuICAgICAgICBsZXQge2lkLCB0aXRsZX0gPSBkYXRhO1xyXG4gICAgICAgIGxldCBhY3RpdmVDb250YWluZXIgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlPnVsJyk7XHJcblxyXG4gICAgICAgIGxldCBodG1sID0gYDxsaSBjbGFzcz1cIm5vZGUtY29udGFpbmVyXCIgZGF0YS1jYXQtaWQ9XCIke2lkfVwiPlxyXG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cImNvbnRhaW5lci10aXRsZVwiIHRpdGxlPVwiJHt0aXRsZX1cIj4ke3RpdGxlfTwvc3Bhbj5cclxuICAgICAgICAgICAgPHVsPjwvdWw+XHJcbiAgICAgICAgPC9saT5gO1xyXG5cclxuICAgICAgICAvLyBpZiBjb250YWluZXIgaGFzIG5vIGVsZW1lbnRcclxuICAgICAgICBpZiAoIWFjdGl2ZUNvbnRhaW5lci5jaGlsZEVsZW1lbnRDb3VudCkge1xyXG4gICAgICAgICAgICBhY3RpdmVDb250YWluZXIuaW5zZXJ0QWRqYWNlbnRIVE1MKCdiZWZvcmVFbmQnLCBodG1sKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgbGV0IHNpYmxpbmdzID0gW10uc2xpY2UuY2FsbChhY3RpdmVDb250YWluZXIuY2hpbGRyZW4pXHJcbiAgICAgICAgICAgICAgICAuZmlsdGVyKChub2RlKSA9PiBub2RlLmNsYXNzTGlzdC5jb250YWlucygnbm9kZS1jb250YWluZXInKSk7XHJcblxyXG4gICAgICAgICAgICAvLyBpZiBjb250YWluZXIgaGFzIGVsZW1lbnQgb25seSB3aXRob3V0IG5vZGUtY2FudGFpbmVyIGNzcyBjbGFzc1xyXG4gICAgICAgICAgICBpZiAoIXNpYmxpbmdzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgYWN0aXZlQ29udGFpbmVyLmluc2VydEFkamFjZW50SFRNTCgnYWZ0ZXJCZWdpbicsIGh0bWwpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgIGxldCBsYXN0ID0gc2libGluZ3MucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAvLyBpZiBjb250YWluZXIgaGFzIGVsZW1lbnQgd2l0aCBub2RlLWNhbnRhaW5lciBjc3MgY2xhc3MgZW5kIGVsZW1lbnQgd2l0aG91dCB0aGlzIGNsYXNzXHJcbiAgICAgICAgICAgICAgICBpZiAobGFzdC5uZXh0RWxlbWVudFNpYmxpbmcpIHtcclxuICAgICAgICAgICAgICAgICAgICBsYXN0Lmluc2VydEFkamFjZW50SFRNTCgnYWZ0ZXJFbmQnLCBodG1sKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgY29udGFpbmVyIGhhcyBlbGVtZW50IG9ubHkgbm9kZS1jYW50YWluZXIgY3NzIGNsYXNzXHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlQ29udGFpbmVyLmluc2VydEFkamFjZW50SFRNTCgnYmVmb3JlRW5kJywgaHRtbCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBsYXN0QWRkZWQgPSBbXS5zbGljZS5jYWxsKHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwoJy5hY3RpdmU+dWw+Lm5vZGUtY29udGFpbmVyJykpLnBvcCgpO1xyXG4gICAgICAgIHRoaXMuZXhwYW5kKGxhc3RBZGRlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlQ29udGFpbmVyKGRhdGEpIHtcclxuICAgICAgICBsZXQgbm9kZSA9IHRoaXMuYWN0aXZlLnF1ZXJ5U2VsZWN0b3IoJy5jb250YWluZXItdGl0bGUnKTtcclxuICAgICAgICBub2RlLnNldEF0dHJpYnV0ZSgndGl0bGUnLCBkYXRhLnRpdGxlKTtcclxuICAgICAgICBub2RlLmZpcnN0Q2hpbGQubm9kZVZhbHVlID0gZGF0YS50aXRsZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRDaGlsZHJlbkNvbnRhaW5lcnMoZGF0YSkge1xyXG4gICAgICAgIGlmICghZGF0YS5sZW5ndGgpIHJldHVybjtcclxuICAgICAgICBpZiAoVHJlZS5jb250YWluc0NvbnRhaW5lcnModGhpcy5hY3RpdmUpKSByZXR1cm47XHJcblxyXG4gICAgICAgIGxldCBhY3RpdmVDb250YWluZXIgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCcuYWN0aXZlPnVsJyk7XHJcbiAgICAgICAgbGV0IGh0bWwgPSBbXTtcclxuXHJcbiAgICAgICAgZGF0YS5mb3JFYWNoKGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgICAgICAgbGV0IHtpZCwgdGl0bGV9ID0gZGF0YTtcclxuICAgICAgICAgICAgaHRtbC5wdXNoKGA8bGkgY2xhc3M9XCJub2RlLWNvbnRhaW5lclwiIGRhdGEtY2F0LWlkPVwiJHtpZH1cIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiY29udGFpbmVyLXRpdGxlXCIgdGl0bGU9XCIke3RpdGxlfVwiPiR7dGl0bGV9PC9zcGFuPlxyXG4gICAgICAgICAgICA8dWw+PC91bD48L2xpPmApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBhY3RpdmVDb250YWluZXIuaW5uZXJIVE1MID0gaHRtbC5qb2luKCcnKTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVDb250YWluZXIoKSB7XHJcbiAgICAgICAgbGV0IG5vZGUgPSB0aGlzLmFjdGl2ZTtcclxuICAgICAgICBpZiAobm9kZS5jbGFzc0xpc3QuY29udGFpbnMoJ3Jvb3QtY29udGFpbmVyJykpIHJldHVybjtcclxuXHJcbiAgICAgICAgbGV0IG5leHRBY3RpdmUsIHBhcmVudCA9IG5vZGUucGFyZW50Tm9kZTtcclxuXHJcbiAgICAgICAgbGV0IG5leHQgPSBub2RlLm5leHRFbGVtZW50U2libGluZztcclxuICAgICAgICBpZiAobmV4dCAmJiBuZXh0LmNsYXNzTGlzdC5jb250YWlucygnbm9kZS1jb250YWluZXInKSkgbmV4dEFjdGl2ZSA9IG5leHQ7XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCBwcmV2ID0gbm9kZS5wcmV2aW91c0VsZW1lbnRTaWJsaW5nO1xyXG4gICAgICAgICAgICBpZiAocHJldiAmJiBwcmV2LmNsYXNzTGlzdC5jb250YWlucygnbm9kZS1jb250YWluZXInKSkgbmV4dEFjdGl2ZSA9IHByZXY7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnBhcmVudC5kYXRhLmRlbGV0ZSgnY2F0YWxvZycpO1xyXG4gICAgICAgIHRoaXMubWFrZUFjdGl2ZShuZXh0QWN0aXZlIHx8IG5vZGUucGFyZW50Tm9kZS5wYXJlbnROb2RlKTtcclxuICAgICAgICBwYXJlbnQucmVtb3ZlQ2hpbGQobm9kZSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGljayhlKSB7XHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUudGFyZ2V0O1xyXG4gICAgICAgIGxldCBjbCA9IHRhcmdldC5jbGFzc0xpc3Q7XHJcblxyXG4gICAgICAgIGlmIChjbC5jb250YWlucygnY29udGFpbmVyLXRpdGxlJykgfHwgY2wuY29udGFpbnMoJ3Jvb3QtdGl0bGUnKSkgdGhpcy5wcm94eSh0YXJnZXQucGFyZW50Tm9kZSk7XHJcbiAgICAgICAgaWYgKGNsLmNvbnRhaW5zKCdub2RlLWNvbnRhaW5lcicpKSAgdGhpcy5wcm94eSh0YXJnZXQpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbnNlbGVjdChmbikgICB7IHRoaXMub24oJ3NlbGVjdCcsIGZuKSB9XHJcbn0iLCJpbXBvcnQgQmFzZVRhYnMgZnJvbSAnLi4vLi4vbGliL0Jhc2VUYWJzJ1xyXG5pbXBvcnQgQWRkQ2F0YWxvZ1RhYnMgZnJvbSAnLi90YWJzL2FkZENhdGFsb2dUYWJzJ1xyXG5pbXBvcnQgVGFibGVWaWV3IGZyb20gJy4vdGFicy9UYWJsZVZpZXcnXHJcbmltcG9ydCBEZWxldGVWaWV3IGZyb20gJy4vdGFicy9EZWxldGVWaWV3J1xyXG5pbXBvcnQgR29vZHMgZnJvbSAnLi90YWJzL0dvb2RzJ1xyXG5pbXBvcnQgT3ZlcmxheSBmcm9tICcuLi9PdmVybGF5J1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGFicyBleHRlbmRzIEJhc2VUYWJzIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcihjb250YWluZXIpXHJcblxyXG4gICAgICAgIHRoaXMud2lkZ2V0c1xyXG4gICAgICAgICAgICAuc2V0KCd0YWJzJywgQWRkQ2F0YWxvZ1RhYnMuaW5pdCgnY2F0YWxvZy1tYW5pcHVsYXRlLWRhdGEnKSlcclxuICAgICAgICAgICAgLnNldChEZWxldGVWaWV3LmluaXQoJ2NhdGFsb2ctZGVsZXRlJykpXHJcbiAgICAgICAgICAgIC5zZXQoR29vZHMuaW5pdCgnZ29vZHMnKSlcclxuXHJcbiAgICAgICAgdGhpcy5vbnNob3duID0gdGhpcy5wcm94eS5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVyKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBUYWJzKGNvbnRhaW5lcilcclxuICAgIH1cclxuXHJcbiAgICBzZXQgZGF0YShkYXRhKSB7XHJcbiAgICAgICAgaWYgKHt9LnRvU3RyaW5nLmNhbGwoZGF0YSkuc2xpY2UoOCwgLTEpICE9PSAnT2JqZWN0JykgdGhyb3cgbmV3IEVycm9yKCdBcmd1bWVudCBwYXNzZWQgdG8gZGF0YSBzZXR0ZXIgbXVzdCBiZSBvZiB0aGUgdHlwZSBvYmplY3QnKVxyXG4gICAgICAgIHRoaXMuX2RhdGEgPSBkYXRhXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGEoKSB7IHJldHVybiB0aGlzLl9kYXRhIH1cclxuXHJcbiAgICBnZXQgdmlld1RhYigpIHtcclxuICAgICAgICBpZiAodGhpcy5fdmlld1RhYiA9PT0gdW5kZWZpbmVkKSB0aGlzLl92aWV3VGFiID0gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvcignYVtkYXRhLWFjdGlvbj1cImNhdGFsb2dfdmlld1wiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ZpZXdUYWJcclxuICAgIH1cclxuXHJcbiAgICBnZXQgYWRkVGFiKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9hZGRUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fYWRkVGFiID0gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvcignYVtkYXRhLWFjdGlvbj1cImNhdGFsb2dfYWRkXCJdJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fYWRkVGFiXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGVkaXRUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2VkaXRUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fZWRpdFRhYiA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb249XCJjYXRhbG9nX2VkaXRcIl0nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9lZGl0VGFiXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlbGV0ZVRhYigpIHtcclxuICAgICAgICBpZiAodGhpcy5fZGVsZXRlVGFiID09PSB1bmRlZmluZWQpIHRoaXMuX2RlbGV0ZVRhYiA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb249XCJjYXRhbG9nX2RlbGV0ZVwiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlbGV0ZVRhYlxyXG4gICAgfVxyXG5cclxuICAgIGdldCBnb29kc1RhYigpIHtcclxuICAgICAgICBpZiAodGhpcy5fZ29vZHNUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fZ29vZHNUYWIgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCdhW2RhdGEtYWN0aW9uPVwiZ29vZHNfbGlzdF92aWV3XCJdJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fZ29vZHNUYWJcclxuICAgIH1cclxuXHJcbiAgICBnZXQgdGFibGVWaWV3KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl90YWJsZVZpZXcgPT09IHVuZGVmaW5lZCkgdGhpcy5fdGFibGVWaWV3ID0gVGFibGVWaWV3LmluaXQodGhpcy5jb250YWluZXIpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3RhYmxlVmlld1xyXG4gICAgfVxyXG5cclxuICAgIHByb3h5KGUpIHtcclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS50YXJnZXRcclxuICAgICAgICBpZiAoIXRoaXMuZXF1YWxzKHRhcmdldCkpIHJldHVyblxyXG5cclxuICAgICAgICBzd2l0Y2ggKHRhcmdldC5hY3RpdmVUYWIpIHtcclxuICAgICAgICAgICAgY2FzZSB0aGlzLnZpZXdUYWIgIDogdGhpcy5vblNob3dWaWV3VGFiKGUpOyBicmVha1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMuYWRkVGFiICAgOiB0aGlzLm9uU2hvd0FkZFRhYihlKTsgYnJlYWtcclxuICAgICAgICAgICAgY2FzZSB0aGlzLmVkaXRUYWIgIDogdGhpcy5vblNob3dFZGl0VGFiKGUpOyBicmVha1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMuZGVsZXRlVGFiOiB0aGlzLm9uU2hvd0RlbGV0ZVRhYihlKTsgYnJlYWtcclxuICAgICAgICAgICAgY2FzZSB0aGlzLmdvb2RzVGFiIDogdGhpcy5vblNob3dHb29kc1RhYihlKTsgYnJlYWtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd09ubHlBZGRUYWIoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlKClcclxuICAgICAgICB0aGlzLmVuYWJsZSh0aGlzLmFkZFRhYilcclxuICAgICAgICB0aGlzLnNob3codGhpcy5hZGRUYWIpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBvblNob3dWaWV3VGFiKGUpIHtcclxuICAgICAgICBsZXQgZGF0YSA9IHRoaXMucGFyZW50LmRhdGEuZ2V0KCdjYXRhbG9nJylcclxuICAgICAgICB0aGlzLnRhYmxlVmlldy5wb3B1bGF0ZShkYXRhKS5yZXNldEltZygpXHJcbiAgICAgICAgaWYgKGRhdGEuaW1nKSB0aGlzLnRhYmxlVmlldy52aWV3SW1nKGRhdGEuaW1nLnBhdGgpXHJcbiAgICAgICAgT3ZlcmxheS5pbnN0YW5jZS5kaXNhYmxlKClcclxuICAgIH1cclxuXHJcbiAgICBvblNob3dEZWxldGVUYWIoZSkge1xyXG4gICAgICAgIHRoaXMuZ2V0KCdkZWxldGVWaWV3JykucmVzZXQoKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd0FkZFRhYihlKSB7XHJcbiAgICAgICAgbGV0IHRhYnMgPSB0aGlzLmdldCgndGFicycpXHJcbiAgICAgICAgbGV0IGZvcm0gPSB0YWJzLmdldCgnZm9ybScpXHJcbiAgICAgICAgbGV0IHtpZH0gPSB0aGlzLnBhcmVudC5kYXRhLmdldCgnY2F0YWxvZycpO1xyXG5cclxuICAgICAgICB0YWJzLnNob3coKVxyXG4gICAgICAgIGZvcm0uZm9jdXMoJ3RpdGxlJykuYWN0aW9uID0gJ2FkZCdcclxuICAgICAgICBmb3JtLmRhdGEgPSB7cGFyZW50X2lkOiBpZH1cclxuXHJcbiAgICAgICAgdGFicy5kaXNhYmxlKHRhYnMudXBsb2FkVGFiKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd0VkaXRUYWIoZSkge1xyXG4gICAgICAgIGxldCB0YWJzID0gdGhpcy5nZXQoJ3RhYnMnKVxyXG4gICAgICAgIGxldCBmb3JtID0gdGFicy5nZXQoJ2Zvcm0nKVxyXG5cclxuICAgICAgICB0YWJzLnNob3coKVxyXG4gICAgICAgIGZvcm0uYWN0aW9uID0gJ2VkaXQnXHJcbiAgICAgICAgZm9ybS5kYXRhICAgPSB0aGlzLnBhcmVudC5kYXRhLmdldCgnY2F0YWxvZycpXHJcblxyXG4gICAgICAgIHRhYnMuZW5hYmxlKClcclxuICAgIH1cclxuXHJcbiAgICBvblNob3dHb29kc1RhYihlKSB7XHJcbiAgICAgICAgbGV0IGdvb2RzID0gdGhpcy5nZXQoJ2dvb2RzJylcclxuICAgICAgICBnb29kcy5nZXQoJ3RhYnMnKS5zaG93KClcclxuICAgICAgICB0aGlzLmhpZGVOYXZpZ2F0aW9uKClcclxuICAgIH1cclxuXHJcbn1cclxuIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2FjaGUge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuZGF0YSAgICAgPSBuZXcgTWFwKCk7XHJcbiAgICAgICAgdGhpcy5pZE1hcHBlciA9IG5ldyBNYXAoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQoZGF0YSkge1xyXG4gICAgICAgIGlmICh7fS50b1N0cmluZy5jYWxsKGRhdGEpLnNsaWNlKDgsIC0xKSA9PT0gJ0FycmF5Jykge1xyXG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykgdGhpcy5zZXQoZGF0YVtpXSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kYXRhLnNldChkYXRhLmlkLCBkYXRhKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlkTWFwcGVyLmhhcyhkYXRhLnBhcmVudF9pZCkpIHRoaXMuaWRNYXBwZXIuZ2V0KGRhdGEucGFyZW50X2lkKS5hZGQoZGF0YS5pZCk7XHJcbiAgICAgICAgICAgIGVsc2UgdGhpcy5pZE1hcHBlci5zZXQoZGF0YS5wYXJlbnRfaWQsIG5ldyBTZXQoW2RhdGEuaWRdKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldChpZCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRhdGEuaGFzKGlkKSA/IHRoaXMuZGF0YS5nZXQoaWQpIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBoYXMoaWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kYXRhLmhhcyhpZCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2hpbGRyZW4ocGlkKSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IFtdO1xyXG4gICAgICAgIGlmICghdGhpcy5pZE1hcHBlci5oYXMocGlkKSkgcmV0dXJuIHJlc3VsdDtcclxuXHJcbiAgICAgICAgbGV0IGRhdGE7XHJcbiAgICAgICAgbGV0IGlkU2V0ID0gdGhpcy5pZE1hcHBlci5nZXQocGlkKTtcclxuICAgICAgICBmb3IgKGxldCBpZCBvZiBpZFNldCkge1xyXG4gICAgICAgICAgICBkYXRhID0gdGhpcy5kYXRhLmdldChpZCk7XHJcbiAgICAgICAgICAgIGlmIChkYXRhKSByZXN1bHQucHVzaChkYXRhKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBoYXNDaGlsZHJlbihwaWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pZE1hcHBlci5oYXMocGlkKTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmUoaWQpIHtcclxuICAgICAgICBpZiAodGhpcy5kYXRhLmhhcyhpZCkpIHtcclxuICAgICAgICAgICAgbGV0IHBpZCA9IHRoaXMuZGF0YS5nZXQoaWQpLnBhcmVudF9pZDtcclxuICAgICAgICAgICAgdGhpcy5pZE1hcHBlci5oYXMocGlkKSAmJiB0aGlzLmlkTWFwcGVyLmdldChwaWQpLmRlbGV0ZShpZCk7XHJcbiAgICAgICAgICAgIHRoaXMuaWRNYXBwZXIuaGFzKGlkKSAmJiB0aGlzLnJlbW92ZVJlY3Vyc2l2ZUZyb21NYXBwZXIoaWQpO1xyXG4gICAgICAgICAgICB0aGlzLmRhdGEuZGVsZXRlKGlkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlUmVjdXJzaXZlRnJvbU1hcHBlcihpZCkge1xyXG4gICAgICAgIGlmICghdGhpcy5pZE1hcHBlci5oYXMoaWQpKSByZXR1cm47XHJcblxyXG4gICAgICAgIGxldCBjaGlsZHJlblNldCA9IHRoaXMuaWRNYXBwZXIuZ2V0KGlkKTtcclxuICAgICAgICBpZiAoY2hpbGRyZW5TZXQuc2l6ZSkge1xyXG4gICAgICAgICAgICBjaGlsZHJlblNldC5mb3JFYWNoKChpZCkgPT4gdGhpcy5yZW1vdmVSZWN1cnNpdmVGcm9tTWFwcGVyKGlkKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuaWRNYXBwZXIuZGVsZXRlKGlkKTtcclxuICAgIH1cclxufSIsImltcG9ydCBjb25maWcgZnJvbSAnLi4vLi4vLi4vY29uZmlnLmpzb24nO1xyXG5pbXBvcnQgQ2FjaGUgZnJvbSAnLi9DYWNoZSc7XHJcbmltcG9ydCBCYXNlTW9kZWwgZnJvbSAnLi4vLi4vLi4vbGliL0Jhc2VNb2RlbCdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1vZGVsIGV4dGVuZHMgQmFzZU1vZGVsIHtcclxuXHJcbiAgICBnZXQgY2FjaGUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NhY2hlID09PSB1bmRlZmluZWQpIHRoaXMuX2NhY2hlID0gbmV3IENhY2hlKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NhY2hlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb25maWcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NvbmZpZyA9PT0gdW5kZWZpbmVkKSB0aGlzLl9jb25maWcgPSBCYXNlTW9kZWwuZ2V0Q29uZmlnKCdjYXRhbG9nJyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBnZXQoaWQpIHtcclxuICAgICAgICBpZiAodGhpcy5jYWNoZS5oYXMoaWQpKSB7XHJcbiAgICAgICAgICAgIGxldCBkYXRhID0gdGhpcy5jYWNoZS5nZXQoaWQpO1xyXG4gICAgICAgICAgICB0aGlzLmVtaXQoJ2dldCcsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGRhdGF9KTtcclxuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShkYXRhKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIHsgbWV0aG9kOiBCYXNlTW9kZWwubWV0aG9kcy5HRVQgfSk7XHJcbiAgICAgICAgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2lkfWA7XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jYWNoZS5zZXQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2dldCcsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGRhdGF9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDaGlsZHJlbihpZCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNhY2hlLmhhc0NoaWxkcmVuKGlkKSkge1xyXG4gICAgICAgICAgICBsZXQgY2hpbGRyZW4gPSB0aGlzLmNhY2hlLmdldENoaWxkcmVuKGlkKTtcclxuICAgICAgICAgICAgdGhpcy5lbWl0KCdnZXRDaGlsZHJlbicsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGNoaWxkcmVufSk7XHJcbiAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoY2hpbGRyZW4pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywgeyBtZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVCB9KTtcclxuICAgICAgICBvcHRpb25zLnVyaSA9IGAke29wdGlvbnMudXJpfXBpZC8ke2lkfWA7XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jYWNoZS5zZXQoZGF0YSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2dldENoaWxkcmVuJywge3RhcmdldDogdGhpcywgZGF0YTogZGF0YX0pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNldChkYXRhKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGJvZHk6IEpTT04uc3RyaW5naWZ5KGRhdGEpLFxyXG4gICAgICAgICAgICBtZXRob2Q6IGRhdGEuaWQgPyBCYXNlTW9kZWwubWV0aG9kcy5QVVQgOiBCYXNlTW9kZWwubWV0aG9kcy5QT1NUXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgb3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuY29uZmlnLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgaWYgKG9wdGlvbnMubWV0aG9kID09PSBCYXNlTW9kZWwubWV0aG9kcy5QVVQpIG9wdGlvbnMudXJpID0gYCR7b3B0aW9ucy51cml9JHtkYXRhLmlkfWA7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICAgICAgICAgIC54aHIob3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGUuc2V0KGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdzZXQnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlKGlkKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywge21ldGhvZDogQmFzZU1vZGVsLm1ldGhvZHMuREVMRVRFfSk7XHJcbiAgICAgICAgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2lkfWA7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICAgICAgICAgIC54aHIob3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGUucmVtb3ZlKGRhdGEuaWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmUnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG9uZ2V0Y2hpbGRyZW4oZm4pIHsgdGhpcy5vbignZ2V0Q2hpbGRyZW4nLCBmbikgfVxyXG59IiwiaW1wb3J0IGNvbmZpZyBmcm9tICcuLi8uLi8uLi9jb25maWcuanNvbidcclxuaW1wb3J0IEJhc2VNb2RlbCBmcm9tICcuLi8uLi8uLi9saWIvQmFzZU1vZGVsJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVXBsb2FkTW9kZWwgZXh0ZW5kcyBCYXNlTW9kZWwge1xyXG5cclxuICAgIGdldCBjb25maWcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NvbmZpZyA9PT0gdW5kZWZpbmVkKSB0aGlzLl9jb25maWcgPSBCYXNlTW9kZWwuZ2V0Q29uZmlnKCdjYXRhbG9nVXBsb2FkJyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbmZpZztcclxuICAgIH1cclxuXHJcbiAgICBzZXQoZGF0YSkge1xyXG4gICAgICAgIGxldCBvcHRpb25zID0ge1xyXG4gICAgICAgICAgICBib2R5OiAgIGRhdGEuZm9ybURhdGEsXHJcbiAgICAgICAgICAgIG1ldGhvZDogQmFzZU1vZGVsLm1ldGhvZHMuUE9TVCxcclxuICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIG9wdGlvbnMpXHJcbiAgICAgICAgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2RhdGEuZGF0YS5jYXRhbG9nX2lkfWBcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdzZXQnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhLmRhdGF9KVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGFcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QnlDYXRhbG9nKGNhdGFsb2dJZCkge1xyXG4gICAgICAgIGxldCBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIHttZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVH0pXHJcbiAgICAgICAgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2NhdGFsb2dJZH1gXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICAgICAgICAgIC54aHIob3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZW1pdCgnZ2V0QnlDYXRhbG9nJywge3RhcmdldDogdGhpcywgZGF0YTogZGF0YX0pXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQoKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdNZXRob2QgaXMgbm90IGltcGxlbWVudGVkJylcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZURlbGV0ZSBmcm9tICcuLi8uLi8uLi9saWIvQmFzZURlbGV0ZSc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEZWxldGVWaWV3IGV4dGVuZHMgQmFzZURlbGV0ZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoY29udGFpbmVyKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIGxldCBjb250YWluZXJOb2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY29udGFpbmVySWQpO1xyXG4gICAgICAgIHJldHVybiBuZXcgRGVsZXRlVmlldyhjb250YWluZXJOb2RlKTtcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZVdpZGdldCBmcm9tICcuLy4uLy4uL0Jhc2VDYXRhbG9nV2lkZ2V0J1xyXG5pbXBvcnQgTW9kZWwgZnJvbSAnLi9nb29kcy9Nb2RlbCdcclxuaW1wb3J0IFRhYnMgZnJvbSAnLi9nb29kcy9UYWJzJ1xyXG5pbXBvcnQgT3ZlcmxheSBmcm9tICcuLy4uLy4uL092ZXJsYXknXHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR29vZHMgZXh0ZW5kcyBCYXNlV2lkZ2V0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXJcclxuICAgICAgICB0aGlzLm1vZGVsID0gbmV3IE1vZGVsKClcclxuXHJcbiAgICAgICAgdGhpcy53aWRnZXRzLnNldChUYWJzLmluaXQodGhpcy5jb250YWluZXIpKVxyXG5cclxuICAgICAgICB0aGlzLm9uc3VibWl0ID0gdGhpcy5zdWJtaXQuYmluZCh0aGlzKVxyXG4gICAgICAgIHRoaXMub25kZWxldGUgPSB0aGlzLnN1Ym1pdERlbGV0ZS5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVySWQpIHtcclxuICAgICAgICBsZXQgY29udGFpbmVyTm9kZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbnRhaW5lcklkKVxyXG4gICAgICAgIHJldHVybiBuZXcgR29vZHMoY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBwb3B1bGF0ZUxpc3QoKSB7XHJcbiAgICAgICAgbGV0IG92ZXJsYXkgPSBPdmVybGF5Lmluc3RhbmNlXHJcbiAgICAgICAgb3ZlcmxheS5lbmFibGUoKVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5nZXRCeUNhdGFsb2codGhpcy5kYXRhLmdldCgnY2F0YWxvZycpLmlkKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGdvb2RzR3JpZCA9IHRoaXMuZ2V0KCd0YWJzID4gZ29vZHNHcmlkJylcclxuICAgICAgICAgICAgICAgIGdvb2RzR3JpZC5wb3B1bGF0ZShkYXRhKVxyXG4gICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcnMpID0+IG92ZXJsYXkuc2V0VGV4dChcIkFuIGVycm9yIGhhcyBvY2N1cnJlZC4gUGxlYXNlLCBjb250YWN0IGRldmVsb3BlclwiKSlcclxuICAgIH1cclxuXHJcbiAgICBzdWJtaXQoZSkge1xyXG4gICAgICAgIGxldCBmb3JtID0gZS50YXJnZXRcclxuICAgICAgICBpZiAoIWZvcm0uZXF1YWxzKHRoaXMsICd0YWJzID4gYWRkR29vZHNUYWJzID4gZGF0YWZvcm0nKSkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBkYXRhICAgID0gZS5kYXRhXHJcbiAgICAgICAgbGV0IG92ZXJsYXkgPSBPdmVybGF5Lmluc3RhbmNlXHJcblxyXG4gICAgICAgIGZvcm0ucmVzZXRFcnJvcnMoKVxyXG4gICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgZGF0YS5pc190b3AgPSBkYXRhLmlzX3RvcCAmJiBkYXRhLmlzX3RvcCA9PT0gJ29uJyA/ICcxJyA6ICcwJ1xyXG4gICAgICAgIGRhdGEuaXNfaW5kZXhfY2Fyb3VzZWwgPSBkYXRhLmlzX2luZGV4X2Nhcm91c2VsICYmIGRhdGEuaXNfaW5kZXhfY2Fyb3VzZWwgPT09ICdvbicgPyAnMScgOiAnMCdcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbFxyXG4gICAgICAgICAgICAuc2V0KGRhdGEpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGEuc2V0KCdnb29kcycsIGRhdGEpXHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHRhYnMgPSB0aGlzLmdldCgndGFicycpXHJcbiAgICAgICAgICAgICAgICB0YWJzLnNob3codGFicy52aWV3VGFiKVxyXG5cclxuICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyb3JzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZXJyb3JzIGluc3RhbmNlb2YgRXJyb3IpIG92ZXJsYXkuc2V0VGV4dChlcnJvcnMubWVzc2FnZSlcclxuICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvcm0uc2V0RXJyb3JzKGVycm9ycylcclxuICAgICAgICAgICAgICAgICAgICBvdmVybGF5LmRpc2FibGUoKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdERlbGV0ZShlKSB7XHJcbiAgICAgICAgbGV0IHRhcmdldCAgICA9IGUudGFyZ2V0XHJcbiAgICAgICAgbGV0IGdvb2RzR3JpZCA9IHRoaXMuZ2V0KCd0YWJzID4gZ29vZHNHcmlkJylcclxuICAgICAgICBpZiAoIXRhcmdldC5lcXVhbHMoZ29vZHNHcmlkLmdldCgnZGVsZXRlVmlldycpKSkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBvdmVybGF5ID0gT3ZlcmxheS5pbnN0YW5jZVxyXG4gICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbFxyXG4gICAgICAgICAgICAucmVtb3ZlKHRoaXMuZGF0YS5nZXQoJ2dvb2RzJykuaWQpXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGdvb2RzR3JpZC5kZWxldGVSb3coKVxyXG4gICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4gb3ZlcmxheS5zZXRUZXh0KGVycm9yLm1lc3NhZ2UpKVxyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbmRlbGV0ZShmbikgeyB0aGlzLm9uKCdkZWxldGUnLCBmbikgfVxyXG4gICAgc2V0IG9uc3VibWl0KGZuKSB7IHRoaXMub24oJ3N1Ym1pdCcsIGZuKSB9XHJcbiAgICBzZXQgb25zaG93bihmbikgIHsgdGhpcy5vbignc2hvd24nLCBmbikgIH1cclxufVxyXG4iLCJpbXBvcnQgY29uZmlnIGZyb20gJy4uLy4uLy4uL2NvbmZpZy5qc29uJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGFibGVWaWV3IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lclxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KHRhYnNDb250YWluZXIpIHtcclxuICAgICAgICBsZXQgY29udGFpbmVyTm9kZSA9IHRhYnNDb250YWluZXIucXVlcnlTZWxlY3RvcignI2NhdGFsb2ctdmlldyB0YWJsZSB0Ym9keScpXHJcbiAgICAgICAgcmV0dXJuIG5ldyBUYWJsZVZpZXcoY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaW1nKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9pbWcgPT09IHVuZGVmaW5lZCkgdGhpcy5faW1nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhdGFsb2ctdmlldycpLnF1ZXJ5U2VsZWN0b3IoJy5pbWctY29udGFpbmVyIGltZycpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ltZ1xyXG4gICAgfVxyXG5cclxuICAgIHBvcHVsYXRlKGRhdGEpIHtcclxuICAgICAgICBsZXQgZm9ybWF0dGVyICA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KCd1ay1VQScpXHJcbiAgICAgICAgbGV0IGxhbmdDb25maWcgPSBjb25maWcuaTE4bi51a1xyXG5cclxuICAgICAgICBsZXQgdHJhbnNsRGF0YSA9IHt9XHJcbiAgICAgICAgdHJhbnNsRGF0YVtsYW5nQ29uZmlnLnRpdGxlXSA9IGRhdGEudGl0bGVcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcuc2x1Z10gPSBkYXRhLnNsdWdcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcuY3JlYXRlZF0gPSBmb3JtYXR0ZXIuZm9ybWF0KG5ldyBEYXRlKHBhcnNlSW50KGRhdGEuY3JlYXRlZCkpKVxyXG4gICAgICAgIHRyYW5zbERhdGFbbGFuZ0NvbmZpZy51cGRhdGVkXSA9IGZvcm1hdHRlci5mb3JtYXQobmV3IERhdGUocGFyc2VJbnQoZGF0YS51cGRhdGVkKSkpXHJcblxyXG4gICAgICAgIGxldCBodG1sID0gW11cclxuICAgICAgICBmb3IobGV0IGtleSBpbiB0cmFuc2xEYXRhKSBpZiAodHJhbnNsRGF0YS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcbiAgICAgICAgICAgIGh0bWwucHVzaChgPHRyPjx0ZD4ke2tleX08L3RkPjx0ZD4ke3RyYW5zbERhdGFba2V5XX08L3RkPjwvdHI+YClcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuaW5uZXJIVE1MID0gaHRtbC5qb2luKCcnKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgdmlld0ltZyhzcmMsIGFsdCA9ICcnLCB0aXRsZSA9ICcnKSB7XHJcbiAgICAgICAgaWYgKCFzcmMpIHRoaXMucmVzZXRJbWcoKVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmltZy5zcmMgICA9IHNyY1xyXG4gICAgICAgICAgICB0aGlzLmltZy5hbHQgICA9IGFsdFxyXG4gICAgICAgICAgICB0aGlzLmltZy50aXRsZSA9IHRpdGxlXHJcbiAgICAgICAgICAgIHRoaXMuaW1nLmNsYXNzTGlzdC5yZW1vdmUoJ2hpZGRlbicpXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXRJbWcoKSB7XHJcbiAgICAgICAgdGhpcy5pbWcuc3JjID0gdGhpcy5pbWcuYWx0ID0gdGhpcy5pbWcudGl0bGUgPSAnJ1xyXG4gICAgICAgIHRoaXMuaW1nLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZVRhYnMgZnJvbSAnLi4vLi4vLi4vbGliL0Jhc2VUYWJzJ1xyXG5pbXBvcnQgRm9ybSBmcm9tICcuLi90YWJzL2FkZENhdGFsb2dUYWJzL0Zvcm0nXHJcbmltcG9ydCBVcGxvYWRGb3JtIGZyb20gJy4uL3RhYnMvYWRkQ2F0YWxvZ1RhYnMvVXBsb2FkRm9ybSdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFkZENhdGFsb2dUYWJzIGV4dGVuZHMgQmFzZVRhYnMge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHN1cGVyKGNvbnRhaW5lcilcclxuXHJcbiAgICAgICAgdGhpcy53aWRnZXRzXHJcbiAgICAgICAgICAgIC5zZXQoRm9ybS5pbml0KCdjYXRhbG9nLWZvcm0tZm9ybScpKVxyXG4gICAgICAgICAgICAuc2V0KCd1cGxvYWRGb3JtJywgVXBsb2FkRm9ybS5pbml0KCdjYXRhbG9nLXVwbG9hZC1mb3JtJykpXHJcblxyXG4gICAgICAgIHRoaXMub25zaG93biA9IHRoaXMucHJveHkuYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZClcclxuICAgICAgICByZXR1cm4gbmV3IEFkZENhdGFsb2dUYWJzKGNvbnRhaW5lck5vZGUpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGFUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2RhdGFUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fZGF0YVRhYiA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb249XCJjYXRhbG9nX2RhdGFcIl0nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9kYXRhVGFiXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHVwbG9hZFRhYigpIHtcclxuICAgICAgICBpZiAodGhpcy5fZ2FsbGVyeVRhYiA9PT0gdW5kZWZpbmVkKSB0aGlzLl9nYWxsZXJ5VGFiID0gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvcignYVtkYXRhLWFjdGlvbj1cImNhdGFsb2dfZ2FsbGVyeVwiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dhbGxlcnlUYWJcclxuICAgIH1cclxuXHJcbiAgICBwcm94eShlKSB7XHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUudGFyZ2V0XHJcbiAgICAgICAgaWYgKCF0aGlzLmVxdWFscyh0YXJnZXQpKSByZXR1cm5cclxuXHJcbiAgICAgICAgc3dpdGNoICh0YXJnZXQuYWN0aXZlVGFiKSB7XHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5kYXRhVGFiICAgOiB0aGlzLm9uU2hvd0RhdGFUYWIoZSk7IGJyZWFrXHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy51cGxvYWRUYWIgOiB0aGlzLm9uU2hvd1VwbG9hZFRhYihlKTsgYnJlYWtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25TaG93RGF0YVRhYihlKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd1VwbG9hZFRhYihlKSB7XHJcbiAgICAgICAgbGV0IGRhdGEgPSB0aGlzLnBhcmVudC5wYXJlbnQuZGF0YVxyXG4gICAgICAgIHRoaXMuZ2V0KCd1cGxvYWRGb3JtJykuZGF0YSA9IGRhdGEuZ2V0KCdjYXRhbG9nJylcclxuICAgIH1cclxuXHJcbiAgICBvbldpZGdldFNob3coZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmVxdWFscyhlLnRhcmdldCkpIHRoaXMuYWRqdXN0VGFicyh0cnVlKVxyXG4gICAgfVxyXG5cclxufSIsImltcG9ydCBCYXNlRm9ybSBmcm9tICcuLi8uLi8uLi8uLi9saWIvQmFzZUZvcm0nXHJcblxyXG5jb25zdCBBQ1RJT05fQUREICA9ICdhZGQnXHJcbmNvbnN0IEFDVElPTl9FRElUID0gJ2VkaXQnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBGb3JtIGV4dGVuZHMgQmFzZUZvcm0ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHN1cGVyKGNvbnRhaW5lcilcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIGxldCBjb250YWluZXJOb2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY29udGFpbmVySWQpXHJcbiAgICAgICAgcmV0dXJuIG5ldyBGb3JtKGNvbnRhaW5lck5vZGUpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHRpdGxlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl90aXRsZSA9PT0gdW5kZWZpbmVkKSB0aGlzLl90aXRsZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdpbnB1dFRpdGxlJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fdGl0bGVcclxuICAgIH1cclxuXHJcbiAgICBpc0FkZCgpIHsgcmV0dXJuIHRoaXMuYWN0aW9uID09PSBBQ1RJT05fQUREIH1cclxuXHJcbiAgICBpc0VkaXQoKSB7IHJldHVybiB0aGlzLmFjdGlvbiA9PT0gQUNUSU9OX0VESVQgfVxyXG5cclxuICAgIHN0YXRpYyBub3JtYWxpemVEYXRhKGRhdGEpIHtcclxuICAgICAgICBmb3IgKGxldCBrZXkgaW4gZGF0YSkgaWYgKGRhdGEuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICBpZiAoZGF0YVtrZXldID09PSBudWxsKSBkYXRhW2tleV0gPSAnMCdcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGRhdGFcclxuICAgIH1cclxuXHJcbiAgICBzZXQgZGF0YShkYXRhKSB7XHJcbiAgICAgICAgbGV0IGZpbHRlcmVkRGF0YSA9IHt9XHJcblxyXG4gICAgICAgIGlmIChkYXRhLmlkKSAgICAgICAgZmlsdGVyZWREYXRhLmlkICAgICAgICA9IGRhdGEuaWRcclxuICAgICAgICBpZiAoZGF0YS5wYXJlbnRfaWQpIGZpbHRlcmVkRGF0YS5wYXJlbnRfaWQgPSBkYXRhLnBhcmVudF9pZFxyXG4gICAgICAgIGlmIChkYXRhLnRpdGxlKSAgICAgZmlsdGVyZWREYXRhLnRpdGxlICAgICA9IGRhdGEudGl0bGVcclxuXHJcbiAgICAgICAgc3VwZXIuZGF0YSA9IEZvcm0ubm9ybWFsaXplRGF0YShmaWx0ZXJlZERhdGEpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGEoKSB7IHJldHVybiBzdXBlci5kYXRhIH1cclxufSIsImltcG9ydCBCYXNlRm9ybSBmcm9tICcuLi8uLi8uLi8uLi9saWIvQmFzZUZvcm0nXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBVcGxvYWRGb3JtIGV4dGVuZHMgQmFzZUZvcm0ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHN1cGVyKGNvbnRhaW5lcilcclxuXHJcbiAgICAgICAgdGhpcy5kZWxldGVTdWJtaXRCdG4ub25jbGljayA9IHRoaXMub25EZWxldGVTdWJtaXQuYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZClcclxuICAgICAgICByZXR1cm4gbmV3IFVwbG9hZEZvcm0oY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBzZXQgZGF0YShkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpXHJcblxyXG4gICAgICAgIGlmIChkYXRhLmltZykgdGhpcy52aWV3SW1nKGRhdGEuaW1nLnBhdGgpXHJcbiAgICAgICAgZWxzZSB0aGlzLnJlc2V0SW1nKClcclxuXHJcbiAgICAgICAgaWYgKGRhdGEuaWQpIHRoaXMuY29udGFpbmVyLmVsZW1lbnRzWydjYXRhbG9nX2lkJ10udmFsdWUgPSBkYXRhLmlkXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGEoKSB7XHJcbiAgICAgICAgbGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKHRoaXMuY29udGFpbmVyKVxyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBkYXRhOiBzdXBlci5kYXRhLFxyXG4gICAgICAgICAgICBmb3JtRGF0YTogZm9ybURhdGFcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGltZygpIHtcclxuICAgICAgICBpZiAodGhpcy5faW1nID09PSB1bmRlZmluZWQpIHRoaXMuX2ltZyA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2ltZycpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ltZ1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkZWxldGVTdWJtaXRCdG4oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2RlbGV0ZVNibXQgPT09IHVuZGVmaW5lZCkgdGhpcy5fZGVsZXRlU2JtdCA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJy5kZWxldGUtYnRuLWJveCBidXR0b24nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9kZWxldGVTYm10XHJcbiAgICB9XHJcblxyXG4gICAgdmlld0ltZyhzcmMsIGFsdCA9ICcnLCB0aXRsZSA9ICcnKSB7XHJcbiAgICAgICAgaWYgKCFzcmMpIHRoaXMucmVzZXRJbWcoKVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmltZy5zcmMgICA9IHNyY1xyXG4gICAgICAgICAgICB0aGlzLmltZy5hbHQgICA9IGFsdFxyXG4gICAgICAgICAgICB0aGlzLmltZy50aXRsZSA9IHRpdGxlXHJcbiAgICAgICAgICAgIHRoaXMuaW1nLmNsYXNzTGlzdC5yZW1vdmUoJ2hpZGRlbicpXHJcbiAgICAgICAgICAgIHRoaXMuZGVsZXRlU3VibWl0QnRuLmNsYXNzTGlzdC5yZW1vdmUoJ2Rpc2FibGVkJylcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICByZXNldEltZygpIHtcclxuICAgICAgICB0aGlzLmltZy5zcmMgPSB0aGlzLmltZy5hbHQgPSB0aGlzLmltZy50aXRsZSA9ICcnXHJcbiAgICAgICAgdGhpcy5pbWcuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuICAgICAgICB0aGlzLmRlbGV0ZVN1Ym1pdEJ0bi5jbGFzc0xpc3QuYWRkKCdkaXNhYmxlZCcpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBvbkRlbGV0ZVN1Ym1pdCgpIHtcclxuICAgICAgICBsZXQge2NhdGFsb2dfaWR9ID0gdGhpcy5kYXRhLmRhdGFcclxuXHJcbiAgICAgICAgdGhpcy5lbWl0KCdkZWxldGUnLCB7XHJcbiAgICAgICAgICAgIHRhcmdldDogdGhpcyxcclxuICAgICAgICAgICAgZGF0YToge2NhdGFsb2dfaWR9XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBzZXQgb25kZWxldGUoZm4pIHtcclxuICAgICAgICB0aGlzLm9uKCdkZWxldGUnLCBmbilcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgY29uZmlnIGZyb20gJy4uLy4uLy4uLy4uL2NvbmZpZy5qc29uJztcclxuaW1wb3J0IEJhc2VNb2RlbCBmcm9tICcuLi8uLi8uLi8uLi9saWIvQmFzZU1vZGVsJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTW9kZWwgZXh0ZW5kcyBCYXNlTW9kZWwge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcblxyXG4gICAgICAgIHRoaXMub25zZXQgICAgPSAoZSkgPT4geyB0aGlzLmNhY2hlLnNldChlLmRhdGEuaWQsIGUuZGF0YSkgfTtcclxuICAgICAgICB0aGlzLm9ucmVtb3ZlID0gKGUpID0+IHsgaWYgKHRoaXMuY2FjaGUuaGFzKGUuZGF0YS5pZCkpIHRoaXMuY2FjaGUuZGVsZXRlKGUuZGF0YS5pZCkgfTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY2FjaGUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NhY2hlID09PSB1bmRlZmluZWQpIHRoaXMuX2NhY2hlID0gbmV3IE1hcCgpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jYWNoZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY29uZmlnKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jb25maWcgPT09IHVuZGVmaW5lZCkgdGhpcy5fY29uZmlnID0gQmFzZU1vZGVsLmdldENvbmZpZygnZ29vZHMnKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29uZmlnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldChpZCkge1xyXG4gICAgICAgIGlkID0gcGFyc2VJbnQoaWQpO1xyXG4gICAgICAgIGlmICh0aGlzLmNhY2hlLmhhcyhpZCkpIHJldHVybiBQcm9taXNlLnJlc29sdmUodGhpcy5jYWNoZS5nZXQoaWQpKTtcclxuICAgICAgICByZXR1cm4gc3VwZXIuZ2V0KGlkKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRCeUNhdGFsb2coY2F0YWxvZ0lkKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywgeyBtZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVCB9KTtcclxuICAgICAgICBvcHRpb25zLnVyaSA9IGAke29wdGlvbnMudXJpfWNpZC8ke2NhdGFsb2dJZH1gO1xyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICAgICAgICAgIC54aHIob3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCBkYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jYWNoZS5zZXQoZGF0YVtpXS5pZCwgZGF0YVtpXSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2dldEJ5Q2F0YWxvZycsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGRhdGF9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgb25nZXRieWNhdGFsb2coZm4pIHsgdGhpcy5vbignZ2V0QnlDYXRhbG9nJywgZm4pIH1cclxufSIsImltcG9ydCBCYXNlVGFicyAgICAgZnJvbSAnLi4vLi4vLi4vLi4vbGliL0Jhc2VUYWJzJ1xyXG5pbXBvcnQgQWRkR29vZHNUYWJzIGZyb20gJy4vdGFicy9BZGRHb29kc1RhYnMnXHJcbmltcG9ydCBHb29kc0dyaWQgICAgZnJvbSAnLi90YWJzL0dvb2RzR3JpZCdcclxuaW1wb3J0IEdvb2RzVmlldyAgICBmcm9tICcuL3RhYnMvR29vZHNWaWV3J1xyXG5pbXBvcnQge0FDVElPTl9BREQsIEFDVElPTl9FRElUfSBmcm9tICcuL3RhYnMvYWRkR29vZHNUYWJzL0RhdGFGb3JtJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgVGFicyBleHRlbmRzIEJhc2VUYWJzIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcihjb250YWluZXIpXHJcblxyXG4gICAgICAgIHRoaXMud2lkZ2V0c1xyXG4gICAgICAgICAgICAuc2V0KCdhZGRHb29kc1RhYnMnLCBBZGRHb29kc1RhYnMuaW5pdCgnZ29vZHMtbWFuaXB1bGF0ZS1kYXRhJykpXHJcbiAgICAgICAgICAgIC5zZXQoJ2dvb2RzR3JpZCcsIEdvb2RzR3JpZC5pbml0KCdnb29kcy1ncmlkJykpXHJcbiAgICAgICAgICAgIC5zZXQoJ2dvb2RzVmlldycsIEdvb2RzVmlldy5pbml0KCdnb29kcy12aWV3JykpXHJcblxyXG4gICAgICAgIHRoaXMub25zaG93biA9IHRoaXMucHJveHkuYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcikge1xyXG4gICAgICAgIHJldHVybiBuZXcgVGFicyhjb250YWluZXIpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGdyaWRUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2dyaWRUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fZ3JpZFRhYiA9IHRoaXMuZG9tLnNlbGVjdCgnYVtkYXRhLWFjdGlvbj1cImdvb2RzX2xpc3RcIl0nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9ncmlkVGFiXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHZpZXdUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3ZpZXdUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fdmlld1RhYiA9IHRoaXMuZG9tLnNlbGVjdCgnYVtkYXRhLWFjdGlvbj1cImdvb2RzX3ZpZXdcIl0nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl92aWV3VGFiXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGFkZFRhYigpIHtcclxuICAgICAgICBpZiAodGhpcy5fYWRkVGFiID09PSB1bmRlZmluZWQpIHRoaXMuX2FkZFRhYiA9IHRoaXMuZG9tLnNlbGVjdCgnYVtkYXRhLWFjdGlvbj1cImdvb2RzX2FkZFwiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FkZFRhYlxyXG4gICAgfVxyXG5cclxuICAgIGdldCBlZGl0VGFiKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9lZGl0VGFiID09PSB1bmRlZmluZWQpIHRoaXMuX2VkaXRUYWIgPSB0aGlzLmRvbS5zZWxlY3QoJ2FbZGF0YS1hY3Rpb249XCJnb29kc19lZGl0XCJdJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWRpdFRhYlxyXG4gICAgfVxyXG5cclxuICAgIGdldCBjYXRhbG9nUmV0dXJuVGFiKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jYXRhbG9nUmV0dXJuVGFiID09PSB1bmRlZmluZWQpIHRoaXMuX2NhdGFsb2dSZXR1cm5UYWIgPSB0aGlzLmRvbS5zZWxlY3QoJ2FbZGF0YS1hY3Rpb249XCJjYXRhbG9nX3ZpZXdcIl0nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9jYXRhbG9nUmV0dXJuVGFiXHJcbiAgICB9XHJcblxyXG4gICAgcHJveHkoZSkge1xyXG4gICAgICAgIGlmICghdGhpcy5lcXVhbHMoZS50YXJnZXQpKSByZXR1cm5cclxuXHJcbiAgICAgICAgc3dpdGNoICh0aGlzLmFjdGl2ZVRhYikge1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMuZ3JpZFRhYiA6dGhpcy5vblNob3dHcmlkVGFiKGUpOyBicmVha1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMuYWRkVGFiICA6dGhpcy5vblNob3dBZGRUYWIoZSk7IGJyZWFrXHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5lZGl0VGFiIDp0aGlzLm9uU2hvd0VkaXRUYWIoZSk7IGJyZWFrXHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy52aWV3VGFiIDp0aGlzLm9uU2hvd1ZpZXdUYWIoZSk7IGJyZWFrXHJcbiAgICAgICAgICAgIC8vY2FzZSB0aGlzLmNhdGFsb2dSZXR1cm5UYWIgOnRoaXMub25DYXRhbG9nUmV0dXJuVGFiKGUpOyBicmVha1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvblNob3dHcmlkVGFiKGUpIHtcclxuICAgICAgICB0aGlzLnJlc2V0SGlkZGVuVGFicygpXHJcbiAgICAgICAgdGhpcy5wYXJlbnQucG9wdWxhdGVMaXN0KClcclxuICAgICAgICB0aGlzLmdldCgnZ29vZHNHcmlkJykuc2hvd0dyaWQoKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd1ZpZXdUYWIoZSkge1xyXG4gICAgICAgIHRoaXMuZWRpdFRhYi5wYXJlbnROb2RlLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXHJcbiAgICAgICAgdGhpcy52aWV3VGFiLnBhcmVudE5vZGUuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcclxuXHJcbiAgICAgICAgdGhpcy5nZXQoJ2dvb2RzVmlldycpLnBvcHVsYXRlKHRoaXMucGFyZW50LmRhdGEuZ2V0KCdnb29kcycpKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd0FkZFRhYihlKSB7XHJcbiAgICAgICAgbGV0IHRhYnMgPSB0aGlzLmdldCgnYWRkR29vZHNUYWJzJylcclxuXHJcbiAgICAgICAgdGhpcy5yZXNldEhpZGRlblRhYnMoKVxyXG4gICAgICAgIHRhYnMuZ2V0KCdkYXRhRm9ybScpLmFjdGlvbiA9IEFDVElPTl9BRERcclxuICAgICAgICB0YWJzLmRpc2FibGUoKS5lbmFibGUodGFicy5kYXRhVGFiKS5zaG93KClcclxuICAgIH1cclxuXHJcbiAgICBvblNob3dFZGl0VGFiKGUpIHtcclxuICAgICAgICBsZXQgdGFicyA9IHRoaXMuZ2V0KCdhZGRHb29kc1RhYnMnKVxyXG5cclxuICAgICAgICB0aGlzLnZpZXdUYWIucGFyZW50Tm9kZS5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKVxyXG4gICAgICAgIHRoaXMuZWRpdFRhYi5wYXJlbnROb2RlLmNsYXNzTGlzdC5yZW1vdmUoJ2hpZGRlbicpXHJcblxyXG4gICAgICAgIHRhYnMuZ2V0KCdkYXRhRm9ybScpLmFjdGlvbiA9IEFDVElPTl9FRElUXHJcbiAgICAgICAgdGFicy5lbmFibGUoKS5zaG93KClcclxuICAgIH1cclxuXHJcbiAgICBvbkNhdGFsb2dSZXR1cm5UYWIoZSkge1xyXG4gICAgICAgIHRoaXMucGFyZW50LnBhcmVudC5zaG93KClcclxuICAgIH1cclxuXHJcbiAgICByZXNldEhpZGRlblRhYnMoKSB7XHJcbiAgICAgICAgdGhpcy52aWV3VGFiLnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuICAgICAgICB0aGlzLmVkaXRUYWIucGFyZW50Tm9kZS5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgQmFzZVRhYnMgICAgICBmcm9tICcuLi8uLi8uLi8uLi8uLi9saWIvQmFzZVRhYnMnXHJcbmltcG9ydCB7RGF0YUZvcm19ICAgIGZyb20gJy4vYWRkR29vZHNUYWJzL0RhdGFGb3JtJ1xyXG5pbXBvcnQgR29vZHNEZXRhaWxlZCBmcm9tICcuL2FkZEdvb2RzVGFicy9Hb29kc0RldGFpbGVkJ1xyXG5pbXBvcnQgR29vZHNHYWxsZXJ5ICBmcm9tICcuL2FkZEdvb2RzVGFicy9Hb29kc0dhbGxlcnknXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBZGRHb29kc1RhYnMgZXh0ZW5kcyBCYXNlVGFicyB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoY29udGFpbmVyKVxyXG5cclxuICAgICAgICB0aGlzLndpZGdldHNcclxuICAgICAgICAgICAgLnNldChEYXRhRm9ybS5pbml0KCdnb29kcy1mb3JtJykpXHJcbiAgICAgICAgICAgIC5zZXQoR29vZHNEZXRhaWxlZC5pbml0KCdnb29kcy1kZXRhaWxlZC1kYXRhJykpXHJcbiAgICAgICAgICAgIC5zZXQoR29vZHNHYWxsZXJ5LmluaXQoJ2dvb2RzLWdhbGxlcnknKSlcclxuXHJcbiAgICAgICAgdGhpcy5vbnNob3duID0gdGhpcy5wcm94eS5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVySWQpIHtcclxuICAgICAgICBsZXQgY29udGFpbmVyTm9kZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbnRhaW5lcklkKVxyXG4gICAgICAgIHJldHVybiBuZXcgQWRkR29vZHNUYWJzKGNvbnRhaW5lck5vZGUpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGFUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2RhdGFUYWIgPT09IHVuZGVmaW5lZCkgdGhpcy5fZGF0YVRhYiA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb249XCJnb29kc19kYXRhXCJdJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fZGF0YVRhYlxyXG4gICAgfVxyXG5cclxuICAgIGdldCBkYXRhRGV0YWlsZWRUYWIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2RhdGFEZXRhaWxlZFRhYiA9PT0gdW5kZWZpbmVkKSB0aGlzLl9kYXRhRGV0YWlsZWRUYWIgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCdhW2RhdGEtYWN0aW9uPVwiZ29vZHNfZGV0YWlsZWRfZGF0YVwiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RhdGFEZXRhaWxlZFRhYlxyXG4gICAgfVxyXG5cclxuICAgIGdldCBnYWxsZXJ5VGFiKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9nYWxsZXJ5VGFiID09PSB1bmRlZmluZWQpIHRoaXMuX2dhbGxlcnlUYWIgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCdhW2RhdGEtYWN0aW9uPVwiZ29vZHNfZ2FsbGVyeVwiXScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dhbGxlcnlUYWJcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGF0YSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYXJlbnQucGFyZW50LmRhdGFcclxuICAgIH1cclxuXHJcbiAgICBwcm94eShlKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmVxdWFscyhlLnRhcmdldCkpIHJldHVyblxyXG5cclxuICAgICAgICBzd2l0Y2ggKHRoaXMuYWN0aXZlVGFiKSB7XHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5kYXRhVGFiOiAgICAgICAgIHRoaXMub25TaG93RGF0YVRhYihlKTsgYnJlYWtcclxuICAgICAgICAgICAgY2FzZSB0aGlzLmRhdGFEZXRhaWxlZFRhYjogdGhpcy5vblNob3dEZXRhaWxlZERhdGFUYWIoZSk7IGJyZWFrXHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5nYWxsZXJ5VGFiOiAgICAgIHRoaXMub25TaG93R2FsbGVyeVRhYihlKTsgYnJlYWtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25TaG93RGF0YVRhYihlKSB7XHJcbiAgICAgICAgbGV0IHBhcmVudENvbnRhaW5lciA9IHRoaXMucGFyZW50LmNvbnRhaW5lclxyXG4gICAgICAgIHBhcmVudENvbnRhaW5lci5wYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3IoYCMke3BhcmVudENvbnRhaW5lci5pZH0+Lm5hdi10YWJzYCkuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcclxuICAgICAgICB0aGlzLmFkanVzdFRhYnModHJ1ZSlcclxuXHJcbiAgICAgICAgdGhpcy5nZXQoJ2RhdGFGb3JtJylcclxuICAgICAgICAgICAgLmRhdGEgPSB7XHJcbiAgICAgICAgICAgICAgICBjYXRhbG9nX2lkOiB0aGlzLmRhdGEuZ2V0KCdjYXRhbG9nJykuaWRcclxuICAgICAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd0RldGFpbGVkRGF0YVRhYihlKSB7XHJcbiAgICAgICAgbGV0IGdvb2RzRGV0YWlsZWQgPSB0aGlzLmdldCgnZ29vZHNEZXRhaWxlZCcpXHJcbiAgICAgICAgbGV0IGRldGFpbGVkR3JpZCAgPSBnb29kc0RldGFpbGVkLmdldCgnZGV0YWlsZWRHcmlkJylcclxuICAgICAgICBsZXQgZm9ybSAgICAgICAgICA9IGRldGFpbGVkR3JpZC5nZXQoJ2Zvcm0nKVxyXG5cclxuICAgICAgICBnb29kc0RldGFpbGVkLnBvcHVsYXRlTGlzdCgpXHJcbiAgICAgICAgZGV0YWlsZWRHcmlkLnNob3dHcmlkKClcclxuICAgICAgICBmb3JtLnJlc2V0KCkuaGlkZSgpXHJcbiAgICB9XHJcblxyXG4gICAgb25TaG93R2FsbGVyeVRhYihlKSB7XHJcbiAgICAgICAgbGV0IGdhbGxlcnkgPSB0aGlzLmdldCgnZ29vZHNHYWxsZXJ5JylcclxuXHJcbiAgICAgICAgZ2FsbGVyeS5nZXQoJ3VwbG9hZEZvcm0nKVxyXG4gICAgICAgICAgICAuZGF0YSA9IHtcclxuICAgICAgICAgICAgICAgIGdvb2RzX2lkOiB0aGlzLmRhdGEuZ2V0KCdnb29kcycpLmlkXHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgZ2FsbGVyeS5nZXQoJ2xpc3QnKS5kaXNhYmxlQWN0aW9uc0J0bnMoKVxyXG4gICAgICAgIGdhbGxlcnkudmlld0ltYWdlc0xpc3QoKVxyXG5cclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZVdpZGdldCBmcm9tICcuLy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlV2lkZ2V0J1xyXG5pbXBvcnQge0FDVElPTl9FRElULCBBQ1RJT05fREVMRVRFfSBmcm9tICcuL2FkZEdvb2RzVGFicy9EYXRhRm9ybSdcclxuaW1wb3J0IERlbGV0ZVZpZXcgZnJvbSAnLi9Hb29kc0dyaWQvRGVsZXRlVmlldydcclxuaW1wb3J0IE92ZXJsYXkgICAgZnJvbSAnLi8uLi8uLi8uLi8uLi9PdmVybGF5J1xyXG5pbXBvcnQgY29uZmlnICAgICBmcm9tICcuLy4uLy4uLy4uLy4uLy4uL2NvbmZpZy5qc29uJ1xyXG5cclxuY29uc3QgQUNUSU9OX1ZJRVcgPSAndmlldydcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdvb2RzR3JpZCBleHRlbmRzIEJhc2VXaWRnZXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHN1cGVyKClcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lclxyXG4gICAgICAgIHRoaXMuZ3JpZCAgICAgID0gdGhpcy5kb20uc2VsZWN0KCcudGFibGUgdGJvZHknKVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWQgID0gbnVsbFxyXG5cclxuICAgICAgICB0aGlzLndpZGdldHMuc2V0KERlbGV0ZVZpZXcuaW5pdCgnZ29vZHMtZGVsZXRlJykpXHJcblxyXG4gICAgICAgIHRoaXMub25jYW5jZWwgICAgID0gdGhpcy5vbkNhbmNlbERlbGV0ZS5iaW5kKHRoaXMpXHJcbiAgICAgICAgdGhpcy5ncmlkLm9uY2xpY2sgPSB0aGlzLnByb3h5LmJpbmQodGhpcylcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIGxldCBjb250YWluZXJOb2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY29udGFpbmVySWQpXHJcbiAgICAgICAgcmV0dXJuIG5ldyBHb29kc0dyaWQoY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGF0YSgpICB7IHJldHVybiB0aGlzLnBhcmVudC5wYXJlbnQuZGF0YSB9XHJcbiAgICBnZXQgbW9kZWwoKSB7IHJldHVybiB0aGlzLnBhcmVudC5wYXJlbnQubW9kZWwgfVxyXG5cclxuICAgIHByb3h5KGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS50YXJnZXRcclxuXHJcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gdGFyZ2V0LmNsb3Nlc3QoJ3RyJylcclxuICAgICAgICBpZiAoc2VsZWN0ZWQpIHRoaXMuc2VsZWN0ZWQgPSBzZWxlY3RlZFxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnBhcmVudE5vZGUuaGFzQXR0cmlidXRlKCdocmVmJykpIHRhcmdldCA9IHRhcmdldC5wYXJlbnROb2RlXHJcbiAgICAgICAgaWYgKCF0YXJnZXQuaGFzQXR0cmlidXRlKCdocmVmJykpIHJldHVyblxyXG5cclxuICAgICAgICBsZXQgcGFyc2VkVXJpID0gdGFyZ2V0LmdldEF0dHJpYnV0ZSgnaHJlZicpLnNsaWNlKDEpLnNwbGl0KCcvJylcclxuICAgICAgICBsZXQgaWQgICAgICAgID0gcGFyc2VkVXJpLnBvcCgpXHJcbiAgICAgICAgbGV0IGFjdGlvbiAgICA9IHBhcnNlZFVyaS5wb3AoKVxyXG4gICAgICAgIGxldCBvdmVybGF5ICAgPSBPdmVybGF5Lmluc3RhbmNlXHJcblxyXG4gICAgICAgIGxldCBzd2l0Y2hBY3Rpb24gPSAoYWN0aW9uKSA9PiB7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEFDVElPTl9FRElUICA6IHRoaXMub25FZGl0QnRuQ2xpY2soZSk7IGJyZWFrXHJcbiAgICAgICAgICAgICAgICBjYXNlIEFDVElPTl9ERUxFVEU6IHRoaXMub25EZWxldGVCdG5DbGljayhlKTsgYnJlYWtcclxuICAgICAgICAgICAgICAgIGNhc2UgQUNUSU9OX1ZJRVcgIDogdGhpcy5vblZpZXdDbGljayhlKTsgYnJlYWtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbGV0IGlzQWxyZWFkeUNhY2hlZCA9IHRoaXMuZGF0YS5oYXMoJ2dvb2RzJykgJiYgdGhpcy5kYXRhLmdldCgnZ29vZHMnKS5pZCA9PSBpZFxyXG5cclxuICAgICAgICBpZiAoaXNBbHJlYWR5Q2FjaGVkKSB7XHJcbiAgICAgICAgICAgIHN3aXRjaEFjdGlvbihhY3Rpb24pXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgb3ZlcmxheS5zZXRUZXh0KCdsb2FkIGRhdGEnKS5lbmFibGUoKVxyXG4gICAgICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgICAgICAuZ2V0KGlkKVxyXG4gICAgICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRhdGEuc2V0KCdnb29kcycsIGRhdGEpXHJcbiAgICAgICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgICAgICAgICBzd2l0Y2hBY3Rpb24oYWN0aW9uKVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25FZGl0QnRuQ2xpY2soZSkge1xyXG4gICAgICAgIGxldCBwYXJlbnRUYWJzID0gdGhpcy5wYXJlbnRcclxuICAgICAgICBsZXQgb3ZlcmxheSAgICA9IE92ZXJsYXkuaW5zdGFuY2VcclxuICAgICAgICBsZXQgZm9ybSAgICAgICA9IHBhcmVudFRhYnMuZ2V0KCdhZGRHb29kc1RhYnMgPiBkYXRhRm9ybScpXHJcblxyXG4gICAgICAgIGZvcm0ucmVzZXQoKS5zaG93KCkuZm9jdXMoJ3RpdGxlJylcclxuXHJcbiAgICAgICAgb3ZlcmxheS5zZXRUZXh0KCdsb2FkIGRhdGEnKS5lbmFibGUoKVxyXG5cclxuICAgICAgICBwYXJlbnRUYWJzLnNob3cocGFyZW50VGFicy5lZGl0VGFiKVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5nZXQodGhpcy5kYXRhLmdldCgnZ29vZHMnKS5pZClcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGZvcm0uZGF0YSA9IGRhdGFcclxuICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgb25EZWxldGVCdG5DbGljayhlKSB7XHJcbiAgICAgICAgbGV0IGRlbGV0ZVZpZXcgPSB0aGlzLmdldCgnZGVsZXRlVmlldycpXHJcbiAgICAgICAgdGhpcy5oaWRlR3JpZCgpXHJcbiAgICAgICAgZGVsZXRlVmlldy5yZXNldCgpLnNob3coKVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2FuY2VsRGVsZXRlKGUpIHtcclxuICAgICAgICBpZiAoIWUudGFyZ2V0LmVxdWFscyh0aGlzLCAnZGVsZXRlVmlldycpKSByZXR1cm5cclxuICAgICAgICB0aGlzLnNob3dHcmlkKClcclxuICAgIH1cclxuXHJcbiAgICBvblZpZXdDbGljayhlKSB7XHJcbiAgICAgICAgbGV0IHBhcmVudCA9IHRoaXMucGFyZW50XHJcbiAgICAgICAgcGFyZW50LnNob3cocGFyZW50LnZpZXdUYWIpXHJcbiAgICB9XHJcblxyXG4gICAgcG9wdWxhdGUoZGF0YSkge1xyXG4gICAgICAgIHRoaXMuZ2V0KCdkZWxldGVWaWV3JykuaGlkZSgpXHJcblxyXG4gICAgICAgIHRoaXMuZ3JpZC5pbm5lckhUTUwgPSAnJ1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5hZGRSb3coZGF0YVtpXSlcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBhZGRSb3coZGF0YSkge1xyXG4gICAgICAgIGxldCB0ZXh0ID0gKHRleHQpID0+IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RkJykuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlVGV4dE5vZGUodGV4dCkpLnBhcmVudE5vZGVcclxuICAgICAgICBsZXQgcm93ID0gdGhpcy5ncmlkLmluc2VydFJvdygpXHJcblxyXG4gICAgICAgIHJvdy5kYXRhc2V0LmlkID0gZGF0YS5pZFxyXG4gICAgICAgIHJvdy5hcHBlbmRDaGlsZCh0ZXh0KHJvdy5yb3dJbmRleCkpXHJcblxyXG4gICAgICAgIGxldCB0aXRsZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3RkJylcclxuICAgICAgICByb3cuYXBwZW5kQ2hpbGQodGl0bGUpXHJcbiAgICAgICAgdGl0bGUuaW5uZXJIVE1MID0gYDxhIGhyZWY9XCIjdmlldy8ke2RhdGEuaWR9XCI+JHtkYXRhLnRpdGxlfTwvYT5gXHJcblxyXG4gICAgICAgIHJvdy5hcHBlbmRDaGlsZCh0ZXh0KGRhdGEucHJpY2UpKVxyXG4gICAgICAgIHJvdy5hcHBlbmRDaGlsZCh0ZXh0KGRhdGEucXVhbnRpdHkpKVxyXG5cclxuXHJcbiAgICAgICAgbGV0IGFjdGlvbnMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCd0ZCcpXHJcbiAgICAgICAgcm93LmFwcGVuZENoaWxkKGFjdGlvbnMpXHJcblxyXG4gICAgICAgIGFjdGlvbnMuaW5uZXJIVE1MID1cclxuICAgICAgICAgICAgYDxhIGhyZWY9XCIjZWRpdC8ke2RhdGEuaWR9XCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgYnRuLXhzXCI+PGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwtc3F1YXJlLW9cIj48L2k+ICR7Y29uZmlnLmkxOG4udWtbJ2VkaXQnXX08L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIjZGVsZXRlLyR7ZGF0YS5pZH1cIiBjbGFzcz1cImJ0biBidG4tZGFuZ2VyIGJ0bi14c1wiPjxpIGNsYXNzPVwiZmEgZmEtdHJhc2gtb1wiPjwvaT4gJHtjb25maWcuaTE4bi51a1snZGVsZXRlJ119PC9hPmBcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVSb3coKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkLnBhcmVudE5vZGUuZGVsZXRlUm93KHRoaXMuc2VsZWN0ZWQucm93SW5kZXgpXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZCA9IG51bGxcclxuICAgICAgICB0aGlzLnJlYnVpbGRHcmlkSW5kZXhlcygpXHJcbiAgICAgICAgdGhpcy5zaG93R3JpZCgpXHJcbiAgICB9XHJcblxyXG4gICAgcmVidWlsZEdyaWRJbmRleGVzKCkge1xyXG4gICAgICAgIGxldCByb3dzID0gdGhpcy5ncmlkLnJvd3NcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaTxyb3dzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHJvd3NbaV0uY2VsbHNbMF0udGV4dENvbnRlbnQgPSByb3dzW2ldLnJvd0luZGV4XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGhpZGVHcmlkKCkge1xyXG4gICAgICAgIHRoaXMuZ3JpZC5wYXJlbnROb2RlLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBzaG93R3JpZCgpIHtcclxuICAgICAgICB0aGlzLmdyaWQucGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG9uY2FuY2VsKGZuKSB7IHRoaXMub24oJ2NhbmNlbCcsIGZuKSB9XHJcbn0iLCJpbXBvcnQgQmFzZURlbGV0ZSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi9saWIvQmFzZURlbGV0ZSc7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEZWxldGVWaWV3IGV4dGVuZHMgQmFzZURlbGV0ZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoY29udGFpbmVyKTtcclxuICAgICAgICB0aGlzLmNhbmNlbEJ0biA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2FbaHJlZl49XCIjY2FuY2VsXCJdJyk7XHJcblxyXG4gICAgICAgIHRoaXMuY2FuY2VsQnRuLm9uY2xpY2sgPSB0aGlzLm9uQ2FuY2VsQnRuQ2xpY2suYmluZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIGxldCBjb250YWluZXJOb2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY29udGFpbmVySWQpO1xyXG4gICAgICAgIHJldHVybiBuZXcgRGVsZXRlVmlldyhjb250YWluZXJOb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNhbmNlbEJ0bkNsaWNrKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpO1xyXG4gICAgICAgIHRoaXMuZW1pdCgnY2FuY2VsJywge3RhcmdldDogdGhpc30pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2soZSkge1xyXG4gICAgICAgIHN1cGVyLm9uQ2xpY2soZSk7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICB9XHJcblxyXG59IiwiaW1wb3J0IGNvbmZpZyAgICAgZnJvbSAnLi4vLi4vLi4vLi4vLi4vY29uZmlnLmpzb24nXHJcbmltcG9ydCBCYXNlV2lkZ2V0IGZyb20gJy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlV2lkZ2V0J1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR29vZHNWaWV3IGV4dGVuZHMgQmFzZVdpZGdldCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gY29udGFpbmVyXHJcbiAgICAgICAgdGhpcy50YWJsZUJvZHkgPSB0aGlzLmRvbS5zZWxlY3QoJ3RhYmxlIHRib2R5JylcclxuICAgICAgICB0aGlzLmVkaXRCdG4gICA9IHRoaXMuZG9tLnNlbGVjdCgnLmNsb3NlLWJveCAuZWRpdC1idG4nKVxyXG5cclxuICAgICAgICB0aGlzLmVkaXRCdG4ub25jbGljayAgPSB0aGlzLm9uRWRpdC5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVySWQpIHtcclxuICAgICAgICBsZXQgY29udGFpbmVyTm9kZSA9IEJhc2VXaWRnZXQuYnlJZChjb250YWluZXJJZClcclxuICAgICAgICByZXR1cm4gbmV3IEdvb2RzVmlldyhjb250YWluZXJOb2RlKVxyXG4gICAgfVxyXG5cclxuICAgIHBvcHVsYXRlKGRhdGEpIHtcclxuICAgICAgICBsZXQgZm9ybWF0dGVyICA9IG5ldyBJbnRsLkRhdGVUaW1lRm9ybWF0KCd1ay1VQScpXHJcbiAgICAgICAgbGV0IGxhbmdDb25maWcgPSBjb25maWcuaTE4bi51a1xyXG5cclxuICAgICAgICBsZXQgdHJhbnNsRGF0YSA9IHt9XHJcbiAgICAgICAgdHJhbnNsRGF0YVtsYW5nQ29uZmlnLnRpdGxlXSA9IGRhdGEudGl0bGVcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcuY29kZV0gPSBkYXRhLmNvZGVcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcuc2x1Z10gPSBkYXRhLnNsdWdcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcuZGVzY3JpcHRpb25dID0gZGF0YS5kZXNjcmlwdGlvblxyXG4gICAgICAgIHRyYW5zbERhdGFbbGFuZ0NvbmZpZy5zaG9ydF9kZXNjcmlwdGlvbl0gPSBkYXRhLnNob3J0X2Rlc2NyaXB0aW9uXHJcbiAgICAgICAgdHJhbnNsRGF0YVtsYW5nQ29uZmlnLnByb2R1Y2VyXSA9IGRhdGEucHJvZHVjZXJcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcucHJpY2VdID0gZGF0YS5wcmljZVxyXG4gICAgICAgIHRyYW5zbERhdGFbbGFuZ0NvbmZpZy5xdWFudGl0eV0gPSBkYXRhLnF1YW50aXR5XHJcbiAgICAgICAgdHJhbnNsRGF0YVtsYW5nQ29uZmlnLmNyZWF0ZWRdID0gZm9ybWF0dGVyLmZvcm1hdChuZXcgRGF0ZShwYXJzZUludChkYXRhLmNyZWF0ZWQpKSlcclxuICAgICAgICB0cmFuc2xEYXRhW2xhbmdDb25maWcudXBkYXRlZF0gPSBmb3JtYXR0ZXIuZm9ybWF0KG5ldyBEYXRlKHBhcnNlSW50KGRhdGEudXBkYXRlZCkpKVxyXG4gICAgICAgIHRyYW5zbERhdGFbbGFuZ0NvbmZpZy5pc190b3BdID0gZGF0YS5pc190b3AgPT0gMCA/IGxhbmdDb25maWcubm8gOiBsYW5nQ29uZmlnLnllc1xyXG4gICAgICAgIHRyYW5zbERhdGFbbGFuZ0NvbmZpZy5pc19pbmRleF9jYXJvdXNlbF0gPSBkYXRhLmlzX2luZGV4X2Nhcm91c2VsID09IDAgPyBsYW5nQ29uZmlnLm5vIDogbGFuZ0NvbmZpZy55ZXNcclxuXHJcbiAgICAgICAgbGV0IGh0bWwgPSBbXVxyXG4gICAgICAgIGZvcihsZXQga2V5IGluIHRyYW5zbERhdGEpIGlmICh0cmFuc2xEYXRhLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuICAgICAgICAgICAgaHRtbC5wdXNoKGA8dHI+PHRkPiR7a2V5fTwvdGQ+PHRkPiR7dHJhbnNsRGF0YVtrZXldfTwvdGQ+PC90cj5gKVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnRhYmxlQm9keS5pbm5lckhUTUwgPSBodG1sLmpvaW4oJycpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBvbkVkaXQoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgICAgIHRoaXMucGFyZW50LmdldCgnZ29vZHNHcmlkJykub25FZGl0QnRuQ2xpY2soKVxyXG4gICAgfVxyXG59IiwiaW1wb3J0IEJhc2VGb3JtIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlRm9ybSc7XHJcblxyXG5leHBvcnQgY29uc3QgQUNUSU9OX0FERCAgICA9ICdhZGQnO1xyXG5leHBvcnQgY29uc3QgQUNUSU9OX0VESVQgICA9ICdlZGl0JztcclxuZXhwb3J0IGNvbnN0IEFDVElPTl9ERUxFVEUgPSAnZGVsZXRlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhRm9ybSBleHRlbmRzIEJhc2VGb3JtIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcihjb250YWluZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBEYXRhRm9ybShjb250YWluZXJOb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0FkZCgpIHsgcmV0dXJuIHRoaXMuYWN0aW9uID09PSBBQ1RJT05fQUREIH1cclxuXHJcbiAgICBpc0VkaXQoKSB7IHJldHVybiB0aGlzLmFjdGlvbiA9PT0gQUNUSU9OX0VESVQgfVxyXG5cclxuICAgIHNldCBkYXRhKGRhdGEpIHtcclxuICAgICAgICBpZiAoZGF0YS5wcmljZSkgZGF0YS5wcmljZSA9IGRhdGEucHJpY2UudG9GaXhlZCgyKVxyXG4gICAgICAgIHN1cGVyLmRhdGEgPSBkYXRhXHJcbiAgICB9XHJcbiAgICBnZXQgZGF0YSgpIHsgcmV0dXJuIHN1cGVyLmRhdGEgfVxyXG59XHJcblxyXG4iLCJpbXBvcnQgQmFzZVdpZGdldCAgIGZyb20gJy4uLy4uLy4uLy4uLy4uL0Jhc2VDYXRhbG9nV2lkZ2V0J1xyXG5pbXBvcnQgTW9kZWwgICAgICAgIGZyb20gJy4vZ29vZHNEZXRhaWxlZC9Nb2RlbCdcclxuaW1wb3J0IERldGFpbGVkR3JpZCBmcm9tICcuL2dvb2RzRGV0YWlsZWQvRGV0YWlsZWRHcmlkJ1xyXG5pbXBvcnQgT3ZlcmxheSAgICAgIGZyb20gJy4vLi4vLi4vLi4vLi4vLi4vT3ZlcmxheSdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdvb2RzRGV0YWlsZWQgZXh0ZW5kcyBCYXNlV2lkZ2V0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXJcclxuICAgICAgICB0aGlzLm1vZGVsICAgICA9IG5ldyBNb2RlbCgpXHJcblxyXG4gICAgICAgIHRoaXMud2lkZ2V0cy5zZXQoRGV0YWlsZWRHcmlkLmluaXQoJ2RldGFpbGVkLWdyaWQnKSlcclxuXHJcbiAgICAgICAgdGhpcy5vbnN1Ym1pdCA9IHRoaXMuc3VibWl0LmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLm9uZGVsZXRlID0gdGhpcy5zdWJtaXREZWxldGUuYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZClcclxuICAgICAgICByZXR1cm4gbmV3IEdvb2RzRGV0YWlsZWQoY29udGFpbmVyTm9kZSlcclxuICAgIH1cclxuXHJcbiAgICBwb3B1bGF0ZUxpc3QoKSB7XHJcbiAgICAgICAgbGV0IG92ZXJsYXkgPSBPdmVybGF5Lmluc3RhbmNlXHJcbiAgICAgICAgb3ZlcmxheS5lbmFibGUoKVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5nZXRCeUdvb2RzKHRoaXMuZGF0YS5nZXQoJ2dvb2RzJykuaWQpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGV0YWlsZWRHcmlkID0gdGhpcy5nZXQoJ2RldGFpbGVkR3JpZCcpXHJcbiAgICAgICAgICAgICAgICBkZXRhaWxlZEdyaWQucG9wdWxhdGUoZGF0YSlcclxuICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyb3JzKSA9PiBvdmVybGF5LnNldFRleHQoXCJBbiBlcnJvciBoYXMgb2NjdXJyZWQuIFBsZWFzZSwgY29udGFjdCBkZXZlbG9wZXJcIikpXHJcbiAgICB9XHJcblxyXG4gICAgc3VibWl0KGUpIHtcclxuICAgICAgICBsZXQgZm9ybSA9IGUudGFyZ2V0XHJcbiAgICAgICAgbGV0IGdyaWQgPSB0aGlzLmdldCgnZGV0YWlsZWRHcmlkJylcclxuXHJcbiAgICAgICAgaWYgKCFmb3JtLmVxdWFscyhncmlkLCAnZm9ybScpKSByZXR1cm5cclxuXHJcbiAgICAgICAgbGV0IGRhdGEgICAgPSBlLmRhdGFcclxuICAgICAgICBsZXQgb3ZlcmxheSA9IE92ZXJsYXkuaW5zdGFuY2VcclxuXHJcbiAgICAgICAgb3ZlcmxheS5lbmFibGUoKVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5zZXQoZGF0YSlcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGZvcm0uaGlkZSgpLnJlc2V0KClcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZm9ybS5pc0FkZCgpKSBncmlkLmFkZFJvdyhkYXRhKVxyXG4gICAgICAgICAgICAgICAgZWxzZSBncmlkLmVkaXRSb3coZGF0YSlcclxuXHJcbiAgICAgICAgICAgICAgICBncmlkLnN1Ym1pdEZvcm0oKVxyXG4gICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcnMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlcnJvcnMgaW5zdGFuY2VvZiBFcnJvcikgb3ZlcmxheS5zZXRUZXh0KGVycm9ycy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ybS5zZXRFcnJvcnMoZXJyb3JzKVxyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgc3VibWl0RGVsZXRlKGUpIHtcclxuICAgICAgICBsZXQgdGFyZ2V0ICAgICAgID0gZS50YXJnZXRcclxuICAgICAgICBsZXQgZGV0YWlsZWRHcmlkID0gdGhpcy5nZXQoJ2RldGFpbGVkR3JpZCcpXHJcbiAgICAgICAgaWYgKCF0YXJnZXQuZXF1YWxzKGRldGFpbGVkR3JpZC5nZXQoJ2RlbGV0ZVZpZXcnKSkpIHJldHVyblxyXG5cclxuICAgICAgICBsZXQgb3ZlcmxheSA9IE92ZXJsYXkuaW5zdGFuY2VcclxuICAgICAgICBsZXQgaWQgICAgICA9IGRldGFpbGVkR3JpZC5zZWxlY3RlZCA/IGRldGFpbGVkR3JpZC5zZWxlY3RlZC5kYXRhLmlkIDogbnVsbFxyXG5cclxuICAgICAgICBvdmVybGF5LmVuYWJsZSgpXHJcbiAgICAgICAgaWYgKCFpZCkge1xyXG4gICAgICAgICAgICAvLyBUT0RPIGhhbmRsZSB3aGVuIGlkIG5vdCBleGlzdHNcclxuICAgICAgICAgICAgcmV0dXJuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5yZW1vdmUoaWQpXHJcbiAgICAgICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGRldGFpbGVkR3JpZC5zdWJtaXREZWxldGUoKVxyXG4gICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4gb3ZlcmxheS5zZXRUZXh0KGVycm9yLm1lc3NhZ2UpKVxyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbnN1Ym1pdChmbikgeyB0aGlzLm9uKCdzdWJtaXQnLCBmbikgfVxyXG4gICAgc2V0IG9uZGVsZXRlKGZuKSB7IHRoaXMub24oJ2RlbGV0ZScsIGZuKSB9XHJcbn0iLCJpbXBvcnQgQmFzZVdpZGdldCBmcm9tICcuLi8uLi8uLi8uLi8uLi9CYXNlQ2F0YWxvZ1dpZGdldCdcclxuaW1wb3J0IE92ZXJsYXkgICAgZnJvbSAnLi4vLi4vLi4vLi4vLi4vT3ZlcmxheSdcclxuaW1wb3J0IE1vZGVsICAgICAgZnJvbSAnLi9nb29kc0dhbGxlcnkvTW9kZWwnXHJcbmltcG9ydCBMaXN0ICAgICAgIGZyb20gJy4vZ29vZHNHYWxsZXJ5L0xpc3QnXHJcbmltcG9ydCBVcGxvYWRGb3JtIGZyb20gJy4vZ29vZHNHYWxsZXJ5L1VwbG9hZEZvcm0nXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBHb29kc0dhbGxlcnkgZXh0ZW5kcyBCYXNlV2lkZ2V0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXJcclxuICAgICAgICB0aGlzLm1vZGVsICAgICA9IG5ldyBNb2RlbCgpXHJcblxyXG4gICAgICAgIHRoaXMud2lkZ2V0c1xyXG4gICAgICAgICAgICAuc2V0KExpc3QuaW5pdCgnZ29vZHMtZ2FsbGVyeS1saXN0JykpXHJcbiAgICAgICAgICAgIC5zZXQoVXBsb2FkRm9ybS5pbml0KCdnb29kcy1nYWxsZXJ5LWZvcm0nKSlcclxuXHJcbiAgICAgICAgdGhpcy5vbnN1Ym1pdCA9IHRoaXMuc3VibWl0LmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLm9uZGVsZXRlID0gdGhpcy5yZW1vdmUuYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBHb29kc0dhbGxlcnkoQmFzZVdpZGdldC5ieUlkKGNvbnRhaW5lcklkKSlcclxuICAgIH1cclxuXHJcbiAgICB2aWV3SW1hZ2VzTGlzdCgpIHtcclxuICAgICAgICBsZXQgZ29vZHNJZCA9IHRoaXMuZGF0YS5nZXQoJ2dvb2RzJykuaWRcclxuICAgICAgICBsZXQgb3ZlcmxheSA9IE92ZXJsYXkuaW5zdGFuY2VcclxuICAgICAgICBsZXQgbGlzdCAgICA9IHRoaXMuZ2V0KCdsaXN0JylcclxuXHJcbiAgICAgICAgb3ZlcmxheS5lbmFibGUoKVxyXG5cclxuICAgICAgICB0aGlzLm1vZGVsXHJcbiAgICAgICAgICAgIC5nZXRCeUdvb2RzKGdvb2RzSWQpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsaXN0LnBvcHVsYXRlKGRhdGEpXHJcbiAgICAgICAgICAgICAgICBvdmVybGF5LmRpc2FibGUoKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdChlKSB7XHJcbiAgICAgICAgbGV0IGZvcm0gPSB0aGlzLmdldCgndXBsb2FkRm9ybScpXHJcbiAgICAgICAgaWYgKCFmb3JtLmVxdWFscyhlLnRhcmdldCkpIHJldHVyblxyXG5cclxuICAgICAgICBsZXQgbGlzdCAgICA9IHRoaXMuZ2V0KCdsaXN0JylcclxuICAgICAgICBsZXQgb3ZlcmxheSA9IE92ZXJsYXkuaW5zdGFuY2VcclxuICAgICAgICBvdmVybGF5LmVuYWJsZSgpXHJcblxyXG4gICAgICAgIHRoaXMubW9kZWxcclxuICAgICAgICAgICAgLnNldChlLmRhdGEpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsaXN0LmFkZChkYXRhKVxyXG4gICAgICAgICAgICAgICAgZm9ybS5jb250YWluZXIucmVzZXQoKVxyXG4gICAgICAgICAgICAgICAgb3ZlcmxheS5kaXNhYmxlKClcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKChlcnJvcnMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChlcnJvcnMgaW5zdGFuY2VvZiBFcnJvcikgb3ZlcmxheS5zZXRUZXh0KGVycm9ycy5tZXNzYWdlKVxyXG4gICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ybS5zZXRFcnJvcnMoZXJyb3JzKVxyXG4gICAgICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlKGUpIHtcclxuICAgICAgICBsZXQgbGlzdCAgID0gdGhpcy5nZXQoJ2xpc3QnKVxyXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLnRhcmdldFxyXG4gICAgICAgIGlmICghbGlzdC5lcXVhbHModGFyZ2V0KSkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBvdmVybGF5ID0gT3ZlcmxheS5pbnN0YW5jZVxyXG4gICAgICAgIG92ZXJsYXkuZW5hYmxlKClcclxuXHJcbiAgICAgICAgdGhpcy5tb2RlbFxyXG4gICAgICAgICAgICAucmVtb3ZlKGUuaW1hZ2VJZClcclxuICAgICAgICAgICAgLnRoZW4oKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIGxpc3QuZGVsZXRlQWN0aXZlKClcclxuICAgICAgICAgICAgICAgIG92ZXJsYXkuZGlzYWJsZSgpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG9uc3VibWl0KGZuKSB7IHRoaXMub24oJ3N1Ym1pdCcsIGZuKSB9XHJcbiAgICBzZXQgb25kZWxldGUoZm4pIHsgdGhpcy5vbignZGVsZXRlJywgZm4pIH1cclxufSIsImltcG9ydCBjb25maWcgZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vLi4vY29uZmlnLmpzb24nXHJcbmltcG9ydCBPdmVybGF5IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL092ZXJsYXknXHJcbmltcG9ydCBCYXNlV2lkZ2V0IGZyb20gJy4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vbGliL0Jhc2VXaWRnZXQnXHJcbmltcG9ydCB7Rm9ybSwgQUNUSU9OX0FERCwgQUNUSU9OX0VESVQsIEFDVElPTl9SRU1PVkV9IGZyb20gJy4vZGV0YWlsZWRHcmlkL0Zvcm0nXHJcbmltcG9ydCBEZWxldGVWaWV3IGZyb20gJy4vZGV0YWlsZWRHcmlkL0RlbGV0ZVZpZXcnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEZXRhaWxlZEdyaWQgZXh0ZW5kcyBCYXNlV2lkZ2V0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcigpXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXJcclxuICAgICAgICB0aGlzLmdyaWQgICAgICA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJy5kZXRhaWxlZC10YWJsZSB0Ym9keScpXHJcbiAgICAgICAgdGhpcy5hZGRCdG4gICAgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCcuYWN0aW9ucyAuYWRkLWJ0bicpXHJcblxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWQgPSBudWxsXHJcblxyXG4gICAgICAgIHRoaXMud2lkZ2V0c1xyXG4gICAgICAgICAgICAuc2V0KEZvcm0uaW5pdCgnZGV0YWlsZWQtZm9ybScpKVxyXG4gICAgICAgICAgICAuc2V0KERlbGV0ZVZpZXcuaW5pdCgnZGV0YWlsZWQtZGVsZXRlJykpXHJcblxyXG4gICAgICAgIHRoaXMub25jYW5jZWwgICAgICAgPSB0aGlzLm9uQ2FuY2VsLmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLmFkZEJ0bi5vbmNsaWNrID0gdGhpcy5vbkFkZEJ0bkNsaWNrLmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLmdyaWQub25jbGljayAgID0gdGhpcy5wcm94eS5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGluaXQoY29udGFpbmVySWQpIHtcclxuICAgICAgICBsZXQgY29udGFpbmVyTm9kZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbnRhaW5lcklkKVxyXG4gICAgICAgIHJldHVybiBuZXcgRGV0YWlsZWRHcmlkKGNvbnRhaW5lck5vZGUpXHJcbiAgICB9XHJcblxyXG4gICAgcHJveHkoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgICAgIGxldCB0YXJnZXQgPSBlLnRhcmdldFxyXG5cclxuICAgICAgICBsZXQgc2VsZWN0ZWRSb3cgPSB0YXJnZXQuY2xvc2VzdCgndHInKVxyXG4gICAgICAgIGlmIChzZWxlY3RlZFJvdykgdGhpcy5zZWxlY3RlZCA9IHsgZGF0YTogdGhpcy5nZXRSb3dEYXRhKHNlbGVjdGVkUm93KSwgbm9kZTogc2VsZWN0ZWRSb3cgfVxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnBhcmVudE5vZGUuaGFzQXR0cmlidXRlKCdocmVmJykpIHRhcmdldCA9IHRhcmdldC5wYXJlbnROb2RlXHJcbiAgICAgICAgaWYgKCF0YXJnZXQuaGFzQXR0cmlidXRlKCdocmVmJykpIHJldHVyblxyXG5cclxuICAgICAgICBsZXQgcGFyc2VkVXJpID0gZS50YXJnZXQuZ2V0QXR0cmlidXRlKCdocmVmJykuc2xpY2UoMSkuc3BsaXQoJy8nKVxyXG4gICAgICAgIGUuaWQgICAgICAgICAgPSBwYXJzZWRVcmkucG9wKClcclxuICAgICAgICBsZXQgYWN0aW9uICAgID0gcGFyc2VkVXJpLnBvcCgpXHJcblxyXG4gICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XHJcbiAgICAgICAgICAgIGNhc2UgQUNUSU9OX0VESVQgIDogdGhpcy5vbkVkaXRCdG5DbGljayhlKTsgYnJlYWtcclxuICAgICAgICAgICAgY2FzZSBBQ1RJT05fUkVNT1ZFOiB0aGlzLm9uRGVsZXRlQnRuQ2xpY2soZSk7IGJyZWFrXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQWRkQnRuQ2xpY2soZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgICAgIGxldCBmb3JtID0gdGhpcy5nZXQoJ2Zvcm0nKVxyXG4gICAgICAgIGZvcm0ucmVzZXQoKS5zaG93KCkuZm9jdXMoJ3RpdGxlJylcclxuICAgICAgICBmb3JtLmFjdGlvbiA9IEFDVElPTl9BRERcclxuICAgICAgICBmb3JtLmRhdGEgICA9IHsgZ29vZHNfaWQ6IHRoaXMucGFyZW50LmRhdGEuZ2V0KCdnb29kcycpLmlkIH1cclxuXHJcbiAgICAgICAgdGhpcy5oaWRlQWRkQnRuKClcclxuICAgIH1cclxuXHJcbiAgICBvbkVkaXRCdG5DbGljayhlKSB7XHJcbiAgICAgICAgbGV0IGZvcm0gPSB0aGlzLmdldCgnZm9ybScpXHJcbiAgICAgICAgbGV0IHJvdyAgPSBlLnRhcmdldC5jbG9zZXN0KCd0cicpXHJcbiAgICAgICAgZm9ybS5hY3Rpb24gPSBBQ1RJT05fRURJVFxyXG4gICAgICAgIGZvcm0ucmVzZXQoKS5zaG93KCkuZm9jdXMoJ3RpdGxlJylcclxuICAgICAgICBmb3JtLmRhdGEgPSB0aGlzLmdldFJvd0RhdGEocm93KVxyXG5cclxuICAgICAgICB0aGlzLmhpZGVBZGRCdG4oKVxyXG4gICAgfVxyXG5cclxuICAgIG9uRGVsZXRlQnRuQ2xpY2soZSkge1xyXG4gICAgICAgIGxldCBkZWxldGVWaWV3ID0gdGhpcy5nZXQoJ2RlbGV0ZVZpZXcnKVxyXG4gICAgICAgIHRoaXMuaGlkZUdyaWQoKVxyXG4gICAgICAgIHRoaXMuZ2V0KCdmb3JtJykuaGlkZSgpXHJcbiAgICAgICAgZGVsZXRlVmlldy5yZXNldCgpLnNob3coKVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2FuY2VsKGUpIHtcclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS50YXJnZXRcclxuICAgICAgICBpZiAodGFyZ2V0LmVxdWFscyh0aGlzLCAnZGVsZXRlVmlldycpKSB0aGlzLnNob3dHcmlkKClcclxuICAgICAgICBpZiAodGFyZ2V0LmVxdWFscyh0aGlzLCAnZm9ybScpKSB0aGlzLnNob3dHcmlkKClcclxuICAgIH1cclxuXHJcbiAgICBzdWJtaXRGb3JtKCkge1xyXG4gICAgICAgIHRoaXMuc2hvd0dyaWQoKVxyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdERlbGV0ZSgpIHtcclxuICAgICAgICB0aGlzLmRlbGV0ZVNlbGVjdGVkUm93KClcclxuICAgICAgICB0aGlzLnNob3dHcmlkKClcclxuICAgIH1cclxuXHJcbiAgICBwb3B1bGF0ZShkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkLmlubmVySFRNTCA9ICcnXHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLmFkZFJvdyhkYXRhW2ldKVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIGFkZFJvdyhkYXRhKSB7XHJcbiAgICAgICAgbGV0IHRleHQgPSAodGV4dCkgPT4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgndGQnKS5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSh0ZXh0KSkucGFyZW50Tm9kZVxyXG5cclxuICAgICAgICBsZXQgdHIgPSB0aGlzLmdyaWQuaW5zZXJ0Um93KClcclxuICAgICAgICB0ci5kYXRhc2V0LmlkID0gZGF0YS5pZFxyXG5cclxuICAgICAgICB0ci5hcHBlbmRDaGlsZCh0ZXh0KHRyLnJvd0luZGV4KSlcclxuICAgICAgICB0ci5hcHBlbmRDaGlsZCh0ZXh0KGRhdGEudGl0bGUpKVxyXG4gICAgICAgIHRyLmFwcGVuZENoaWxkKHRleHQoZGF0YS52YWx1ZSkpXHJcblxyXG4gICAgICAgIGxldCBhY3Rpb25zID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgndGQnKVxyXG4gICAgICAgIGFjdGlvbnMuaW5uZXJIVE1MID1cclxuICAgICAgICAgICAgYDxhIGhyZWY9XCIjZWRpdC8ke2RhdGEuaWR9XCIgY2xhc3M9XCJidG4gYnRuLWRlZmF1bHQgYnRuLXhzXCI+PGkgY2xhc3M9XCJmYSBmYS1wZW5jaWwtc3F1YXJlLW9cIj48L2k+ICR7Y29uZmlnLmkxOG4udWtbJ2VkaXQnXX08L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIjcmVtb3ZlLyR7ZGF0YS5pZH1cIiBjbGFzcz1cImJ0biBidG4tZGFuZ2VyIGJ0bi14c1wiPjxpIGNsYXNzPVwiZmEgZmEtdHJhc2gtb1wiPjwvaT4gJHtjb25maWcuaTE4bi51a1snZGVsZXRlJ119PC9hPmBcclxuXHJcbiAgICAgICAgdHIuYXBwZW5kQ2hpbGQoYWN0aW9ucylcclxuXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZCA9IHtcclxuICAgICAgICAgICAgZGF0YTogZGF0YSxcclxuICAgICAgICAgICAgbm9kZTogdHJcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdFJvdyhkYXRhKSB7XHJcbiAgICAgICAgbGV0IGNlbGxzID0gdGhpcy5zZWxlY3RlZC5ub2RlLmNlbGxzXHJcbiAgICAgICAgY2VsbHNbMV0udGV4dENvbnRlbnQgPSB0aGlzLnNlbGVjdGVkLmRhdGEudGl0bGUgPSBkYXRhLnRpdGxlXHJcbiAgICAgICAgY2VsbHNbMl0udGV4dENvbnRlbnQgPSB0aGlzLnNlbGVjdGVkLmRhdGEudmFsdWUgPSBkYXRhLnZhbHVlXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVTZWxlY3RlZFJvdygpIHtcclxuICAgICAgICB0aGlzLmdyaWQucGFyZW50Tm9kZS5kZWxldGVSb3codGhpcy5zZWxlY3RlZC5ub2RlLnJvd0luZGV4KVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWQgPSBudWxsXHJcbiAgICAgICAgdGhpcy5yZWJ1aWxkR3JpZEluZGV4ZXMoKVxyXG4gICAgfVxyXG5cclxuICAgIHJlYnVpbGRHcmlkSW5kZXhlcygpIHtcclxuICAgICAgICBsZXQgcm93cyA9IHRoaXMuZ3JpZC5yb3dzXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGk8cm93cy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICByb3dzW2ldLmNlbGxzWzBdLnRleHRDb250ZW50ID0gcm93c1tpXS5yb3dJbmRleFxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRSb3dEYXRhKHJvdykge1xyXG4gICAgICAgIGxldCBjZWxscyA9IHJvdy5jZWxsc1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIGlkICAgIDpyb3cuZGF0YXNldC5pZCxcclxuICAgICAgICAgICAgdGl0bGUgOmNlbGxzWzFdLnRleHRDb250ZW50LFxyXG4gICAgICAgICAgICB2YWx1ZSA6Y2VsbHNbMl0udGV4dENvbnRlbnRcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUdyaWQoKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkLnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuICAgICAgICB0aGlzLmhpZGVBZGRCdG4oKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0dyaWQoKSB7XHJcbiAgICAgICAgdGhpcy5ncmlkLnBhcmVudE5vZGUuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcclxuICAgICAgICB0aGlzLmFkZEJ0bi5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUFkZEJ0bigpIHtcclxuICAgICAgICB0aGlzLmFkZEJ0bi5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG9uY2FuY2VsKGZuKSB7IHRoaXMub24oJ2NhbmNlbCcsIGZuKSB9XHJcbn1cclxuIiwiaW1wb3J0IGNvbmZpZyBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi8uLi9jb25maWcuanNvbidcclxuaW1wb3J0IEJhc2VNb2RlbCBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi8uLi9saWIvQmFzZU1vZGVsJ1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTW9kZWwgZXh0ZW5kcyBCYXNlTW9kZWwge1xyXG5cclxuICAgIGdldCBjb25maWcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2NvbmZpZyA9PT0gdW5kZWZpbmVkKSB0aGlzLl9jb25maWcgPSBCYXNlTW9kZWwuZ2V0Q29uZmlnKCdnb29kc0RldGFpbGVkJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29uZmlnXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QnlHb29kcyhnb29kc0lkKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywgeyBtZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVCB9KVxyXG4gICAgICAgIG9wdGlvbnMudXJpID0gYCR7b3B0aW9ucy51cml9Z2lkLyR7Z29vZHNJZH1gXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdnZXRCeUdvb2RzJywge3RhcmdldDogdGhpcywgZGF0YTogZGF0YX0pXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxufSIsImltcG9ydCBCYXNlRGVsZXRlIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlRGVsZXRlJztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIERlbGV0ZVZpZXcgZXh0ZW5kcyBCYXNlRGVsZXRlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcihjb250YWluZXIpO1xyXG4gICAgICAgIHRoaXMuY2FuY2VsQnRuID0gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvcignYVtocmVmXj1cIiNjYW5jZWxcIl0nKTtcclxuXHJcbiAgICAgICAgdGhpcy5jYW5jZWxCdG4ub25jbGljayA9IHRoaXMub25DYW5jZWxCdG5DbGljay5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBEZWxldGVWaWV3KGNvbnRhaW5lck5vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2FuY2VsQnRuQ2xpY2soZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgICAgICB0aGlzLnJlc2V0KCk7XHJcbiAgICAgICAgdGhpcy5lbWl0KCdjYW5jZWwnLCB7dGFyZ2V0OiB0aGlzfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGljayhlKSB7XHJcbiAgICAgICAgc3VwZXIub25DbGljayhlKTtcclxuICAgICAgICB0aGlzLmhpZGUoKTtcclxuICAgIH1cclxuXHJcbn0iLCJpbXBvcnQgQmFzZUZvcm0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vLi4vLi4vbGliL0Jhc2VGb3JtJztcclxuXHJcbmV4cG9ydCBjb25zdCBBQ1RJT05fQUREICA9ICdhZGQnO1xyXG5leHBvcnQgY29uc3QgQUNUSU9OX0VESVQgPSAnZWRpdCc7XHJcbmV4cG9ydCBjb25zdCBBQ1RJT05fUkVNT1ZFID0gJ3JlbW92ZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybSBleHRlbmRzIEJhc2VGb3JtIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICBzdXBlcihjb250YWluZXIpO1xyXG5cclxuICAgICAgICB0aGlzLmNhbmNlbEJ0biA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJ2J1dHRvblt0eXBlPVwic3VibWl0XCJdK2J1dHRvblt0eXBlPVwiYnV0dG9uXCJdJyk7XHJcblxyXG4gICAgICAgIHRoaXMuY2FuY2VsQnRuLm9uY2xpY2sgPSB0aGlzLm9uQ2FuY2VsRm9ybS5iaW5kKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgbGV0IGNvbnRhaW5lck5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChjb250YWluZXJJZCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBGb3JtKGNvbnRhaW5lck5vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQWRkKCkgeyByZXR1cm4gdGhpcy5hY3Rpb24gPT09IEFDVElPTl9BREQgfVxyXG5cclxuICAgIGlzRWRpdCgpIHsgcmV0dXJuIHRoaXMuYWN0aW9uID09PSBBQ1RJT05fRURJVCB9XHJcblxyXG4gICAgb25DYW5jZWxGb3JtKGUpIHtcclxuICAgICAgICB0aGlzLnJlc2V0KCk7XHJcbiAgICAgICAgdGhpcy5oaWRlKCk7XHJcbiAgICAgICAgdGhpcy5lbWl0KCdjYW5jZWwnLCB7dGFyZ2V0OiB0aGlzfSk7XHJcbiAgICB9XHJcblxyXG59IiwiaW1wb3J0IEJhc2VXaWRnZXQgZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vLi4vbGliL0Jhc2VXaWRnZXQnXHJcbmltcG9ydCBPdmVybGF5ICAgIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL092ZXJsYXknXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBMaXN0IGV4dGVuZHMgQmFzZVdpZGdldCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gY29udGFpbmVyXHJcblxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLm9uY2xpY2sgPSB0aGlzLnByb3h5LmJpbmQodGhpcylcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgTGlzdChCYXNlV2lkZ2V0LmJ5SWQoY29udGFpbmVySWQpKVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBsaXN0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9saXN0ID09PSB1bmRlZmluZWQpIHRoaXMuX2xpc3QgPSB0aGlzLmRvbS5zZWxlY3QoJy5nYWxsZXJ5LWxpc3QnKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9saXN0XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlbGV0ZUJ0bigpIHtcclxuICAgICAgICBpZiAodGhpcy5fZGVsZXRlQnRuID09PSB1bmRlZmluZWQpIHRoaXMuX2RlbGV0ZUJ0biA9IHRoaXMuZG9tLnNlbGVjdCgnLmRlbGV0ZS1idG4nKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9kZWxldGVCdG5cclxuICAgIH1cclxuXHJcbiAgICBnZXQgZXhwYW5kQnRuKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9leHBhbmRCdG4gPT09IHVuZGVmaW5lZCkgdGhpcy5fZXhwYW5kQnRuID0gdGhpcy5kb20uc2VsZWN0KCcuZXhwYW5kLWJ0bicpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2V4cGFuZEJ0blxyXG4gICAgfVxyXG5cclxuICAgIGdldCBleHBhbmRJbWcoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2V4cGFuZEltZyA9PT0gdW5kZWZpbmVkKSB0aGlzLl9leHBhbmRJbWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdpbWcnKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9leHBhbmRJbWdcclxuICAgIH1cclxuXHJcbiAgICBwcm94eShlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUudGFyZ2V0XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpKSByZXR1cm5cclxuICAgICAgICBpZiAodGFyZ2V0ID09PSB0aGlzLmRlbGV0ZUJ0bikgcmV0dXJuIHRoaXMub25EZWxldGVDbGljayhlKVxyXG4gICAgICAgIGlmICh0YXJnZXQgPT09IHRoaXMuZXhwYW5kQnRuKSByZXR1cm4gdGhpcy5vbkV4cGFuZChlKVxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LnRhZ05hbWUudG9Mb3dlckNhc2UoKSA9PT0gJ2ltZycpIHRhcmdldCA9IHRhcmdldC5wYXJlbnROb2RlXHJcbiAgICAgICAgaWYgKHRhcmdldC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT09ICdhJykgcmV0dXJuIHRoaXMuYWN0aXZlSXRlbSh0YXJnZXQpXHJcbiAgICB9XHJcblxyXG4gICAgcG9wdWxhdGUoZGF0YSkge1xyXG4gICAgICAgIHRoaXMubGlzdC5pbm5lckhUTUwgPSAnJ1xyXG5cclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRhdGEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5hZGQoZGF0YVtpXSlcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcmVsb2FkSW1hZ2VzKClcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIGFkZChkYXRhKSB7XHJcbiAgICAgICAgbGV0IGxpICA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2xpJylcclxuICAgICAgICBsZXQgYSAgID0gbGkuYXBwZW5kQ2hpbGQoZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpKVxyXG4gICAgICAgIGxldCBpbWcgPSBhLmFwcGVuZENoaWxkKGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2ltZycpKVxyXG5cclxuICAgICAgICBhLmhyZWYgPSBkYXRhLnBhdGhcclxuICAgICAgICBhLmRhdGFzZXQuaWQgPSBkYXRhLmlkXHJcbiAgICAgICAgYS5jbGFzc0xpc3QuYWRkKCd0aHVtYm5haWwnKVxyXG5cclxuICAgICAgICBpbWcuc3JjID0gKGRhdGEudGh1bWJzICYmIGRhdGEudGh1bWJzLm1pbikgfHwgZGF0YS5taW5cclxuICAgICAgICBpbWcuYWx0ID0gJydcclxuXHJcbiAgICAgICAgdGhpcy5saXN0LmFwcGVuZENoaWxkKGxpKVxyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZUFjdGl2ZSgpIHtcclxuICAgICAgICBsZXQgYWN0aXZlTm9kZSA9IHRoaXMuZG9tLm5vZGUodGhpcy5saXN0KS5zZWxlY3QoJy5hY3RpdmUnKVxyXG4gICAgICAgIGlmICghYWN0aXZlTm9kZSkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBwYXJlbnQgPSBhY3RpdmVOb2RlLnBhcmVudE5vZGVcclxuICAgICAgICBwYXJlbnQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChwYXJlbnQpXHJcblxyXG4gICAgICAgIHRoaXMuZGlzYWJsZUFjdGlvbnNCdG5zKClcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBhY3RpdmVJdGVtKG5vZGUpIHtcclxuICAgICAgICB0aGlzLmVuYWJsZUFjdGlvbnNCdG5zKClcclxuXHJcbiAgICAgICAgdGhpcy5kb20ubm9kZSh0aGlzLmxpc3QpXHJcbiAgICAgICAgICAgIC5zZWxlY3RBbGwoJy5hY3RpdmUnKVxyXG4gICAgICAgICAgICAuZm9yRWFjaCgobm9kZSkgPT4gbm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKSlcclxuICAgICAgICBub2RlLmNsYXNzTGlzdC5hZGQoJ2FjdGl2ZScpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBvbkRlbGV0ZUNsaWNrKGUpIHtcclxuICAgICAgICBsZXQgYWN0aXZlID0gdGhpcy5kb20ubm9kZSh0aGlzLmxpc3QpLnNlbGVjdCgnLmFjdGl2ZScpXHJcbiAgICAgICAgaWYgKCFhY3RpdmUpIHJldHVyblxyXG5cclxuICAgICAgICB0aGlzLmVtaXQoJ2RlbGV0ZScsIHtcclxuICAgICAgICAgICAgdGFyZ2V0OiB0aGlzLFxyXG4gICAgICAgICAgICBpbWFnZUlkOiBhY3RpdmUuZGF0YXNldC5pZFxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgb25FeHBhbmQoZSkge1xyXG4gICAgICAgIGxldCBhY3RpdmUgPSB0aGlzLmRvbS5ub2RlKHRoaXMubGlzdCkuc2VsZWN0KCcuYWN0aXZlJylcclxuICAgICAgICBpZiAoIWFjdGl2ZSkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCBvdmVybGF5ID0gT3ZlcmxheS5pbnN0YW5jZVxyXG4gICAgICAgIGxldCBpbWcgICAgID0gdGhpcy5leHBhbmRJbWdcclxuICAgICAgICBpbWcuc3JjICAgICA9IGFjdGl2ZS5nZXRBdHRyaWJ1dGUoJ2hyZWYnKVxyXG5cclxuICAgICAgICBvdmVybGF5XHJcbiAgICAgICAgICAgIC5zZXROb2RlKGltZylcclxuICAgICAgICAgICAgLnJlbW92ZVByZWxvYWRlcigpXHJcbiAgICAgICAgICAgIC5zaG93Q2xvc2VCdG4oKVxyXG4gICAgICAgICAgICAuZW5hYmxlKClcclxuICAgIH1cclxuXHJcbiAgICBkaXNhYmxlQWN0aW9uc0J0bnMoKSB7XHJcbiAgICAgICAgbGV0IGRlbENsID0gdGhpcy5kZWxldGVCdG4uY2xhc3NMaXN0XHJcbiAgICAgICAgbGV0IGV4cENsID0gdGhpcy5leHBhbmRCdG4uY2xhc3NMaXN0XHJcblxyXG4gICAgICAgIGlmICghZGVsQ2wuY29udGFpbnMoJ2Rpc2FibGVkJykpIGRlbENsLmFkZCgnZGlzYWJsZWQnKVxyXG4gICAgICAgIGlmICghZXhwQ2wuY29udGFpbnMoJ2Rpc2FibGVkJykpIGV4cENsLmFkZCgnZGlzYWJsZWQnKVxyXG4gICAgfVxyXG5cclxuICAgIGVuYWJsZUFjdGlvbnNCdG5zKCkge1xyXG4gICAgICAgIGxldCBkZWxDbCA9IHRoaXMuZGVsZXRlQnRuLmNsYXNzTGlzdFxyXG4gICAgICAgIGxldCBleHBDbCA9IHRoaXMuZXhwYW5kQnRuLmNsYXNzTGlzdFxyXG5cclxuICAgICAgICBpZiAoZGVsQ2wuY29udGFpbnMoJ2Rpc2FibGVkJykpIGRlbENsLnJlbW92ZSgnZGlzYWJsZWQnKVxyXG4gICAgICAgIGlmIChleHBDbC5jb250YWlucygnZGlzYWJsZWQnKSkgZXhwQ2wucmVtb3ZlKCdkaXNhYmxlZCcpXHJcbiAgICB9XHJcblxyXG4gICAgcHJlbG9hZEltYWdlcygpIHtcclxuICAgICAgICBsZXQgbGlua3MgPSB0aGlzLmRvbS5ub2RlKHRoaXMubGlzdCkuc2VsZWN0QWxsKCdhJylcclxuICAgICAgICBpZiAoIWxpbmtzKSByZXR1cm5cclxuXHJcbiAgICAgICAgbGV0IGltZ05vZGUgPSB0aGlzLmV4cGFuZEltZ1xyXG4gICAgICAgIGxpbmtzLmZvckVhY2goKGxpbmspID0+IGltZ05vZGUuc3JjID0gbGluay5nZXRBdHRyaWJ1dGUoJ2hyZWYnKSlcclxuICAgICAgICBpbWdOb2RlLnNyYyA9ICcnXHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcbn0iLCJpbXBvcnQgY29uZmlnIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uLy4uL2NvbmZpZy5qc29uJ1xyXG5pbXBvcnQgQmFzZU1vZGVsIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlTW9kZWwnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNb2RlbCBleHRlbmRzIEJhc2VNb2RlbCB7XHJcblxyXG4gICAgZ2V0IGNvbmZpZygpIHtcclxuICAgICAgICBpZiAodGhpcy5fY29uZmlnID09PSB1bmRlZmluZWQpIHRoaXMuX2NvbmZpZyA9IEJhc2VNb2RlbC5nZXRDb25maWcoJ2dvb2RzSW1hZ2VzJylcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29uZmlnXHJcbiAgICB9XHJcblxyXG4gICAgc2V0KGRhdGEpIHtcclxuICAgICAgICBsZXQgb3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgYm9keTogICBkYXRhLmZvcm1EYXRhLFxyXG4gICAgICAgICAgICBtZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLlBPU1QsXHJcbiAgICAgICAgICAgIGhlYWRlcnM6IHtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgb3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuY29uZmlnLCBvcHRpb25zKVxyXG4gICAgICAgIG9wdGlvbnMudXJpID0gYCR7b3B0aW9ucy51cml9JHtkYXRhLmRhdGEuZ29vZHNfaWR9YFxyXG5cclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgICAgICAgICAueGhyKG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ3NldCcsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGRhdGEuZGF0YX0pXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIGdldEJ5R29vZHMoZ29vZHNJZCkge1xyXG4gICAgICAgIGxldCBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIHttZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVH0pXHJcbiAgICAgICAgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2dvb2RzSWR9YFxyXG5cclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgICAgICAgICAueGhyKG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2dldEJ5R29vZHMnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhfSlcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0KCkge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcignTWV0aG9kIGlzIG5vdCBpbXBsZW1lbnRlZCcpXHJcbiAgICB9XHJcblxyXG59IiwiaW1wb3J0IEJhc2VGb3JtIGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uLy4uL2xpYi9CYXNlRm9ybSdcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVwbG9hZEZvcm0gZXh0ZW5kcyBCYXNlRm9ybSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoY29udGFpbmVyKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBVcGxvYWRGb3JtKEJhc2VGb3JtLmJ5SWQoY29udGFpbmVySWQpKVxyXG4gICAgfVxyXG5cclxuICAgIHNldCBkYXRhKGRhdGEpIHtcclxuICAgICAgICB0aGlzLnJlc2V0KClcclxuICAgICAgICBpZiAoZGF0YS5nb29kc19pZCkgdGhpcy5jb250YWluZXIuZWxlbWVudHNbJ2dvb2RzX2lkJ10udmFsdWUgPSBkYXRhLmdvb2RzX2lkXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRhdGEoKSB7XHJcbiAgICAgICAgbGV0IGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKHRoaXMuY29udGFpbmVyKVxyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBkYXRhOiBzdXBlci5kYXRhLFxyXG4gICAgICAgICAgICBmb3JtRGF0YTogZm9ybURhdGFcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0iLCJtb2R1bGUuZXhwb3J0cz17XHJcbiAgXCJtb2RlbFwiOiB7XHJcbiAgICBcImNhdGFsb2dcIjoge1xyXG4gICAgICBcInVyaVwiOiBcIi9hcGkvY2F0YWxvZ3MvXCJcclxuICAgIH0sXHJcbiAgICBcImNhdGFsb2dVcGxvYWRcIjoge1xyXG4gICAgICBcInVyaVwiOiBcIi9hcGkvY2F0YWxvZy11cGxvYWQvXCJcclxuICAgIH0sXHJcbiAgICBcImdvb2RzXCI6IHtcclxuICAgICAgXCJ1cmlcIjogXCIvYXBpL2dvb2RzL1wiXHJcbiAgICB9LFxyXG4gICAgXCJnb29kc0RldGFpbGVkXCI6IHtcclxuICAgICAgXCJ1cmlcIjogXCIvYXBpL2dvb2RzLWRldGFpbGVkL1wiXHJcbiAgICB9LFxyXG4gICAgXCJnb29kc0ltYWdlc1wiOiB7XHJcbiAgICAgIFwidXJpXCI6IFwiL2FwaS9nb29kcy1pbWFnZXMvXCJcclxuICAgIH1cclxuICB9LFxyXG5cclxuICBcImkxOG5cIjoge1xyXG4gICAgXCJ1a1wiOiB7XHJcbiAgICAgIFwieWVzXCI6IFwi0KLQsNC6XCIsXHJcbiAgICAgIFwibm9cIjogXCLQndGWXCIsXHJcbiAgICAgIFwiZWRpdFwiOiBcItCg0LXQtNCw0LPRg9Cy0LDRgtC4XCIsXHJcbiAgICAgIFwiZGVsZXRlXCI6IFwi0JLQuNC00LDQu9C40YLQuFwiLFxyXG4gICAgICBcImRhdGEgcHJvY2Vzc2luZ1wiOiBcItCU0LDQvdGWINC+0LHRgNC+0LHQu9GP0Y7RgtGM0YHRj1wiLFxyXG4gICAgICBcImxvYWQgZGF0YVwiOiBcItCX0LDQstCw0L3RgtCw0LbQtdC90L3RjyDQtNCw0L3QuNGFXCIsXHJcbiAgICAgIFwidGl0bGVcIjogXCLQndCw0LfQstCwXCIsXHJcbiAgICAgIFwiYWxpYXNcIjogXCLQn9GB0LXQstC00L7QvdGW0LxcIixcclxuICAgICAgXCJjb2RlXCI6IFwi0JrQvtC0XCIsXHJcbiAgICAgIFwiZGVzY3JpcHRpb25cIjogXCLQntC/0LjRgVwiLFxyXG4gICAgICBcInNob3J0X2Rlc2NyaXB0aW9uXCI6IFwi0JrQvtGA0L7RgtC60LjQuSDQvtC/0LjRgVwiLFxyXG4gICAgICBcInByb2R1Y2VyXCI6IFwi0JLQuNGA0L7QsdC90LjQulwiLFxyXG4gICAgICBcInByaWNlXCI6IFwi0KbRltC90LBcIixcclxuICAgICAgXCJxdWFudGl0eVwiOiBcItCa0ZbQu9GM0LrRltGB0YLRjFwiLFxyXG4gICAgICBcImlzX3RvcFwiOiBcItCb0ZbQtNC10YDQuCDQv9GA0L7QtNCw0LZcIixcclxuICAgICAgXCJpc19pbmRleF9jYXJvdXNlbFwiOiBcItCS0ZbQtNC+0LHRgNCw0LbQsNGU0YLRjNGB0Y8g0LIg0LrQsNGA0YPRgdC10LvRllwiLFxyXG4gICAgICBcInNsdWdcIjogXCLQndCw0LfQstCwINC/0L7RgdC40LvQsNC90L3Rj1wiLFxyXG4gICAgICBcImNyZWF0ZWRcIiA6IFwi0KHRgtCy0L7RgNC10L3QvlwiLFxyXG4gICAgICBcInVwZGF0ZWRcIiA6IFwi0J7QsdC90L7QstC70LXQvdC+XCIsXHJcbiAgICAgIFwiYWxlcnQhXCI6IFwi0KPQstCw0LPQsCFcIixcclxuICAgICAgXCJBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZWxlbWVudD9cIjogXCLQktC4INC00ZbQudGB0L3QviDRhdC+0YfQtdGC0LUg0LLQuNC00LDQu9C40YLQuCDRhtC10Lkg0LXQu9C10LzQtdC90YI/XCIsXHJcbiAgICAgIFwiRWxlbWVudCBpcyBub3QgZW1wdHkuIEZpcnN0LCB5b3UgaGF2ZSB0byByZW1vdmUgY29udGVudC5cIjogXCLQldC70LXQvNC10L3RgiDQvdC1INC/0L7RgNC+0LbQvdGW0LkuINCh0L/QvtGH0LDRgtC60YMsINCy0Lgg0L/QvtCy0LjQvdC90ZYg0LLQuNC00LDQu9C40YLQuCDQstC80ZbRgdGCINC10LvQtdC80LXQvdGC0LBcIixcclxuICAgICAgXCJBbiBlcnJvciBoYXMgb2NjdXJyZWQuIFBsZWFzZSwgY29udGFjdCBkZXZlbG9wZXJcIjogXCLQodGC0LDQu9Cw0YHRjyDQv9C+0LzQuNC70LrQsC4g0JHRg9C00Ywg0LvQsNGB0LrQsCwg0LfQsifRj9C20ZbRgtGM0YHRjyDQtyDRgNC+0LfRgNC+0LHQvdC40LrQvtC8XCIsXHJcbiAgICAgIFwiTm8gY2F0YWxvZyBmb3VuZFwiOiBcItCa0LDRgtCw0LvQvtCzINC90LUg0LfQvdCw0LnQtNC10L3QvlwiLFxyXG4gICAgICBcIk5vIGdvb2RzIGZvdW5kXCI6IFwi0KLQvtCy0LDRgCDQvdC1INC30L3QsNC50LTQtdC90L5cIixcclxuICAgICAgXCJObyBnb29kcyBkZXRhaWwgZm91bmRcIjogXCLQldC70LXQvNC10L3RgiDQvdC1INC30L3QsNC50LTQtdC90L5cIixcclxuICAgICAgXCJUaGUgaW5wdXQgaXMgbW9yZSB0aGFuIDEwMCBjaGFyYWN0ZXJzIGxvbmdcIjogXCLQlNC+0LLQttC40L3QsCDQt9Cw0L/QuNGB0YMg0L3QtSDQv9C+0LLQuNC90L3QsCDQv9C10YDQtdCy0LjRidGD0LLQsNGC0LggMTAwINGB0LjQvNCy0L7Qu9GW0LJcIixcclxuICAgICAgXCJUaGUgaW5wdXQgaXMgbW9yZSB0aGFuIDI1NSBjaGFyYWN0ZXJzIGxvbmdcIjogXCLQlNC+0LLQttC40L3QsCDQt9Cw0L/QuNGB0YMg0L3QtSDQv9C+0LLQuNC90L3QsCDQv9C10YDQtdCy0LjRidGD0LLQsNGC0LggMjU1INGB0LjQvNCy0L7Qu9GW0LJcIixcclxuICAgICAgXCJUaGUgaW5wdXQgaXMgbW9yZSB0aGFuIDEyMCBjaGFyYWN0ZXJzIGxvbmdcIjogXCLQlNC+0LLQttC40L3QsCDQt9Cw0L/QuNGB0YMg0L3QtSDQv9C+0LLQuNC90L3QsCDQv9C10YDQtdCy0LjRidGD0LLQsNGC0LggMTIwINGB0LjQvNCy0L7Qu9GW0LJcIixcclxuICAgICAgXCJDYXRhbG9nIHdpdGggdGhpcyBuYW1lIGFscmVhZHkgZXhpc3RzXCI6IFwi0JrQsNGC0LDQu9C+0LMg0Lcg0YLQsNC60L7RjiDQvdCw0LfQstC+0Y4g0LLQttC1INGW0YHQvdGD0ZRcIixcclxuICAgICAgXCJHb29kcyB3aXRoIHRoaXMgbmFtZSBhbHJlYWR5IGV4aXN0c1wiIDogXCLQotC+0LLQsNGAINC3INGC0LDQutC+0Y4g0L3QsNC30LLQvtGOINCy0LbQtSDRltGB0L3Rg9GUXCIsXHJcbiAgICAgIFwiUmVjb3JkIHdpdGggdGhpcyB0aXRsZSBhbHJlYWR5IGV4aXN0c1wiOiBcItCX0LDQv9C40YEg0Lcg0YLQsNC60L7RjiDQvdCw0LfQstC+0Y4g0LLQttC1INGW0YHQvdGD0ZRcIixcclxuICAgICAgXCJWYWx1ZSBpcyByZXF1aXJlZCBhbmQgY2FuJ3QgYmUgZW1wdHlcIjogXCLQn9C+0LvQtSDQvtCx0L7QsifRj9C30LrQvtCy0LUg0LTQu9GPINC30LDQv9C+0LLQvdC10L3QvdGPINGWINC90LUg0LzQvtC20LUg0LHRg9GC0Lgg0L/Rg9GB0YLQuNC8XCIsXHJcbiAgICAgIFwiVGhlIGlucHV0IGRvZXMgbm90IGFwcGVhciB0byBiZSBhbiBpbnRlZ2VyXCI6IFwi0JLQstC10LTQtdC90LUg0LfQvdCw0YfQtdC90L3RjyDQv9C+0LLQuNC90L3QtSDQsdGD0YLQuCDRhtGW0LvQuNC8INGH0LjRgdC70L7QvFwiLFxyXG4gICAgICBcIkludmFsaWQgaW5wdXQuIElucHV0IG1hc2s6IDk5OSA5OTkgOTk5Ljk5XCI6IFwi0JLQstC10LTQtdC90LUg0LfQvdCw0YfQtdC90L3RjyDQvdC1INCy0ZbRgNC90LUuINCc0LDRgdC60LAg0LfQvdCw0YfQtdC90L3RjzogOTk5IDk5OSA5OTkuOTlcIixcclxuICAgICAgXCJDYXRhbG9nIGhhcyBnb29kcy4gRmlyc3QgY2xlYXIgY2F0YWxvZ1wiOiBcItCa0LDRgtCw0LvQvtCzINC80ZbRgdGC0LjRgtGMINGC0L7QstCw0YDQuC4g0KHQv9C+0YfQsNGC0LrRgyDQvtGH0LjRgdGC0ZbRgtGMINC60LDRgtCw0LvQvtCzXCIsXHJcbiAgICAgIFwiQ2F0YWxvZyBoYXMgY2hpbGRyZW4gY2F0YWxvZ3MuIEZpcnN0IGNsZWFyIGNhdGFsb2dcIjogXCLQmtCw0YLQsNC70L7QsyDQvNGW0YHRgtC40YLRjCDRltC90YjRliDQutCw0YLQsNC70L7Qs9C4LiDQodC/0L7Rh9Cw0YLQutGDINC+0YfQuNGB0YLRltGC0Ywg0LrQsNGC0LDQu9C+0LNcIixcclxuICAgICAgXCJGaWxlIHdhcyBub3QgdXBsb2FkZWRcIjogXCLQpNCw0LnQuyDQvdC1INC30LDQstCw0L3RgtCw0LbQtdC90L5cIixcclxuICAgICAgXCJGaWxlIGlzIG5vIGltYWdlXCI6IFwi0KTQsNC50LssINGP0LrQuNC5INCy0Lgg0L/RgNC+0LHRg9GU0YLQtSDQt9Cw0LLQsNC90YLQsNC20LjRgtC4INC90LUg0ZQg0LfQvtCx0YDQsNC20LXQvdC90Y/QvFwiLFxyXG4gICAgICBcIk1heGltdW0gYWxsb3dlZCBzaXplIGZvciBmaWxlIGlzIDIgTUJcIjogXCLQnNCw0LrRgdC40LzQsNC70YzQvdC40Lkg0YDQvtC30LzRltGAINGE0LDQudC70YMsINGP0LrQuNC5INC80L7QttC90LAg0LfQsNCy0LDQvdGC0LDQttC40YLQuCAtIDIgTWJcIlxyXG4gICAgfVxyXG4gIH1cclxufSIsImltcG9ydCBCYXNlV2lkZ2V0IGZyb20gJy4vQmFzZVdpZGdldCc7XHJcbmltcG9ydCBjb25maWcgZnJvbSAnLi4vY29uZmlnLmpzb24nO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZURlbGV0ZSBleHRlbmRzIEJhc2VXaWRnZXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lciwgZGVsZXRlQnRuID0gbnVsbCkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSBjb250YWluZXI7XHJcbiAgICAgICAgdGhpcy5kZWxldGVCdG4gPSBkZWxldGVCdG4gPyBkZWxldGVCdG4gOiB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yKCdhW2hyZWZePVwiI2RlbGV0ZVwiXScpO1xyXG5cclxuICAgICAgICB0aGlzLmRlbGV0ZUJ0bi5vbmNsaWNrID0gdGhpcy5vbkNsaWNrLmJpbmQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGFsZXJ0Qm94KCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9hbGVydEJveCA9PT0gdW5kZWZpbmVkKSB0aGlzLl9hbGVydEJveCA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoJy5tc2ctdGV4dCcpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hbGVydEJveDtcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmRlbGV0ZUJ0bi5jbGFzc0xpc3QuY29udGFpbnMoJ2Rpc2FibGVkJykpIHtcclxuICAgICAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZS5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmVtaXQoJ2RlbGV0ZScsIHt0YXJnZXQ6IHRoaXN9KTtcclxuICAgIH1cclxuXHJcbiAgICBkaXNhYmxlT2tCdG4oKSB7XHJcbiAgICAgICAgdGhpcy5zaG93QWxlcnRNZXNzYWdlKCdFbGVtZW50IGlzIG5vdCBlbXB0eS4gRmlyc3QsIHlvdSBoYXZlIHRvIHJlbW92ZSBjb250ZW50LicpO1xyXG4gICAgICAgIHRoaXMuZGVsZXRlQnRuLmNsYXNzTGlzdC5hZGQoJ2Rpc2FibGVkJyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd0FsZXJ0TWVzc2FnZShtc2cpIHtcclxuICAgICAgICBtc2cgICAgICAgICAgID0gY29uZmlnLmkxOG4udWtbbXNnXTtcclxuICAgICAgICBsZXQgYWxlcnRUZXh0ID0gY29uZmlnLmkxOG4udWtbJ2FsZXJ0ISddO1xyXG4gICAgICAgIHRoaXMuYWxlcnRCb3guaW5uZXJIVE1MID0gYDxzdHJvbmc+JHthbGVydFRleHR9PC9zdHJvbmc+ICR7bXNnfWA7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5zaG93QWxlcnRNZXNzYWdlKCdBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZGVsZXRlIHRoaXMgZWxlbWVudD8nKTtcclxuICAgICAgICBsZXQgY2wgPSB0aGlzLmRlbGV0ZUJ0bi5jbGFzc0xpc3Q7XHJcbiAgICAgICAgaWYgKGNsLmNvbnRhaW5zKCdkaXNhYmxlZCcpKSBjbC5yZW1vdmUoJ2Rpc2FibGVkJyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdygpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3QucmVtb3ZlKCdoaWRkZW4nKTtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpO1xyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IEJhc2VXaWRnZXQgZnJvbSAnLi9CYXNlV2lkZ2V0J1xyXG5pbXBvcnQgZm9ybVRvT2JqICBmcm9tICdmb3JtLXRvLW9iaidcclxuaW1wb3J0IHBvcHVsYXRlICAgZnJvbSAnLi9wb3B1bGF0ZSdcclxuaW1wb3J0IGNvbmZpZyAgICAgZnJvbSAnLi4vY29uZmlnLmpzb24nXHJcblxyXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCYXNlRm9ybSBleHRlbmRzIEJhc2VXaWRnZXQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHN1cGVyKClcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lclxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLm9uc3VibWl0ID0gdGhpcy5vblN1Ym1pdEZvcm0uYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHNldCBhY3Rpb24oYWN0aW9uKSB7IHRoaXMuY29udGFpbmVyLnNldEF0dHJpYnV0ZSgnYWN0aW9uJywgYWN0aW9uKSB9XHJcbiAgICBnZXQgYWN0aW9uKCkgICAgICAgeyByZXR1cm4gdGhpcy5jb250YWluZXIuZ2V0QXR0cmlidXRlKCdhY3Rpb24nKSB9XHJcblxyXG4gICAgc2V0IGRhdGEoZGF0YSkge1xyXG4gICAgICAgIHRoaXMucmVzZXQoKVxyXG4gICAgICAgIHBvcHVsYXRlKHRoaXMuY29udGFpbmVyLCBkYXRhKVxyXG4gICAgfVxyXG4gICAgZ2V0IGRhdGEoKSB7IHJldHVybiBmb3JtVG9PYmoodGhpcy5jb250YWluZXIpIH1cclxuXHJcbiAgICByZXNldCgpIHtcclxuICAgICAgICBsZXQgaGlkZGVuSW5wdXRzID0gdGhpcy5jb250YWluZXIucXVlcnlTZWxlY3RvckFsbCgnaW5wdXRbdHlwZT1cImhpZGRlblwiXScpXHJcbiAgICAgICAgbGV0IGVhY2ggPSBBcnJheS5wcm90b3R5cGUuZm9yRWFjaFxyXG5cclxuICAgICAgICBlYWNoLmNhbGwoaGlkZGVuSW5wdXRzLCAoaW5wdXQpID0+IGlucHV0LnZhbHVlID0gJycpO1xyXG5cclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5yZXNldCgpXHJcbiAgICAgICAgdGhpcy5yZXNldEVycm9ycygpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBmb2N1cyhuYW1lKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuZWxlbWVudHNbbmFtZV0uZm9jdXMoKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgc2V0RXJyb3JzKGVycm9ycykge1xyXG4gICAgICAgIGxldCBjb250YWluZXIgPSB0aGlzLmNvbnRhaW5lclxyXG4gICAgICAgIGxldCB0cmFuc2xhdGUgPSBjb25maWcuaTE4bi51a1xyXG5cclxuICAgICAgICB0aGlzLnJlc2V0RXJyb3JzKClcclxuXHJcbiAgICAgICAgZm9yKGxldCBpbnB1dE5hbWUgaW4gZXJyb3JzKSBpZiAoZXJyb3JzLmhhc093blByb3BlcnR5KGlucHV0TmFtZSkpIHtcclxuICAgICAgICAgICAgbGV0IGlucHV0ID0gY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3IoYFtuYW1lPVwiJHtpbnB1dE5hbWV9XCJdYClcclxuICAgICAgICAgICAgaWYgKCFpbnB1dCkgY29udGludWVcclxuXHJcbiAgICAgICAgICAgIGxldCBtc2dzID0gZXJyb3JzW2lucHV0TmFtZV1cclxuICAgICAgICAgICAgZm9yIChsZXQga2V5IGluIG1zZ3MpIGlmIChtc2dzLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuICAgICAgICAgICAgICAgIGxldCBpY29uID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaScpXHJcbiAgICAgICAgICAgICAgICBpY29uLmNsYXNzTGlzdC5hZGQoJ2ZhJywgJ2ZhLWV4Y2xhbWF0aW9uLXRyaWFuZ2xlJylcclxuXHJcbiAgICAgICAgICAgICAgICBsZXQgbXNnQm94ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3BhbicpXHJcbiAgICAgICAgICAgICAgICBtc2dCb3guY2xhc3NMaXN0LmFkZCgnaGVscC1ibG9jaycpXHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHRleHQgPSB0cmFuc2xhdGVbbXNnc1trZXldXSA/IHRyYW5zbGF0ZVttc2dzW2tleV1dIDogbXNnc1trZXldXHJcbiAgICAgICAgICAgICAgICBtc2dCb3guYXBwZW5kQ2hpbGQoaWNvbilcclxuICAgICAgICAgICAgICAgIG1zZ0JveC5hcHBlbmRDaGlsZChkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZSh0ZXh0KSlcclxuICAgICAgICAgICAgICAgIGlucHV0LnBhcmVudE5vZGUuYXBwZW5kQ2hpbGQobXNnQm94KVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBsZXQgZmdOb2RlID0gaW5wdXQuY2xvc2VzdCgnLmZvcm0tZ3JvdXAnKVxyXG4gICAgICAgICAgICBpZiAoZmdOb2RlKSBmZ05vZGUuY2xhc3NMaXN0LmFkZCgnaGFzLWVycm9yJylcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICByZXNldEVycm9ycygpIHtcclxuICAgICAgICBsZXQgZWFjaCA9IEFycmF5LnByb3RvdHlwZS5mb3JFYWNoXHJcbiAgICAgICAgbGV0IGdyb3VwQm94ZXMgPSB0aGlzLmNvbnRhaW5lci5xdWVyeVNlbGVjdG9yQWxsKCcuZm9ybS1ncm91cC5oYXMtZXJyb3InKVxyXG5cclxuICAgICAgICBlYWNoLmNhbGwoZ3JvdXBCb3hlcywgKGdyb3VwKSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBtc2dzQm94ZXMgPSBncm91cC5xdWVyeVNlbGVjdG9yQWxsKCcuaGVscC1ibG9jaycpXHJcbiAgICAgICAgICAgIGVhY2guY2FsbChtc2dzQm94ZXMsIChtc2cpID0+IG1zZy5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKG1zZykpXHJcbiAgICAgICAgICAgIGdyb3VwLmNsYXNzTGlzdC5yZW1vdmUoJ2hhcy1lcnJvcicpXHJcbiAgICAgICAgfSlcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3VibWl0Rm9ybShlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcblxyXG4gICAgICAgIHRoaXMucmVzZXRFcnJvcnMoKVxyXG5cclxuICAgICAgICB0aGlzLmVtaXQoJ3N1Ym1pdCcsIHtcclxuICAgICAgICAgICAgdGFyZ2V0OiB0aGlzLFxyXG4gICAgICAgICAgICBkYXRhOiB0aGlzLmRhdGFcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG5cclxuICAgIHNob3coKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuY2xhc3NMaXN0LnJlbW92ZSgnaGlkZGVuJylcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIGhpZGUoKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuY2xhc3NMaXN0LmFkZCgnaGlkZGVuJylcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbnN1Ym1pdChmbikge1xyXG4gICAgICAgIHRoaXMub24oJ3N1Ym1pdCcsIGZuKVxyXG4gICAgfVxyXG59XHJcbiIsImltcG9ydCBjb25maWcgZnJvbSAnLi4vY29uZmlnLmpzb24nO1xyXG5pbXBvcnQgeGhyIGZyb20gJ3hocic7XHJcbmltcG9ydCBFbWl0dGVyIGZyb20gJy4vRW1pdHRlcidcclxuXHJcbmxldCBtZXRob2RzQ29uc3RzID0gT2JqZWN0LmZyZWV6ZSh7XHJcbiAgICAnR0VUJyAgIDogJ0dFVCcsXHJcbiAgICAnUE9TVCcgIDogJ1BPU1QnLFxyXG4gICAgJ1BVVCcgICA6ICdQVVQnLFxyXG4gICAgJ0RFTEVURSc6ICdERUxFVEUnXHJcbn0pO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZU1vZGVsIGV4dGVuZHMgRW1pdHRlciB7XHJcblxyXG4gICAgc3RhdGljIGdldCBtZXRob2RzKCkge1xyXG4gICAgICAgIHJldHVybiBtZXRob2RzQ29uc3RzO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXREZWZhdWx0T3B0aW9ucygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICBcIlgtUmVxdWVzdGVkLVdpdGhcIjogXCJYTUxIdHRwUmVxdWVzdFwiLFxyXG4gICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgICAgICBcIkFjY2VwdFwiOlwiYXBwbGljYXRpb24vanNvblwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8sdGltZW91dDogMzAwMDBcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBnZXRDb25maWcoa2V5KSB7XHJcbiAgICAgICAgbGV0IGdsb2JhbENvbmZpZyA9IChjb25maWcubW9kZWwgJiYgY29uZmlnLm1vZGVsW2tleV0pID8gY29uZmlnLm1vZGVsW2tleV0gOiBudWxsO1xyXG5cclxuICAgICAgICByZXR1cm4gZ2xvYmFsQ29uZmlnXHJcbiAgICAgICAgICAgID8gT2JqZWN0LmFzc2lnbihCYXNlTW9kZWwuZ2V0RGVmYXVsdE9wdGlvbnMoKSwgZ2xvYmFsQ29uZmlnKVxyXG4gICAgICAgICAgICA6IEJhc2VNb2RlbC5nZXREZWZhdWx0T3B0aW9ucygpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyB4aHIob3B0aW9ucykge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XHJcbiAgICAgICAgICAgIHhocihvcHRpb25zLCAoZXJyb3IsIHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcGFyc2VkO1xyXG5cclxuICAgICAgICAgICAgICAgIHN3aXRjaCAocmVzcG9uc2Uuc3RhdHVzQ29kZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMjAwOiAvLyBPS1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMjAxOiAvLyBDcmVhdGVkXHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAyMDQ6IC8vIE5vIENvbnRlbnRcclxuICAgICAgICAgICAgICAgICAgICBjYXNlIDIwNjogLy8gUGFydGlhbCBDb250ZW50XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcnNlZCA9IEpTT04ucGFyc2UocmVzcG9uc2UuYm9keSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUocGFyc2VkLmRhdGEgPyBwYXJzZWQuZGF0YSA6IHBhcnNlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgNDAwOiAvLyBCYWQgUmVxdWVzdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJzZWQgPSBKU09OLnBhcnNlKHJlc3BvbnNlLmJvZHkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QocGFyc2VkLm1lc3NhZ2VzID8gcGFyc2VkLm1lc3NhZ2VzIDogcGFyc2VkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KG5ldyBFcnJvcignQW4gZXJyb3IgaGFzIG9jY3VycmVkLiBQbGVhc2UsIGNvbnRhY3QgZGV2ZWxvcGVyJykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB4aHIob3B0aW9ucykge1xyXG4gICAgICAgIHJldHVybiBCYXNlTW9kZWwueGhyKG9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldChkYXRhKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIGJvZHk6ICAgSlNPTi5zdHJpbmdpZnkoZGF0YSksXHJcbiAgICAgICAgICAgIG1ldGhvZDogZGF0YS5pZCA/IEJhc2VNb2RlbC5tZXRob2RzLlBVVCA6IEJhc2VNb2RlbC5tZXRob2RzLlBPU1RcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBvcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIG9wdGlvbnMpO1xyXG5cclxuICAgICAgICBpZiAob3B0aW9ucy5tZXRob2QgPT09IEJhc2VNb2RlbC5tZXRob2RzLlBVVCkgb3B0aW9ucy51cmkgPSBgJHtvcHRpb25zLnVyaX0ke2RhdGEuaWR9YDtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdzZXQnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0KGlkKSB7XHJcbiAgICAgICAgbGV0IG9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywgeyBtZXRob2Q6IEJhc2VNb2RlbC5tZXRob2RzLkdFVCB9KTtcclxuICAgICAgICBvcHRpb25zLnVyaSA9IGAke29wdGlvbnMudXJpfSR7aWR9YDtcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgICAgICAgICAueGhyKG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXQoJ2dldCcsIHt0YXJnZXQ6IHRoaXMsIGRhdGE6IGRhdGF9KTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRhO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmUoaWQpIHtcclxuICAgICAgICBsZXQgb3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMuY29uZmlnLCB7bWV0aG9kOiBCYXNlTW9kZWwubWV0aG9kcy5ERUxFVEV9KTtcclxuICAgICAgICBvcHRpb25zLnVyaSA9IGAke29wdGlvbnMudXJpfSR7aWR9YDtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgICAgICAgICAgLnhocihvcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbigoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0KCdyZW1vdmUnLCB7dGFyZ2V0OiB0aGlzLCBkYXRhOiBkYXRhfSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZGF0YTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IG9uc2V0KGZuKSAgICB7IHRoaXMub24oJ3NldCcsIGZuKSB9XHJcbiAgICBzZXQgb25nZXQoZm4pICAgIHsgdGhpcy5vbignZ2V0JywgZm4pIH1cclxuICAgIHNldCBvbnJlbW92ZShmbikgeyB0aGlzLm9uKCdyZW1vdmUnLCBmbikgfVxyXG59IiwiaW1wb3J0IEJhc2VXaWRnZXQgZnJvbSAnLi9CYXNlV2lkZ2V0JztcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJhc2VUYWJzIGV4dGVuZHMgQmFzZVdpZGdldCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29udGFpbmVyKSB7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyICAgICA9IGNvbnRhaW5lclxyXG4gICAgICAgIHRoaXMuYWN0aXZlVGFiICAgICA9IG51bGxcclxuICAgICAgICB0aGlzLmFjdGl2ZUNvbnRlbnQgPSBudWxsXHJcblxyXG4gICAgICAgIHRoaXMub253aWRnZXRzaG93ID0gdGhpcy5vbldpZGdldFNob3cuYmluZCh0aGlzKVxyXG4gICAgICAgIHRoaXMub253aWRnZXRoaWRlID0gdGhpcy5vbldpZGdldEhpZGUuYmluZCh0aGlzKVxyXG4gICAgICAgIHRoaXMub25zaG93biAgICAgID0gdGhpcy5vblNob3duLmJpbmQodGhpcylcclxuICAgICAgICB0aGlzLm9uY2xpY2sgICAgICA9IHRoaXMub25DbGljay5iaW5kKHRoaXMpXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5hdigpIHtcclxuICAgICAgICBpZiAodGhpcy5fbmF2ID09PSB1bmRlZmluZWQpIHRoaXMuX25hdiA9IHRoaXMuY29udGFpbmVyLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvcihgIyR7dGhpcy5jb250YWluZXIuaWR9Pi5uYXZgKVxyXG4gICAgICAgIHJldHVybiB0aGlzLl9uYXZcclxuICAgIH1cclxuXHJcbiAgICBzZXQgb25jbGljayhmbikge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5hdi5vbmNsaWNrID0gZm5cclxuICAgIH1cclxuXHJcbiAgICBnZXRUYWJCeUFjdGlvbihhY3Rpb24pIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5uYXYucXVlcnlTZWxlY3RvcihgYVtkYXRhLWFjdGlvbj1cIiR7YWN0aW9ufVwiXWApXHJcbiAgICB9XHJcblxyXG4gICAgc2hvdyh0YWIgPSBudWxsKSB7XHJcbiAgICAgICAgaWYgKCF0YWIpIHRhYiA9IHRoaXMubmF2LnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb25dJylcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiB0YWIgPT0gJ3N0cmluZycpIHRhYiA9IHRoaXMubmF2LnF1ZXJ5U2VsZWN0b3IoJ2FbZGF0YS1hY3Rpb249XCIke3RhYn1cIl0nKVxyXG5cclxuICAgICAgICBpZiAodGFiLnBhcmVudE5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlZCcpKSByZXR1cm5cclxuXHJcbiAgICAgICAgbGV0IGFjdGlvbiAgICAgPSB0YWIuZGF0YXNldC5hY3Rpb25cclxuICAgICAgICBsZXQgY29udGVudEJveCA9IHRoaXMuY29udGFpbmVyLnBhcmVudE5vZGUucXVlcnlTZWxlY3RvcihgIyR7dGhpcy5jb250YWluZXIuaWR9Pi50YWItY29udGVudD4ke3RhYi5nZXRBdHRyaWJ1dGUoJ2hyZWYnKX1gKVxyXG5cclxuICAgICAgICAvLyBzaG93IGNvbnRhaW5lciBpZiBoaWRkZW5cclxuICAgICAgICBsZXQgY2wgPSB0aGlzLmNvbnRhaW5lci5jbGFzc0xpc3RcclxuICAgICAgICAvL2xldCBpc1dpZGdldFNob3dFbWl0ZWQgPSBmYWxzZVxyXG4gICAgICAgIGlmIChjbC5jb250YWlucygnaGlkZGVuJykpIHtcclxuICAgICAgICAgICAgLy9pc1dpZGdldFNob3dFbWl0ZWQgPSB0cnVlXHJcbiAgICAgICAgICAgIHRoaXMuZW1pdCgnd2lkZ2V0LXNob3cnLCB7dGFyZ2V0OiB0aGlzfSlcclxuICAgICAgICAgICAgY2wucmVtb3ZlKCdoaWRkZW4nKVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2wgPSB0aGlzLm5hdi5jbGFzc0xpc3RcclxuICAgICAgICBpZiAoY2wuY29udGFpbnMoJ2hpZGRlbicpKSB7XHJcbiAgICAgICAgICAgIC8vaWYgKCFpc1dpZGdldFNob3dFbWl0ZWQpIHtcclxuICAgICAgICAgICAgLy8gICAgaXNXaWRnZXRTaG93RW1pdGVkID0gdHJ1ZVxyXG4gICAgICAgICAgICAvLyAgICB0aGlzLmVtaXQoJ3dpZGdldC1zaG93Jywge3RhcmdldDogdGhpc30pXHJcbiAgICAgICAgICAgIC8vfVxyXG4gICAgICAgICAgICBjbC5yZW1vdmUoJ2hpZGRlbicpXHJcbiAgICAgICAgICAgIHRoaXMuaGlkZUNoaWxkcmVuKClcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBldnQgPSB7XHJcbiAgICAgICAgICAgIHRhcmdldDogICAgICAgICB0aGlzLFxyXG4gICAgICAgICAgICB3aWxsQmVTaG93blRhYjogdGFiLFxyXG4gICAgICAgICAgICBhY3Rpb246ICAgICAgICAgYWN0aW9uXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmVtaXQoJ3Nob3cnLCBldnQpXHJcbiAgICAgICAgdGhpcy5pbmFjdGl2YXRlQWxsKClcclxuXHJcbiAgICAgICAgdGFiLnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZCgnYWN0aXZlJylcclxuICAgICAgICB0aGlzLmFjdGl2ZVRhYiA9IHRhYlxyXG5cclxuICAgICAgICBpZiAoY29udGVudEJveCkgY29udGVudEJveC5jbGFzc0xpc3QuYWRkKCdhY3RpdmUnKVxyXG4gICAgICAgIHRoaXMuYWN0aXZlQ29udGVudCA9IGNvbnRlbnRCb3hcclxuXHJcbiAgICAgICAgZGVsZXRlIGV2dC53aWxsQmVTaG93blRhYlxyXG4gICAgICAgIHRoaXMuZW1pdCgnc2hvd24nLCBldnQpXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBoaWRlKCkge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLmNsYXNzTGlzdC5hZGQoJ2hpZGRlbicpXHJcbiAgICAgICAgdGhpcy5lbWl0KCd3aWRnZXQtaGlkZScsIHt0YXJnZXQ6IHRoaXN9KVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUNoaWxkcmVuKHJlZ2lzdHJ5ID0gbnVsbCkge1xyXG4gICAgICAgIGlmICghcmVnaXN0cnkpIHJlZ2lzdHJ5ID0gdGhpcy53aWRnZXRzLnJlZ2lzdHJ5XHJcbiAgICAgICAgaWYgKCFyZWdpc3RyeS5zaXplKSByZXR1cm5cclxuXHJcbiAgICAgICAgZm9yIChsZXQgd2lkZ2V0IG9mIHJlZ2lzdHJ5LnZhbHVlcygpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaGlkZUNoaWxkcmVuKHdpZGdldC53aWRnZXRzLnJlZ2lzdHJ5KVxyXG4gICAgICAgICAgICBpZiAod2lkZ2V0IGluc3RhbmNlb2YgQmFzZVRhYnMpIHdpZGdldC5oaWRlKClcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBoaWRlTmF2aWdhdGlvbigpIHtcclxuICAgICAgICB0aGlzLm5hdi5jbGFzc0xpc3QuYWRkKCdoaWRkZW4nKVxyXG4gICAgfVxyXG5cclxuICAgIGRpc2FibGUodGFiID0gbnVsbCkge1xyXG4gICAgICAgIGlmICghdGFiKSB7XHJcbiAgICAgICAgICAgIGxldCB0YWJzICA9IHRoaXMubmF2LnF1ZXJ5U2VsZWN0b3JBbGwoJ2EnKVxyXG4gICAgICAgICAgICBsZXQgc2xpY2UgPSBBcnJheS5wcm90b3R5cGUuc2xpY2VcclxuXHJcbiAgICAgICAgICAgIHNsaWNlLmNhbGwodGFicykuZm9yRWFjaChmdW5jdGlvbihub2RlKSB7XHJcbiAgICAgICAgICAgICAgICBub2RlLnBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZCgnZGlzYWJsZWQnKVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRhYi5wYXJlbnROb2RlLmNsYXNzTGlzdC5hZGQoJ2Rpc2FibGVkJylcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBlbmFibGUodGFiID0gbnVsbCkge1xyXG4gICAgICAgIGlmICghdGFiKSB7XHJcbiAgICAgICAgICAgIGxldCB0YWJzID0gdGhpcy5uYXYucXVlcnlTZWxlY3RvckFsbCgnLmRpc2FibGVkJylcclxuICAgICAgICAgICAgbGV0IHNsaWNlID0gQXJyYXkucHJvdG90eXBlLnNsaWNlXHJcblxyXG4gICAgICAgICAgICBzbGljZS5jYWxsKHRhYnMpLmZvckVhY2goZnVuY3Rpb24obm9kZSkge1xyXG4gICAgICAgICAgICAgICAgbm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdkaXNhYmxlZCcpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKHRhYiAmJiB0YWIucGFyZW50Tm9kZS5jbGFzc0xpc3QuY29udGFpbnMoJ2Rpc2FibGVkJykpIHRhYi5wYXJlbnROb2RlLmNsYXNzTGlzdC5yZW1vdmUoJ2Rpc2FibGVkJylcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBpbmFjdGl2YXRlQWxsKCkge1xyXG4gICAgICAgIGxldCBhY3RpdmVFbGVtZW50cyA9IHRoaXMuY29udGFpbmVyLnF1ZXJ5U2VsZWN0b3JBbGwoJy5hY3RpdmUnKVxyXG4gICAgICAgIGZvcihsZXQgaWR4ID0gMDsgaWR4PGFjdGl2ZUVsZW1lbnRzLmxlbmd0aDsgaWR4KyspIHtcclxuICAgICAgICAgICAgYWN0aXZlRWxlbWVudHNbaWR4XS5jbGFzc0xpc3QucmVtb3ZlKCdhY3RpdmUnKVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2soZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG5cclxuICAgICAgICB2YXIgdGFyZ2V0ID0gZS50YXJnZXRcclxuICAgICAgICBpZiAodGFyZ2V0LnRhZ05hbWUudG9Mb3dlckNhc2UoKSAhPSAnYScpIHJldHVyblxyXG4gICAgICAgIGlmICh0YXJnZXQuY2xvc2VzdCgnLmNsb3NlLWNvbnRhaW5lcicpKSByZXR1cm5cclxuICAgICAgICB0aGlzLnNob3codGFyZ2V0KVxyXG4gICAgfVxyXG5cclxuICAgIG9uV2lkZ2V0U2hvdyhlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZXF1YWxzKGUudGFyZ2V0KSkgdGhpcy5hZGp1c3RUYWJzKClcclxuICAgIH1cclxuXHJcbiAgICBvbldpZGdldEhpZGUoZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmVxdWFscyhlLnRhcmdldCkpIHRoaXMuYWRqdXN0VGFicyh0cnVlKVxyXG4gICAgfVxyXG5cclxuICAgIG9uU2hvd24oZSkge1xyXG4gICAgICAgIGlmICh0aGlzLmVxdWFscyhlLnRhcmdldCkpICByZXR1cm5cclxuICAgICAgICBpZiAoZS50YXJnZXQuYWN0aXZlQ29udGVudCkgcmV0dXJuXHJcblxyXG4gICAgICAgIGxldCB0YWIgPSB0aGlzLmdldFRhYkJ5QWN0aW9uKGUuYWN0aW9uKVxyXG4gICAgICAgIGlmICh0YWIpIHRoaXMuc2hvdyh0YWIpXHJcbiAgICB9XHJcblxyXG4gICAgYWRqdXN0VGFicyhjbGVhciA9IGZhbHNlKSB7XHJcbiAgICAgICAgbGV0IHBhbmUgPSB0aGlzLmNvbnRhaW5lci5jbG9zZXN0KCcudGFiLXBhbmUnKVxyXG4gICAgICAgIGlmIChwYW5lKSB7XHJcbiAgICAgICAgICAgIGxldCBwcyA9IHBhbmUuc3R5bGUsIG5zID0gdGhpcy5uYXYuc3R5bGVcclxuICAgICAgICAgICAgbnMubWFyZ2luTGVmdCA9IHBzLnBhZGRpbmdUb3AgPSAhIWNsZWFyID8gJycgOiAnMHB4J1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBvbnNob3coZm4pICAgICAgIHsgdGhpcy5vbignc2hvdycsIGZuKSB9XHJcbiAgICBzZXQgb25zaG93bihmbikgICAgICB7IHRoaXMub24oJ3Nob3duJywgZm4pIH1cclxuICAgIHNldCBvbndpZGdldHNob3coZm4pIHsgdGhpcy5vbignd2lkZ2V0LXNob3cnLCBmbikgfVxyXG4gICAgc2V0IG9ud2lkZ2V0aGlkZShmbikgeyB0aGlzLm9uKCd3aWRnZXQtaGlkZScsIGZuKSB9XHJcbn0iLCJpbXBvcnQgRW1pdHRlciBmcm9tICcuL0VtaXR0ZXInXHJcblxyXG5jbGFzcyBSZWdpc3RyeSB7XHJcblxyXG4gICAgY29uc3RydWN0b3Iod2lkZ2V0c0NvbnRhaW5lcikge1xyXG4gICAgICAgIHRoaXMud2lkZ2V0c0NvbnRhaW5lciA9IHdpZGdldHNDb250YWluZXJcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgZ2V0IHRyZWUoKSB7XHJcbiAgICAgICAgaWYgKFJlZ2lzdHJ5Ll90cmVlID09PSB1bmRlZmluZWQpIFJlZ2lzdHJ5Ll90cmVlID0gbmV3IFdlYWtNYXAoKVxyXG4gICAgICAgIHJldHVybiBSZWdpc3RyeS5fdHJlZVxyXG4gICAgfVxyXG5cclxuICAgIGdldCByZWdpc3RyeSgpIHtcclxuICAgICAgICBpZiAodGhpcy5fcmVnaXN0cnkgPT09IHVuZGVmaW5lZCkgdGhpcy5fcmVnaXN0cnkgPSBuZXcgTWFwKClcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVnaXN0cnlcclxuICAgIH1cclxuXHJcbiAgICBzZXQod2lkZ2V0KSB7XHJcbiAgICAgICAgbGV0IGFsaWFzXHJcbiAgICAgICAgaWYgKHR5cGVvZiB3aWRnZXQgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGFsaWFzID0gd2lkZ2V0LnRvTG93ZXJDYXNlKClcclxuICAgICAgICAgICAgd2lkZ2V0ID0gYXJndW1lbnRzWzFdXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2UgYWxpYXMgPSB3aWRnZXQuY29uc3RydWN0b3IubmFtZS50b0xvd2VyQ2FzZSgpXHJcblxyXG5cclxuICAgICAgICBpZiAodGhpcy5yZWdpc3RyeS5oYXMoYWxpYXMpKSB0aHJvdyBuZXcgRXJyb3IoJ1dpZGdldCB3aXRoIHRoaXMgYWxpYXMgYWxyZWFkeSBleGlzdHMnKVxyXG5cclxuICAgICAgICB0aGlzLnJlZ2lzdHJ5LnNldChhbGlhcywgd2lkZ2V0KVxyXG4gICAgICAgIFJlZ2lzdHJ5LnRyZWUuc2V0KHdpZGdldCwgdGhpcy53aWRnZXRzQ29udGFpbmVyKVxyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0KGFsaWFzKSB7XHJcbiAgICAgICAgYWxpYXMgPSBhbGlhcy50b0xvd2VyQ2FzZSgpXHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVnaXN0cnkuaGFzKGFsaWFzKSA/IHRoaXMucmVnaXN0cnkuZ2V0KGFsaWFzKSA6IG51bGxcclxuICAgIH1cclxuXHJcbiAgICBoYXMoYWxpYXMpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZWdpc3RyeS5oYXMoYWxpYXMpXHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG5jbGFzcyBEb20ge1xyXG5cclxuICAgIHNldCBjYW5vbmljYWwobm9kZSkge1xyXG4gICAgICAgIHRoaXMuX2Nhbm9uaWNhbCA9IG5vZGVcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY2Fub25pY2FsKCkge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5fY2Fub25pY2FsIHx8IGRvY3VtZW50KVxyXG4gICAgfVxyXG5cclxuICAgIG5vZGUobm9kZSkge1xyXG4gICAgICAgIHRoaXMuX25vZGUgPSBub2RlXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgYnlJZChub2RlSWQpIHtcclxuICAgICAgICByZXR1cm4gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobm9kZUlkKVxyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdChzZWxlY3Rvcikge1xyXG4gICAgICAgIGxldCByZXN1bHQgPSAodGhpcy5fbm9kZSB8fCB0aGlzLmNhbm9uaWNhbCkucXVlcnlTZWxlY3RvcihzZWxlY3RvcilcclxuICAgICAgICB0aGlzLl9ub2RlID0gbnVsbFxyXG4gICAgICAgIHJldHVybiByZXN1bHRcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RBbGwoc2VsZWN0b3IpIHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gKHRoaXMuX25vZGUgfHwgdGhpcy5jYW5vbmljYWwpLnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpIHx8IFtdXHJcbiAgICAgICAgdGhpcy5fbm9kZSA9IG51bGxcclxuICAgICAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwocmVzdWx0KVxyXG4gICAgfVxyXG5cclxuICAgIG1hdGNoZXMoc2VsZWN0b3IpIHtcclxuICAgICAgICBsZXQgbm9kZSAgICAgPSB0aGlzLl9ub2RlIHx8IHRoaXMuY2Fub25pY2FsXHJcbiAgICAgICAgbGV0IHAgICAgICAgID0gRWxlbWVudC5wcm90b3R5cGU7XHJcbiAgICAgICAgbGV0IHBvbHlmaWxsID0gKHMpID0+IEFycmF5LnByb3RvdHlwZS5pbmRleE9mLmNhbGwobm9kZS5xdWVyeVNlbGVjdG9yQWxsKHMpLCB0aGlzKSAhPT0gLTFcclxuICAgICAgICBsZXQgZiAgICAgICAgPSBwLm1hdGNoZXMgfHwgcC53ZWJraXRNYXRjaGVzU2VsZWN0b3IgfHwgcC5tb3pNYXRjaGVzU2VsZWN0b3IgfHwgcC5tc01hdGNoZXNTZWxlY3RvciB8fCBwb2x5ZmlsbFxyXG4gICAgICAgIGxldCByZXN1bHQgICA9IGYuY2FsbChub2RlLCBzZWxlY3RvcilcclxuICAgICAgICB0aGlzLl9ub2RlICAgPSBudWxsXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdFxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZVdpZGdldCBleHRlbmRzIEVtaXR0ZXIge1xyXG5cclxuICAgIGdldCB3aWRnZXRzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLl93aWRnZXRzID09PSB1bmRlZmluZWQpIHRoaXMuX3dpZGdldHMgPSBuZXcgUmVnaXN0cnkodGhpcylcclxuICAgICAgICByZXR1cm4gdGhpcy5fd2lkZ2V0c1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwYXJlbnQoKSB7IHJldHVybiBSZWdpc3RyeS50cmVlLmhhcyh0aGlzKSA/IFJlZ2lzdHJ5LnRyZWUuZ2V0KHRoaXMpIDogbnVsbCB9XHJcblxyXG4gICAgZXF1YWxzKHdpZGdldCwgc2VsZWN0b3IgPSAnJykge1xyXG4gICAgICAgIGlmIChzZWxlY3Rvci5sZW5ndGgpIHdpZGdldCA9IHdpZGdldC5nZXQoc2VsZWN0b3IpXHJcbiAgICAgICAgaWYgKCF3aWRnZXQpIHRocm93IG5ldyBFcnJvcignV2lkZ2V0IGZvciBjb21wYXJpc29uIGlzIHVuZGVmaW5lZCcpXHJcblxyXG4gICAgICAgIGlmICghdGhpcy5jb250YWluZXIgfHwgIXdpZGdldC5jb250YWluZXIpIHJldHVybiBmYWxzZVxyXG4gICAgICAgIGlmICghdGhpcy5jb250YWluZXIuaWQgfHwgIXdpZGdldC5jb250YWluZXIuaWQpIHJldHVybiBmYWxzZVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5jb250YWluZXIuaWQgPT09IHdpZGdldC5jb250YWluZXIuaWRcclxuICAgIH1cclxuXHJcbiAgICBnZXQod2lkZ2V0TmFtZSkge1xyXG4gICAgICAgIGlmICh+d2lkZ2V0TmFtZS5pbmRleE9mKCc+JykpIHtcclxuICAgICAgICAgICAgbGV0IHdpZGdldCA9IHRoaXMsXHJcbiAgICAgICAgICAgICAgICBuYW1lcyAgPSB3aWRnZXROYW1lLnNwbGl0KCc+JyksXHJcbiAgICAgICAgICAgICAgICBsZW4gICAgPSBuYW1lcy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICBuYW1lICAgPSAnJ1xyXG5cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBsZW47IGkrKykge1xyXG4gICAgICAgICAgICAgICAgbmFtZSA9IG5hbWVzW2ldLnRyaW0oKVxyXG4gICAgICAgICAgICAgICAgaWYgKGkgPT09IGxlbi0xKSByZXR1cm4gd2lkZ2V0LndpZGdldHMuZ2V0KG5hbWUpXHJcbiAgICAgICAgICAgICAgICBlbHNlIHdpZGdldCA9IHdpZGdldC5nZXQobmFtZSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMud2lkZ2V0cy5nZXQod2lkZ2V0TmFtZSlcclxuICAgIH1cclxuXHJcbiAgICBlbWl0KGV2ZW50KSB7XHJcbiAgICAgICAgbGV0IGFyZ3MgPSBbXS5zbGljZS5jYWxsKGFyZ3VtZW50cylcclxuXHJcbiAgICAgICAgaWYgKGFyZ3NbMV0gIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICBpZiAoe30udG9TdHJpbmcuY2FsbChhcmdzWzFdKS5zbGljZSg4LCAtMSkgPT09ICdPYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWFyZ3NbMV0udHlwZSkgYXJnc1sxXS50eXBlID0gZXZlbnRcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc3VwZXIuZW1pdC5hcHBseSh0aGlzLCBhcmdzKVxyXG5cclxuICAgICAgICBsZXQgcGFyZW50ID0gdGhpcy5wYXJlbnRcclxuICAgICAgICB3aGlsZShwYXJlbnQgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgaWYgKHBhcmVudC5oYXNMaXN0ZW5lcnMgJiYgcGFyZW50Lmhhc0xpc3RlbmVycyhldmVudCkpIHN1cGVyLmVtaXQuYXBwbHkocGFyZW50LCBhcmdzKVxyXG4gICAgICAgICAgICBwYXJlbnQgPSBwYXJlbnQucGFyZW50XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBkb20oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuX2RvbSA9PT0gdW5kZWZpbmVkKSB0aGlzLl9kb20gPSBuZXcgRG9tKClcclxuICAgICAgICBpZiAodGhpcy5jb250YWluZXIgIT09IHVuZGVmaW5lZCkgdGhpcy5fZG9tLmNhbm9uaWNhbCA9IHRoaXMuY29udGFpbmVyXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RvbVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBieUlkKG5vZGVJZCkge1xyXG4gICAgICAgIHJldHVybiBEb20uYnlJZChub2RlSWQpXHJcbiAgICB9XHJcblxyXG59IiwiXHJcbi8qKlxyXG4gKiBFeHBvc2UgYEVtaXR0ZXJgLlxyXG4gKi9cclxuXHJcbi8vbW9kdWxlLmV4cG9ydHMgPSBFbWl0dGVyO1xyXG5cclxuLyoqXHJcbiAqIEluaXRpYWxpemUgYSBuZXcgYEVtaXR0ZXJgLlxyXG4gKlxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbmZ1bmN0aW9uIEVtaXR0ZXIob2JqKSB7XHJcbiAgICBpZiAob2JqKSByZXR1cm4gbWl4aW4ob2JqKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIE1peGluIHRoZSBlbWl0dGVyIHByb3BlcnRpZXMuXHJcbiAqXHJcbiAqIEBwYXJhbSB7T2JqZWN0fSBvYmpcclxuICogQHJldHVybiB7T2JqZWN0fVxyXG4gKiBAYXBpIHByaXZhdGVcclxuICovXHJcblxyXG5mdW5jdGlvbiBtaXhpbihvYmopIHtcclxuICAgIGZvciAodmFyIGtleSBpbiBFbWl0dGVyLnByb3RvdHlwZSkge1xyXG4gICAgICAgIG9ialtrZXldID0gRW1pdHRlci5wcm90b3R5cGVba2V5XTtcclxuICAgIH1cclxuICAgIHJldHVybiBvYmo7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBMaXN0ZW4gb24gdGhlIGdpdmVuIGBldmVudGAgd2l0aCBgZm5gLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm5cclxuICogQHJldHVybiB7RW1pdHRlcn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5FbWl0dGVyLnByb3RvdHlwZS5vbiA9XHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5hZGRFdmVudExpc3RlbmVyID0gZnVuY3Rpb24oZXZlbnQsIGZuKXtcclxuICAgICAgICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcbiAgICAgICAgKHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF0gPSB0aGlzLl9jYWxsYmFja3NbJyQnICsgZXZlbnRdIHx8IFtdKVxyXG4gICAgICAgICAgICAucHVzaChmbik7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9O1xyXG5cclxuLyoqXHJcbiAqIEFkZHMgYW4gYGV2ZW50YCBsaXN0ZW5lciB0aGF0IHdpbGwgYmUgaW52b2tlZCBhIHNpbmdsZVxyXG4gKiB0aW1lIHRoZW4gYXV0b21hdGljYWxseSByZW1vdmVkLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm5cclxuICogQHJldHVybiB7RW1pdHRlcn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5FbWl0dGVyLnByb3RvdHlwZS5vbmNlID0gZnVuY3Rpb24oZXZlbnQsIGZuKXtcclxuICAgIGZ1bmN0aW9uIG9uKCkge1xyXG4gICAgICAgIHRoaXMub2ZmKGV2ZW50LCBvbik7XHJcbiAgICAgICAgZm4uYXBwbHkodGhpcywgYXJndW1lbnRzKTtcclxuICAgIH1cclxuXHJcbiAgICBvbi5mbiA9IGZuO1xyXG4gICAgdGhpcy5vbihldmVudCwgb24pO1xyXG4gICAgcmV0dXJuIHRoaXM7XHJcbn07XHJcblxyXG4vKipcclxuICogUmVtb3ZlIHRoZSBnaXZlbiBjYWxsYmFjayBmb3IgYGV2ZW50YCBvciBhbGxcclxuICogcmVnaXN0ZXJlZCBjYWxsYmFja3MuXHJcbiAqXHJcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudFxyXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmblxyXG4gKiBAcmV0dXJuIHtFbWl0dGVyfVxyXG4gKiBAYXBpIHB1YmxpY1xyXG4gKi9cclxuXHJcbkVtaXR0ZXIucHJvdG90eXBlLm9mZiA9XHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9XHJcbiAgICAgICAgRW1pdHRlci5wcm90b3R5cGUucmVtb3ZlQWxsTGlzdGVuZXJzID1cclxuICAgICAgICAgICAgRW1pdHRlci5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lciA9IGZ1bmN0aW9uKGV2ZW50LCBmbil7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gYWxsXHJcbiAgICAgICAgICAgICAgICBpZiAoMCA9PSBhcmd1bWVudHMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fY2FsbGJhY2tzID0ge307XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gc3BlY2lmaWMgZXZlbnRcclxuICAgICAgICAgICAgICAgIHZhciBjYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3NbJyQnICsgZXZlbnRdO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFjYWxsYmFja3MpIHJldHVybiB0aGlzO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIHJlbW92ZSBhbGwgaGFuZGxlcnNcclxuICAgICAgICAgICAgICAgIGlmICgxID09IGFyZ3VtZW50cy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyByZW1vdmUgc3BlY2lmaWMgaGFuZGxlclxyXG4gICAgICAgICAgICAgICAgdmFyIGNiO1xyXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjYWxsYmFja3MubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICBjYiA9IGNhbGxiYWNrc1tpXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY2IgPT09IGZuIHx8IGNiLmZuID09PSBmbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYWxsYmFja3Muc3BsaWNlKGksIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgICAgICAgfTtcclxuXHJcbi8qKlxyXG4gKiBFbWl0IGBldmVudGAgd2l0aCB0aGUgZ2l2ZW4gYXJncy5cclxuICpcclxuICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50XHJcbiAqIEBwYXJhbSB7TWl4ZWR9IC4uLlxyXG4gKiBAcmV0dXJuIHtFbWl0dGVyfVxyXG4gKi9cclxuXHJcbkVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbihldmVudCl7XHJcbiAgICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcbiAgICB2YXIgYXJncyA9IFtdLnNsaWNlLmNhbGwoYXJndW1lbnRzLCAxKVxyXG4gICAgICAgICwgY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzWyckJyArIGV2ZW50XTtcclxuXHJcbiAgICBpZiAoY2FsbGJhY2tzKSB7XHJcbiAgICAgICAgY2FsbGJhY2tzID0gY2FsbGJhY2tzLnNsaWNlKDApO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwLCBsZW4gPSBjYWxsYmFja3MubGVuZ3RoOyBpIDwgbGVuOyArK2kpIHtcclxuICAgICAgICAgICAgY2FsbGJhY2tzW2ldLmFwcGx5KHRoaXMsIGFyZ3MpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcztcclxufTtcclxuXHJcbi8qKlxyXG4gKiBSZXR1cm4gYXJyYXkgb2YgY2FsbGJhY2tzIGZvciBgZXZlbnRgLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHJldHVybiB7QXJyYXl9XHJcbiAqIEBhcGkgcHVibGljXHJcbiAqL1xyXG5cclxuRW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24oZXZlbnQpe1xyXG4gICAgdGhpcy5fY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzIHx8IHt9O1xyXG4gICAgcmV0dXJuIHRoaXMuX2NhbGxiYWNrc1snJCcgKyBldmVudF0gfHwgW107XHJcbn07XHJcblxyXG4vKipcclxuICogQ2hlY2sgaWYgdGhpcyBlbWl0dGVyIGhhcyBgZXZlbnRgIGhhbmRsZXJzLlxyXG4gKlxyXG4gKiBAcGFyYW0ge1N0cmluZ30gZXZlbnRcclxuICogQHJldHVybiB7Qm9vbGVhbn1cclxuICogQGFwaSBwdWJsaWNcclxuICovXHJcblxyXG5FbWl0dGVyLnByb3RvdHlwZS5oYXNMaXN0ZW5lcnMgPSBmdW5jdGlvbihldmVudCl7XHJcbiAgICByZXR1cm4gISEgdGhpcy5saXN0ZW5lcnMoZXZlbnQpLmxlbmd0aDtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IEVtaXR0ZXI7IiwiXHJcbmZ1bmN0aW9uIHRyaWdnZXIoZWxlbSkge1xyXG4gICAgbGV0IGV2ZW50ID0gbmV3IEV2ZW50KCdjaGFuZ2UnLCB7XHJcbiAgICAgICAgJ2J1YmJsZXMnOiB0cnVlLFxyXG4gICAgICAgICdjYW5jZWxhYmxlJzogdHJ1ZVxyXG4gICAgfSk7XHJcblxyXG4gICAgZWxlbS5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFBvcHVsYXRlIGZvcm0gZmllbGRzIGZyb20gYSBKU09OIG9iamVjdC5cclxuICpcclxuICogQHBhcmFtIGZvcm0gb2JqZWN0IFRoZSBmb3JtIGVsZW1lbnQgY29udGFpbmluZyB5b3VyIGlucHV0IGZpZWxkcy5cclxuICogQHBhcmFtIGRhdGEgYXJyYXkgSlNPTiBkYXRhIHRvIHBvcHVsYXRlIHRoZSBmaWVsZHMgd2l0aC5cclxuICogQHBhcmFtIGJhc2VuYW1lIHN0cmluZyBPcHRpb25hbCBiYXNlbmFtZSB3aGljaCBpcyBhZGRlZCB0byBgbmFtZWAgYXR0cmlidXRlc1xyXG4gKi9cclxubGV0IHBvcHVsYXRlID0gZnVuY3Rpb24oIGZvcm0sIGRhdGEsIGJhc2VuYW1lKSB7XHJcblxyXG4gICAgZm9yKGxldCBrZXkgaW4gZGF0YSkgaWYgKGRhdGEuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG5cclxuICAgICAgICBsZXQgbmFtZSA9IGtleTtcclxuICAgICAgICBsZXQgdmFsdWUgPSBkYXRhW2tleV07XHJcblxyXG4gICAgICAgIC8vIGhhbmRsZSBhcnJheSBuYW1lIGF0dHJpYnV0ZXNcclxuICAgICAgICBpZih0eXBlb2YoYmFzZW5hbWUpICE9PSBcInVuZGVmaW5lZFwiKSBuYW1lID0gYmFzZW5hbWUgKyBcIltcIiArIGtleSArIFwiXVwiO1xyXG5cclxuICAgICAgICBpZih2YWx1ZS5jb25zdHJ1Y3RvciA9PT0gQXJyYXkpIHtcclxuICAgICAgICAgICAgbmFtZSArPSAnW10nO1xyXG4gICAgICAgIH0gZWxzZSBpZih0eXBlb2YgdmFsdWUgPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgICBwb3B1bGF0ZSggZm9ybSwgdmFsdWUsIG5hbWUpO1xyXG4gICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIG9ubHkgcHJvY2VlZCBpZiBlbGVtZW50IGlzIHNldFxyXG4gICAgICAgIGxldCBlbGVtZW50ID0gZm9ybS5lbGVtZW50cy5uYW1lZEl0ZW0oIG5hbWUgKTtcclxuICAgICAgICBpZighZWxlbWVudCkgY29udGludWU7XHJcblxyXG4gICAgICAgIGxldCB0eXBlID0gZWxlbWVudC50eXBlIHx8IGVsZW1lbnRbMF0udHlwZTtcclxuXHJcbiAgICAgICAgc3dpdGNoKHR5cGUgKSB7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnZhbHVlID0gdmFsdWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ3JhZGlvJzpcclxuICAgICAgICAgICAgY2FzZSAnY2hlY2tib3gnOlxyXG4gICAgICAgICAgICAgICAgbGV0IGxlbiA9IGVsZW1lbnQubGVuZ3RoO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFsZW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBlbGVtZW50LmNoZWNrZWQgPSAhIXZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IobGV0IGo9MDsgaiA8IGxlbjsgaisrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZW1lbnRbal0uY2hlY2tlZCA9ICh2YWx1ZS5pbmRleE9mKGVsZW1lbnRbal0udmFsdWUpID4gLTEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ3NlbGVjdC1tdWx0aXBsZSc6XHJcbiAgICAgICAgICAgICAgICBsZXQgdmFsdWVzID0gdmFsdWUuY29uc3RydWN0b3IgPT0gQXJyYXkgPyB2YWx1ZSA6IFt2YWx1ZV07XHJcblxyXG4gICAgICAgICAgICAgICAgZm9yKGxldCBrID0gMDsgayA8IGVsZW1lbnQub3B0aW9ucy5sZW5ndGg7IGsrKykge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZW1lbnQub3B0aW9uc1trXS5zZWxlY3RlZCB8PSAodmFsdWVzLmluZGV4T2YoZWxlbWVudC5vcHRpb25zW2tdLnZhbHVlKSA+IC0xICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgIGNhc2UgJ3NlbGVjdCc6XHJcbiAgICAgICAgICAgIGNhc2UgJ3NlbGVjdC1vbmUnOlxyXG4gICAgICAgICAgICAgICAgZWxlbWVudC52YWx1ZSA9IHZhbHVlLnRvU3RyaW5nKCkgfHwgdmFsdWU7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyaWdnZXIoZWxlbWVudCk7XHJcbiAgICB9XHJcblxyXG59O1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgcG9wdWxhdGU7Il19
