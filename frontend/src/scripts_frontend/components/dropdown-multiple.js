
+function($) {'use strict';

    var DropdownMultiple = function($element) {
        this.$elem = $element.closest('.dropdown-multiple');
        this.$openElem = $element.hasClass('open') ? $element : this.$elem.find('.open');
        this.$hiddenElem = this.$elem.find('.open+input[type=hidden]');
    };

    DropdownMultiple.prototype.open = function() {
        if (this.$elem.hasClass('open')) return;
        this.$elem.addClass('open');
        this.$openElem.attr('aria-expanded', 'true');
    };

    DropdownMultiple.prototype.close = function() {
        if (this.$elem.hasClass('open')) {
            this.$elem.removeClass("open");
        }
        this.$openElem.attr('aria-expanded', 'false');

        var categoryList = [];
        var idsList = [];
        this.$elem.find('input:checked').each(function() {
            var $this = $(this);
            categoryList.push($this.next('label')[0].innerHTML);
            idsList.push($this.val());
        });
        this.$openElem.val(categoryList.join(', '));
        this.$hiddenElem.val(idsList.join(','));
    };


    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('widget.dropdown-multiple');

            if (!data) $this.data('widget.dropdown-multiple', (data = new DropdownMultiple($this)));
            if (typeof option == 'string') data[option]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = DropdownMultiple;


    $(document)
        .on('click.widget.dropdown-multiple', '.dropdown-multiple .open', function() {
            Plugin.call($(this), 'open');
        })
        .on('click.widget.dropdown-multiple', '.dropdown-multiple .close', function() {
            Plugin.call($(this), 'close');
        });

}(jQuery);