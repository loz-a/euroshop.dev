
+function($) {'use strict';

    var Dropdown = function($element) {
        this.$elem        = $element;
        this.$container   = $element.closest('.dropdown');
        this.$openBtn     = $element.hasClass('.dropdown-btn') ? $element : this.$container.find('.dropdown-btn');
        this.$openBtnIcon = this.$openBtn.find('.btn-icon');
        this.$hiddenElem  = this.$container.find('.dropdown-btn+input[type=hidden]');
    };

    Dropdown.prototype.open = function() {
        $('.dropdown').removeClass('open');
        this.$container.addClass('open');
    };

    Dropdown.prototype.close = function() {

        var $itemIcon = this.$elem.find('.item-icon').clone();
        this.$openBtn.text(this.$elem.text());
        $itemIcon.insertAfter(this.$openBtn.get(0).firstChild);
        this.$openBtnIcon.insertAfter($itemIcon);

        var value = this.$elem.attr('href').replace('/', '');
        this.$hiddenElem.val(value);

        this.$container.removeClass('open');
    };

    function Plugin(option) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('widget.dropdown');

            if (!data) $this.data('widget.dropdown', (data = new Dropdown($this)));
            if (typeof option == 'string') data[option]();
        });
    }

    $.fn.dropdown             = Plugin;
    $.fn.dropdown.Constructor = Dropdown;


    $(document)
        .on('click.widget.dropdown', '.dropdown .dropdown-btn', function() {
            Plugin.call($(this), 'open');
        })
        .on('click.widget.dropdown', '.dropdown .dropdown-menu a', function(evt) {
            evt.preventDefault();
            Plugin.call($(this), 'close');
        });

}(jQuery);