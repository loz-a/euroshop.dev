
+function($) { 'use strict';

    var Carousel = function($element, options) {
        this.$elem          = $element;
        this.$container     = $element.closest('.carousel-container');
        this.$listContainer = this.$container.find('.carousel');
        this.options        = options;

        this.checkVisibility();
        if (options.type == 'column') {
           this.adjustHeight();
        }

        this.addScrollListeners();
    };

    Carousel.DEFAULTS = {
        duration: 300,
        type: 'column' // or row
    };

    Carousel.prototype.getNumberOfVisible = function() {
        var items = this.$listContainer.find('li');

        if (!items.length) return 0;

        var parentContainerWidth = this.$listContainer[0].parentNode.clientWidth;
        return Math.round(parentContainerWidth/items[0].offsetWidth);
    };

    Carousel.prototype.checkVisibility = function() {
        var visibleCount = this.options.visibleCount || this.getNumberOfVisible();
        var isVisible = false;

        if (this.$listContainer.find('li').length > visibleCount) isVisible = true;
        this.$container.find('.navigation').css('display', isVisible ? '' : 'none');
    };

    Carousel.prototype.adjustHeight = function() {
        var height       = 0;
        var visibleCount = this.options.visibleCount || this.getNumberOfVisible();

        this.$listContainer
            .find('li:lt(' + visibleCount + ')')
            .each(function() {
                height += $(this).outerHeight(true);
            });
        height && this.$listContainer.parent().css('height', height + 'px');
    };

    Carousel.prototype.up = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('top', $liList.last().outerHeight(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({top: 0}, this.options.duration);
    };

    Carousel.prototype.down = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({top: $liList.first().outerHeight(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({top: 0});
                }, this)
            );
    };

    Carousel.prototype.prev = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer.css('margin-left', $liList.outerWidth(true) * (-1));
        $liList.last().insertBefore($liList.first());
        this.$listContainer.animate({'margin-left': 0}, this.options.duration);
    };

    Carousel.prototype.next = function() {
        var $liList = this.$listContainer.find('li');
        this.$listContainer
            .animate({'margin-left': $liList.first().outerWidth(true) * (-1)},
                this.options.duration,
                $.proxy(function() {
                    $liList.first().insertAfter($liList.last());
                    this.$listContainer.css({'margin-left': 0})
                }, this)
            );
    };

    Carousel.prototype.addScrollListeners = function() {
        var $mainContainer = $('html, body');

        this.$listContainer.find('li').on('swipeleft', $.proxy(function(e) {
            if (this.options.type === 'row') this.next();
        }, this));

        this.$listContainer.find('li').on('swiperight', $.proxy(function(e) {
            if (this.options.type === 'row') this.prev();
        }, this));

        this.$listContainer.find('li').on('swipeup', $.proxy(function(e) {
            if (this.options.type === 'column') this.down();
            else {
                $mainContainer.animate({
                    scrollTop: this.$listContainer.offset().top + this.$listContainer.outerHeight()
                }, 1000);
            }
        }, this));

        this.$listContainer.find('li').on('swipedown', $.proxy(function(e) {
            if (this.options.type === 'column') this.up();
            else {
                $mainContainer.animate({
                    scrollTop: this.$listContainer.offset().top - this.$listContainer.outerHeight()
                }, 1000);
            }
        }, this));
    };

    function Plugin(option) {
        return this.each(function() {
            var $this   = $(this);
            var data    = $this.data('widget.carousel');
            var parentData = $this.hasClass('.carousel') ? null : $this.closest('.carousel-container').find('.carousel').data();
            var options = $.extend({}, Carousel.DEFAULTS, typeof parentData == 'object' && parentData, $this.data(), typeof option == 'object' && option);
            var action  = typeof option == 'string' ? option : options.slide;

            if (!data) $this.data('widget.carousel', (data = new Carousel($this, options)));
            if (action) data[action]();
        });
    }

    $.fn.dropdownMultiple             = Plugin;
    $.fn.dropdownMultiple.Constructor = Carousel;


    $(document)
        .on('click.widget.carousel', '.carousel-container .carousel-control', function(evt) {
            evt.preventDefault();
            var $this = $(this);
            Plugin.call($this, $this.data());
        });

    $(window).on('load', function() {
        $('.carousel').each(function() {
            var $carousel = $(this);
            Plugin.call($carousel, $carousel.data());
        });
    });

    var mql = window.matchMedia("(orientation: portrait)");
    mql.addListener(function(m) {
        $('.carousel').each(function() {
            var $carousel = $(this).data('widget.carousel');
            if ($carousel) $carousel.checkVisibility();
        });
    });

}(jQuery);