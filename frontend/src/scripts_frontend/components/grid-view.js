
+function($) { 'use strict';

    var listViewClass = 'products-list-column';
    var gridViewClass = 'products-list-row';

    var $listContainer = $('.products-list-container');

    $(document)
        .on('click.grid-view', '.list-view-pane a', function(evt) {
            evt.preventDefault();

            var $this       = $(this);
            var targetClass = $this.attr('data-target-class');

            if (targetClass == listViewClass) {
                $listContainer.find('ul').removeClass(gridViewClass).addClass(listViewClass);
            } else if (targetClass == gridViewClass) {
                $listContainer.find('ul').removeClass(listViewClass).addClass(gridViewClass);
            }

            $this.closest('ul').find('a').removeClass('active');
            $this.addClass('active');
        });

}(jQuery);