$(function() {

    imgToLinkObserver('.carousel', '.item .image-container', '.pr-header a');
    imgToLinkObserver('.products-list-row', '.pr-item .pr-image-container', '.pr-header a');

    function imgToLinkObserver (containerSelector, imageSelector, linkSelector) {
        var $container = $(containerSelector);

        $container.find(imageSelector).css('cursor', 'pointer');
        $container.on('click', imageSelector, handler);

        function handler() {
            var $link = $(this).parent().find(linkSelector);
            if ($link.length) location.assign($link.attr('href'));
        }
    }

});

