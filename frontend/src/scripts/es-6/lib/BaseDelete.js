import BaseWidget from './BaseWidget';
import config from '../config.json';

export default class BaseDelete extends BaseWidget {

    constructor(container, deleteBtn = null) {
        super();
        this.container = container;
        this.deleteBtn = deleteBtn ? deleteBtn : this.container.querySelector('a[href^="#delete"]');

        this.deleteBtn.onclick = this.onClick.bind(this);
    }

    get alertBox() {
        if (this._alertBox === undefined) this._alertBox = this.container.querySelector('.msg-text');
        return this._alertBox;
    }

    onClick(e) {
        e.preventDefault();

        if (this.deleteBtn.classList.contains('disabled')) {
            e.stopPropagation();
            e.stopImmediatePropagation();
            return;
        }
        this.emit('delete', {target: this});
    }

    disableOkBtn() {
        this.showAlertMessage('Element is not empty. First, you have to remove content.');
        this.deleteBtn.classList.add('disabled');
        return this;
    }

    showAlertMessage(msg) {
        msg           = config.i18n.uk[msg];
        let alertText = config.i18n.uk['alert!'];
        this.alertBox.innerHTML = `<strong>${alertText}</strong> ${msg}`;
        return this;
    }

    reset() {
        this.showAlertMessage('Are you sure you want to delete this element?');
        let cl = this.deleteBtn.classList;
        if (cl.contains('disabled')) cl.remove('disabled');
        return this;
    }

    show() {
        this.container.classList.remove('hidden');
        return this;
    }

    hide() {
        this.container.classList.add('hidden');
        return this;
    }
}