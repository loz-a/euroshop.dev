
function trigger(elem) {
    let event = new Event('change', {
        'bubbles': true,
        'cancelable': true
    });

    elem.dispatchEvent(event);
}

/**
 * Populate form fields from a JSON object.
 *
 * @param form object The form element containing your input fields.
 * @param data array JSON data to populate the fields with.
 * @param basename string Optional basename which is added to `name` attributes
 */
let populate = function( form, data, basename) {

    for(let key in data) if (data.hasOwnProperty(key)) {

        let name = key;
        let value = data[key];

        // handle array name attributes
        if(typeof(basename) !== "undefined") name = basename + "[" + key + "]";

        if(value.constructor === Array) {
            name += '[]';
        } else if(typeof value == "object") {
            populate( form, value, name);
            continue;
        }

        // only proceed if element is set
        let element = form.elements.namedItem( name );
        if(!element) continue;

        let type = element.type || element[0].type;

        switch(type ) {
            default:
                element.value = value;
                break;

            case 'radio':
            case 'checkbox':
                let len = element.length;
                if (!len) {
                    element.checked = !!value;
                } else {
                    for(let j=0; j < len; j++) {
                        element[j].checked = (value.indexOf(element[j].value) > -1);
                    }
                }

                break;

            case 'select-multiple':
                let values = value.constructor == Array ? value : [value];

                for(let k = 0; k < element.options.length; k++) {
                    element.options[k].selected |= (values.indexOf(element.options[k].value) > -1 );
                }
                break;

            case 'select':
            case 'select-one':
                element.value = value.toString() || value;
                break;

        }
        trigger(element);
    }

};

export default populate;