import Emitter from './Emitter'

class Registry {

    constructor(widgetsContainer) {
        this.widgetsContainer = widgetsContainer
    }

    static get tree() {
        if (Registry._tree === undefined) Registry._tree = new WeakMap()
        return Registry._tree
    }

    get registry() {
        if (this._registry === undefined) this._registry = new Map()
        return this._registry
    }

    set(widget) {
        let alias
        if (typeof widget === 'string') {
            alias = widget.toLowerCase()
            widget = arguments[1]
        }
        else alias = widget.constructor.name.toLowerCase()


        if (this.registry.has(alias)) throw new Error('Widget with this alias already exists')

        this.registry.set(alias, widget)
        Registry.tree.set(widget, this.widgetsContainer)
        return this
    }

    get(alias) {
        alias = alias.toLowerCase()
        return this.registry.has(alias) ? this.registry.get(alias) : null
    }

    has(alias) {
        return this.registry.has(alias)
    }
}


class Dom {

    set canonical(node) {
        this._canonical = node
    }

    get canonical() {
        return (this._canonical || document)
    }

    node(node) {
        this._node = node
        return this
    }

    static byId(nodeId) {
        return document.getElementById(nodeId)
    }

    select(selector) {
        let result = (this._node || this.canonical).querySelector(selector)
        this._node = null
        return result
    }

    selectAll(selector) {
        let result = (this._node || this.canonical).querySelectorAll(selector) || []
        this._node = null
        return Array.prototype.slice.call(result)
    }

    matches(selector) {
        let node     = this._node || this.canonical
        let p        = Element.prototype;
        let polyfill = (s) => Array.prototype.indexOf.call(node.querySelectorAll(s), this) !== -1
        let f        = p.matches || p.webkitMatchesSelector || p.mozMatchesSelector || p.msMatchesSelector || polyfill
        let result   = f.call(node, selector)
        this._node   = null
        return result
    }
}


export default class BaseWidget extends Emitter {

    get widgets() {
        if (this._widgets === undefined) this._widgets = new Registry(this)
        return this._widgets
    }

    get parent() { return Registry.tree.has(this) ? Registry.tree.get(this) : null }

    equals(widget, selector = '') {
        if (selector.length) widget = widget.get(selector)
        if (!widget) throw new Error('Widget for comparison is undefined')

        if (!this.container || !widget.container) return false
        if (!this.container.id || !widget.container.id) return false

        return this.container.id === widget.container.id
    }

    get(widgetName) {
        if (~widgetName.indexOf('>')) {
            let widget = this,
                names  = widgetName.split('>'),
                len    = names.length,
                name   = ''

            for (let i = 0; i < len; i++) {
                name = names[i].trim()
                if (i === len-1) return widget.widgets.get(name)
                else widget = widget.get(name)
            }
        }

        return this.widgets.get(widgetName)
    }

    emit(event) {
        let args = [].slice.call(arguments)

        if (args[1] !== undefined) {
            if ({}.toString.call(args[1]).slice(8, -1) === 'Object') {
                if (!args[1].type) args[1].type = event
            }
        }

        super.emit.apply(this, args)

        let parent = this.parent
        while(parent !== null) {
            if (parent.hasListeners && parent.hasListeners(event)) super.emit.apply(parent, args)
            parent = parent.parent
        }
    }

    get dom() {
        if (this._dom === undefined) this._dom = new Dom()
        if (this.container !== undefined) this._dom.canonical = this.container
        return this._dom
    }

    static byId(nodeId) {
        return Dom.byId(nodeId)
    }

}