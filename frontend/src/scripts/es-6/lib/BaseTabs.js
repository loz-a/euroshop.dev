import BaseWidget from './BaseWidget';

export default class BaseTabs extends BaseWidget {

    constructor(container) {
        super()
        this.container     = container
        this.activeTab     = null
        this.activeContent = null

        this.onwidgetshow = this.onWidgetShow.bind(this)
        this.onwidgethide = this.onWidgetHide.bind(this)
        this.onshown      = this.onShown.bind(this)
        this.onclick      = this.onClick.bind(this)
    }

    get nav() {
        if (this._nav === undefined) this._nav = this.container.parentNode.querySelector(`#${this.container.id}>.nav`)
        return this._nav
    }

    set onclick(fn) {
        return this.nav.onclick = fn
    }

    getTabByAction(action) {
        return this.nav.querySelector(`a[data-action="${action}"]`)
    }

    show(tab = null) {
        if (!tab) tab = this.nav.querySelector('a[data-action]')

        if (typeof tab == 'string') tab = this.nav.querySelector('a[data-action="${tab}"]')

        if (tab.parentNode.classList.contains('disabled')) return

        let action     = tab.dataset.action
        let contentBox = this.container.parentNode.querySelector(`#${this.container.id}>.tab-content>${tab.getAttribute('href')}`)

        // show container if hidden
        let cl = this.container.classList
        //let isWidgetShowEmited = false
        if (cl.contains('hidden')) {
            //isWidgetShowEmited = true
            this.emit('widget-show', {target: this})
            cl.remove('hidden')
        }

        cl = this.nav.classList
        if (cl.contains('hidden')) {
            //if (!isWidgetShowEmited) {
            //    isWidgetShowEmited = true
            //    this.emit('widget-show', {target: this})
            //}
            cl.remove('hidden')
            this.hideChildren()
        }

        let evt = {
            target:         this,
            willBeShownTab: tab,
            action:         action
        }

        this.emit('show', evt)
        this.inactivateAll()

        tab.parentNode.classList.add('active')
        this.activeTab = tab

        if (contentBox) contentBox.classList.add('active')
        this.activeContent = contentBox

        delete evt.willBeShownTab
        this.emit('shown', evt)
        return this
    }

    hide() {
        this.container.classList.add('hidden')
        this.emit('widget-hide', {target: this})
        return this
    }

    hideChildren(registry = null) {
        if (!registry) registry = this.widgets.registry
        if (!registry.size) return

        for (let widget of registry.values()) {
            this.hideChildren(widget.widgets.registry)
            if (widget instanceof BaseTabs) widget.hide()
        }
        return this
    }

    hideNavigation() {
        this.nav.classList.add('hidden')
    }

    disable(tab = null) {
        if (!tab) {
            let tabs  = this.nav.querySelectorAll('a')
            let slice = Array.prototype.slice

            slice.call(tabs).forEach(function(node) {
                node.parentNode.classList.add('disabled')
            })
        } else {
            tab.parentNode.classList.add('disabled')
        }
        return this
    }

    enable(tab = null) {
        if (!tab) {
            let tabs = this.nav.querySelectorAll('.disabled')
            let slice = Array.prototype.slice

            slice.call(tabs).forEach(function(node) {
                node.classList.remove('disabled')
            })
        } else {
            if (tab && tab.parentNode.classList.contains('disabled')) tab.parentNode.classList.remove('disabled')
        }
        return this
    }

    inactivateAll() {
        let activeElements = this.container.querySelectorAll('.active')
        for(let idx = 0; idx<activeElements.length; idx++) {
            activeElements[idx].classList.remove('active')
        }
        return this
    }

    onClick(e) {
        e.preventDefault()

        var target = e.target
        if (target.tagName.toLowerCase() != 'a') return
        if (target.closest('.close-container')) return
        this.show(target)
    }

    onWidgetShow(e) {
        if (this.equals(e.target)) this.adjustTabs()
    }

    onWidgetHide(e) {
        if (this.equals(e.target)) this.adjustTabs(true)
    }

    onShown(e) {
        if (this.equals(e.target))  return
        if (e.target.activeContent) return

        let tab = this.getTabByAction(e.action)
        if (tab) this.show(tab)
    }

    adjustTabs(clear = false) {
        let pane = this.container.closest('.tab-pane')
        if (pane) {
            let ps = pane.style, ns = this.nav.style
            ns.marginLeft = ps.paddingTop = !!clear ? '' : '0px'
        }
        return this
    }

    set onshow(fn)       { this.on('show', fn) }
    set onshown(fn)      { this.on('shown', fn) }
    set onwidgetshow(fn) { this.on('widget-show', fn) }
    set onwidgethide(fn) { this.on('widget-hide', fn) }
}