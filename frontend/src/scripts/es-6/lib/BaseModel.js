import config from '../config.json';
import xhr from 'xhr';
import Emitter from './Emitter'

let methodsConsts = Object.freeze({
    'GET'   : 'GET',
    'POST'  : 'POST',
    'PUT'   : 'PUT',
    'DELETE': 'DELETE'
});

export default class BaseModel extends Emitter {

    static get methods() {
        return methodsConsts;
    }

    static getDefaultOptions() {
        return {
            headers: {
                "X-Requested-With": "XMLHttpRequest",
                "Content-Type": "application/json",
                "Accept":"application/json"
            }
            //,timeout: 30000
        };
    }

    static getConfig(key) {
        let globalConfig = (config.model && config.model[key]) ? config.model[key] : null;

        return globalConfig
            ? Object.assign(BaseModel.getDefaultOptions(), globalConfig)
            : BaseModel.getDefaultOptions();
    }

    static xhr(options) {
        return new Promise((resolve, reject) => {
            xhr(options, (error, response) => {
                let parsed;

                switch (response.statusCode) {
                    case 200: // OK
                    case 201: // Created
                    case 204: // No Content
                    case 206: // Partial Content
                        parsed = JSON.parse(response.body);
                        resolve(parsed.data ? parsed.data : parsed);
                        break;
                    case 400: // Bad Request
                        parsed = JSON.parse(response.body);
                        reject(parsed.messages ? parsed.messages : parsed);
                        break;
                    default:
                        reject(new Error('An error has occurred. Please, contact developer'));
                }
            });
        });
    }

    xhr(options) {
        return BaseModel.xhr(options);
    }

    set(data) {
        let options = {
            body:   JSON.stringify(data),
            method: data.id ? BaseModel.methods.PUT : BaseModel.methods.POST
        };

        options = Object.assign({}, this.config, options);

        if (options.method === BaseModel.methods.PUT) options.uri = `${options.uri}${data.id}`;

        return this
            .xhr(options)
            .then((data) => {
                this.emit('set', {target: this, data: data});
                return data;
            });
    }

    get(id) {
        let options = Object.assign({}, this.config, { method: BaseModel.methods.GET });
        options.uri = `${options.uri}${id}`;
        return this
            .xhr(options)
            .then((data) => {
                this.emit('get', {target: this, data: data});
                return data;
            });
    }

    remove(id) {
        let options = Object.assign({}, this.config, {method: BaseModel.methods.DELETE});
        options.uri = `${options.uri}${id}`;

        return this
            .xhr(options)
            .then((data) => {
                this.emit('remove', {target: this, data: data});
                return data;
            });
    }

    set onset(fn)    { this.on('set', fn) }
    set onget(fn)    { this.on('get', fn) }
    set onremove(fn) { this.on('remove', fn) }
}