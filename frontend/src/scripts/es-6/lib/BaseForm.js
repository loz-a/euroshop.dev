import BaseWidget from './BaseWidget'
import formToObj  from 'form-to-obj'
import populate   from './populate'
import config     from '../config.json'

export default class BaseForm extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.container.onsubmit = this.onSubmitForm.bind(this)
    }

    set action(action) { this.container.setAttribute('action', action) }
    get action()       { return this.container.getAttribute('action') }

    set data(data) {
        this.reset()
        populate(this.container, data)
    }
    get data() { return formToObj(this.container) }

    reset() {
        let hiddenInputs = this.container.querySelectorAll('input[type="hidden"]')
        let each = Array.prototype.forEach

        each.call(hiddenInputs, (input) => input.value = '');

        this.container.reset()
        this.resetErrors()
        return this
    }

    focus(name) {
        this.container.elements[name].focus()
        return this
    }

    setErrors(errors) {
        let container = this.container
        let translate = config.i18n.uk

        this.resetErrors()

        for(let inputName in errors) if (errors.hasOwnProperty(inputName)) {
            let input = container.querySelector(`[name="${inputName}"]`)
            if (!input) continue

            let msgs = errors[inputName]
            for (let key in msgs) if (msgs.hasOwnProperty(key)) {
                let icon = document.createElement('i')
                icon.classList.add('fa', 'fa-exclamation-triangle')

                let msgBox = document.createElement('span')
                msgBox.classList.add('help-block')

                let text = translate[msgs[key]] ? translate[msgs[key]] : msgs[key]
                msgBox.appendChild(icon)
                msgBox.appendChild(document.createTextNode(text))
                input.parentNode.appendChild(msgBox)
            }

            let fgNode = input.closest('.form-group')
            if (fgNode) fgNode.classList.add('has-error')
        }
        return this
    }

    resetErrors() {
        let each = Array.prototype.forEach
        let groupBoxes = this.container.querySelectorAll('.form-group.has-error')

        each.call(groupBoxes, (group) => {
            let msgsBoxes = group.querySelectorAll('.help-block')
            each.call(msgsBoxes, (msg) => msg.parentNode.removeChild(msg))
            group.classList.remove('has-error')
        })
        return this
    }

    onSubmitForm(e) {
        e.preventDefault()

        this.resetErrors()

        this.emit('submit', {
            target: this,
            data: this.data
        })
    }

    show() {
        this.container.classList.remove('hidden')
        return this
    }

    hide() {
        this.container.classList.add('hidden')
        return this
    }

    set onsubmit(fn) {
        this.on('submit', fn)
    }
}
