import config from '../config.json'

export default class Overlay {

    constructor(container) {
        this.container = container
    }

    static createContainer() {
        let overlayText = document.createElement('div')
        overlayText.classList.add('overlay-content', 'preloader')
        overlayText.appendChild(document.createTextNode(config.i18n.uk['data processing']))

        let overlayContainer = document.createElement('div')
        overlayContainer.id = 'overlay'
        overlayContainer.classList.add('overlay', 'hidden')
        overlayContainer.appendChild(overlayText)

        document.body.appendChild(overlayContainer)

        return overlayContainer
    }

    static get instance() {
        if (Overlay._instance === undefined) {
            Overlay._instance = new Overlay(Overlay.createContainer())
        }
        return Overlay._instance
    }

    get contentBox() {
        if (this._contentBox === undefined) this._contentBox = this.container.querySelector('.overlay-content')
        return this._contentBox
    }

    get closeBtn() {
        if (this._closeBtn === undefined) {
            this._closeBtn = this.container.appendChild(document.createElement('a'))
            this._closeBtn.setAttribute('href', '#close')
            this._closeBtn.classList.add('close-btn')

            this._closeBtn.onclick = (e) => {
                e.preventDefault()
                this.disable()
                    .hideCloseBtn()
                    .resetText()
            }

            let icon = this._closeBtn.appendChild(document.createElement('i'))
            icon.classList.add('fa', 'fa-times')
        }
        return this._closeBtn
    }

    set closable(flag) {
        this._closable = !!flag

    }

    get closable() {
        if (this._closable === undefined) this._closable = false
        return this._closable
    }

    enable() {
        this.container.classList.remove('hidden')
        return this
    }

    disable() {
        let cl = this.container.classList
        if (!cl.contains('hidden')) cl.add('hidden')
        this.resetText()
        return this
    }

    setText(text) {
        let translateText = config.i18n.uk[text]
        this.contentBox.innerHTML = ''
        this.contentBox.appendChild(document.createTextNode(translateText))
        return this
    }

    resetText() {
        this.addPreloader()
        return this.setText('data processing')
    }

    setHtml(html) {
        this.contentBox.innerHTML = html
        return this
    }

    resetHtml() {
        return this.setHtml('')
    }

    setNode(node) {
        this.resetHtml()
        this.contentBox.appendChild(node)
        return this
    }

    addPreloader() {
        let cl = this.contentBox.classList
        if (!cl.contains('preloader')) cl.add('preloader')
        return this
    }

    removePreloader() {
        let cl = this.contentBox.classList
        if (cl.contains('preloader')) cl.remove('preloader')
        return this
    }

    showCloseBtn() {
        let cl = this.closeBtn.classList
        if (cl.contains('hidden')) cl.remove('hidden')
        return this
    }

    hideCloseBtn() {
        let cl = this.closeBtn.classList
        if (!cl.contains('hidden')) cl.add('hidden')
        return this
    }
}