import BaseWidget from './../lib/BaseWidget'

export default class BaseCatalogWidget extends BaseWidget {

    constructor() {
        super()
        BaseCatalogWidget._data = new Map()
    }

    get data() {
        return BaseCatalogWidget._data
    }

}