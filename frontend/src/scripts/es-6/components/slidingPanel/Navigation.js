export default class Navigation {

    constructor(container) {
        this.container = container
        this.container.onclick = this.onClick.bind(this)
    }

    static init(containerId) {
        const containerNode = document.getElementById(containerId)
        return new Navigation(containerNode)
    }

    show(node) {
        node.style.display = ''
        return this
    }

    hide(node) {
        node.style.display = 'none'
        return this
    }

    onClick(e) {
        const target = e.target

        if (target.tagName.toLowerCase() !== 'a') return

        if (target.getAttribute('href') === '#') {
            e.preventDefault()
            target.parentNode.classList.toggle('expanded')
        }
    }

}