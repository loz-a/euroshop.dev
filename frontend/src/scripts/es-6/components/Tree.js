import BaseWidget from './../lib/BaseWidget';

export default class Tree extends BaseWidget {

    constructor(container, model) {
        super();
        this.container = container;
        this.model     = model;
        this.root      = this.container.querySelector('.root-container');
        this.active    = null;

        this.container.onclick = this.onClick.bind(this);

        this.model.ongetchildren = (e) => this.addChildrenContainers(e.data);
        this.model.onset         = (e) => {
            if (e.data.created === e.data.updated) this.addContainer(e.data)
            else this.updateContainer(e.data);
        }
        this.model.onremove = this.removeContainer.bind(this);
    }

    static init(containerId, model) {
        let containerNode = document.getElementById(containerId);
        return new Tree(containerNode, model);
    }

    proxy(node) {
        return !Object.is(this.active, node) && Tree.isExpand(node)
            ? this.makeActive(node)
            : (Tree.isExpand(node) ? this.collapse(node) : this.expand(node));
    }

    static isExpand(node) {
        return node.classList.contains('expand');
    }

    expand(node) {
        node.classList.add('expand');
        this.makeActive(node);
        return this;
    }

    collapse(node) {
        let cl = node.classList;
        if(cl.contains('expand')) cl.remove('expand');
        return this;
    }

    makeActive(node) {
        this.active && this.active.classList.remove('active');
        node.classList.add('active');
        this.active = node;
        this.emit('select', {target: this, selectedNode: node});
        return this;
    }

    static containsContainers(node) {
        return !!node.querySelector('.node-container');
    }

    addContainer(data) {
        let {id, title} = data;
        let activeContainer = this.container.querySelector('.active>ul');

        let html = `<li class="node-container" data-cat-id="${id}">
            <span class="container-title" title="${title}">${title}</span>
            <ul></ul>
        </li>`;

        // if container has no element
        if (!activeContainer.childElementCount) {
            activeContainer.insertAdjacentHTML('beforeEnd', html);
        } else {

            let siblings = [].slice.call(activeContainer.children)
                .filter((node) => node.classList.contains('node-container'));

            // if container has element only without node-cantainer css class
            if (!siblings.length) {
                activeContainer.insertAdjacentHTML('afterBegin', html);
            } else {

                let last = siblings.pop();
                // if container has element with node-cantainer css class end element without this class
                if (last.nextElementSibling) {
                    last.insertAdjacentHTML('afterEnd', html);
                } else {
                    // if container has element only node-cantainer css class
                    activeContainer.insertAdjacentHTML('beforeEnd', html);
                }
            }
        }

        let lastAdded = [].slice.call(this.container.querySelectorAll('.active>ul>.node-container')).pop();
        this.expand(lastAdded);
    }

    updateContainer(data) {
        let node = this.active.querySelector('.container-title');
        node.setAttribute('title', data.title);
        node.firstChild.nodeValue = data.title;
    }

    addChildrenContainers(data) {
        if (!data.length) return;
        if (Tree.containsContainers(this.active)) return;

        let activeContainer = this.container.querySelector('.active>ul');
        let html = [];

        data.forEach(function(data) {
            let {id, title} = data;
            html.push(`<li class="node-container" data-cat-id="${id}">
                <span class="container-title" title="${title}">${title}</span>
            <ul></ul></li>`);
        });

        activeContainer.innerHTML = html.join('');
    }

    removeContainer() {
        let node = this.active;
        if (node.classList.contains('root-container')) return;

        let nextActive, parent = node.parentNode;

        let next = node.nextElementSibling;
        if (next && next.classList.contains('node-container')) nextActive = next;
        else {
            let prev = node.previousElementSibling;
            if (prev && prev.classList.contains('node-container')) nextActive = prev;
        }

        this.parent.data.delete('catalog');
        this.makeActive(nextActive || node.parentNode.parentNode);
        parent.removeChild(node);
    }

    onClick(e) {
        let target = e.target;
        let cl = target.classList;

        if (cl.contains('container-title') || cl.contains('root-title')) this.proxy(target.parentNode);
        if (cl.contains('node-container'))  this.proxy(target);
    }

    set onselect(fn)   { this.on('select', fn) }
}