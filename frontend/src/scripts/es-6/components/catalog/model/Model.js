import config from '../../../config.json';
import Cache from './Cache';
import BaseModel from '../../../lib/BaseModel'

export default class Model extends BaseModel {

    get cache() {
        if (this._cache === undefined) this._cache = new Cache();
        return this._cache;
    }

    get config() {
        if (this._config === undefined) this._config = BaseModel.getConfig('catalog');
        return this._config;
    }

    get(id) {
        if (this.cache.has(id)) {
            let data = this.cache.get(id);
            this.emit('get', {target: this, data: data});
            return Promise.resolve(data);
        }

        let options = Object.assign({}, this.config, { method: BaseModel.methods.GET });
        options.uri = `${options.uri}${id}`;
        return this
            .xhr(options)
            .then((data) => {
                this.cache.set(data);
                this.emit('get', {target: this, data: data});
                return data;
            });
    }

    getChildren(id) {
        if (this.cache.hasChildren(id)) {
            let children = this.cache.getChildren(id);
            this.emit('getChildren', {target: this, data: children});
            return Promise.resolve(children);
        }

        let options = Object.assign({}, this.config, { method: BaseModel.methods.GET });
        options.uri = `${options.uri}pid/${id}`;
        return this
            .xhr(options)
            .then((data) => {
                this.cache.set(data);
                this.emit('getChildren', {target: this, data: data});
                return data;
            });
    }

    set(data) {
        let options = {
            body: JSON.stringify(data),
            method: data.id ? BaseModel.methods.PUT : BaseModel.methods.POST
        };

        options = Object.assign({}, this.config, options);

        if (options.method === BaseModel.methods.PUT) options.uri = `${options.uri}${data.id}`;

        return this
            .xhr(options)
            .then((data) => {
                this.cache.set(data);
                this.emit('set', {target: this, data: data});
                return data;
            });
    }

    remove(id) {
        let options = Object.assign({}, this.config, {method: BaseModel.methods.DELETE});
        options.uri = `${options.uri}${id}`;

        return this
            .xhr(options)
            .then((data) => {
                this.cache.remove(data.id);
                this.emit('remove', {target: this, data: data});
                return data;
            });
    }

    set ongetchildren(fn) { this.on('getChildren', fn) }
}