import config from '../../../config.json'
import BaseModel from '../../../lib/BaseModel'

export default class UploadModel extends BaseModel {

    get config() {
        if (this._config === undefined) this._config = BaseModel.getConfig('catalogUpload');
        return this._config;
    }

    set(data) {
        let options = {
            body:   data.formData,
            method: BaseModel.methods.POST,
            headers: {
            }
        }

        options = Object.assign({}, this.config, options)
        options.uri = `${options.uri}${data.data.catalog_id}`

        return this
            .xhr(options)
            .then((data) => {
                this.emit('set', {target: this, data: data.data})
                return data
            });
    }

    getByCatalog(catalogId) {
        let options = Object.assign({}, this.config, {method: BaseModel.methods.GET})
        options.uri = `${options.uri}${catalogId}`

        return this
            .xhr(options)
            .then((data) => {
                this.emit('getByCatalog', {target: this, data: data})
                return data
            });
    }

    get() {
        throw new Error('Method is not implemented')
    }

}