export default class Cache {

    constructor() {
        this.data     = new Map();
        this.idMapper = new Map();
    }

    set(data) {
        if ({}.toString.call(data).slice(8, -1) === 'Array') {
            for(let i = 0; i < data.length; i++) this.set(data[i]);
        } else {
            this.data.set(data.id, data);

            if (this.idMapper.has(data.parent_id)) this.idMapper.get(data.parent_id).add(data.id);
            else this.idMapper.set(data.parent_id, new Set([data.id]));
        }
        return this;
    }

    get(id) {
        return this.data.has(id) ? this.data.get(id) : null;
    }

    has(id) {
        return this.data.has(id);
    }

    getChildren(pid) {
        let result = [];
        if (!this.idMapper.has(pid)) return result;

        let data;
        let idSet = this.idMapper.get(pid);
        for (let id of idSet) {
            data = this.data.get(id);
            if (data) result.push(data);
        }
        return result;
    }

    hasChildren(pid) {
        return this.idMapper.has(pid);
    }

    remove(id) {
        if (this.data.has(id)) {
            let pid = this.data.get(id).parent_id;
            this.idMapper.has(pid) && this.idMapper.get(pid).delete(id);
            this.idMapper.has(id) && this.removeRecursiveFromMapper(id);
            this.data.delete(id);
        }
        return this;
    }

    removeRecursiveFromMapper(id) {
        if (!this.idMapper.has(id)) return;

        let childrenSet = this.idMapper.get(id);
        if (childrenSet.size) {
            childrenSet.forEach((id) => this.removeRecursiveFromMapper(id));
        }
        this.idMapper.delete(id);
    }
}