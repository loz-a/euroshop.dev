import BaseTabs from '../../lib/BaseTabs'
import AddCatalogTabs from './tabs/addCatalogTabs'
import TableView from './tabs/TableView'
import DeleteView from './tabs/DeleteView'
import Goods from './tabs/Goods'
import Overlay from '../Overlay'

export default class Tabs extends BaseTabs {

    constructor(container) {
        super(container)

        this.widgets
            .set('tabs', AddCatalogTabs.init('catalog-manipulate-data'))
            .set(DeleteView.init('catalog-delete'))
            .set(Goods.init('goods'))

        this.onshown = this.proxy.bind(this)
    }

    static init(container) {
        return new Tabs(container)
    }

    set data(data) {
        if ({}.toString.call(data).slice(8, -1) !== 'Object') throw new Error('Argument passed to data setter must be of the type object')
        this._data = data
    }

    get data() { return this._data }

    get viewTab() {
        if (this._viewTab === undefined) this._viewTab = this.container.querySelector('a[data-action="catalog_view"]')
        return this._viewTab
    }

    get addTab() {
        if (this._addTab === undefined) this._addTab = this.container.querySelector('a[data-action="catalog_add"]')
        return this._addTab
    }

    get editTab() {
        if (this._editTab === undefined) this._editTab = this.container.querySelector('a[data-action="catalog_edit"]')
        return this._editTab
    }

    get deleteTab() {
        if (this._deleteTab === undefined) this._deleteTab = this.container.querySelector('a[data-action="catalog_delete"]')
        return this._deleteTab
    }

    get goodsTab() {
        if (this._goodsTab === undefined) this._goodsTab = this.container.querySelector('a[data-action="goods_list_view"]')
        return this._goodsTab
    }

    get tableView() {
        if (this._tableView === undefined) this._tableView = TableView.init(this.container)
        return this._tableView
    }

    proxy(e) {
        let target = e.target
        if (!this.equals(target)) return

        switch (target.activeTab) {
            case this.viewTab  : this.onShowViewTab(e); break
            case this.addTab   : this.onShowAddTab(e); break
            case this.editTab  : this.onShowEditTab(e); break
            case this.deleteTab: this.onShowDeleteTab(e); break
            case this.goodsTab : this.onShowGoodsTab(e); break
        }
    }

    showOnlyAddTab() {
        this.disable()
        this.enable(this.addTab)
        this.show(this.addTab)
        return this
    }

    onShowViewTab(e) {
        let data = this.parent.data.get('catalog')
        this.tableView.populate(data).resetImg()
        if (data.img) this.tableView.viewImg(data.img.path)
        Overlay.instance.disable()
    }

    onShowDeleteTab(e) {
        this.get('deleteView').reset()
    }

    onShowAddTab(e) {
        let tabs = this.get('tabs')
        let form = tabs.get('form')
        let {id} = this.parent.data.get('catalog');

        tabs.show()
        form.focus('title').action = 'add'
        form.data = {parent_id: id}

        tabs.disable(tabs.uploadTab)
    }

    onShowEditTab(e) {
        let tabs = this.get('tabs')
        let form = tabs.get('form')

        tabs.show()
        form.action = 'edit'
        form.data   = this.parent.data.get('catalog')

        tabs.enable()
    }

    onShowGoodsTab(e) {
        let goods = this.get('goods')
        goods.get('tabs').show()
        this.hideNavigation()
    }

}
