import BaseWidget from './../../BaseCatalogWidget'
import Model from './goods/Model'
import Tabs from './goods/Tabs'
import Overlay from './../../Overlay'


export default class Goods extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.model = new Model()

        this.widgets.set(Tabs.init(this.container))

        this.onsubmit = this.submit.bind(this)
        this.ondelete = this.submitDelete.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new Goods(containerNode)
    }

    populateList() {
        let overlay = Overlay.instance
        overlay.enable()

        this.model
            .getByCatalog(this.data.get('catalog').id)
            .then((data) => {
                let goodsGrid = this.get('tabs > goodsGrid')
                goodsGrid.populate(data)
                overlay.disable()
            })
            .catch((errors) => overlay.setText("An error has occurred. Please, contact developer"))
    }

    submit(e) {
        let form = e.target
        if (!form.equals(this, 'tabs > addGoodsTabs > dataform')) return

        let data    = e.data
        let overlay = Overlay.instance

        form.resetErrors()
        overlay.enable()

        data.is_top = data.is_top && data.is_top === 'on' ? '1' : '0'
        data.is_index_carousel = data.is_index_carousel && data.is_index_carousel === 'on' ? '1' : '0'

        this.model
            .set(data)
            .then((data) => {
                this.data.set('goods', data)

                let tabs = this.get('tabs')
                tabs.show(tabs.viewTab)

                overlay.disable()
            })
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    form.setErrors(errors)
                    overlay.disable()
                }
            })
    }

    submitDelete(e) {
        let target    = e.target
        let goodsGrid = this.get('tabs > goodsGrid')
        if (!target.equals(goodsGrid.get('deleteView'))) return

        let overlay = Overlay.instance
        overlay.enable()

        this.model
            .remove(this.data.get('goods').id)
            .then(() => {
                goodsGrid.deleteRow()
                overlay.disable()
            })
            .catch((error) => overlay.setText(error.message))
    }

    set ondelete(fn) { this.on('delete', fn) }
    set onsubmit(fn) { this.on('submit', fn) }
    set onshown(fn)  { this.on('shown', fn)  }
}
