import BaseForm from '../../../../lib/BaseForm'

export default class UploadForm extends BaseForm {

    constructor(container) {
        super(container)

        this.deleteSubmitBtn.onclick = this.onDeleteSubmit.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new UploadForm(containerNode)
    }

    set data(data) {
        this.reset()

        if (data.img) this.viewImg(data.img.path)
        else this.resetImg()

        if (data.id) this.container.elements['catalog_id'].value = data.id
    }

    get data() {
        let formData = new FormData(this.container)

        return {
            data: super.data,
            formData: formData
        }
    }

    get img() {
        if (this._img === undefined) this._img = this.container.querySelector('img')
        return this._img
    }

    get deleteSubmitBtn() {
        if (this._deleteSbmt === undefined) this._deleteSbmt = this.container.querySelector('.delete-btn-box button')
        return this._deleteSbmt
    }

    viewImg(src, alt = '', title = '') {
        if (!src) this.resetImg()
        else {
            this.img.src   = src
            this.img.alt   = alt
            this.img.title = title
            this.img.classList.remove('hidden')
            this.deleteSubmitBtn.classList.remove('disabled')
        }
        return this
    }

    resetImg() {
        this.img.src = this.img.alt = this.img.title = ''
        this.img.classList.add('hidden')
        this.deleteSubmitBtn.classList.add('disabled')
        return this
    }

    onDeleteSubmit() {
        let {catalog_id} = this.data.data

        this.emit('delete', {
            target: this,
            data: {catalog_id}
        })
    }

    set ondelete(fn) {
        this.on('delete', fn)
    }

}