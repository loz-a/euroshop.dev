import BaseForm from '../../../../lib/BaseForm'

const ACTION_ADD  = 'add'
const ACTION_EDIT = 'edit'

export default class Form extends BaseForm {

    constructor(container) {
        super(container)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new Form(containerNode)
    }

    get title() {
        if (this._title === undefined) this._title = document.getElementById('inputTitle')
        return this._title
    }

    isAdd() { return this.action === ACTION_ADD }

    isEdit() { return this.action === ACTION_EDIT }

    static normalizeData(data) {
        for (let key in data) if (data.hasOwnProperty(key)) {
            if (data[key] === null) data[key] = '0'
        }
        return data
    }

    set data(data) {
        let filteredData = {}

        if (data.id)        filteredData.id        = data.id
        if (data.parent_id) filteredData.parent_id = data.parent_id
        if (data.title)     filteredData.title     = data.title

        super.data = Form.normalizeData(filteredData)
    }

    get data() { return super.data }
}