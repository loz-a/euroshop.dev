import BaseTabs     from '../../../../lib/BaseTabs'
import AddGoodsTabs from './tabs/AddGoodsTabs'
import GoodsGrid    from './tabs/GoodsGrid'
import GoodsView    from './tabs/GoodsView'
import {ACTION_ADD, ACTION_EDIT} from './tabs/addGoodsTabs/DataForm'

export default class Tabs extends BaseTabs {

    constructor(container) {
        super(container)

        this.widgets
            .set('addGoodsTabs', AddGoodsTabs.init('goods-manipulate-data'))
            .set('goodsGrid', GoodsGrid.init('goods-grid'))
            .set('goodsView', GoodsView.init('goods-view'))

        this.onshown = this.proxy.bind(this)
    }

    static init(container) {
        return new Tabs(container)
    }

    get gridTab() {
        if (this._gridTab === undefined) this._gridTab = this.dom.select('a[data-action="goods_list"]')
        return this._gridTab
    }

    get viewTab() {
        if (this._viewTab === undefined) this._viewTab = this.dom.select('a[data-action="goods_view"]')
        return this._viewTab
    }

    get addTab() {
        if (this._addTab === undefined) this._addTab = this.dom.select('a[data-action="goods_add"]')
        return this._addTab
    }

    get editTab() {
        if (this._editTab === undefined) this._editTab = this.dom.select('a[data-action="goods_edit"]')
        return this._editTab
    }

    get catalogReturnTab() {
        if (this._catalogReturnTab === undefined) this._catalogReturnTab = this.dom.select('a[data-action="catalog_view"]')
        return this._catalogReturnTab
    }

    proxy(e) {
        if (!this.equals(e.target)) return

        switch (this.activeTab) {
            case this.gridTab :this.onShowGridTab(e); break
            case this.addTab  :this.onShowAddTab(e); break
            case this.editTab :this.onShowEditTab(e); break
            case this.viewTab :this.onShowViewTab(e); break
            //case this.catalogReturnTab :this.onCatalogReturnTab(e); break
        }
    }

    onShowGridTab(e) {
        this.resetHiddenTabs()
        this.parent.populateList()
        this.get('goodsGrid').showGrid()
    }

    onShowViewTab(e) {
        this.editTab.parentNode.classList.add('hidden')
        this.viewTab.parentNode.classList.remove('hidden')

        this.get('goodsView').populate(this.parent.data.get('goods'))
    }

    onShowAddTab(e) {
        let tabs = this.get('addGoodsTabs')

        this.resetHiddenTabs()
        tabs.get('dataForm').action = ACTION_ADD
        tabs.disable().enable(tabs.dataTab).show()
    }

    onShowEditTab(e) {
        let tabs = this.get('addGoodsTabs')

        this.viewTab.parentNode.classList.add('hidden')
        this.editTab.parentNode.classList.remove('hidden')

        tabs.get('dataForm').action = ACTION_EDIT
        tabs.enable().show()
    }

    onCatalogReturnTab(e) {
        this.parent.parent.show()
    }

    resetHiddenTabs() {
        this.viewTab.parentNode.classList.add('hidden')
        this.editTab.parentNode.classList.add('hidden')
        return this
    }
}