import BaseForm from '../../../../../../../lib/BaseForm'

export default class UploadForm extends BaseForm {

    constructor(container) {
        super(container)
    }

    static init(containerId) {
        return new UploadForm(BaseForm.byId(containerId))
    }

    set data(data) {
        this.reset()
        if (data.goods_id) this.container.elements['goods_id'].value = data.goods_id
    }

    get data() {
        let formData = new FormData(this.container)

        return {
            data: super.data,
            formData: formData
        }
    }
}