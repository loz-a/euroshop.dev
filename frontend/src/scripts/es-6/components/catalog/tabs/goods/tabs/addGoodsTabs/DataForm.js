import BaseForm from '../../../../../../lib/BaseForm';

export const ACTION_ADD    = 'add';
export const ACTION_EDIT   = 'edit';
export const ACTION_DELETE = 'delete';

export class DataForm extends BaseForm {

    constructor(container) {
        super(container);
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId);
        return new DataForm(containerNode);
    }

    isAdd() { return this.action === ACTION_ADD }

    isEdit() { return this.action === ACTION_EDIT }

    set data(data) {
        if (data.price) data.price = data.price.toFixed(2)
        super.data = data
    }
    get data() { return super.data }
}

