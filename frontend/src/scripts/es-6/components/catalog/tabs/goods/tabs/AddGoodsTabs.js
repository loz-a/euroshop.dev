import BaseTabs      from '../../../../../lib/BaseTabs'
import {DataForm}    from './addGoodsTabs/DataForm'
import GoodsDetailed from './addGoodsTabs/GoodsDetailed'
import GoodsGallery  from './addGoodsTabs/GoodsGallery'

export default class AddGoodsTabs extends BaseTabs {

    constructor(container) {
        super(container)

        this.widgets
            .set(DataForm.init('goods-form'))
            .set(GoodsDetailed.init('goods-detailed-data'))
            .set(GoodsGallery.init('goods-gallery'))

        this.onshown = this.proxy.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new AddGoodsTabs(containerNode)
    }

    get dataTab() {
        if (this._dataTab === undefined) this._dataTab = this.container.querySelector('a[data-action="goods_data"]')
        return this._dataTab
    }

    get dataDetailedTab() {
        if (this._dataDetailedTab === undefined) this._dataDetailedTab = this.container.querySelector('a[data-action="goods_detailed_data"]')
        return this._dataDetailedTab
    }

    get galleryTab() {
        if (this._galleryTab === undefined) this._galleryTab = this.container.querySelector('a[data-action="goods_gallery"]')
        return this._galleryTab
    }

    get data() {
        return this.parent.parent.data
    }

    proxy(e) {
        if (!this.equals(e.target)) return

        switch (this.activeTab) {
            case this.dataTab:         this.onShowDataTab(e); break
            case this.dataDetailedTab: this.onShowDetailedDataTab(e); break
            case this.galleryTab:      this.onShowGalleryTab(e); break
        }
    }

    onShowDataTab(e) {
        let parentContainer = this.parent.container
        parentContainer.parentNode.querySelector(`#${parentContainer.id}>.nav-tabs`).classList.remove('hidden')
        this.adjustTabs(true)

        this.get('dataForm')
            .data = {
                catalog_id: this.data.get('catalog').id
            }
    }

    onShowDetailedDataTab(e) {
        let goodsDetailed = this.get('goodsDetailed')
        let detailedGrid  = goodsDetailed.get('detailedGrid')
        let form          = detailedGrid.get('form')

        goodsDetailed.populateList()
        detailedGrid.showGrid()
        form.reset().hide()
    }

    onShowGalleryTab(e) {
        let gallery = this.get('goodsGallery')

        gallery.get('uploadForm')
            .data = {
                goods_id: this.data.get('goods').id
            }

        gallery.get('list').disableActionsBtns()
        gallery.viewImagesList()

    }

}