import config from '../../../../../../../config.json'
import BaseModel from '../../../../../../../lib/BaseModel'

export default class Model extends BaseModel {

    get config() {
        if (this._config === undefined) this._config = BaseModel.getConfig('goodsDetailed')
        return this._config
    }

    getByGoods(goodsId) {
        let options = Object.assign({}, this.config, { method: BaseModel.methods.GET })
        options.uri = `${options.uri}gid/${goodsId}`
        return this
            .xhr(options)
            .then((data) => {
                this.emit('getByGoods', {target: this, data: data})
                return data
            });
    }
}