import config from '../../../../../../../config.json'
import BaseModel from '../../../../../../../lib/BaseModel'

export default class Model extends BaseModel {

    get config() {
        if (this._config === undefined) this._config = BaseModel.getConfig('goodsImages')
        return this._config
    }

    set(data) {
        let options = {
            body:   data.formData,
            method: BaseModel.methods.POST,
            headers: {
            }
        }

        options = Object.assign({}, this.config, options)
        options.uri = `${options.uri}${data.data.goods_id}`

        return this
            .xhr(options)
            .then((data) => {
                this.emit('set', {target: this, data: data.data})
                return data
            })
    }

    getByGoods(goodsId) {
        let options = Object.assign({}, this.config, {method: BaseModel.methods.GET})
        options.uri = `${options.uri}${goodsId}`

        return this
            .xhr(options)
            .then((data) => {
                this.emit('getByGoods', {target: this, data: data})
                return data
            })
    }

    get() {
        throw new Error('Method is not implemented')
    }

}