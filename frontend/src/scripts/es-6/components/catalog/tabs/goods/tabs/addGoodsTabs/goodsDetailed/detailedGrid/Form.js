import BaseForm from '../../../../../../../../lib/BaseForm';

export const ACTION_ADD  = 'add';
export const ACTION_EDIT = 'edit';
export const ACTION_REMOVE = 'remove';

export class Form extends BaseForm {

    constructor(container) {
        super(container);

        this.cancelBtn = this.container.querySelector('button[type="submit"]+button[type="button"]');

        this.cancelBtn.onclick = this.onCancelForm.bind(this);
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId);
        return new Form(containerNode);
    }

    isAdd() { return this.action === ACTION_ADD }

    isEdit() { return this.action === ACTION_EDIT }

    onCancelForm(e) {
        this.reset();
        this.hide();
        this.emit('cancel', {target: this});
    }

}