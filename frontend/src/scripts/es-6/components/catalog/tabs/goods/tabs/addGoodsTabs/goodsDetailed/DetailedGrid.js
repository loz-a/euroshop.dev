import config from '../../../../../../../config.json'
import Overlay from '../../../../../../Overlay'
import BaseWidget from './../../../../../../../lib/BaseWidget'
import {Form, ACTION_ADD, ACTION_EDIT, ACTION_REMOVE} from './detailedGrid/Form'
import DeleteView from './detailedGrid/DeleteView'

export default class DetailedGrid extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.grid      = this.container.querySelector('.detailed-table tbody')
        this.addBtn    = this.container.querySelector('.actions .add-btn')

        this.selected = null

        this.widgets
            .set(Form.init('detailed-form'))
            .set(DeleteView.init('detailed-delete'))

        this.oncancel       = this.onCancel.bind(this)
        this.addBtn.onclick = this.onAddBtnClick.bind(this)
        this.grid.onclick   = this.proxy.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new DetailedGrid(containerNode)
    }

    proxy(e) {
        e.preventDefault()
        let target = e.target

        let selectedRow = target.closest('tr')
        if (selectedRow) this.selected = { data: this.getRowData(selectedRow), node: selectedRow }

        if (target.parentNode.hasAttribute('href')) target = target.parentNode
        if (!target.hasAttribute('href')) return

        let parsedUri = e.target.getAttribute('href').slice(1).split('/')
        e.id          = parsedUri.pop()
        let action    = parsedUri.pop()

        switch (action) {
            case ACTION_EDIT  : this.onEditBtnClick(e); break
            case ACTION_REMOVE: this.onDeleteBtnClick(e); break
        }
    }

    onAddBtnClick(e) {
        e.preventDefault()
        let form = this.get('form')
        form.reset().show().focus('title')
        form.action = ACTION_ADD
        form.data   = { goods_id: this.parent.data.get('goods').id }

        this.hideAddBtn()
    }

    onEditBtnClick(e) {
        let form = this.get('form')
        let row  = e.target.closest('tr')
        form.action = ACTION_EDIT
        form.reset().show().focus('title')
        form.data = this.getRowData(row)

        this.hideAddBtn()
    }

    onDeleteBtnClick(e) {
        let deleteView = this.get('deleteView')
        this.hideGrid()
        this.get('form').hide()
        deleteView.reset().show()
    }

    onCancel(e) {
        let target = e.target
        if (target.equals(this, 'deleteView')) this.showGrid()
        if (target.equals(this, 'form')) this.showGrid()
    }

    submitForm() {
        this.showGrid()
    }

    submitDelete() {
        this.deleteSelectedRow()
        this.showGrid()
    }

    populate(data) {
        this.grid.innerHTML = ''

        for (let i = 0; i < data.length; i++) {
            this.addRow(data[i])
        }
        return this
    }

    addRow(data) {
        let text = (text) => document.createElement('td').appendChild(document.createTextNode(text)).parentNode

        let tr = this.grid.insertRow()
        tr.dataset.id = data.id

        tr.appendChild(text(tr.rowIndex))
        tr.appendChild(text(data.title))
        tr.appendChild(text(data.value))

        let actions = document.createElement('td')
        actions.innerHTML =
            `<a href="#edit/${data.id}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i> ${config.i18n.uk['edit']}</a>
            <a href="#remove/${data.id}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ${config.i18n.uk['delete']}</a>`

        tr.appendChild(actions)

        this.selected = {
            data: data,
            node: tr
        }
    }

    editRow(data) {
        let cells = this.selected.node.cells
        cells[1].textContent = this.selected.data.title = data.title
        cells[2].textContent = this.selected.data.value = data.value
        return this
    }

    deleteSelectedRow() {
        this.grid.parentNode.deleteRow(this.selected.node.rowIndex)
        this.selected = null
        this.rebuildGridIndexes()
    }

    rebuildGridIndexes() {
        let rows = this.grid.rows
        for (let i = 0; i<rows.length; i++) {
            rows[i].cells[0].textContent = rows[i].rowIndex
        }
    }

    getRowData(row) {
        let cells = row.cells
        return {
            id    :row.dataset.id,
            title :cells[1].textContent,
            value :cells[2].textContent
        }
    }

    hideGrid() {
        this.grid.parentNode.classList.add('hidden')
        this.hideAddBtn()
        return this
    }

    showGrid() {
        this.grid.parentNode.classList.remove('hidden')
        this.addBtn.classList.remove('hidden')
        return this
    }

    hideAddBtn() {
        this.addBtn.classList.add('hidden')
        return this
    }

    set oncancel(fn) { this.on('cancel', fn) }
}
