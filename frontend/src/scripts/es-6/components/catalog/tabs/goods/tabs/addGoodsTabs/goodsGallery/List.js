import BaseWidget from '../../../../../../../lib/BaseWidget'
import Overlay    from '../../../../../../Overlay'

export default class List extends BaseWidget {

    constructor(container) {
        super()
        this.container = container

        this.container.onclick = this.proxy.bind(this)
    }

    static init(containerId) {
        return new List(BaseWidget.byId(containerId))
    }

    get list() {
        if (this._list === undefined) this._list = this.dom.select('.gallery-list')
        return this._list
    }

    get deleteBtn() {
        if (this._deleteBtn === undefined) this._deleteBtn = this.dom.select('.delete-btn')
        return this._deleteBtn
    }

    get expandBtn() {
        if (this._expandBtn === undefined) this._expandBtn = this.dom.select('.expand-btn')
        return this._expandBtn
    }

    get expandImg() {
        if (this._expandImg === undefined) this._expandImg = document.createElement('img')
        return this._expandImg
    }

    proxy(e) {
        e.preventDefault()
        let target = e.target

        if (target.classList.contains('disabled')) return
        if (target === this.deleteBtn) return this.onDeleteClick(e)
        if (target === this.expandBtn) return this.onExpand(e)

        if (target.tagName.toLowerCase() === 'img') target = target.parentNode
        if (target.tagName.toLowerCase() === 'a') return this.activeItem(target)
    }

    populate(data) {
        this.list.innerHTML = ''

        for (let i = 0; i < data.length; i++) {
            this.add(data[i])
        }
        this.preloadImages()
        return this
    }

    add(data) {
        let li  = document.createElement('li')
        let a   = li.appendChild(document.createElement('a'))
        let img = a.appendChild(document.createElement('img'))

        a.href = data.path
        a.dataset.id = data.id
        a.classList.add('thumbnail')

        img.src = (data.thumbs && data.thumbs.min) || data.min
        img.alt = ''

        this.list.appendChild(li)
    }

    deleteActive() {
        let activeNode = this.dom.node(this.list).select('.active')
        if (!activeNode) return

        let parent = activeNode.parentNode
        parent.parentNode.removeChild(parent)

        this.disableActionsBtns()

        return this
    }

    activeItem(node) {
        this.enableActionsBtns()

        this.dom.node(this.list)
            .selectAll('.active')
            .forEach((node) => node.classList.remove('active'))
        node.classList.add('active')
        return this
    }

    onDeleteClick(e) {
        let active = this.dom.node(this.list).select('.active')
        if (!active) return

        this.emit('delete', {
            target: this,
            imageId: active.dataset.id
        })
    }

    onExpand(e) {
        let active = this.dom.node(this.list).select('.active')
        if (!active) return

        let overlay = Overlay.instance
        let img     = this.expandImg
        img.src     = active.getAttribute('href')

        overlay
            .setNode(img)
            .removePreloader()
            .showCloseBtn()
            .enable()
    }

    disableActionsBtns() {
        let delCl = this.deleteBtn.classList
        let expCl = this.expandBtn.classList

        if (!delCl.contains('disabled')) delCl.add('disabled')
        if (!expCl.contains('disabled')) expCl.add('disabled')
    }

    enableActionsBtns() {
        let delCl = this.deleteBtn.classList
        let expCl = this.expandBtn.classList

        if (delCl.contains('disabled')) delCl.remove('disabled')
        if (expCl.contains('disabled')) expCl.remove('disabled')
    }

    preloadImages() {
        let links = this.dom.node(this.list).selectAll('a')
        if (!links) return

        let imgNode = this.expandImg
        links.forEach((link) => imgNode.src = link.getAttribute('href'))
        imgNode.src = ''

        return this
    }
}