import BaseWidget   from '../../../../../BaseCatalogWidget'
import Model        from './goodsDetailed/Model'
import DetailedGrid from './goodsDetailed/DetailedGrid'
import Overlay      from './../../../../../Overlay'

export default class GoodsDetailed extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.model     = new Model()

        this.widgets.set(DetailedGrid.init('detailed-grid'))

        this.onsubmit = this.submit.bind(this)
        this.ondelete = this.submitDelete.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new GoodsDetailed(containerNode)
    }

    populateList() {
        let overlay = Overlay.instance
        overlay.enable()

        this.model
            .getByGoods(this.data.get('goods').id)
            .then((data) => {
                let detailedGrid = this.get('detailedGrid')
                detailedGrid.populate(data)
                overlay.disable()
            })
            .catch((errors) => overlay.setText("An error has occurred. Please, contact developer"))
    }

    submit(e) {
        let form = e.target
        let grid = this.get('detailedGrid')

        if (!form.equals(grid, 'form')) return

        let data    = e.data
        let overlay = Overlay.instance

        overlay.enable()

        this.model
            .set(data)
            .then((data) => {
                form.hide().reset()

                if (form.isAdd()) grid.addRow(data)
                else grid.editRow(data)

                grid.submitForm()
                overlay.disable()
            })
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    form.setErrors(errors)
                    overlay.disable()
                }
            })
    }

    submitDelete(e) {
        let target       = e.target
        let detailedGrid = this.get('detailedGrid')
        if (!target.equals(detailedGrid.get('deleteView'))) return

        let overlay = Overlay.instance
        let id      = detailedGrid.selected ? detailedGrid.selected.data.id : null

        overlay.enable()
        if (!id) {
            // TODO handle when id not exists
            return
        }

        this.model
            .remove(id)
            .then(() => {
                detailedGrid.submitDelete()
                overlay.disable()
            })
            .catch((error) => overlay.setText(error.message))
    }

    set onsubmit(fn) { this.on('submit', fn) }
    set ondelete(fn) { this.on('delete', fn) }
}