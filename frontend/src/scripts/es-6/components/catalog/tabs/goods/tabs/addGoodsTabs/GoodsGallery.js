import BaseWidget from '../../../../../BaseCatalogWidget'
import Overlay    from '../../../../../Overlay'
import Model      from './goodsGallery/Model'
import List       from './goodsGallery/List'
import UploadForm from './goodsGallery/UploadForm'

export default class GoodsGallery extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.model     = new Model()

        this.widgets
            .set(List.init('goods-gallery-list'))
            .set(UploadForm.init('goods-gallery-form'))

        this.onsubmit = this.submit.bind(this)
        this.ondelete = this.remove.bind(this)
    }

    static init(containerId) {
        return new GoodsGallery(BaseWidget.byId(containerId))
    }

    viewImagesList() {
        let goodsId = this.data.get('goods').id
        let overlay = Overlay.instance
        let list    = this.get('list')

        overlay.enable()

        this.model
            .getByGoods(goodsId)
            .then((data) => {
                list.populate(data)
                overlay.disable()
            })
    }

    submit(e) {
        let form = this.get('uploadForm')
        if (!form.equals(e.target)) return

        let list    = this.get('list')
        let overlay = Overlay.instance
        overlay.enable()

        this.model
            .set(e.data)
            .then((data) => {
                list.add(data)
                form.container.reset()
                overlay.disable()
            })
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    form.setErrors(errors)
                    overlay.disable()
                }
            })
    }

    remove(e) {
        let list   = this.get('list')
        let target = e.target
        if (!list.equals(target)) return

        let overlay = Overlay.instance
        overlay.enable()

        this.model
            .remove(e.imageId)
            .then((data) => {
                list.deleteActive()
                overlay.disable()
            })
    }

    set onsubmit(fn) { this.on('submit', fn) }
    set ondelete(fn) { this.on('delete', fn) }
}