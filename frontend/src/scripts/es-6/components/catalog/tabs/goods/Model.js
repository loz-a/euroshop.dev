import config from '../../../../config.json';
import BaseModel from '../../../../lib/BaseModel'

export default class Model extends BaseModel {

    constructor() {
        super();

        this.onset    = (e) => { this.cache.set(e.data.id, e.data) };
        this.onremove = (e) => { if (this.cache.has(e.data.id)) this.cache.delete(e.data.id) };
    }

    get cache() {
        if (this._cache === undefined) this._cache = new Map();
        return this._cache;
    }

    get config() {
        if (this._config === undefined) this._config = BaseModel.getConfig('goods');
        return this._config;
    }

    get(id) {
        id = parseInt(id);
        if (this.cache.has(id)) return Promise.resolve(this.cache.get(id));
        return super.get(id);
    }

    getByCatalog(catalogId) {
        let options = Object.assign({}, this.config, { method: BaseModel.methods.GET });
        options.uri = `${options.uri}cid/${catalogId}`;
        return this
            .xhr(options)
            .then((data) => {
                for(let i = 0; i < data.length; i++) {
                    this.cache.set(data[i].id, data[i]);
                }
                this.emit('getByCatalog', {target: this, data: data});
                return data;
            });
    }

    set ongetbycatalog(fn) { this.on('getByCatalog', fn) }
}