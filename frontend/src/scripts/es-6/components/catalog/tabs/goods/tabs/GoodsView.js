import config     from '../../../../../config.json'
import BaseWidget from '../../../../../lib/BaseWidget'

export default class GoodsView extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.tableBody = this.dom.select('table tbody')
        this.editBtn   = this.dom.select('.close-box .edit-btn')

        this.editBtn.onclick  = this.onEdit.bind(this)
    }

    static init(containerId) {
        let containerNode = BaseWidget.byId(containerId)
        return new GoodsView(containerNode)
    }

    populate(data) {
        let formatter  = new Intl.DateTimeFormat('uk-UA')
        let langConfig = config.i18n.uk

        let translData = {}
        translData[langConfig.title] = data.title
        translData[langConfig.code] = data.code
        translData[langConfig.slug] = data.slug
        translData[langConfig.description] = data.description
        translData[langConfig.short_description] = data.short_description
        translData[langConfig.producer] = data.producer
        translData[langConfig.price] = data.price
        translData[langConfig.quantity] = data.quantity
        translData[langConfig.created] = formatter.format(new Date(parseInt(data.created)))
        translData[langConfig.updated] = formatter.format(new Date(parseInt(data.updated)))
        translData[langConfig.is_top] = data.is_top == 0 ? langConfig.no : langConfig.yes
        translData[langConfig.is_index_carousel] = data.is_index_carousel == 0 ? langConfig.no : langConfig.yes

        let html = []
        for(let key in translData) if (translData.hasOwnProperty(key)) {
            html.push(`<tr><td>${key}</td><td>${translData[key]}</td></tr>`)
        }
        this.tableBody.innerHTML = html.join('')
        return this
    }

    onEdit(e) {
        e.preventDefault()
        this.parent.get('goodsGrid').onEditBtnClick()
    }
}