import BaseDelete from '../../../../../../lib/BaseDelete';

export default class DeleteView extends BaseDelete {

    constructor(container) {
        super(container);
        this.cancelBtn = this.container.querySelector('a[href^="#cancel"]');

        this.cancelBtn.onclick = this.onCancelBtnClick.bind(this);
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId);
        return new DeleteView(containerNode);
    }

    onCancelBtnClick(e) {
        e.preventDefault();
        this.hide();
        this.reset();
        this.emit('cancel', {target: this});
    }

    onClick(e) {
        super.onClick(e);
        this.hide();
    }

}