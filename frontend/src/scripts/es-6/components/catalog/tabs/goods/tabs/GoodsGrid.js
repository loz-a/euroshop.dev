import BaseWidget from './../../../../../lib/BaseWidget'
import {ACTION_EDIT, ACTION_DELETE} from './addGoodsTabs/DataForm'
import DeleteView from './GoodsGrid/DeleteView'
import Overlay    from './../../../../Overlay'
import config     from './../../../../../config.json'

const ACTION_VIEW = 'view'

export default class GoodsGrid extends BaseWidget {

    constructor(container) {
        super()
        this.container = container
        this.grid      = this.dom.select('.table tbody')
        this.selected  = null

        this.widgets.set(DeleteView.init('goods-delete'))

        this.oncancel     = this.onCancelDelete.bind(this)
        this.grid.onclick = this.proxy.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new GoodsGrid(containerNode)
    }

    get data()  { return this.parent.parent.data }
    get model() { return this.parent.parent.model }

    proxy(e) {
        e.preventDefault()
        let target = e.target

        let selected = target.closest('tr')
        if (selected) this.selected = selected

        if (target.parentNode.hasAttribute('href')) target = target.parentNode
        if (!target.hasAttribute('href')) return

        let parsedUri = target.getAttribute('href').slice(1).split('/')
        let id        = parsedUri.pop()
        let action    = parsedUri.pop()
        let overlay   = Overlay.instance

        let switchAction = (action) => {
            switch (action) {
                case ACTION_EDIT  : this.onEditBtnClick(e); break
                case ACTION_DELETE: this.onDeleteBtnClick(e); break
                case ACTION_VIEW  : this.onViewClick(e); break
            }
        }

        let isAlreadyCached = this.data.has('goods') && this.data.get('goods').id == id

        if (isAlreadyCached) {
            switchAction(action)
        } else {
            overlay.setText('load data').enable()
            this.model
                .get(id)
                .then((data) => {
                    this.data.set('goods', data)
                    overlay.disable()
                    switchAction(action)
                })
        }
    }

    onEditBtnClick(e) {
        let parentTabs = this.parent
        let overlay    = Overlay.instance
        let form       = parentTabs.get('addGoodsTabs > dataForm')

        form.reset().show().focus('title')

        overlay.setText('load data').enable()

        parentTabs.show(parentTabs.editTab)

        this.model
            .get(this.data.get('goods').id)
            .then((data) => {
                form.data = data
                overlay.disable()
            })
    }

    onDeleteBtnClick(e) {
        let deleteView = this.get('deleteView')
        this.hideGrid()
        deleteView.reset().show()
    }

    onCancelDelete(e) {
        if (!e.target.equals(this, 'deleteView')) return
        this.showGrid()
    }

    onViewClick(e) {
        let parent = this.parent
        parent.show(parent.viewTab)
    }

    populate(data) {
        this.get('deleteView').hide()

        this.grid.innerHTML = ''

        for (let i = 0; i < data.length; i++) {
            this.addRow(data[i])
        }
        return this
    }

    addRow(data) {
        let text = (text) => document.createElement('td').appendChild(document.createTextNode(text)).parentNode
        let row = this.grid.insertRow()

        row.dataset.id = data.id
        row.appendChild(text(row.rowIndex))

        let title = document.createElement('td')
        row.appendChild(title)
        title.innerHTML = `<a href="#view/${data.id}">${data.title}</a>`

        row.appendChild(text(data.price))
        row.appendChild(text(data.quantity))


        let actions = document.createElement('td')
        row.appendChild(actions)

        actions.innerHTML =
            `<a href="#edit/${data.id}" class="btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i> ${config.i18n.uk['edit']}</a>
            <a href="#delete/${data.id}" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> ${config.i18n.uk['delete']}</a>`
    }

    deleteRow() {
        this.grid.parentNode.deleteRow(this.selected.rowIndex)
        this.selected = null
        this.rebuildGridIndexes()
        this.showGrid()
    }

    rebuildGridIndexes() {
        let rows = this.grid.rows
        for (let i = 0; i<rows.length; i++) {
            rows[i].cells[0].textContent = rows[i].rowIndex
        }
    }

    hideGrid() {
        this.grid.parentNode.classList.add('hidden')
        return this
    }

    showGrid() {
        this.grid.parentNode.classList.remove('hidden')
        return this
    }

    set oncancel(fn) { this.on('cancel', fn) }
}