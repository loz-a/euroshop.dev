import config from '../../../config.json'

export default class TableView {

    constructor(container) {
        this.container = container
    }

    static init(tabsContainer) {
        let containerNode = tabsContainer.querySelector('#catalog-view table tbody')
        return new TableView(containerNode)
    }

    get img() {
        if (this._img === undefined) this._img = document.getElementById('catalog-view').querySelector('.img-container img')
        return this._img
    }

    populate(data) {
        let formatter  = new Intl.DateTimeFormat('uk-UA')
        let langConfig = config.i18n.uk

        let translData = {}
        translData[langConfig.title] = data.title
        translData[langConfig.slug] = data.slug
        translData[langConfig.created] = formatter.format(new Date(parseInt(data.created)))
        translData[langConfig.updated] = formatter.format(new Date(parseInt(data.updated)))

        let html = []
        for(let key in translData) if (translData.hasOwnProperty(key)) {
            html.push(`<tr><td>${key}</td><td>${translData[key]}</td></tr>`)
        }
        this.container.innerHTML = html.join('')
        return this
    }

    viewImg(src, alt = '', title = '') {
        if (!src) this.resetImg()
        else {
            this.img.src   = src
            this.img.alt   = alt
            this.img.title = title
            this.img.classList.remove('hidden')
        }
        return this
    }

    resetImg() {
        this.img.src = this.img.alt = this.img.title = ''
        this.img.classList.add('hidden')
        return this
    }

}