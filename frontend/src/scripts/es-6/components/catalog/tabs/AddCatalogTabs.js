import BaseTabs from '../../../lib/BaseTabs'
import Form from '../tabs/addCatalogTabs/Form'
import UploadForm from '../tabs/addCatalogTabs/UploadForm'

export default class AddCatalogTabs extends BaseTabs {

    constructor(container) {
        super(container)

        this.widgets
            .set(Form.init('catalog-form-form'))
            .set('uploadForm', UploadForm.init('catalog-upload-form'))

        this.onshown = this.proxy.bind(this)
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new AddCatalogTabs(containerNode)
    }

    get dataTab() {
        if (this._dataTab === undefined) this._dataTab = this.container.querySelector('a[data-action="catalog_data"]')
        return this._dataTab
    }

    get uploadTab() {
        if (this._galleryTab === undefined) this._galleryTab = this.container.querySelector('a[data-action="catalog_gallery"]')
        return this._galleryTab
    }

    proxy(e) {
        let target = e.target
        if (!this.equals(target)) return

        switch (target.activeTab) {
            case this.dataTab   : this.onShowDataTab(e); break
            case this.uploadTab : this.onShowUploadTab(e); break
        }
    }

    onShowDataTab(e) {

    }

    onShowUploadTab(e) {
        let data = this.parent.parent.data
        this.get('uploadForm').data = data.get('catalog')
    }

    onWidgetShow(e) {
        if (this.equals(e.target)) this.adjustTabs(true)
    }

}