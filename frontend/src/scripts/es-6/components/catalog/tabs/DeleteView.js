import BaseDelete from '../../../lib/BaseDelete';

export default class DeleteView extends BaseDelete {

    constructor(container) {
        super(container);
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId);
        return new DeleteView(containerNode);
    }

}