import BaseWidget from './BaseCatalogWidget'
import Tree from './Tree'
import Tabs from './catalog/Tabs'
import Model from './catalog/model/Model'
import UploadModel from './catalog/model/UploadModel'
import Overlay from './Overlay'

export default class Catalog extends BaseWidget {

    constructor(container) {
        super()
        this.container   = container
        this.model       = new Model()
        this.uploadModel = new UploadModel()

        this.widgets
            .set(Tree.init('tree', this.model))
            .set(Tabs.init(this.container))

        this.onsubmit = this.submit.bind(this)
        this.onselect = this.select.bind(this)
        this.ondelete = this.remove.bind(this)

        let tree = this.get('tree')
        tree.makeActive(tree.root)

        this.container.querySelector('.close-container a').onclick = () => this.get('tabs').hide()
    }

    static init(containerId) {
        let containerNode = document.getElementById(containerId)
        return new Catalog(containerNode)
    }

    select(e) {
        let tabs  = this.get('tabs')
        let id = e.selectedNode.dataset.catId

        if (id === '0') {
            this.data.set('catalog', {id})
            tabs.showOnlyAddTab()
            return
        }

        let tree    = this.get('tree')
        let overlay = Overlay.instance

        tabs.enable()
        overlay.setText('load data').enable()

        this.model.get(id)
            .then((data) => {
                this.data.set('catalog', data)
                if (data.children_count) return this.model.getChildren(data.id)
            })
            .then(() => {
                let catalogData = this.data.get('catalog')

                if(catalogData.img) {
                    tabs.tableView.viewImg(catalogData.img.path)
                    tabs.show()
                } else {
                    this.uploadModel
                        .getByCatalog(this.data.get('catalog').id)
                        .then((data) => {
                            this.data.get('catalog').img = data
                            tabs.show()
                        })
                        .catch((error) => console.log(error))
                }
            })
            .catch((error) => console.log(error))
    }

    submit(e) {
        if (e.target.equals(this, 'tabs > tabs > form')) this.submitData(e)
        if (e.target.equals(this, 'tabs > tabs > uploadForm')) this.submitUploadData(e)
    }

    submitUploadData(e) {
        let form    = e.target
        let data    = e.data
        let overlay = Overlay.instance
        let {id}    = this.data.get('catalog')

        data.data.catalog_id = id
        data.formData.append('catalog_id', id)

        form.img.onload = () => overlay.disable()

        overlay.enable()

        this.uploadModel
            .set(data)
            .then((data) => {
                delete data.catalog_id
                this.data.get('catalog').img = data
                form.viewImg(data.path)
                form.reset()
            })
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    form.setErrors(errors)
                    overlay.disable()
                }
            })
    }

    submitData(e) {
        let form    = e.target
        let data    = e.data
        let overlay = Overlay.instance

        overlay.enable()

        this.model
            .set(data)
            .then((data) => {
                if (this.data.has('catalog')) {
                    let catalogData = this.data.get('catalog')
                    let isResaveImg = catalogData.id === data.id && catalogData.img && !data.img
                    if (isResaveImg) data.img = catalogData.img
                }

                this.data.set('catalog', data)
                this.get('tabs').show()
            })
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    form.setErrors(errors)
                    overlay.disable()
                }
            })
    }

    remove(e) {
        let target = e.target
        if (target.equals(this, 'tabs > deleteView')) this.deleteData(e)
        if (target.equals(this, 'tabs > tabs > uploadForm')) this.deleteUploadData(e)
    }

    deleteData(e) {
        let deleteView = e.target
        let overlay    = Overlay.instance
        let data       = this.data.get('catalog')

        overlay.enable()

        this.model
            .remove(data.id)
            .then(() => {
                let imgId = (data.img && data.img.id) || null
                if (imgId) return this.uploadModel.remove(imgId)
            })
            .then(() => overlay.disable())
            .catch((errors) => {
                if (errors instanceof Error) overlay.setText(errors.message)
                else {
                    deleteView
                        .disableOkBtn()
                        .showAlertMessage(errors[Object.keys(errors)[0]])

                    overlay.disable()
                }
            })
    }

    deleteUploadData(e) {
        let overlay    = Overlay.instance
        let data       = this.data.get('catalog')
        let uploadForm = e.target

        let imgId = (data.img && data.img.id) || null;

        if (imgId) {
            overlay.enable()

            this.uploadModel
                .remove(imgId)
                .then(() => {
                    uploadForm.resetImg()
                    overlay.disable()
                })
                .catch((errors) => {
                    if (errors instanceof Error) overlay.setText(errors.message)
                    else overlay.disable()
                })
        }
    }

    set onsubmit(fn) { this.on('submit', fn)}
    set onshown(fn)  { this.on('shown', fn) }
    set onselect(fn) { this.on('select', fn) }
    set ondelete(fn) { this.on('delete', fn) }
}
