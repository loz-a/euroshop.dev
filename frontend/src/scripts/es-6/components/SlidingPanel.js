export default class SlidingPanel {

    constructor(container) {
        this.container = container
        this.container.onclick = this.onClick.bind(this)
    }

    static init(containerId) {
        const containerNode = document.getElementById(containerId)
        return new SlidingPanel(containerNode)
    }

    hide() {
        this.container.parentNode.classList.remove('open')
    }

    show() {
        this.container.parentNode.classList.add('open')
    }

    isOpen() {
        return this.container.parentNode.classList.contains('open')
    }

    onClick(e) {
        e.preventDefault()

        let target = e.target
        if (target.tagName.toLowerCase() == 'i') {
            target = target.parentNode
        }

        if (this.isOpen()) this.hide()
        else this.show()
    }
}