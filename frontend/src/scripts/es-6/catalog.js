import Catalog      from './components/Catalog'

document.addEventListener('DOMContentLoaded', () => {
    Catalog.init('catalog')
});