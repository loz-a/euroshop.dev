import SlidingPanel from './components/SlidingPanel'
import Navigation   from './components/slidingPanel/Navigation'

document.addEventListener('DOMContentLoaded', () => {
    SlidingPanel.init('sliding-panel-controls')
    Navigation.init('admin-navigation')
});