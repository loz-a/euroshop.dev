(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SlidingPanel = (function () {
    function SlidingPanel(container) {
        _classCallCheck(this, SlidingPanel);

        this.container = container;
        this.container.onclick = this.onClick.bind(this);
    }

    _createClass(SlidingPanel, [{
        key: 'hide',
        value: function hide() {
            this.container.parentNode.classList.remove('open');
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.parentNode.classList.add('open');
        }
    }, {
        key: 'isOpen',
        value: function isOpen() {
            return this.container.parentNode.classList.contains('open');
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            e.preventDefault();

            var target = e.target;
            if (target.tagName.toLowerCase() == 'i') {
                target = target.parentNode;
            }

            if (this.isOpen()) this.hide();else this.show();
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new SlidingPanel(containerNode);
        }
    }]);

    return SlidingPanel;
})();

exports.default = SlidingPanel;

},{}],2:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Navigation = (function () {
    function Navigation(container) {
        _classCallCheck(this, Navigation);

        this.container = container;
        this.container.onclick = this.onClick.bind(this);
    }

    _createClass(Navigation, [{
        key: 'show',
        value: function show(node) {
            node.style.display = '';
            return this;
        }
    }, {
        key: 'hide',
        value: function hide(node) {
            node.style.display = 'none';
            return this;
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            var target = e.target;

            if (target.tagName.toLowerCase() !== 'a') return;

            if (target.getAttribute('href') === '#') {
                e.preventDefault();
                target.parentNode.classList.toggle('expanded');
            }
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new Navigation(containerNode);
        }
    }]);

    return Navigation;
})();

exports.default = Navigation;

},{}],3:[function(require,module,exports){
'use strict';

var _SlidingPanel = require('./components/SlidingPanel');

var _SlidingPanel2 = _interopRequireDefault(_SlidingPanel);

var _Navigation = require('./components/slidingPanel/Navigation');

var _Navigation2 = _interopRequireDefault(_Navigation);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.addEventListener('DOMContentLoaded', function () {
    _SlidingPanel2.default.init('sliding-panel-controls');
    _Navigation2.default.init('admin-navigation');
});

},{"./components/SlidingPanel":1,"./components/slidingPanel/Navigation":2}]},{},[3])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXFNsaWRpbmdQYW5lbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcY29tcG9uZW50c1xcc2xpZGluZ1BhbmVsXFxOYXZpZ2F0aW9uLmpzIiwic3JjXFxzY3JpcHRzXFxlcy02XFxtYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7OztJQ0FxQixZQUFZO0FBRTdCLGFBRmlCLFlBQVksQ0FFakIsU0FBUyxFQUFFOzhCQUZOLFlBQVk7O0FBR3pCLFlBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFBO0FBQzFCLFlBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0tBQ25EOztpQkFMZ0IsWUFBWTs7K0JBWXRCO0FBQ0gsZ0JBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUE7U0FDckQ7OzsrQkFFTTtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQ2xEOzs7aUNBRVE7QUFDTCxtQkFBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFBO1NBQzlEOzs7Z0NBRU8sQ0FBQyxFQUFFO0FBQ1AsYUFBQyxDQUFDLGNBQWMsRUFBRSxDQUFBOztBQUVsQixnQkFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTtBQUNyQixnQkFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLEdBQUcsRUFBRTtBQUNyQyxzQkFBTSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUE7YUFDN0I7O0FBRUQsZ0JBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQSxLQUN6QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUE7U0FDbkI7Ozs2QkEzQlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFNLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQzFELG1CQUFPLElBQUksWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQ3pDOzs7V0FWZ0IsWUFBWTs7O2tCQUFaLFlBQVk7Ozs7Ozs7Ozs7Ozs7SUNBWixVQUFVO0FBRTNCLGFBRmlCLFVBQVUsQ0FFZixTQUFTLEVBQUU7OEJBRk4sVUFBVTs7QUFHdkIsWUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUE7QUFDMUIsWUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7S0FDbkQ7O2lCQUxnQixVQUFVOzs2QkFZdEIsSUFBSSxFQUFFO0FBQ1AsZ0JBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQTtBQUN2QixtQkFBTyxJQUFJLENBQUE7U0FDZDs7OzZCQUVJLElBQUksRUFBRTtBQUNQLGdCQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUE7QUFDM0IsbUJBQU8sSUFBSSxDQUFBO1NBQ2Q7OztnQ0FFTyxDQUFDLEVBQUU7QUFDUCxnQkFBTSxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQTs7QUFFdkIsZ0JBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSyxHQUFHLEVBQUUsT0FBTTs7QUFFaEQsZ0JBQUksTUFBTSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLEVBQUU7QUFDckMsaUJBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQTtBQUNsQixzQkFBTSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFBO2FBQ2pEO1NBQ0o7Ozs2QkF4QlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFNLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFBO0FBQzFELG1CQUFPLElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1NBQ3ZDOzs7V0FWZ0IsVUFBVTs7O2tCQUFWLFVBQVU7Ozs7Ozs7Ozs7Ozs7OztBQ0cvQixRQUFRLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUUsWUFBTTtBQUNoRCwyQkFBYSxJQUFJLENBQUMsd0JBQXdCLENBQUMsQ0FBQTtBQUMzQyx5QkFBVyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtDQUN0QyxDQUFDLENBQUMiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiZXhwb3J0IGRlZmF1bHQgY2xhc3MgU2xpZGluZ1BhbmVsIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lclxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLm9uY2xpY2sgPSB0aGlzLm9uQ2xpY2suYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyTm9kZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbnRhaW5lcklkKVxyXG4gICAgICAgIHJldHVybiBuZXcgU2xpZGluZ1BhbmVsKGNvbnRhaW5lck5vZGUpXHJcbiAgICB9XHJcblxyXG4gICAgaGlkZSgpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5wYXJlbnROb2RlLmNsYXNzTGlzdC5yZW1vdmUoJ29wZW4nKVxyXG4gICAgfVxyXG5cclxuICAgIHNob3coKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIucGFyZW50Tm9kZS5jbGFzc0xpc3QuYWRkKCdvcGVuJylcclxuICAgIH1cclxuXHJcbiAgICBpc09wZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29udGFpbmVyLnBhcmVudE5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJylcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrKGUpIHtcclxuICAgICAgICBlLnByZXZlbnREZWZhdWx0KClcclxuXHJcbiAgICAgICAgbGV0IHRhcmdldCA9IGUudGFyZ2V0XHJcbiAgICAgICAgaWYgKHRhcmdldC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT0gJ2knKSB7XHJcbiAgICAgICAgICAgIHRhcmdldCA9IHRhcmdldC5wYXJlbnROb2RlXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5pc09wZW4oKSkgdGhpcy5oaWRlKClcclxuICAgICAgICBlbHNlIHRoaXMuc2hvdygpXHJcbiAgICB9XHJcbn0iLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBOYXZpZ2F0aW9uIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb250YWluZXIpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbnRhaW5lclxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLm9uY2xpY2sgPSB0aGlzLm9uQ2xpY2suYmluZCh0aGlzKVxyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBpbml0KGNvbnRhaW5lcklkKSB7XHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyTm9kZSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGNvbnRhaW5lcklkKVxyXG4gICAgICAgIHJldHVybiBuZXcgTmF2aWdhdGlvbihjb250YWluZXJOb2RlKVxyXG4gICAgfVxyXG5cclxuICAgIHNob3cobm9kZSkge1xyXG4gICAgICAgIG5vZGUuc3R5bGUuZGlzcGxheSA9ICcnXHJcbiAgICAgICAgcmV0dXJuIHRoaXNcclxuICAgIH1cclxuXHJcbiAgICBoaWRlKG5vZGUpIHtcclxuICAgICAgICBub2RlLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSdcclxuICAgICAgICByZXR1cm4gdGhpc1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2xpY2soZSkge1xyXG4gICAgICAgIGNvbnN0IHRhcmdldCA9IGUudGFyZ2V0XHJcblxyXG4gICAgICAgIGlmICh0YXJnZXQudGFnTmFtZS50b0xvd2VyQ2FzZSgpICE9PSAnYScpIHJldHVyblxyXG5cclxuICAgICAgICBpZiAodGFyZ2V0LmdldEF0dHJpYnV0ZSgnaHJlZicpID09PSAnIycpIHtcclxuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgICAgIHRhcmdldC5wYXJlbnROb2RlLmNsYXNzTGlzdC50b2dnbGUoJ2V4cGFuZGVkJylcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59IiwiaW1wb3J0IFNsaWRpbmdQYW5lbCBmcm9tICcuL2NvbXBvbmVudHMvU2xpZGluZ1BhbmVsJ1xyXG5pbXBvcnQgTmF2aWdhdGlvbiAgIGZyb20gJy4vY29tcG9uZW50cy9zbGlkaW5nUGFuZWwvTmF2aWdhdGlvbidcclxuXHJcbmRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ0RPTUNvbnRlbnRMb2FkZWQnLCAoKSA9PiB7XHJcbiAgICBTbGlkaW5nUGFuZWwuaW5pdCgnc2xpZGluZy1wYW5lbC1jb250cm9scycpXHJcbiAgICBOYXZpZ2F0aW9uLmluaXQoJ2FkbWluLW5hdmlnYXRpb24nKVxyXG59KTsiXX0=
