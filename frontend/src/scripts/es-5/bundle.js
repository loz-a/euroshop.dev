(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

Object.defineProperty(exports, "__esModule", {
    value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var SlidingPanel = (function () {
    function SlidingPanel(container) {
        _classCallCheck(this, SlidingPanel);

        this.container = container;
        this.container.onclick = this.onClick.bind(this);
    }

    _createClass(SlidingPanel, [{
        key: 'hide',
        value: function hide() {
            this.container.parentNode.classList.remove('open');
        }
    }, {
        key: 'show',
        value: function show() {
            this.container.parentNode.classList.add('open');
        }
    }, {
        key: 'isOpen',
        value: function isOpen() {
            return this.container.parentNode.classList.contains('open');
        }
    }, {
        key: 'onClick',
        value: function onClick(e) {
            e.preventDefault();

            var target = e.target;
            if (target.tagName.toLowerCase() == 'i') {
                target = target.parentNode;
            }

            if (this.isOpen()) this.hide();else this.show();
        }
    }], [{
        key: 'init',
        value: function init(containerId) {
            var containerNode = document.getElementById(containerId);
            return new SlidingPanel(containerNode);
        }
    }]);

    return SlidingPanel;
})();

exports.default = SlidingPanel;

},{}],2:[function(require,module,exports){
'use strict';

var _SlidingPanel = require('./components/SlidingPanel');

var _SlidingPanel2 = _interopRequireDefault(_SlidingPanel);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

document.addEventListener('DOMContentLoaded', function () {
    _SlidingPanel2.default.init('sliding-panel-controls');
});

},{"./components/SlidingPanel":1}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmNcXHNjcmlwdHNcXGVzLTZcXGNvbXBvbmVudHNcXFNsaWRpbmdQYW5lbC5qcyIsInNyY1xcc2NyaXB0c1xcZXMtNlxcbWFpbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7SUNBcUIsWUFBWTtBQUU3QixhQUZpQixZQUFZLENBRWpCLFNBQVMsRUFBRTs4QkFGTixZQUFZOztBQUd6QixZQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztBQUMzQixZQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNwRDs7aUJBTGdCLFlBQVk7OytCQVl0QjtBQUNILGdCQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3REOzs7K0JBRU07QUFDSCxnQkFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNuRDs7O2lDQUVRO0FBQ0wsbUJBQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMvRDs7O2dDQUVPLENBQUMsRUFBRTtBQUNQLGFBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs7QUFFbkIsZ0JBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDdEIsZ0JBQUksTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxHQUFHLEVBQUU7QUFDckMsc0JBQU0sR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO2FBQzlCOztBQUVELGdCQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBTSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDcEQ7Ozs2QkExQlcsV0FBVyxFQUFFO0FBQ3JCLGdCQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO0FBQ3pELG1CQUFPLElBQUksWUFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1NBQzFDOzs7V0FWZ0IsWUFBWTs7O2tCQUFaLFlBQVk7Ozs7Ozs7Ozs7O0FDRWpDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBRSxZQUFNO0FBQ2hELDJCQUFhLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFBO0NBQzlDLENBQUMsQ0FBQyIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJleHBvcnQgZGVmYXVsdCBjbGFzcyBTbGlkaW5nUGFuZWwge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbnRhaW5lcikge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gY29udGFpbmVyO1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLm9uY2xpY2sgPSB0aGlzLm9uQ2xpY2suYmluZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgaW5pdChjb250YWluZXJJZCkge1xyXG4gICAgICAgIGxldCBjb250YWluZXJOb2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoY29udGFpbmVySWQpO1xyXG4gICAgICAgIHJldHVybiBuZXcgU2xpZGluZ1BhbmVsKGNvbnRhaW5lck5vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGhpZGUoKSB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIucGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKCdvcGVuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvdygpIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lci5wYXJlbnROb2RlLmNsYXNzTGlzdC5hZGQoJ29wZW4nKTtcclxuICAgIH1cclxuXHJcbiAgICBpc09wZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29udGFpbmVyLnBhcmVudE5vZGUuY2xhc3NMaXN0LmNvbnRhaW5zKCdvcGVuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGljayhlKSB7XHJcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICBsZXQgdGFyZ2V0ID0gZS50YXJnZXQ7XHJcbiAgICAgICAgaWYgKHRhcmdldC50YWdOYW1lLnRvTG93ZXJDYXNlKCkgPT0gJ2knKSB7XHJcbiAgICAgICAgICAgIHRhcmdldCA9IHRhcmdldC5wYXJlbnROb2RlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNPcGVuKCkpIHRoaXMuaGlkZSgpOyBlbHNlIHRoaXMuc2hvdygpO1xyXG4gICAgfVxyXG59IiwiaW1wb3J0IFNsaWRpbmdQYW5lbCBmcm9tICcuL2NvbXBvbmVudHMvU2xpZGluZ1BhbmVsJ1xyXG5cclxuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsICgpID0+IHtcclxuICAgIFNsaWRpbmdQYW5lbC5pbml0KCdzbGlkaW5nLXBhbmVsLWNvbnRyb2xzJylcclxufSk7Il19
