"use strict";

const path         = require('path');
const gulp         = require('gulp');
const Autoprefixer = require('less-plugin-autoprefix');
const plumber      = require('gulp-plumber');
const less         = require('gulp-less');
const concat       = require('gulp-concat');
const merge        = require('merge2');
const sourcemaps   = require("gulp-sourcemaps");
const babelify     = require("babelify");
const browserify   = require('browserify');
const source       = require('vinyl-source-stream');
const notify       = require('gulp-notify');
const cleanCss     = require('gulp-clean-css');
const rename       = require('gulp-rename');
const uglify       = require('gulp-uglify');

const config = {
    root: {
        src: path.join(__dirname, './src'),
        dest: path.join(__dirname, './../public')
    },
    less: {
        src:  '/less',
        dest: '/less',
        glob: {
            main: '/*.less',
            all: '/**/*.less'
        },
        includes: '/includes'
    },
    less_frontend: {
        src:  '/less_frontend',
        dest: '/less_frontend',
        glob: {
            main: '/*.less',
            all: '/**/*.less'
        },
        includes: '/includes'
    },
    css_frontend: {
        dest_filename: 'main-v2'
    },
    css: {
        src: '/css',
        dest: '/css',
        dest_filename: 'styles.css',
        glob: {
            all: '/**/*.css',
            min: '/**/*.min.css'
        },
        vendor: {
            bootstrap: '/vendor/bootstrap/css/bootstrap.css'
        }
    },
    js: {
        src:  '/scripts',
        dest: '/scripts',
        main_filename: 'main.js',
        catalog_filename: 'catalog.js',
        //browserify_filename: 'main.js',
        es_standarts_foldername: {
            es5: 'es-5',
            es6: 'es-6'
        },
        glob: {
            all: '/**/*.js',
            min: '/**/*.min.js'
        }
    },
    js_frontend: {
        src:  '/scripts_frontend',
        dest: '/scripts',
        filename: 'scripts.js',
        filename_dist: 'scripts.dist.js',
        filename_dist_min: 'scripts.dist.min.js',
        glob: {
            all: '/**/*.js'
        }
    }
};

/*
 * css backend
 */

gulp.task('less-compile', () => {
    let autoprefix = new Autoprefixer({ browsers: ["last 3 versions"]});

    return merge(
        //gulp.src(config.css.vendor.bootstrap),
        gulp.src(path.join(config.root.src, config.less.src, config.less.glob.main))
            .pipe(plumber())
            .pipe(less({
                paths: [path.join(config.root.src, config.less.src, config.less.includes)],
                plugins: [autoprefix]
            }))
            .pipe(gulp.dest(path.join(config.root.src, config.css.src)))
            .on('error', console.error.bind(console))
        )
        .pipe(plumber())
        .pipe(concat(config.css.dest_filename))
        .pipe(gulp.dest(path.join(config.root.src, config.css.src)));
});

gulp.task('build-public-css', ['less-compile'], () => {
    return gulp.src([
            path.join(config.root.src, config.css.src, config.css.dest_filename),
            path.join(config.root.src, config.css.src, config.css.vendor.bootstrap)
        ])
        .pipe(plumber())
        .pipe(gulp.dest(path.join(config.root.dest, config.css.dest)))
        .pipe(notify('build css task was success'));
});

/*
* css frontend
*/

gulp.task('less-frontend-compile', () => {
    let autoprefix = new Autoprefixer({ browsers: ["last 3 versions"]});

    return merge(
        gulp.src(path.join(config.root.src, config.less_frontend.src, config.less_frontend.glob.main))
            .pipe(plumber())
            .pipe(less({
                paths: [path.join(config.root.src, config.less_frontend.src, config.less_frontend.includes)],
                plugins: [autoprefix]
            }))
            .pipe(gulp.dest(path.join(config.root.src, config.css.src)))
            .on('error', console.error.bind(console))
    )
        .pipe(plumber())
        .pipe(concat(`${config.css_frontend.dest_filename}.css`))
        .pipe(gulp.dest(path.join(config.root.src, config.css.src)));
});

gulp.task('minify-frontend-css', ['less-frontend-compile'], () => {
    return gulp.src(path.join(config.root.src, config.css.src, `/${config.css_frontend.dest_filename}.css`))
        .pipe(plumber())
        .pipe(cleanCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.join(config.root.src, config.css.src)));
});

gulp.task('build-public-frontend-css', ['minify-frontend-css'], () => {
    return gulp.src(path.join(config.root.src, config.css.src, `/${config.css_frontend.dest_filename}.min.css`))
        .pipe(plumber())
        .pipe(gulp.dest(path.join(config.root.dest, config.css.dest)))
        .pipe(notify('build fontend css task was success'));
});

/*
 * css backend
 */

gulp.task('convert-es6-es5-main', () => {
    let jsc  = config.js;
    let src  = path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es6, jsc.main_filename);
    let dest = path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es5);

    return browserify({ entries: src, extensions: ['.js'], debug: true })
        .transform(babelify, { presets: ['es2015'] })
        .bundle()
        .pipe(source(jsc.main_filename))
        .pipe(gulp.dest(dest))
        .on('error', console.error.bind(console));
});

gulp.task('convert-es6-es5-catalog', () => {
    let jsc  = config.js;
    let src  = path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es6, jsc.catalog_filename);
    let dest = path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es5);

    return browserify({ entries: src, extensions: ['.js'], debug: true })
        .transform(babelify, { presets: ['es2015'] })
        .bundle()
        .pipe(source(jsc.catalog_filename))
        .pipe(gulp.dest(dest))
        .on('error', console.error.bind(console));
});

gulp.task('build-public-js', ['convert-es6-es5-main', 'convert-es6-es5-catalog'], () => {
    let jsc = config.js;
    //let src = path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es5, jsc.main_filename);
    let dest = path.join(config.root.dest, config.js.dest);

    return gulp.src([
            path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es5, jsc.main_filename),
            path.join(config.root.src, jsc.src, jsc.es_standarts_foldername.es5, jsc.catalog_filename)
        ])
        .pipe(plumber())
        .pipe(gulp.dest(dest))
        .pipe(notify('build js task was success'));
});


/*
 * js frontend
 */

gulp.task('js-frontend-concat', () => {
    let jsConfig = config.js_frontend;
    return gulp.src([
            path.join(config.root.src, jsConfig.src, '/components/*.js'),
            path.join(config.root.src, jsConfig.src, jsConfig.filename)
        ])
        .pipe(plumber())
        .pipe(concat(jsConfig.filename_dist))
        .pipe(gulp.dest(path.join(config.root.src, jsConfig.src)));
});

gulp.task('js-frontend-compress', ['js-frontend-concat'], () => {
    let jsConfig = config.js_frontend;
    return gulp.src([
            path.join(config.root.src, jsConfig.src, jsConfig.filename_dist)
        ])
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.join(config.root.src, jsConfig.src)));
});

gulp.task('js-frontend-vendor-build', () => {
    let jsConfig = config.js_frontend;
    return gulp.src([
            path.join(config.root.src, jsConfig.src, '/vendor/jquery.event.move.js'),
            path.join(config.root.src, jsConfig.src, '/vendor/jquery.event.swipe.js')
        ])
        .pipe(plumber())
        .pipe(concat('swipe.dist.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(path.join(config.root.dest, jsConfig.dest)))
        .pipe(notify('build fontend vendor js task was success'));
});

gulp.task('build-public-frontend-js', ['js-frontend-compress'], () => {
    let jsConfig = config.js_frontend;
    return gulp.src(path.join(config.root.src, jsConfig.src, jsConfig.filename_dist_min))
        .pipe(plumber())
        .pipe(gulp.dest(path.join(config.root.dest, jsConfig.dest)))
        .pipe(notify('build fontend js task was success'));
});

/*
 * watchers and defaults
 */
gulp.task('watch', () => {
    let watchercss = gulp.watch(
        path.join(config.root.src, config.less.src, config.less.glob.all),
        ['build-public-css']
    );
    watchercss.on('change', (event) => console.log('File ' + event.path + ' was ' + event.type + ', running css tasks...'));

    let watcherjson = gulp.watch(
        path.join(config.root.src, config.js.src, config.js.es_standarts_foldername.es6, '/**/*.json'),
        ['build-public-js']
    );
    watcherjson.on('change', (event) => console.log('File ' + event.path + ' was ' + event.type + ', running json tasks...'));

    let watcherjs = gulp.watch(
        path.join(config.root.src, config.js.src, config.js.es_standarts_foldername.es6, config.js.glob.all),
        ['build-public-js']
    );
    watcherjs.on('change', (event) => console.log('File ' + event.path + ' was ' + event.type + ', running js tasks...'));
});

gulp.task('watch-frontend-css', () => {
    const cssWatcher = gulp.watch(
        path.join(config.root.src, config.less_frontend.src, config.less_frontend.glob.all),
        ['build-public-frontend-css']
    );
    cssWatcher.on('change', (event) => console.log('File ' + event.path + ' was ' + event.type + ', running frontend css tasks...'));
});

gulp.task('watch-frontend-js', () => {
    let jsConfig = config.js_frontend;
    const jsWatcher = gulp.watch([
            path.join(config.root.src, jsConfig.src, '/components/*.js'),
            path.join(config.root.src, jsConfig.src, jsConfig.filename)
        ],
        //path.join(config.root.src, config.js_frontend.src, config.js_frontend.glob.all),
        ['build-public-frontend-js']
    );
    jsWatcher.on('change', (event) => console.log('File ' + event.path + ' was ' + event.type + ', running frontend js tasks...'));
});

gulp.task('default', ['watch', 'build-public-css', 'build-public-js']);
gulp.task('default-frontend', [
    'build-public-frontend-css', 'build-public-frontend-js', 'watch-frontend-css', 'watch-frontend-js'
]);