<?php
namespace Core\App\Controller;

use Core\App;
use Core\App\Controller\Event\Before as BeforeEvent;
use Core\App\Controller\Event\After as AfterEvent;

abstract class AbstractController implements ControllerInterface
{
    public function callAction(App $application)
    {
        $result     = null;
        $actionName = null;
        $routeMatch = $application -> getServiceManager() -> get('route_match');

        $actionName = sprintf('%sAction', $routeMatch -> getParam('action', 'index'));
        $application -> trigger(new BeforeEvent($application, $this, $actionName));

        $result = call_user_func_array(array($this, $actionName), $application -> getParams() -> get());

         $application -> trigger(new AfterEvent($application, $this, $actionName, $result));

        return $result;
    }


} 