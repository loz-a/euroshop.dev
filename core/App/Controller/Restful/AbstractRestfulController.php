<?php
namespace Core\App\Controller\Restful;

use Core\App;
use Core\App\Controller\ControllerInterface;
use Core\App\Controller\Event\Before as BeforeEvent;
use Core\App\Controller\Event\After as AfterEvent;
use Core\View\Model\AbstractModel;
use Core\View\Model\JsonErrorModel;
use Core\View\Model\JsonModel;
use Zend\Http\Response;
use Zend\Mvc\Router\RouteMatch;
use ArrayAccess;

abstract class AbstractRestfulController implements ControllerInterface
{
    protected $allowMethods = array('post', 'put', 'delete', 'get');

    /**
     * Create a new resource
     *
     * @param  mixed $data
     * @return mixed
     */
    abstract public function create($data);


    protected function createComplete($application, $result)
    {
        $sm             = $application -> getServiceManager();
        $uri            = $sm -> get('request') -> getUri();
        $response       = $sm -> get('response');
        $identifierName = 'id';

        $this -> addAccessControlHeaders($response, $uri);

        if (is_scalar($result)) {
            $result = compact('result');
        }

        if (is_array($result)) {

            if (!isset($result['data']) and !isset($result['identifier']) and !isset($result['messages'])) {
                $result['data'] = $result;
            }
            else if (isset($result['identifier'])) {
                $identifierName = $result['identifier'];
            }

            if (isset($result['messages'])) { /* if result is not valid */
                $model = new JsonErrorModel();
                $model -> setMessages($result['messages']);
            }
            else { /* if result is valid */
                $model = new JsonModel();
            }

            $model -> fromArray($result['data']);
            $result = $model;
        }

        if ($result instanceof JsonErrorModel) {
            $response -> setStatusCode(Response::STATUS_CODE_400);
            return $result;
        }

        if (!isset($result[$identifierName])) {
            throw new Exception\UndefinedResourceIdException(sprintf('Resource id is undefined. Resource identifier name - "%s"', $identifierName));
        }

        $matchedRouteName = $sm -> get('route_match') -> getMatchedRouteName();

        $location = $application -> getRoutesManager()
            -> urlFor(sprintf('%s/id', $matchedRouteName), array('id' => $result[$identifierName]));

        $response
            -> setStatusCode(Response::STATUS_CODE_201)
            -> getHeaders()
            -> addHeaderLine('Location', $location);

        return $result;
    } // createComplete()


    /**
     * Delete an existing resource
     *
     * @param  mixed $id
     * @return mixed
     */
    abstract public function deleteById($id);


    /**
     * Delete the entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @return mixed
     * @throws Exception\RuntimeException
     */
    public function deleteList()
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    }


    /**
     * Return single resource
     *
     * @param  mixed $id
     * @return mixed
     */
    abstract public function getById($id);


    /**
     * Return list of resources
     *
     * @return mixed
     */
    public function getList()
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    } // getList()


    protected function actionComplete($application, $result)
    {
        $sm       = $application -> getServiceManager();
        $response = $sm -> get('response');
        $uri      = $sm -> get('request') -> getUri();

        $this -> addAccessControlHeaders($response, $uri);

        if ($result instanceof JsonErrorModel) {
            $response -> setStatusCode(Response::STATUS_CODE_400);
            return;
        }

        $response -> setStatusCode(Response::STATUS_CODE_200);
    } // actionComplete()


    public function getPartialList($start, $end)
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    } // getPartialList()


    protected function getPartialListComplete($application, $result, $start, $end, $total)
    {
        $sm       = $application -> getServiceManager();
        $response = $sm -> get('response');
        $uri      = $sm -> get('request') -> getUri();

        $this -> addAccessControlHeaders($response, $uri);

        if ($result instanceof JsonErrorModel) {
            $response -> setStatusCode(Response::STATUS_CODE_400);
            return;
        }

        $response
            -> setStatusCode(Response::STATUS_CODE_206)
            -> getHeaders()
            -> addHeaderLine('Content-Range', sprintf('items %s-%s/%s', $start, $end, $total));
    } // getPartialListComplete()


    /**
     * Retrieve HEAD metadata for the resource
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  null|mixed $id
     * @return mixed
     * @throws Exception\RuntimeException
     */
    public function head($id = null)
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    }


    /**
     * Respond to the OPTIONS method
     *
     * Typically, set the Allow header with allowed HTTP methods, and
     * return the response.
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @return mixed
     * @throws Exception\RuntimeException
     */
    public function options()
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    }


    /**
     * Respond to the PATCH method
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  $id
     * @param  $data
     * @return mixed
     * @throws Exception\RuntimeException
     */
    public function patch($id, $data)
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    }


    /**
     * Replace an entire resource collection
     *
     * Not marked as abstract, as that would introduce a BC break
     * (introduced in 2.1.0); instead, raises an exception if not implemented.
     *
     * @param  mixed $data
     * @return mixed
     * @throws Exception\RuntimeException
     */
    public function replaceList($data)
    {
        throw new Exception\RuntimeException(sprintf(
            '%s is unimplemented', __METHOD__
        ));
    }


    /**
     * Update an existing resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return mixed
     */
    abstract public function update($id, $data);


    public function callAction(App $application)
    {
        $result        = null;
        $methodOptions = $this -> getCallMethodByRequestMethod($application);

        $application -> trigger(new BeforeEvent($application, $this, $methodOptions['method']));

        switch ($methodOptions['method']) {
            case 'create':
                $result = $this -> create($application -> getParams() -> toArray());
                $result = $this -> createComplete($application, $result);
                break;

            case 'update':
                $result = $this -> update($methodOptions['id'], $application -> getParams() -> toArray());
                $this -> actionComplete($application, $result);
                break;

            case 'replaceList':
                $result = $this -> replaceList($application -> getParams() -> toArray());
                $this -> actionComplete($application, $result);
                break;

            case 'deleteById':
                $result = $this -> deleteById($methodOptions['id']);
                $this -> actionComplete($application, $result);
                break;

            case 'deleteList':
                $result = $this -> deleteList();
                $this -> actionComplete($application, $result);
                break;

            case 'getById':
                $result = $this -> getById($methodOptions['id']);
                $this -> actionComplete($application, $result);
                break;

            case 'getList':
                $result = $this -> getList();
                $this -> actionComplete($application, $result);
                break;

            case 'getPartialList':
                $start = $methodOptions['patrial_list_range']['start'];
                $end   = $methodOptions['patrial_list_range']['end'];
                $result = $this -> getPartialList($start, $end);

                $total  = (is_array($result) and isset($result['total'])) ? $result['total'] : $end;
                $end    = min($total, $end);

                $this -> getPartialListComplete($application, $result, $start, $end, $total);
                break;

            case 'head':
                $this -> head($methodOptions['id']);
                $result = $application -> getServiceManager() -> get('response');
                $result -> setContent('');
                return $result;

            case 'options':
                $this -> options();
                $result = $application -> getServiceManager() -> get('response');
                return $result;

            case 'patch':
                if (false === $methodOptions['id']) {
                    $result = $application -> getServiceManager() -> get('response');
                    $result -> setStatusCode(405);
                    return $result;
                }
                $result = $this -> patch($methodOptions['id'], $application -> getParams() -> toArray());
                $this -> actionComplete($application, $result);
                break;

            default:
                $result = $application -> getServiceManager() -> get('response');
                $result -> setStatusCode(405);
                return $result;
        }

        if (is_array($result)) {
            $result = $this -> get('view_model.json') -> fromArray($result);
        }

        if (!$result instanceof AbstractModel) {
            throw new Exception\InvalidResultException('Result must implement %s', AbstractModel::class);
        }

        $application -> trigger(new AfterEvent($application, $this, $methodOptions['method'], $result));

        return $result;
    } // callAction()


    /**
     * @param RouteMatch $routeMatch
     * @param \Zend\Http\PhpEnvironment\Request $request
     * @return bool
     */
    protected function getIdentifier($routeMatch, $request)
    {
        $id = $routeMatch -> getParam('id', false);
        if ($id) {
            return $id;
        }

        $id = $request -> getQuery() -> get('id', false);
        if ($id) {
            return $id;
        }

        return false;
    } // getIdentifier()


    /**
     * @param $application
     * @return array
     */
    protected function getCallMethodByRequestMethod($application)
    {
        $result  = array();
        $sm      = $application -> getServiceManager();
        $request = $sm -> get('request');
        $method  = strtolower($request -> getMethod());

        switch ($method) {
            case 'delete':
                $id = $this -> getIdentifier($sm -> get('route_match'), $request);

                if (false !== $id) {
                    $result['method'] = 'deleteById';
                    $result['id']     = $id;
                    break;
                }
                $result['method'] = 'deleteList';
                break;

            case 'get':
                $id = $this -> getIdentifier($sm -> get('route_match'), $request);

                if (false !== $id) {
                    $result['method'] = 'getById';
                    $result['id']     = $id;
                    break;
                }

                $partialListRange = $this -> getPatrialListRange($sm -> get('request'));

                if (false !== $partialListRange) {
                    $result['method'] = 'getPartialList';
                    $result['patrial_list_range'] = $partialListRange;
                    break;
                }

                $result['method'] = 'getList';
                break;

            case 'post':
                $result['method'] = 'create';
                break;

            case 'put':
                $id   = $this -> getIdentifier($sm -> get('route_match'), $request);

                $result['id']     = (false !== $id) ? $id : null;
                $result['method'] = (false !== $id) ? 'update' : 'replaceList';
                break;

            case 'head':
                $id = $this -> getIdentifier($sm -> get('route_match'), $request);

                $result['id']     = (false !== $id) ? $id : null;
                $result['method'] = 'head';
                $this -> head($id);
                break;

            case 'options':
                $result['method'] = 'options';
                break;

            case 'patch':
                $id = $this -> getIdentifier($sm -> get('route_match'), $request);

                $result['id']     = (false !== $id) ? $id : null;
                $result['method'] = 'patch';
                break;
            }

        return $result;
    } // getCallMethodByRequestMethod()


    /**
     * @param $response
     * @param $uri
     * @return $this
     */
    protected function addAccessControlHeaders($response, $uri)
    {
        $allowMethods = implode(' ', array_map('strtoupper', $this -> allowMethods));
        $allowOrigin  = sprintf('%s://%s', $uri -> getScheme(), $uri -> getHost());

        $response
            -> getHeaders()
            -> addHeaderLine('Access-Control-Allow-Methods', $allowMethods)
            -> addHeaderLine('Access-Control-Allow-Origin', $allowOrigin);

        return $this;
    } // addAccessControlHeaders()


    /**
     * @param \Zend\Http\PhpEnvironment\Request $request
     * @return array|false
     */
    protected function getPatrialListRange($request)
    {
        $start = 0;
        $end   = 0;
        $limit = '';

        $queryParams = $request -> getQuery();
        foreach($queryParams as $param => $value) {
            if (strpos($param, 'limit') === 0) {
                $limit = $param;
                break;
            }
        }

        if ($limit) {
            preg_match('/(\d+),*(\d+)*/', $limit, $matches);
            if(count($matches) > 2){
                $start = $matches[2];
                $end = $matches[1] + $start;
            }
            else {
                $end = $matches[1];
            }
        }
        else {
            $range = $request -> getServer('HTTP_RANGE', '');

            if (!$range) {
                $range = $request -> getServer('HTTP_X_RANGE', '');
            }

            if($range) {
                preg_match('/(\d+)-(\d+)/', $range, $matches);
                $start = $matches[1];
                $end = $matches[2] + 1;
            }
        }

        if (0 === $start and 0 === $end and '' === $limit) {
            return false;
        }

        return array(
            'start' => (int) $start,
            'end'   => (int) $end
        );
    } // getPatrialListRange()

} 