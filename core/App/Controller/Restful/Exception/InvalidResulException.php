<?php
namespace Core\App\Controller\Restful\Exception;

class InvalidResultException extends \Exception
    implements ExceptionInterface
{
}