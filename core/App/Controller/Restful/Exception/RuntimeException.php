<?php
namespace Core\App\Controller\Restful\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
} 