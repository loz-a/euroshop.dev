<?php
namespace Core\App\Exception;

use Core\Exception\ExceptionInterface;

class ModulesConfigurationUndefinedException extends \Exception implements ExceptionInterface
{

} 