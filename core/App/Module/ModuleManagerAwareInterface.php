<?php
namespace Core\App\Module;

interface ModuleManagerAwareInterface extends ModuleManagerCapableInterface
{
    public function setModuleManager(Manager $moduleManager);
} 