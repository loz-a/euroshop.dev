<?php
namespace Core\App\Module;

interface ModuleManagerCapableInterface
{
    public function getModuleManager();
} 