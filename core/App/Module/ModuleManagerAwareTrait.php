<?php
namespace Core\App\Module;

trait ModuleManagerAwareTrait
{
    /**
     * @var Manager
     */
    protected $moduleManager;

    /**
     * @param \Core\App\Module\Manager $moduleManager
     * @return $this
     */
    public function setModuleManager(Manager $moduleManager)
    {
        $this->moduleManager = $moduleManager;
        return $this;
    }

    /**
     * @return \Core\App\Module\Manager
     */
    public function getModuleManager()
    {
        return $this->moduleManager;
    }


} 