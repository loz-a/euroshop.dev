<?php
namespace Core\App\View;

use Interop\Container\ContainerInterface;
use Core\View\View;
use Core\View\ViewEvent;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\Factory\FactoryInterface;

class Factory implements  FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        include_once getcwd() . '/core/View/functions.php';

        $config = $container -> get('config') -> view;

        $container -> get('view.path_stack_resolver') -> addTemplatePaths($config -> template_path_stack -> toArray());
        $container -> get('view.map_resolver')        -> addTemplatePaths($config -> template_map -> toArray());

        $event = new ViewEvent();
        $event
            -> setRequest($container -> get('request'))
            -> setResponse($container -> get('response'));

        $view  = new View();
        $view -> setEvent($event);

        $view
            -> getEventManager()
            -> attach(ViewEvent::EVENT_MODEL, function($e) use ($container) {
                $target = $e -> getTarget();

                $containerId = sprintf('view.%s_renderer', $target -> getModel() -> modelType());
                $target -> setRenderer($container -> get($containerId));
            });

        $container
            -> setFactory('view.widgets', function() use ($config, $view) {
                $config   = isset($config -> widgets_service_manager) ? $config -> widgets_service_manager : array();
                return (new WidgetManager($config -> toArray()))
                    -> setView($view);
            });

        return $view;
    } // createService()
} 