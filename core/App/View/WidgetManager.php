<?php
namespace Core\App\View;

use Core\View\View;
use Core\View\Widget\AbstractWidget;
use Core\View\Widget\Exception\InvalidWidgetException;
use Zend\ServiceManager\ServiceManager;

class WidgetManager extends ServiceManager
{
    /**
     * @var View
     */
    protected $view;
    /**
     * Constructor
     *
     * Add a default initializer to ensure the plugin is valid after instance
     * creation.
     *
     * @param  null|array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this -> addInitializer(array($this, 'injectView'));
    }


    public function setView(View $view)
    {
        $this -> view = $view;
        return $this;
    } // setView()


    public function getView()
    {
        return $this -> view;
    } // getView()


    public function injectView($widget)
    {
        $view = $this -> getView();
        if (null === $view) {
            return;
        }
        $widget -> setView($view);
    } // injectView()


    public function get($name, $options = array(), $usePeeringServiceManagers = true)
    { 
        $this -> creationOptions = $options;
        $instance = parent::get($name, $usePeeringServiceManagers);
        $this -> creationOptions = null;
        $this -> validateWidget($instance);
        return $instance;
    }


    public function setService($name, $service, $shared = true)
    {
        if ($service) {
            $this -> validateWidget($service);
        }
        parent::setService($name, $service, $shared);
        return $this;
    } // setService()


    public function validateWidget($widget)
    {
        if ($widget instanceof AbstractWidget) {
            return;
        }
        throw new InvalidWidgetException(sprintf(
            'Widget of type %s is invalid; must implement Core\View\Widget\WidgetInterface',
            (is_object($widget) ? get_class($widget) : gettype($widget)),
            __NAMESPACE__
        ));
    }

} 