<?php
namespace Core\App\Db;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use PDO;

class ConnectionFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dbConfig = $container -> get('config') -> db;
        
        $dsn      = $dbConfig -> dsn;
        $user     = $dbConfig -> user;
        $password = $dbConfig -> password;
        
        $dbh = new PDO($dsn, $user, $password);
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if ($dbh -> getAttribute(PDO::ATTR_DRIVER_NAME) == 'mysql') {
            $dbh -> setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
        }

        return $dbh;
    }

} 