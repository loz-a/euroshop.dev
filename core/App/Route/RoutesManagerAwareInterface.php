<?php
namespace Core\App\Route;

interface RoutesManagerAwareInterface extends RoutesManagerCapableInterface
{
    public function setRoutesManager(Manager $routesManager);
} 