<?php
namespace Core\Stdlib\Filter;

use Zend\Filter\AbstractFilter;

class Translit extends AbstractFilter
{
    const STR_LOWERCASE = 'strtolower';
    const STR_UPPERCASE = 'strtoupper';
    const STR_UCWORDS   = 'ucfirst';

    protected $spaceReplacer = ' ';
    protected $convertTo;
    protected $onlyLetters = false;
    protected $replacement;


    public function __construct()
    {
        $this -> replacement = [
            'а' => 'a',
            'б' => 'b',
            'в' => 'v',
            'г'	=> 'h',
            'Ґ' => 'g',
            'д'	=> 'd',
            'е'	=> 'e',
            'є'	=> 'ye',
            'ж'	=> 'zh',
            'з' => 'z',
            'и'	=> 'y',
            'і'	=> 'i',
            'ї' => 'yi',
            'й' => 'y',
            'к' => 'k',
            'л' => 'l',
            'м' => 'm',
            'н' => 'n',
            'о'	=> 'o',
            'п'	=> 'p',
            'р'	=> 'r',
            'с'	=> 's',
            'т'	=> 't',
            'у'	=> 'u',
            'ф'	=> 'f',
            'х' => 'kh',
            'ц' => 'ts',
            'ч' => 'ch',
            'ш' => 'sh',
            'щ' => 'shch',
            'ю' => 'yu',
            'я' => 'ya',
            'ы' => 'i',
            'э' => 'e',
            'ё' => 'e',
            "№" => "N",
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г'	=> 'H',
            'Ґ' => 'G',
            'Д'	=> 'D',
            'Е'	=> 'E',
            'Є'	=> 'Ye',
            'Ж'	=> 'Zh',
            'З' => 'Z',
            'И'	=> 'Y',
            'І'	=> 'I',
            'Ї' => 'Yi',
            'Й' => 'Y',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О'	=> 'O',
            'П'	=> 'P',
            'Р'	=> 'R',
            'С'	=> 'S',
            'Т'	=> 'T',
            'У'	=> 'U',
            'Ф'	=> 'F',
            'Х' => 'Kh',
            'Ц' => 'Ts',
            'Ч' => 'Ch',
            'Ш' => 'Sh',
            'Щ' => 'Shch',
            'Ю' => 'Yu',
            'Я' => 'Ya',
            'Ы' => 'I',
            'Э' => 'E',
            'Ё' => 'E',
            'ь' => "'",
            'Ь' => "'",
            'ъ' => "'",
            'Ъ' => "'"
        ];
    } // __construct()

    /**
     * @param str $replacer
     * @return $this
     */
    public function setSpaceReplacer($replacer)
    {
        $this -> spaceReplacer = (string) $replacer;
        return $this;
    } // setSpaceReplacer()


    /**
     * @return $this
     */
    public function setLowerCase()
    {
        $this -> convertTo = self::STR_LOWERCASE;
        return $this;
    } // setLowerCase()


    /**
     * @return $this
     */
    public function setUpperCase()
    {
        $this -> convertTo = self::STR_UPPERCASE;
        return $this;
    } // setUpperCase()


    /**
     * @return $this
     */
    public function setUCFirst()
    {
        $this -> convertTo = self::STR_UCWORDS;
        return $this;
    } // setUCFirst()


    /**
     * @param $flag
     * @return $this
     */
    public function filterOnlyLetters($flag = true)
    {
        $this -> onlyLetters = (bool) $flag;
        return $this;
    } // filterOnlyLetters()


    public function filter($value)
    {
        $value = trim($value);
        $this -> replacement[' '] = $this -> spaceReplacer;

        $value = strtr($value, $this -> replacement);

        if ($this -> onlyLetters) {
            $value = preg_replace('/[^A-Za-z0-9\-\_\ ]/', '', $value);
        }


        if (null !== $this -> convertTo) {
            $value = call_user_func($this -> convertTo, $value);
        }

        return $value;
    } // filter

} // Translit
