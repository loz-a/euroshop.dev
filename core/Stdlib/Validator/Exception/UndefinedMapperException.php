<?php
namespace Core\Stdlib\Validator\Exception;

class UndefinedMapperException extends \Exception
    implements ExceptionInterface
{

} 