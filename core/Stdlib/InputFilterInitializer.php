<?php
namespace Core\Stdlib;

use Interop\Container\ContainerInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\ServiceManager\Initializer\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\InitializableInterface;

class InputFilterInitializer implements InitializerInterface
{

    public function __invoke(ContainerInterface $container, $instance)
    {
        if ($instance instanceof InputFilterInterface) {

            if (method_exists($instance, 'setServiceLocator')) {
                $instance -> setServiceLocator($container);
            }

            if ($instance instanceof InitializableInterface) {
                $instance -> init();
            }
        }
        return $instance;
    } // __invoke()
    
    public function initialize($instance, ServiceLocatorInterface $services)
    {
        return $this($services, $instance);
    } // initialize()

} 