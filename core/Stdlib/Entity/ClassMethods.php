<?php
namespace Core\Stdlib\Entity;


class ClassMethods
{
    public function __call($name, $args)
    {
        if (strpos($name, 'get') === 0) {
            $property = strtolower(substr($name, 3));
            if (!property_exists($this, $property)) {
                throw new Exception\InvalidPropertyException(sprintf('Invalid property %s', $property));
            }
            return $this -> $property;
        }

        if (strpos($name, 'set') === 0) {
            $property = strtolower(substr($name, 3));
            if (!property_exists($this, $property)) {
                throw new Exception\InvalidPropertyException(sprintf('Invalid property %s', $property));
            }
            $this -> $property = $args[0];
            return $this;
        }
        throw new Exception\BadMethodCallException(sprintf(
            'Method "%s" does not exist in %s',
            $name,
            get_class($this)
        ));
    }
}