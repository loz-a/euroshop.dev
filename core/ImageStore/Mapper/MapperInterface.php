<?php
namespace Core\ImageStore\Mapper;


interface MapperInterface
{
    /**
     * @param int $id
     * @return \Core\ImageStore\Entity\ImageInterface
     */
    public function get($id);

    /**
     * @param \Core\ImageStore\Entity\ImageInterface $image
     * @return \Core\ImageStore\Entity\ImageInterface
     */
    public function set($image);

    /**
     * @param int $id
     * @return \Core\ImageStore\Entity\ImageInterface
     */
    public function delete($id);

//    public function has($path);
}