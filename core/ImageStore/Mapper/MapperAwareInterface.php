<?php
namespace Core\ImageStore\Mapper;

interface MapperAwareInterface
{
    public function setMapper(MapperInterface $mapper);

    public function getMapper();
}