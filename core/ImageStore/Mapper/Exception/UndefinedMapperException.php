<?php
namespace Core\ImageStore\Mapper\Exception;

class UndefinedMapperException extends \Exception
    implements ExceptionInterface
{

}