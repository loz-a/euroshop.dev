<?php
namespace Core\ImageStore\Mapper;

trait MapperAwareTrait
{
    /**
     * @var MapperInterface
     */
    protected $mapper;

    public function setMapper(MapperInterface $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedMapperException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()
}