<?php
namespace Core\ImageStore\Filesystem\Adapter\Exception;

class ClassNotFoundException extends \Exception
    implements ExceptionInterface
{

}