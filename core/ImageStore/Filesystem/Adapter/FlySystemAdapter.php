<?php
namespace Core\ImageStore\Filesystem\Adapter;


use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;

class FlySystemAdapter implements AdapterInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;


    /**
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this -> setAdapterFromOptions($options);
    }

    /**
     * @param string $path
     * @return bool|false|string
     */
    public function read($path)
    {
        try {
            return $this -> filesystem -> read($path);
        } catch (FileNotFoundException $exception) {
            return false;
        }
    }


    /**
     * @param string $path
     * @return bool
     */
    public function has($path)
    {
        return $this -> filesystem -> has($path);
    }


    /**
     * @param string $path
     * @param string $contents
     * @return bool
     */
    public function put($path, $contents)
    {
        return $this -> filesystem -> put($path, $contents);
    }


    /**
     * @param string $path
     * @return bool
     */
    public function delete($path)
    {
        try {
            return $this -> filesystem -> delete($path);
        } catch (FileNotFoundException $exception) {
            return false;
        }
    }


    /**
     * @param string $old
     * @param string $new
     * @return bool
     */
    public function rename($old, $new)
    {
        return $this -> filesystem -> rename($old, $new);
    } // rename()


    /**
     * @param array $options
     * @return $this
     * @throws Exception\ClassNotFoundException
     */
    public function setAdapterFromOptions(array $options)
    {
        $adapterClassName = $options['adapter'];
        $options   = $options['options'];

        if (!class_exists($adapterClassName)) {
            throw new Exception\ClassNotFoundException(sprintf('Class "%s"', $adapterClassName));
        }

        $reflection = new \ReflectionClass($adapterClassName);
        $adapter    = $reflection -> newInstanceArgs($options);

        $this -> filesystem = new Filesystem($adapter);

        return $this;
    } // setAdapterFromOptions()


    /**
     * @return \League\Flysystem\AdapterInterface
     */
    public function getFilesystemAdapter()
    {
        return $this -> filesystem -> getAdapter();
    } // getFilesystemAdapter()
}