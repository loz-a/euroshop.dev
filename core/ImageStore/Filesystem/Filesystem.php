<?php
namespace Core\ImageStore\Filesystem;

use Core\ImageStore\Filesystem\Adapter\AdapterInterface;

class Filesystem implements AdapterAwareInterface
{
    /**
     * @var AdapterInterface
     */
    protected $adapter;

    /**
     * @param AdapterInterface $adapter
     */
    public function __construct(AdapterInterface $adapter)
    {
        $this -> adapter = $adapter;
    }


    /**
     * Check whether a file exists.
     *
     * @param string $path
     *
     * @return bool
     */
    public function has($path)
    {
        return $this -> adapter -> has($path);
    }


    /**
     * Read a file.
     *
     * @param string $path The path to the file.
     *
     * @return string|false The file contents or false on failure.
     */
    public function read($path)
    {
        return $this -> adapter -> read($path);
    }


    /**
     * Create a file or update if exists.
     *
     * @param string $path     The path to the file.
     * @param string $contents The file contents.
     *
     * @return bool True on success, false on failure.
     */
    public function put($path, $contents)
    {
        return $this -> adapter -> put($path, $contents);
    }


    /**
     * delete a file in exists
     *
     * @param string $path
     * @return bool
     */
    public function delete($path)
    {
        return $this -> adapter -> delete($path);
    } // delete()


    /**
     * rename file
     *
     * @param string $old
     * @param string $new
     * @return bool
     */
    public function rename($old, $new)
    {
        return $this -> adapter -> rename($old, $new);
    } // rename()


    /**
     * @return AdapterInterface
     */
    public function getAdapter()
    {
        return $this -> adapter;
    }


    /**
     * @param AdapterInterface $adapter
     * @return $this
     */
    public function setAdapter(AdapterInterface $adapter)
    {
        $this -> adapter = $adapter;
        return $this;
    }


}