<?php
namespace Core\ImageStore\Filesystem;

interface AdapterAwareInterface
{
    public function getAdapter();

    public function setAdapter(Adapter\AdapterInterface $adapter);
}