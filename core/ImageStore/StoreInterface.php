<?php
namespace Core\ImageStore;

use Core\ImageStore\Entity\ImageInterface;

interface StoreInterface
{
    public function put(ImageInterface $image);

    public function delete(ImageInterface $image);
}