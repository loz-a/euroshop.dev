<?php
namespace Core\ImageStore\Entity;

class Image implements ImageInterface
{
    protected $id;

    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    protected $alias;

    public function setAlias($alias)
    {
        $this -> alias = $alias;
        return $this;
    }

    public function getAlias()
    {
        return $this -> alias;
    }


    protected $path;

    public function setPath($path)
    {
        $this -> path = $path;
        return $this;
    }

    public function getPath()
    {
        return $this -> path;
    }


    protected $pid = null;

    public function setParentId($pid)
    {
        $this -> pid = $pid;
        return $this;
    }

    public function getParentId()
    {
        return $this -> pid;
    }


    protected $uploadData;

    public function setUploadData($data)
    {
        $this -> uploadData = $data;
        return $this;
    }

    public function getUploadData()
    {
        if ($this -> uploadData and (!is_array($this -> uploadData))) {
            $this -> uploadData = unserialize($this -> uploadData);
        }
        return  $this -> uploadData;
    }


    protected $thumbs = [];

    public function setThumbs(array $thumbs)
    {
        $this -> thumbs = $thumbs;
        return $this;
    }

    public function getThumbs()
    {
        return $this -> thumbs;
    }
}