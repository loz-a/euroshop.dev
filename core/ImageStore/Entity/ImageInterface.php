<?php
namespace Core\ImageStore\Entity;

interface ImageInterface
{
    public function setId($id);

    public function getId();

    public function setAlias($alias);

    public function getAlias();

    public function setPath($path);

    public function getPath();

    public function setParentId($pid);

    public function getParentId();

    public function setUploadData($data);

    public function getUploadData();

    public function setThumbs(array $thumbs);

    public function getThumbs();
}