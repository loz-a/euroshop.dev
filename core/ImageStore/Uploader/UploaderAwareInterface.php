<?php
namespace Core\ImageStore\Uploader;


interface UploaderAwareInterface
{
    /**
     * @param Uploader $uploader
     * @return mixed
     */
    public function setUploader(Uploader $uploader);

    /**
     * @return Uploader
     */
    public function getUploader();
}