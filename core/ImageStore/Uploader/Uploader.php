<?php
namespace Core\ImageStore\Uploader;

use Core\ImageStore\Filesystem\Filesystem;
use Intervention\Image\ImageManager;

class Uploader
{
    protected $options;

    protected $uploadPath;

    protected $transformers = [];
    
    protected $thumbs = [];

    /**
     * @var ImageManager
     */
    protected $imageManager;

    /**
     * @var Filesystem
     */
    protected $filesystem;



    public function __construct(
        Filesystem $filesystem,
        ImageManager $imageManager,
        array $options = []
    ) {
        $this -> filesystem = $filesystem;
        $this -> imageManager = $imageManager;

        if ($options) {
            $this -> setOptions($options);
        }
    } // __construct()


    public function setOptions(array $options)
    {
        if (isset($options['upload_path'])) {
            $this -> setUploadPath($options['upload_path']);
        }

        if (isset($options['transform'])) {
            $this -> setTransformers($options['transform']);
        }

        if (isset($options['thumbs'])) {
            $this -> setThumbs($options['thumbs']);
        }

        return $this;
    } // setOptions()


    public function setUploadPath($path)
    {
        $this -> uploadPath = str_replace(DIRECTORY_SEPARATOR, '/', $path);
        return $this;
    } // setUploadPath()


    public function getUploadPath()
    {
        if (null === $this -> uploadPath) {
            throw new Exception\UndefinedUploadPathException('Upload path is undefined');
        }
        return $this -> uploadPath;
    } // getUploadPath()


    public function setTransformers(array $transformers)
    {
        $this -> transformers = $transformers;
        return $this;
    } // setTransformers()


    public function setThumbs(array $thumbs)
    {
        foreach ($thumbs as $thumb => $options) {
            $uploader = new self($this -> filesystem, $this -> imageManager);

            $uploader -> setUploadPath(sprintf('%s%s', $this -> getUploadPath(), $options['upload_path']));

            if (isset($options['transform'])) {
                $uploader -> setTransformers($options['transform']);
            }

            if (isset($options['thumbs'])) {
                $uploader -> setThumbs($options['thumbs']);
            }

            $this -> thumbs[$thumb] = $uploader;
        }
        return $this;
    } // setThumbs()


    public function getThumbs()
    {
        return $this -> thumbs;
    } // getThumbs()


    public function getFilesystem()
    {
        return $this -> filesystem;
    } // getFilesystem()


    public function upload($file, Callable $callback = null)
    {
        if (!is_array($file)) {
            throw new Exception\InvalidArgumentException('Invalid argument. Array expected');
        }

        $content = file_get_contents($file['tmp_name']);
        $content = $this -> transform($content);

        $path   = sprintf('%s/%s', $this -> getUploadPath(), $file['name']);
        $result = $this -> getFilesystem() -> put($path, $content);

        if (!$result) {
            return false;
        }

        if ($callback) {
            $path = call_user_func_array($callback, [$path, $this, $file]);
        }

        return [
            'path'  => $path,
            'thumbs' => $this -> handleThumbs($path)
        ];
    } // upload()


    public function delete($path)
    {
        return $this -> getFilesystem() -> delete($path);
    } // delete()


    protected function transform($content)
    {
        $image = $this -> imageManager -> make($content);

        foreach($this -> transformers as $method => $params) {
            call_user_func_array([$image, $method], $params);
        }

        return $image -> encode($image -> mime()) -> getEncoded();
    } // transform()


    protected function handleThumbs($path)
    {
        $thumbs = [];

        $handle = function($uploader) use ($path) {
            $content = $uploader -> getFilesystem() -> read($path);
            $content = $uploader -> transform($content);

            $pi = pathinfo($path);
            $path = sprintf('%s/%s.%s', $uploader -> getUploadPath(), $pi['filename'], $pi['extension']);
            $result = $this -> getFilesystem() -> put($path, $content);
            return $result ? $path : false;
        };

        foreach($this -> thumbs as $key => $uploader) {
            $result = $handle($uploader);
            if ($result) {
                $thumbs[$key] = $result;
            }
        }

        return $thumbs;
    } // handleThumbs()
}