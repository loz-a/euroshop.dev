<?php
namespace Core\ImageStore\Uploader;


trait UploaderAwareTrait
{
    /**
     * @var Uploader
     */
    protected $uploader;

    public function setUploader(Uploader $uploader)
    {
        $this -> uploader = $uploader;
        return $this;
    } // setUploader()


    public function getUploader()
    {
        if (null === $this -> uploader) {
            throw new Exception\UndefinedUploaderException('Uploader is undefined');
        }
        return $this -> uploader;
    } // getUploader()
}