<?php
namespace Core\ImageStore\Uploader\Exception;

class UndefinedUploadPathException extends \Exception
    implements ExceptionInterface
{

}