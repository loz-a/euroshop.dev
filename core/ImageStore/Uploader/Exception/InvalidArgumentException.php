<?php
namespace Core\ImageStore\Uploader\Exception;


class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

}