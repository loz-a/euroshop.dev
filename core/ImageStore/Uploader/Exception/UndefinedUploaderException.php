<?php
namespace Core\ImageStore\Uploader\Exception;

class UndefinedUploaderException extends \Exception
    implements ExceptionInterface
{

}