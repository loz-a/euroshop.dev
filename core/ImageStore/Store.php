<?php
namespace Core\ImageStore;

use Core\ImageStore\Mapper\MapperAwareInterface;
use Core\ImageStore\Mapper\MapperAwareTrait;
use Core\ImageStore\Uploader\UploaderAwareInterface;
use Core\ImageStore\Uploader\UploaderAwareTrait;
use Core\ImageStore\Entity\ImageInterface;

class Store implements StoreInterface, UploaderAwareInterface, MapperAwareInterface
{
    use UploaderAwareTrait, MapperAwareTrait;

    /**
     * @param ImageInterface $entity
     * @param callable|null $callback
     * @return bool|ImageInterface
     * @throws Mapper\Exception\UndefinedMapperException
     * @throws Uploader\Exception\UndefinedUploaderException
     */
    public function put(ImageInterface $entity, Callable $callback = null)
    {
        $file     = $entity   -> getUploadData();
        $uploader = $this     -> getUploader();
        $result   = $uploader -> upload($file, $callback);

        if (!$result) {
            return false;
        }

        $entity -> setPath($result['path']);

        if (isset($result['thumbs'])) {
            $entity -> setThumbs($result['thumbs']);
        }

        $entity = $this -> getMapper() -> set($entity);

        return $entity;
    }


    /**
     * @param ImageInterface $entity
     * @return ImageInterface
     * @throws Mapper\Exception\UndefinedMapperException
     * @throws Uploader\Exception\UndefinedUploaderException
     */
    public function delete(ImageInterface $entity)
    {
        $uploader = $this -> getUploader();
        $uploader -> delete($entity -> getPath());

        $thumbs = $entity -> getThumbs();
        if ($thumbs) {
            foreach($thumbs as $path) {
                $uploader -> delete($path);
            }
        }

        $this -> getMapper() -> delete($entity -> getId());
        return $entity;
    }
}
