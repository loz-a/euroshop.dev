<?php
namespace Core\View;
use Core\View\Model\ModelTypeInterface;

/**
 * View interface
 */
interface ViewInterface
{
    /**
     * Render a template, optionally passing a view model/variables
     *
     * @param  mixed $model
     * @return string
     */
    public function render(ModelTypeInterface $model = null);
}
