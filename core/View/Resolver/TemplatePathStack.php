<?php
namespace Core\View\Resolver;

use Core\View\Exception;
use IteratorAggregate;
use SplStack;
use SplFileInfo;

/**
 * Default resolver implementation
 *
 * By default, assumes ".phtml" extension, and that a normal directory
 * separator ('/') is used. However, both are configurable.
 *
 * @subpackage Resolver
 */
class TemplatePathStack implements ResolverInterface, IteratorAggregate
{
    /**
     * Directory separator token in template names.
     * @var string
     */
    protected $separator = '/';

    /**
     * File suffix to use with templates.
     * @var string
     */
    protected $suffix = '.phtml';

    /**
     * Path on which to look for templates.
     * @var SplStack
     */
    protected $pathsStack;

    /**
     * Set directory separator character
     *
     * @param  string $separator
     * @return TemplatePathStack
     */
    public function setSeparator($separator)
    {
        $this -> separator = (string) $separator;
        return $this;
    } // setSeparator()


    /**
     * Get directory separator character
     *
     * @return string
     */
    public function getSeparator()
    {
        return $this -> separator;
    } // getSeparator()


    /**
     * Set file suffix
     *
     * @param  string $suffix
     * @return TemplatePathStack
     */
    public function setSuffix($suffix)
    {
        $suffix = ltrim((string) $suffix, '.');
        $this -> suffix = '.' . $suffix;
        return $this;
    } // setSuffix()


    /**
     * Get template file suffix
     *
     * @return string
     */
    public function getSuffix()
    {
        return $this -> suffix;
    } // getSuffix()


    /**
     * Set value for templatePath
     *
     * @param  string $templatePath
     * @return TemplatePathStack
     * @throws Exception\InvalidTemplatePathException
     */
    public function addTemplatePath($templatePath)
    {
        $templatePath = rtrim((string) $templatePath, '/\\');
        $this -> getTemplatePathStack() -> push($templatePath);
        return $this;
    } // addTemplatePath()


    /**
     * @param array $templates
     * @return $this
     */
    public function addTemplatePaths(array $templates)
    {
        foreach ($templates as $templatePath) {
            $this -> addTemplatePath($templatePath);
        }
        return $this;
    } // addTemplatePaths()


    /**
     * Get value for templatePath
     *
     * @return mixed
     */
    public function getTemplatePathStack()
    {
        if (!$this -> pathsStack instanceof SplStack) {
            $this -> pathsStack = new SplStack;
        }

        return $this -> pathsStack;
    } // getTemplatePathStack()


    public function getIterator()
    {
        foreach ($this -> getTemplatePathStack() as $path) {
            yield $path;
        }
    } // getIterator()


    /**
     * Clear/initialize the template path stack
     *
     * @return void
     */
    public function clearTemplatePath()
    {
        $this -> pathsStack = new SplStack();
    }

    /**
     * Resolve a template to its file
     *
     * @param  string $template
     * @return false|string Returns false if unable to resolve the template to a path
     */
    public function resolve($template)
    {
        $segments    = explode($this -> getSeparator(), $template);
        $tplFilePath = implode(DIRECTORY_SEPARATOR, $segments) . $this -> getSuffix();

        foreach ($this -> getIterator() as $tplDirPath) {

            $fileInfo = new SplFileInfo(sprintf('%s%s%s', $tplDirPath, DIRECTORY_SEPARATOR, $tplFilePath));
            if ($fileInfo -> isReadable()) {
                return $fileInfo -> getRealPath();
            }
        }
        return false;
    }


}
