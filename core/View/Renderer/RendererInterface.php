<?php
namespace Core\View\Renderer;

interface RendererInterface
{
    public function render($nameOrModel, $model = null);
}