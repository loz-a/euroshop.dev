<?php
namespace Core\View\Renderer\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\View\Renderer\JsonRenderer as ViewJsonRenderer;

class JsonRenderer implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $renderer = new ViewJsonRenderer();
        $renderer -> setResponse($container -> get('response'));
        return $renderer;
    } // createService()

} 