<?php
namespace Core\View\Renderer\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\View\Renderer\PhpRenderer as ViewPhpRenderer;

class PhpRenderer implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $renderer = new ViewPhpRenderer();
        $renderer -> addResolver($container -> get('view.map_resolver'), 10);
        $renderer -> addResolver($container -> get('view.path_stack_resolver'), 5);

        return $renderer;
    } // create()
} 