<?php
namespace Core\View\Renderer;

use Core\View\Exception\InvalidResponseException;
use Core\View\Exception\InvalidViewModelException;
use Core\View\Model\JsonModel;
use Zend\Http\PhpEnvironment\Response;

class JsonRenderer implements RendererInterface
{
    /**
     * @var Response
     */
    protected $response;

    /**
     * @param Response $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this -> response = $response;
        return $this;
    } // setResponse()


    /**
     * @return Response
     * @throws \Core\View\Exception\InvalidResponseException
     */
    public function getResponse()
    {
        if (null === $this -> response) {
            throw new InvalidResponseException('Response is invalid or undefined');
        }

        return $this -> response;
    } // getResponse()


    /**
     * @param JsonModel $nameOrModel
     * @param null $model
     * @return string
     * @throws \Core\View\Exception\InvalidViewModelException
     */
    public function render($nameOrModel, $model = null)
    {
        if (!$nameOrModel instanceof JsonModel) {
            throw new InvalidViewModelException('Invalid viewModel type. Expected Core\View\Model\JsonModel');
        }

        $headers = $this -> getResponse() -> getHeaders();
        $headers -> addHeaderLine('content-type', 'application/json');
        return $nameOrModel -> toJson();
    } // render()

} // JsonRenderer