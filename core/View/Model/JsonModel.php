<?php
namespace Core\View\Model;

use Zend\Json\Json;

class JsonModel extends AbstractModel
    implements ModelTypeInterface
{
    const MODEL_TYPE = 'json';

    public function toJson()
    {
        return Json::encode($this -> toArray());
    } // toJson()


    public function modelType()
    {
        return self::MODEL_TYPE;
    } // modelType()
}