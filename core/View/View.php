<?php
namespace Core\View;

use Core\View\Exception\InvalidEventException;
use Core\View\Exception\InvalidRendererException;
use Core\View\Exception\InvalidViewModelException;
use Core\View\Model\ModelTypeInterface;
use Core\View\Renderer\RendererInterface;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

class View implements EventManagerAwareInterface, ViewInterface
{
    use EventManagerAwareTrait;

    /**
     * @var ViewEvent
     */
    protected $event;

    /**
     * @var ModelTypeInterface
     */
    protected $model;

    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * @param $model
     * @return mixed
     */
    public function render(ModelTypeInterface $model = null)
    {
        if ($model) {
            $this -> setModel($model);
        }

        $results = $this
            -> getEventManager()
            -> trigger(ViewEvent::EVENT_RENDER, $this -> getEvent());

        return $results -> last();
    } // render()


    /**
     * @return mixed
     */
    public function __toString()
    {
        return $this -> render($this -> getModel());
    } // __toString()


    /**
     * @param ModelTypeInterface $model
     * @return $this
     */
    public function setModel(ModelTypeInterface $model)
    {
        $this -> model = $model;

        $this
            -> getEventManager()
            -> trigger(ViewEvent::EVENT_MODEL, $this -> getEvent());

        return $this;
    } // setModel()


    /**
     * @return ModelTypeInterface
     * @throws Exception\InvalidViewModelException
     */
    public function getModel()
    {
        if (null === $this -> model) {
            throw new InvalidViewModelException('View model is undefined');
        }
        return $this -> model;
    } // getModel()


    /**
     * @param RendererInterface $renderer
     * @return $this
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this -> renderer = $renderer;
        return $this;
    } // setRenderer()


    /**
     * @return RendererInterface
     * @throws Exception\InvalidRendererException
     */
    public function getRenderer()
    {
        if (null === $this -> renderer) {
            throw new InvalidRendererException('Renderer is undefined');
        }
        return $this -> renderer;
    } // getRenderer()


    /**
     * @param ViewEvent $event
     * @return $this
     */
    public function setEvent(ViewEvent $event)
    {
        $this -> event = $event;
        $this -> event -> setTarget($this);

        return $this;
    } // setEvent()


    /**
     * @return ViewEvent
     * @throws Exception\InvalidEventException
     */
    public function getEvent()
    {
        if (!$this -> event) {
            throw new InvalidEventException('View event is undefined');
        }
        return $this -> event;
    } // getEvent()

} // View