<?php
namespace Core\View\Annotations;

use Core\App;
use Core\View\Model\ViewModel;
use Core\View\ViewEvent as Event;

/**
 *  Annotation example:
 *  @Core.View.Annotations.Template index/index/index
 */
class Template
{
    public function after($tagContent)
    {
        $em = App::getInstance() -> getServiceManager() -> get('view') -> getEventManager();

        $em -> attach(Event::EVENT_RENDER, function(Event $e) use ($tagContent) {
                $view  = $e -> getTarget();
                $model = $view -> getModel();

                if (!$model instanceof ViewModel) {
                    return;
                }

                $result = $view -> getRenderer() -> render($tagContent, $model);

                $e -> setParam('__RESULT__', $result);
                return $result;
            }, 15);

    } // after()

}
