<?php
namespace Core\Exception;

/**
 * Exception indicating an invalid HTTP method
 */
class InvalidMethodException extends \InvalidArgumentException implements ExceptionInterface
{}
