<?php
namespace Core\Annotations;

trait AnnotationsManagerAwareTrait
{
    /**
     * @var Manager
     */
    protected $annotationsManager;

    /**
     * @param Manager $manager
     * @return $this
     */
    public function setAnnotationsManager(Manager $manager)
    {
        $this -> annotationsManager = $manager;
        return $this;
    }

    /**
     * @return Manager
     */
    public function getAnnotationsManager()
    {
        if (null === $this -> annotationsManager) {
            $this -> annotationsManager = new Manager();
        }
        return $this -> annotationsManager;
    }
} 