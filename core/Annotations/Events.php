<?php
namespace Core\Annotations;

use Core\App\AppAwareInterface;
use Core\App\Controller\Event\After as AfterEvent;
use Core\App\Controller\Event\Before as BeforeEvent;
use Core\App\Exception\InvalidControllerException;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Code\Reflection\MethodReflection;
use Zend\Code\Reflection\DocBlock\Tag\GenericTag;
use Zend\Code\Reflection\Exception\InvalidArgumentException as ReflectionInvalidArgumentException;

class Events implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    public function attach(EventManagerInterface $events)
    {
        $this -> listeners[] = $events -> attach('call.controller.pre',  array($this, 'callControllerBefore'));
        $this -> listeners[] = $events -> attach('call.controller.post', array($this, 'callControllerAfter'));
    } // attach()


    public function callControllerBefore(BeforeEvent $e)
    {
        $this -> handleActions('before', $e);
    } // callControllerBefore()


    public function callControllerAfter(AfterEvent $e)
    {
        $this -> handleActions('after', $e);
    } // callControllerAfter()


    /**
     * @param AnnotationsManagerAwareInterface $controller
     * @param string $actionMethod
     * @return array|null
     * @throws \Core\Exception\BadFunctionCallException
     */
    protected function parse(AnnotationsManagerAwareInterface $controller, $actionMethod)
    {
        $method = new MethodReflection($controller, $actionMethod);
        $result = array();

        try {
            $docBlock = $method -> getDocBlock();

            $tags = $docBlock ? $docBlock -> getTags() : array();

            foreach ($tags as $tag) {
                if ($tag instanceof GenericTag) {
                    $result[$tag -> getName()] = $tag -> getContent();
                }
            }

        }
        catch (ReflectionInvalidArgumentException $e) {
            $result = array();
        }
        return $result;
    } // parse()


    /**
     * @param AnnotationsManagerAwareInterface $controller
     * @param $actionMethod
     * @return array|null
     */
    protected function getDocBlock(AnnotationsManagerAwareInterface $controller, $actionMethod)
    {
        $am = $controller -> getAnnotationsManager();

        $docBlock = $am -> getDocBlock($actionMethod);
        if (!$docBlock) {
            $docBlock = $this -> parse($controller, $actionMethod);
            $am -> addDockBlock($actionMethod, $docBlock);
        }
        return $docBlock;
    } // getDocBlock()


    /**
     * @param $handlerMethod
     * @param BeforeEvent $event
     * @return null
     * @throws \Core\App\Exception\InvalidControllerException
     */
    protected function handleActions($handlerMethod, BeforeEvent $event)
    {
        $controller   = $event -> getController();
        $actionMethod = $event -> getActionName();

        if (!$controller instanceof AppAwareInterface) {
            throw new InvalidControllerException('Invalid controller; must be instance of Core\\App\\AppAwareInterface');
        }

        $docBlock = $this -> getDocBlock($controller, $actionMethod);

        if (!$docBlock) {
            return null;
        }

        $sm = $controller -> getApplication() -> getServiceManager();

        foreach ($docBlock as $actionName => $tagContent) {

            if ($sm -> has($actionName)) {
                $action = $sm -> get($actionName);
            }
            else {
                $actionClass = str_replace('.', '\\', $actionName);
                $action = new $actionClass();
                $sm -> setService($actionName, $action);
            }

            if (method_exists($action, $handlerMethod)) {
                $action -> {$handlerMethod}($tagContent, $event);
            }
        }
    } // handleActions()

} // Manager