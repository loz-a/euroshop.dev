<?php
namespace Core\Annotations\Action;

use Core\App;
use Core\Exception\InvalidTypeException;

/**
 *  Annotation example:
 *  single argument
 *  @Core.Annotations.Action.Request int id
 *
 *  lot of arguments:
 *  @Core.Annotations.Action.Request int id, str user, bool flag
 */
class Request
{
    public function before($tagContent)
    {
        $app    = App::getInstance();
        $params = $app -> getParams();

        if (!$params -> count()) {
            return;
        }

        $splittedTagContent = explode(',', $tagContent);
        $temp = array();

        foreach ($splittedTagContent as $pair) {

            list($type, $name) = explode(' ', trim($pair));
            $value = $params -> get($name);

            if (null !== $value) {
                try {
                    $value = cast_to_type($value, $type);
                }
                catch(InvalidTypeException $e) {
                    throw new InvalidTypeException(sprintf('Invalid type "%s" for parameter "%s"', $type, $name));
                }
            }
            $temp[$name] = $value;
        }
        $params -> reset() -> set($temp);
    } // before()
} 