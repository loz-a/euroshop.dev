<?php
namespace Core\Db\Table\Exception;

class InvalidConnectionException extends \Exception
    implements ExceptionInterface
{

} 