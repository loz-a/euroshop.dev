<?php
namespace Core\Db\Table\Exception;


class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

} 