<?php
namespace Core\Db\Table;

use Interop\Container\ContainerInterface;
use Zend\Filter\Word\CamelCaseToUnderscore;
use Zend\ServiceManager\InitializerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Hydrator\ClassMethods;
use Zend\Hydrator\HydratorAwareInterface;

class Initializer implements InitializerInterface
{
    /**
     * @var \PDO
     */
    protected $dbConnection;

    /**
     * @var \Zend\Config\Config
     */
    protected $tablesNames;

    /**
     * @var CamelCaseToUnderscore
     */
    protected $camelCaseToUnderscoreFilterInstance;


    public function __invoke(ContainerInterface $container, $instance)
    {
        if (!$instance instanceof DbTableAwareInterface) {
            return;
        }

        $dbTable = new Table();

        $this
            -> injectHydrator($instance, $container)
            -> injectDbConnection($dbTable, $container)
            -> injectTableName($dbTable, $instance, $container);

        $instance -> setDbTable($dbTable);
        return $instance;
    }


    public function initialize($instance, ServiceLocatorInterface $services)
    {
        return $this($services, $instance);
    } // initialize()


    /**
     * @param $instance
     * @param $tableAlias
     * @param $serviceLocator
     * @return $this
     */
    protected function injectHydrator($instance, $serviceLocator)
    {
        if (!$instance instanceof HydratorAwareInterface) {
            return $this;
        }

        $hydrator = null;

        if (method_exists($instance, 'getHydratorAlias')) {
            $hydrator = $serviceLocator -> get($instance -> getHydratorAlias());
        }

        if (null === $hydrator) {
            $hydrator = new ClassMethods();
        }

        $instance -> setHydrator($hydrator);
        return $this;
    } // injectHydrator()


    /**
     * @param $dbTable
     * @param $serviceLocator
     * @return $this
     */
    protected function injectDbConnection($dbTable, $serviceLocator)
    {
        if (null === $this -> dbConnection) {
            $this -> dbConnection = $serviceLocator -> get('db.connection');
        }
        $dbTable -> setConnection($this -> dbConnection);
        return $this;
    } // injectDbConnection()


    /**
     * @param $dbTable
     * @param $tableAlias
     * @param $serviceLocator
     * @return $this
     */
    protected function injectTableName($dbTable, $instance, $serviceLocator)
    {
        if (null === $this -> tablesNames) {
            $this -> tablesNames = $serviceLocator -> get('config') -> get('db') -> get('tables_names');
        }

        $alias = $this -> getTableAlias($instance);
        $tables = isset($this -> tablesNames) ? $this -> tablesNames -> get($alias['table_alias']) : null;

        if (null !== $tables) {
            if (is_string($tables)) {
                $dbTable -> setTableName($tables);
            }
            else {
                $tables = $tables -> toArray();

                if (isset($tables[$alias['instance_name']])) {
                    $dbTable -> setTableName($tables[$alias['instance_name']]);
                    unset($tables[$alias['instance_name']]);
                }
                $dbTable -> setRelatedTablesNames($tables);
            }
        }
        return $this;
    } // injectTableName()


    /**
     * @return CamelCaseToUnderscore
     */
    protected function getCamelCaseToUderscoreFilter()
    {
        if (null === $this -> camelCaseToUnderscoreFilterInstance) {
            $this -> camelCaseToUnderscoreFilterInstance = new CamelCaseToUnderscore();
        }

        return $this -> camelCaseToUnderscoreFilterInstance;
    } // getCamelCaseToUderscoreFilter()


    /**
     * @param $instance
     * @return array
     */
    protected function getTableAlias($instance)
    {
        $filter       = $this -> getCamelCaseToUderscoreFilter();
        $exploded     = explode('\\', get_class($instance));
        $moduleName   = strtolower($filter -> filter(current($exploded)));
        $instanceName = strtolower($filter -> filter(end($exploded)));
        return [
            'instance_name' => $instanceName,
            'table_alias'   => sprintf('%s_%s', $moduleName, $instanceName)
        ];
    } // getTableAlias()
} 