<?php
namespace Core\Db\Sql\QueryBuilder\Exception;


class ImposibleInsertException extends \Exception
    implements ExceptionInterface
{

}