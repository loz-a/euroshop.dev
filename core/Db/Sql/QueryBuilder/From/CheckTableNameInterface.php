<?php
namespace Core\Db\Sql\QueryBuilder\From;


interface CheckTableNameInterface
{
    public function hasTableName();
} 