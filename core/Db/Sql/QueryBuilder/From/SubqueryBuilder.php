<?php
namespace Core\Db\Sql\QueryBuilder\From;

use Core\Db\Sql\QueryBuilder;

class SubqueryBuilder extends QueryBuilder
{
    public function build()
    {
        list($sql, $boundParams) = parent::build();

        $search        = array(':where_', ':join_', ':group_');
        $replace       = array(':where_from_', ':join_from_', ':group_from_');
        $resultParams  = array();

        foreach ($boundParams as $k => $v) {
            $resultParams[
                str_replace($search, $replace, $k)
            ] = $v;
        }

        $sql = str_replace($search, $replace, $sql);
        $this -> params() -> reset() -> set($resultParams);

        $sql = sprintf('(%s)', $sql);

        return array($sql, $this -> params() -> toArray());
    } // build()

} 