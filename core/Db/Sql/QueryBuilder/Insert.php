<?php
namespace Core\Db\Sql\QueryBuilder;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameInterface;
use Core\Db\Sql\QueryBuilder\From\CheckTableNameTrait;

class Insert implements BuilderInterface, CheckTableNameInterface
{
    use CheckTableNameTrait;
    /**
     * @var string
     */
    protected $table;

    /**
     * @var array
     */
    protected $columns;

    /**
     * @var array|QueryBuilder
     */
    protected $values;


    public function __construct($table = null)
    {
        if ($table) {
            $this -> into($table);
        }
    } // __construct()

    /**
     * @param $table
     * @return $this
     */
    public function into($table)
    {
        $this -> table = $table;
        return $this;
    } // into()


    /**
     * @param array $columns
     * @return $this
     */
    public function columns($columns)
    {
        if (!is_array($columns)) {
            $columns = func_get_args();
        }

        $this -> columns = $columns;
        return $this;
    } // columns()


    /**
     * @param $values
     * @return $this
     * @throws Exception\ImposibleInsertException
     */
    public function values($values)
    {
        $isMulti = !is_null($this -> values);

        if ($isMulti) {
            if (!is_array($this -> values)) {
                throw new Exception\ImposibleInsertException('Preset values should be an array');
            }

            if (method_exists($values, 'qb')) {
                throw new Exception\InvalidArgumentException('QueryBuilder class instance is invalid value for multi insert');
            }

            if (!is_array($values)) {
                $values = func_get_args();
            }

            if (count($this -> values) != count($this -> values, COUNT_RECURSIVE)) { // is multidimensional
                array_push($this -> values, $values);
            }
            else {
                $multiValues = [$this -> values, $values];
                $this -> values = $multiValues;
            }
        }
        else {
            if (is_array($values)) {
                $this -> values = $values;
            }
            else
            if (is_object($values) and method_exists($values, 'qb')) {
                $this -> values = $values -> qb();
            }
            else {
                $this -> values = func_get_args();
            }
        }

        return $this;
    } // values()


    public function build()
    {
        if (null === $this -> table) {
            throw new Exception\InvalidClauseException('Table name is undefined');
        }
        $result = sprintf('insert into %s', $this -> table);

        if (null === $this -> values) {
            $result = sprintf('%s default values', $result);
            return [$result, []];
        }

        if ($this -> values instanceof QueryBuilder) {
            $subquery = $this -> values -> build();

            $result = (null === $this -> columns)
                ? sprintf('%s %s', $result, $subquery[0])
                : sprintf('%s (%s) %s', $result, implode(', ', $this -> columns), $subquery[0]);

            return [$result, $subquery[1]];
        }

        $placeholder = '';
        $resultValues = $this -> values;

        if (count($this -> values) != count($this -> values, COUNT_RECURSIVE)) { // is multidimensional
            $temp = [];
            foreach($this -> values as $values) {
                if (is_array($values)) {
                    $valuesCount = count($values);
                    $temp[] = sprintf('(%s)', implode(',', array_fill(0, $valuesCount, '?')));
                }
            }
            $placeholder  = substr(implode(', ', $temp), 1, -1);
            $resultValues = call_user_func_array('array_merge',
                array_map(function($values) {return array_values($values);}, $this -> values)
            );
        }
        else {
            $placeholder = substr(str_repeat('?,', count($this -> values)), 0, -1);
        }

        if (null === $this -> columns) {
            $result = sprintf('%s values (%s)', $result, $placeholder);
            return [$result, $resultValues];
        }

//        if (count($this -> columns) !== count($this -> values)) {
//            throw new Exception\InvalidClauseException('Columns number should be equal the number of values');
//        }

        $result = sprintf('%s (%s) values (%s)', $result, implode(', ', $this -> columns), $placeholder);
        return [$result, $resultValues];
    } // build()
} 