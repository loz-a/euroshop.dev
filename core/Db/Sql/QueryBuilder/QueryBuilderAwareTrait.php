<?php
namespace Core\Db\Sql\QueryBuilder;

trait QueryBuilderAwareTrait
{
    /**
     * @var BuilderInterface
     */
    protected $queryBuilder;

    public function __construct(BuilderInterface $queryBuilder)
    {
        $this -> queryBuilder = $queryBuilder;
    } // __construct()


    /**
     * @return BuilderInterface
     */
    public function qb()
    {
        return $this -> queryBuilder;
    } // qb()


    /**
     * @param $name
     * @param $args
     * @return mixed
     * @throws Exception\BadMethodCallException
     */
    public function __call($name, $args)
    {
        if (method_exists($this -> queryBuilder, $name)) {
            return call_user_func_array(array($this -> queryBuilder, $name), $args);
        }

        throw new Exception\BadMethodCallException(sprintf('Invalid method %s', $name));
    } // __call()

} 