<?php
namespace Core\Db\Sql\QueryBuilder;


class Where implements BuilderInterface
{
    use PredicatesTrait;

    /**
     * @return string
     * @throws Exception\InvalidClauseException
     */
    public function build()
    {
        if ($this -> hasPredicates()) {
            return sprintf('where %s', $this -> join());
        }
        return '';
    }
}