<?php
namespace Core\Db\Sql;

class FindBuilder
{
    /**
     * @var string
     */
    protected $tableName;

    /**
     * @param $query
     * @return array|bool
     */
    protected function parsePredicates($query)
    {
        if ($matches = preg_split('/(?=[A-Z])/',$query)) {
            $matches = array_filter(array_map('strtolower', $matches));
        }

        if (!count($matches)) {
            return false;
        }

        $result = array();
        foreach ($matches as $value) {
            $lastIdx = count($result) - 1;
            if (isset($result[$lastIdx])
                and !$this -> isOperator($result[$lastIdx])
                and !$this -> isOperator($value)
            ) {
                $result[$lastIdx] = sprintf('%s_%s', $result[$lastIdx], $value);
            }
            else {
                $result[] = $value;
            }
        }
              
        if ($this -> isOperator($result[0])) {
            array_shift($result);
        }

        $count = count($result);
        if ($count and $this -> isOperator($result[$count - 1])) {
            array_pop($result);
        }

        return $result;
    } // parsePredicates()


    /**
     * @param array $params
     * @param array $values
     * @return array
     * @throws Exception\InvalidArgumentException
     * @throws Exception\InvalidSqlConditionException
     */
    protected function buildWhereStatement(array $params, array $values)
    {
        $splitSql    = array();
        $boundParams = array();

        foreach ($params as $idx => $param) {
            if (!$this -> isOperator($param)) {
                $splitSql[] = sprintf('%s = :%s', $param, $param);

                if ($value = array_shift($values)) {
                    $boundParams[sprintf(':%s', $param)] = $value;
                }
                else {
                    throw new Exception\InvalidArgumentException(sprintf('No value for field %s', $param));
                }
            }
            else {
                $splitSql[] = $param;
            }
        }

        if (sizeof($values)) {
            throw new Exception\InvalidArgumentException('Too much values for where statement');
        }
        
        array_unshift($splitSql, sprintf('select * from %s where', $this -> getTableName()));
        return array($splitSql, $boundParams);
    } // buildWhereStatement()


    /**
     * @param string $value
     * @return bool
     */
    protected function isOperator($value)
    {
        return 'or' === $value or 'and' === $value;
    } // isOperator()
    

    /**
     * @param $name
     * @param $args
     * @return mixed
     * @throws Exception\BadMethodCallException
     */
    public function __call($name, $args)
    {
        foreach (array('findBy', 'findOneBy') as $func) {
            if (strpos($name, $func) === 0) {
                $query      = str_replace($func, '', $name);
                $predicates = $this -> parsePredicates($query);

                if (false === $predicates) {
                    throw new Exception\BadMethodCallException(sprintf('Invalid method - %s', $func));
                }
                return call_user_func_array(array($this, $func), array($predicates, $args));
            }
        }
        return null;
    } // __call()


    /**
     * @param array $predicates
     * @param array $values
     * @return array
     */
    protected function findBy(array $predicates, array $values)
    {
        $sql = $this -> buildWhereStatement($predicates, $values);
        $sql[0] = implode(' ', $sql[0]);
        return $sql;
    } // findBy()


    /**
     * @param array $predicates
     * @param array $values
     * @return array
     */
    protected function findOneBy(array $predicates, array $values)
    {
        $sql = $this -> buildWhereStatement($predicates, $values);
        $sql[0][] = 'limit 1';
        $sql[0] = implode(' ', $sql[0]);
        return $sql;
    } // findOneBy()


    /**
     * @return string
     * @throws Exception\InvalidClauseException
     */
    public function getTableName()
    {
        if (null === $this -> tableName) {
            throw new Exception\InvalidClauseException('Table name is undefined');
        }
        return $this -> tableName;
    } // getTableName()


    /**
     * @param $tableName
     * @return $this
     */
    public function setTableName($tableName)
    {
        $this -> tableName = $tableName;
        return $this;
    } // setTableName()
    
} 