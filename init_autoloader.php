<?php

if (file_exists('vendor/autoload.php')) {
    $loader = include 'vendor/autoload.php';
}

function autoload($class)
{
    $temp = explode('\\', $class);

    if (!in_array($temp[0], ['Core', 'App'])) {
        return false;
    }
    $filepath = '';

    $filepath = array_merge([getcwd(), lcfirst($temp[0])], array_slice($temp, 1));
    $file = implode(DIRECTORY_SEPARATOR, $filepath) . '.php';
    if (file_exists($file)) {
        require_once $file;
        return true;
    }
    else {
        $filepath = implode(DIRECTORY_SEPARATOR, $filepath) . DIRECTORY_SEPARATOR . ucfirst($temp[count($temp) - 1]);
        $file = $filepath . '.php';
        if (file_exists($file)) {
            require_once $file;
            return true;
        }
    }
    return false;
}

spl_autoload_register('autoload');
