<?php
return [

    'routes' => [
        'catalogs' => [
            'type' => 'Literal',
            'options' => [
                'route'    => '/catalogs',
                'defaults' => [
                    'controller' => Catalog\Controller\Index::class,
                    'action'     => 'index'
                ]
            ]
        ],
        'catalogs_api' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/api/catalogs/',
                'defaults' => [
                    'controller' => Catalog\Controller\Catalog\Catalog::class,
                ]
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'id' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => ':id',
                        'constraints' => ['id' => '\d+']
                    ],
                    'may_terminate' => true
                ],
                'pid' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => 'pid/:pid',
                        'constraints' => ['pid' => '\d+']
                    ],
                    'may_terminate' => true
                ]
            ]
        ],

        'catalog_upload_api' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/api/catalog-upload/',
                'defaults' => [
                    'controller' => Catalog\Controller\Catalog\Images::class,
                ]
            ],
            'may_terminate' => false,
            'child_routes'  => [
                'id' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => ':id',
                        'constraints' => ['id' => '\d+']
                    ],
                    'may_terminate' => false,
                    'child_routes' => [
                        'post' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'post',
                                'defaults' => ['action' => 'upload']
                            ],
                            'may_terminate' => true
                        ],
                        'get' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'get',
                                'defaults' => ['action' => 'getByCatalog']
                            ],
                            'may_terminate' => true
                        ],
                        'delete' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'delete',
                                'defaults' => ['action' => 'delete']
                            ],
                            'may_terminate' => true
                        ]
                    ]
                ]
            ]
        ],

        'goods_api' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/api/goods/',
                'defaults' => [
                    'controller' => Catalog\Controller\Goods\Goods::class,
                ]
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'id' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => ':id',
                        'constraints' => ['id' => '\d+']
                    ],
                    'may_terminate' => true
                ],
                'cid' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => 'cid/:cid',
                        'constraints' => ['cid' => '\d+']
                    ],
                    'may_terminate' => true
                ]
            ]
        ],

        'goods_detailed_api' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/api/goods-detailed/',
                'defaults' => [
                    'controller' => Catalog\Controller\Goods\Detailed::class
                ]
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'id' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => ':id',
                        'constraints' => ['id' => '\d+']
                    ],
                    'may_terminate' => true
                ],
                'gid' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => 'gid/:gid',
                        'constraints' => ['gid' => '\d+']
                    ],
                    'may_terminate' => true
                ]
            ]
        ],


        'goods_images_api' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/api/goods-images/',
                'defaults' => [
                    'controller' => Catalog\Controller\Goods\Images::class,
                ]
            ],
            'may_terminate' => false,
            'child_routes'  => [
                'id' => [
                    'type'    => 'Segment',
                    'options' => [
                        'route' => ':id',
                        'constraints' => ['id' => '\d+']
                    ],
                    'may_terminate' => false,
                    'child_routes' => [
                        'post' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'post',
                                'defaults' => ['action' => 'upload']
                            ],
                            'may_terminate' => true
                        ],
                        'get' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'get',
                                'defaults' => ['action' => 'getByGoods']
                            ],
                            'may_terminate' => true
                        ],
                        'delete' => [
                            'type'    => 'method',
                            'options' => [
                                'verb'     => 'delete',
                                'defaults' => ['action' => 'delete']
                            ],
                            'may_terminate' => true
                        ]
                    ]
                ]
            ]
        ],
    ],

    'view' => [
        'template_path_stack' => [
            __DIR__ . '/templates'
        ],
        'template_map' => [
            'catalog/index/index'   => __DIR__ . '/templates/catalog/index/index.phtml',
            'catalog/widget/slider' => __DIR__ . '/templates/catalog/widget/slider.phtml',
            'catalog/widget/catalogs-navigation' => __DIR__ . '/templates/catalog/widget/catalogs-navigation.phtml',
            'catalog/widget/bestsellers' => __DIR__ . '/templates/catalog/widget/bestsellers.phtml',
            'catalog/widget/last-added' => __DIR__ . '/templates/catalog/widget/last-added.phtml',
        ],
        'widgets_service_manager' => [
            'invokables' => [
                'goods.slider' => Catalog\Widget\Slider::class,
                'goods.catalogs_navigation' => Catalog\Widget\CatalogsNavigation::class,
                'goods.bestsellers' => Catalog\Widget\Bestsellers::class,
                'goods.last_added'  => Catalog\Widget\LastAdded::class,
            ]
        ]
    ],

    'db' => [
        'tables_names' => [
            'catalog_catalogs' => [ // moduleName_instanceName
                'catalogs' => 'catalog_catalogs', // instanceName => tableName
                'catalogs_view' => 'catalog_catalogs_view'
            ],
            'catalog_catalogs_image_store' => [
                'catalogs_image_store' => 'image_store',
                'catalogs_images'      => 'catalog_catalogs_images'
            ],
            'catalog_goods' => [
                'goods'        => 'catalog_goods',
                'goods_images' => 'catalog_goods_images',
                'image_store'  => 'image_store',
                'goods_join_images_view' => 'catalog_goods_full_view'
            ],
            'catalog_goods_detailed' => [
                'goods_detailed' => 'catalog_goods_detailed',
                'goods'          => 'catalog_goods'
            ],
            'catalog_goods_image_store' => [
                'goods_image_store' => 'image_store',
                'goods_images'      => 'catalog_goods_images'
            ]
        ]
    ],

    'service_manager' => [
        'invokables' => [
            Catalog\Controller\Index::class           => Catalog\Controller\Index::class,
            Catalog\Controller\Catalog\Catalog::class => Catalog\Controller\Catalog\Catalog::class,
            Catalog\Controller\Catalog\Images::class  => Catalog\Controller\Catalog\Images::class,
            Catalog\Controller\Goods\Goods::class     => Catalog\Controller\Goods\Goods::class,
            Catalog\Controller\Goods\Detailed::class  => Catalog\Controller\Goods\Detailed::class,
            Catalog\Controller\Goods\Images::class    => Catalog\Controller\Goods\Images::class,

            Catalog\Mapper\Catalog\Catalogs::class      => Catalog\Mapper\Catalog\Catalogs::class,
            Catalog\Mapper\Catalog\CatalogsImageStore::class => Catalog\Mapper\Catalog\CatalogsImageStore::class,
            Catalog\Mapper\Goods\Goods::class           => Catalog\Mapper\Goods\Goods::class,
            Catalog\Mapper\Goods\GoodsDetailed::class   => Catalog\Mapper\Goods\GoodsDetailed::class,
            Catalog\Mapper\Goods\GoodsImageStore::class => Catalog\Mapper\Goods\GoodsImageStore::class,

            Catalog\Hydrator\Catalog::class         => Catalog\Hydrator\Catalog::class,
            Catalog\Hydrator\Goods::class           => Catalog\Hydrator\Goods::class,
            Catalog\Hydrator\GoodsDetailed::class   => Catalog\Hydrator\GoodsDetailed::class,

            Catalog\Validator\Upload\UploadCatalogImage::class => Catalog\Validator\Upload\UploadCatalogImage::class,
            Catalog\Validator\Upload\UploadGoodsImage::class   => Catalog\Validator\Upload\UploadGoodsImage::class,

            Catalog\Filter\ShortDescription::class  => Catalog\Filter\ShortDescription::class,
        ],
        'factories' => [
            Catalog\Service\Catalog::class         => Catalog\Service\Factory\Catalog::class,
            Catalog\Service\CatalogUpload::class   => Catalog\Service\Factory\CatalogUpload::class,
            Catalog\Service\Goods::class           => Catalog\Service\Factory\Goods::class,
            Catalog\Service\GoodsDetailed::class   => Catalog\Service\Factory\GoodsDetailed::class,
            Catalog\Service\GoodsImages::class     => Catalog\Service\Factory\GoodsImages::class,

            Catalog\Validator\CatalogDelete::class => Catalog\Validator\Factory\CatalogDelete::class,
            Catalog\Validator\Catalog::class       => Catalog\Validator\Factory\Catalog::class,
            Catalog\Validator\Goods::class         => Catalog\Validator\Factory\Goods::class,
            Catalog\Validator\GoodsDetailed::class => Catalog\Validator\Factory\GoodsDetailed::class,

            Catalog\Options\ModuleOptions::class      => Catalog\Options\Factory::class,
            Catalog\Service\Factory\Filesystem::class => Catalog\Service\Factory\Filesystem::class,
            Catalog\Service\Factory\InterventionImageManager::class => Catalog\Service\Factory\InterventionImageManager::class,
        ]
    ],

    'admin_navigation' => [
        [
            'title'      => 'Каталог',
            'icon.class' => 'fa fa-th-list',
            'matched.route.name' => 'catalogs',
            'order' => 2
        ]
    ],

    'image_store' => [
        'catalog' => [
            'image_manager_driver' => 'gd',
            'uploader' => [

                'catalogs' => [
                    'upload_path' => '/upload/catalog/catalogs',
                    'transform' => [
                        'fit' => [140, 130] // width, height
                    ],

                ],

                'goods' => [
                    'upload_path' => '/upload/catalog/goods',
                    'transform' => [
                        'fit' => [308, 400]
                    ],
                    'thumbs' => [
                        'medium' => [
                            'upload_path' => '/medium',
                            'transform' => [
                                'fit' => [340, 406]
                            ]
                        ],
                        'min' => [
                            'upload_path' => '/min',
                            'transform' => [
                                'fit' => [70, 91]
                            ]
                        ]
                    ]
                ]

            ],
        ]
    ]

];