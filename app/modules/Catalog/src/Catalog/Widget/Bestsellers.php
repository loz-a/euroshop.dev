<?php
namespace Catalog\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;
use Catalog\Mapper\Goods\Goods as GoodsMapper;

class Bestsellers extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm          = $app -> getServiceManager();
        $bestsellers = $sm -> get(GoodsMapper::class) -> getBestsellers();

        return $sm -> get('view') -> getRenderer() -> render('catalog/widget/bestsellers', compact('bestsellers'));
    } // __invoke()
}