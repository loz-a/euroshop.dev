<?php
namespace Catalog\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;
use Catalog\Mapper\Goods\Goods as GoodsMapper;

class Slider extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm    = $app -> getServiceManager();
        $goods = $sm -> get(GoodsMapper::class) -> getByIndexCarousel();

        return $sm -> get('view') -> getRenderer() -> render('catalog/widget/slider', compact('goods'));
    } // __invoke()
}