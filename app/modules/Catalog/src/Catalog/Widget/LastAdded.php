<?php
namespace Catalog\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;
use Catalog\Mapper\Goods\Goods as GoodsMapper;

class LastAdded extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm          = $app -> getServiceManager();
        $goods = $sm -> get(GoodsMapper::class) -> getLastAdded();

        return $sm -> get('view') -> getRenderer() -> render('catalog/widget/last-added', compact('goods'));
    } // __invoke()
}