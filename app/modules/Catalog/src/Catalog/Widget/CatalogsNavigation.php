<?php
namespace Catalog\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;
use Catalog\Mapper\Catalog\Catalogs as CatalogsMapper;

class CatalogsNavigation extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm       = $app -> getServiceManager();
        $catalogs = $sm -> get(CatalogsMapper::class) -> getByPid(null);

        return $sm -> get('view') -> getRenderer() -> render('catalog/widget/catalogs-navigation', compact('catalogs'));
    } // __invoke()
}