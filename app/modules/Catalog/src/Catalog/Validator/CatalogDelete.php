<?php
namespace Catalog\Validator;

use Zend\Validator\AbstractValidator;

class CatalogDelete extends AbstractValidator
{
    /**
     * @var NoRecordExists
     */
    protected $hasCatalogValidator;

    /**
     * @var NoRecordExists
     */
    protected $hasGoodsValidator;


    public function isValid($value)
    {
        $this -> setValue($value);

        $catalogValidator = $this -> getHasCatalogValidator();
        $goodsValidator   = $this -> getHasGoodsValidator();

        return $catalogValidator -> isValid($value)
                    && $goodsValidator -> isValid($value);
    } // isValid()


    public function getMessages()
    {
        return array_merge(
            $this -> getHasCatalogValidator() -> getMessages(),
            $this -> getHasGoodsValidator()   -> getMessages()
        );
    } // getMessages()


    /**
     * @return NoRecordExists
     * @throws Exception\UndefinedNoRecordExistsValidatorException
     */
    public function getHasCatalogValidator()
    {
        if (null === $this -> hasCatalogValidator) {
            throw new Exception\UndefinedNoRecordExistsValidatorException('NoRecordExistsValidator for catalog is undefined');
        }
        return $this -> hasCatalogValidator;
    }

    /**
     * @param NoRecordExists $hasCatalogValidator
     * @return $this
     */
    public function setHasCatalogValidator(NoRecordExists $hasCatalogValidator)
    {
        $this -> hasCatalogValidator = $hasCatalogValidator;
        return $this;
    }


    /**
     * @return NoRecordExists
     * @throws Exception\UndefinedNoRecordExistsValidatorException
     */
    public function getHasGoodsValidator()
    {
        if (null === $this -> hasGoodsValidator) {
            throw new Exception\UndefinedNoRecordExistsValidatorException(sprintf('%s for goods is undefined', NoRecordExists::class));
        }
        return $this -> hasGoodsValidator;
    }

    /**
     * @param NoRecordExists $hasGoodsValidator
     * @return $this
     */
    public function setHasGoodsValidator(NoRecordExists $hasGoodsValidator)
    {
        $this -> hasGoodsValidator = $hasGoodsValidator;
        return $this;
    }




}