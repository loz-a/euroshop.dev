<?php
namespace Catalog\Validator\Upload;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\FileInput;
use Zend\InputFilter\Input;
use Zend\Validator\File\IsImage;
use Zend\Validator\File\Size;
use Zend\Filter\ToInt;

class UploadCatalogImage extends InputFilter
{
    public function init()
    {
        $image = new FileInput('image');
        $image
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new IsImage([
                'messages' => [
                    IsImage::FALSE_TYPE => 'File is no image'
                ]
            ]))
            -> attach(new Size([
                'max' => '2MB',
                'messages' => [
                    Size::TOO_BIG => 'Maximum allowed size for file is 2 MB'
                ]
            ]));

        $catalogId = new Input('catalog_id');
        $catalogId -> getFilterchain()
                   -> attach(new ToInt());

        $this -> add($image);
        $this -> add($catalogId);
    } // init()
}