<?php
namespace Catalog\Validator;

use Core\App\RequestParams;
use Catalog\Options\ModuleOptions;

trait ValidatorTrait
{

    protected $mapper;

    /**
     * @var RequestParams;
     */
    protected $params;

    /**
     * @var ModuleOptions
     */
    protected $options;

    /**
     * @param RequestParams $params
     * @return $this
     */
    public function setParams(RequestParams $params)
    {
        $this -> params = $params;
        return $this;
    } // setParams()

    /**
     * @return RequestParams
     * @throws Exception\UndefinedRequestParamsException
     */
    public function getParams()
    {
        if (null === $this -> params) {
            throw new Exception\UndefinedRequestParamsException('Request parameters is undefined');
        }
        return $this -> params;
    } // getParams()


    abstract public function setMapper($mapper);

    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedMapperException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    /**
     * @param ModuleOptions $options
     * @return $this
     */
    public function setModuleOptions(ModuleOptions $options)
    {
        $this -> options = $options;
        return $this;
    } // setModuleOptions()

    /**
     * @return ModuleOptions
     */
    public function getModuleOptions()
    {
        return $this -> options;
    } // getModuleOptions()
}