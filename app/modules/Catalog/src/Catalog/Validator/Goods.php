<?php
namespace Catalog\Validator;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\Between;
use Zend\Validator\StringLength;
use Zend\I18n\Validator\IsInt;
use Zend\Validator\Regex;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\Filter\ToInt;
use Zend\Filter\Callback;
use Catalog\Mapper\Goods\GoodsInterface;


class Goods extends InputFilter
{
    use ValidatorTrait;

    public function init()
    {
        $this
            -> add($this -> getTitle())
            -> add($this -> getCatalogId())
            -> add($this -> getDescription())
            -> add($this -> getShortDescription())
            -> add($this -> getProducer())
            -> add($this -> getPrice())
            -> add($this -> getQuantity())
            -> add($this -> getIsTop())
            -> add($this -> getIsIndexCarousel());

    } // init()


    protected function getTitle()
    {
        $params = $this -> getParams();

        $title = new Input('title');
        $title
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this -> getModuleOptions() -> getGoodsTitleMaxLength()
            ]))
            -> attach(new NoRecordExists([
                'mapper'     => $this -> getMapper(),
                'key'        => 'title',
                'container_id' => $params -> get('catalog_id'),
                'exclude'    => $params -> get('id'),
                'messages'  => [
                    NoRecordExists::ERROR_RECORD_FOUND => 'Goods with this name already exists'
                ]
            ]));

        $title
            -> getFilterChain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $title;
    } // getTitle()


    protected function getCatalogId()
    {
        $catalogId = new Input('catalog_id');
        $catalogId
            -> setRequired(true)
            -> getFilterchain()
            -> attach(new ToInt());

        return $catalogId;
    } // getCatalogId()


    protected function getDescription()
    {
        $description = new Input('description');
        $description
            -> setRequired(true)
            -> getFilterchain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $description;
    } // getDescription()


    protected function getShortDescription()
    {
        $description = new Input('short_description');
        $description
            -> setRequired(false)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this -> getModuleOptions() -> getGoodsShortDescriptionMaxLength()
            ]));

        $description
            -> getFilterchain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $description;
    } // getShortDescription()

    protected function getProducer()
    {
        $producer = new Input('producer');
        $producer
            -> setRequired(false)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this -> getModuleOptions() -> getGoodsProducerMaxLength()
            ]));

        $producer
            -> getFilterChain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $producer;
    } // getProducer()


    protected function getPrice()
    {
        $price = new Input('price');
        $price
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new Regex([
                'pattern'  => '/^\d*\.\d{2}$/',
                'messages' => [
                    Regex::NOT_MATCH => 'Invalid input. Input mask: 999 999 999.99'
                ]
            ]));

        $price
            -> getFilterChain()
            -> attach(new Callback(function($value) {
                if (false !== strpos($value, ',')) {
                    $value = str_replace(',', '.', $value);
                }
                $value = str_replace(' ', '', $value);
                return $value;
            }));

        return $price;
    } // getPrice()


    protected function getQuantity()
    {
        $quantity = new Input('quantity');
        $quantity
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new IsInt());

        return $quantity;
    } // getQuantity()


    protected function getIsTop()
    {
        $isTop = new Input('is_top');
        $isTop
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new Between(['mix' => 0, 'max' => 1]));
        return $isTop;
    } // getIsTop()


    protected function getIsIndexCarousel()
    {
        $isIndexCarousel = new Input('is_index_carousel');
        $isIndexCarousel
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new Between(['mix' => 0, 'max' => 1]));
        return $isIndexCarousel;
    } // getIsIndexCarousel()


    public function setMapper($mapper)
    {
        if (!$mapper instanceof GoodsInterface) {
            throw new Exception\InvalidArgumentException(sprintf('Invalid mapper type. %s expected', GoodsInterface::class));
        }
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()

}