<?php
namespace Catalog\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Validator\Goods as GoodsValidator;
use Catalog\Mapper\Goods\Goods as GoodsMapper;
use Catalog\Options\ModuleOptions;

class Goods implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new GoodsValidator())
            -> setMapper($container -> get(GoodsMapper::class))
            -> setParams($container -> get('app') -> getParams())
            -> setModuleOptions($container -> get(ModuleOptions::class));
    }
}