<?php
namespace Catalog\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Validator\Catalog as CatalogValidator;
use Catalog\Mapper\Catalog\Catalogs as CatalogMapper;
use Catalog\Options\ModuleOptions;

class Catalog implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new CatalogValidator())
            -> setMapper($container -> get(CatalogMapper::class))
            -> setParams($container -> get('app') -> getParams())
            -> setModuleOptions($container -> get(ModuleOptions::class));
    }
}