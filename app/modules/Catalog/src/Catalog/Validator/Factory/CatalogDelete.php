<?php
namespace Catalog\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Mapper\Catalog\Catalogs as CatalogMapper;
use Catalog\Mapper\Goods\Goods as GoodsMapper;
use Catalog\Validator\NoRecordExists as NoRecordExistsValidator;
use Catalog\Validator\CatalogDelete as CatalogDeleteValidator;


class CatalogDelete implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $catalogValidator = new NoRecordExistsValidator([
            'mapper'   => $container -> get(CatalogMapper::class),
            'key'      => 'parent_id',
            'messages' => [
                NoRecordExistsValidator::ERROR_RECORD_FOUND => 'Catalog has children catalogs. First clear catalog'
            ]
        ]);

        $goodsValidator = new NoRecordExistsValidator([
            'mapper'   => $container -> get(GoodsMapper::class),
            'key'      => 'catalog_id',
            'messages' => [
                NoRecordExistsValidator::ERROR_RECORD_FOUND => 'Catalog has goods. First clear catalog'
            ]
        ]);

        $validator = new CatalogDeleteValidator();
        $validator
            -> setHasCatalogValidator($catalogValidator)
            -> setHasGoodsValidator($goodsValidator);
        return $validator;
    }
}