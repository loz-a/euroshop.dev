<?php
namespace Catalog\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Validator\GoodsDetailed as GoodsDetailedValidator;
use Catalog\Mapper\Goods\GoodsDetailed as GoodsDetailedMapper;
use Catalog\Options\ModuleOptions;

class GoodsDetailed implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new GoodsDetailedValidator())
            -> setMapper($container -> get(GoodsDetailedMapper::class))
            -> setParams($container -> get('app') -> getParams())
            -> setModuleOptions($container -> get(ModuleOptions::class));
    }
}