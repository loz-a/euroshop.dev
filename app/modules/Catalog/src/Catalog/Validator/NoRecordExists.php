<?php
namespace Catalog\Validator;

use Core\Stdlib\Validator\AbstractRecord;
use Core\Stdlib\Validator\Exception\InvalidKeyException;
use Catalog\Mapper\TitleSearcherInterface;

class NoRecordExists extends AbstractRecord
{
    /**
     * @var int
     */
    protected $containerId;

    public function __construct(array $options)
    {
        if ( array_key_exists('container_id', $options) ) {
            $this -> setContainerId($options['container_id']);
        }

        parent::__construct($options);
    } // __construct()

    /**
     * @param $mapper
     * @return $this
     * @throws Exception\InvalidArgumentException
     */
    public function setMapper($mapper)
    {
        if (!$mapper instanceof TitleSearcherInterface) {
            throw new Exception\InvalidArgumentException(sprintf('Invalid mapper. %s instance expected', TitleSearcherInterface::class));
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @param mixed $value
     * @return bool|void
     */
    public function isValid($value)
    {
        $this -> setValue($value);

        $result = $this -> query($value);
        if ($result) {
            $this -> error(self::ERROR_RECORD_FOUND);
            return false;
        }
        return true;
    } // isValid()


    public function setContainerId($cid)
    {
        $this -> containerId = $cid;
        return $this;
    } // setContainerId()

    public function getContainerId()
    {
        return $this -> containerId;
    } // getContainerId()


    protected function query($value)
    {
        $key    = $this -> getKey();
        $result = false;

        switch ($key) {
            case 'parent_id':
                $result = $this -> getMapper() -> lookupByParentId($value, $this -> getExclude());
                break;
            case 'catalog_id':
                $result = $this -> getMapper() -> lookupByCatalogId($value, $this -> getExclude());
                break;
            case 'title':
                $result = $this -> getMapper() -> lookupByTitle($value, $this -> getContainerId(), $this -> getExclude());
                break;
            default:
                throw new InvalidKeyException(sprintf('Invalid key - %s'), $key);
        }
        return $result;
    } // query()

} 