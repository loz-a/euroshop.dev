<?php
namespace Catalog\Validator\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

} 