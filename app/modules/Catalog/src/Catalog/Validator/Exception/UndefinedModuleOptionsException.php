<?php
namespace Catalog\Validator\Exception;


class UndefinedModuleOptionsException extends \Exception
    implements ExceptionInterface
{

} 