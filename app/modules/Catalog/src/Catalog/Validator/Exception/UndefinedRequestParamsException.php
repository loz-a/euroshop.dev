<?php
namespace Catalog\Validator\Exception;


class UndefinedRequestParamsException extends \Exception
    implements ExceptionInterface
{

} 