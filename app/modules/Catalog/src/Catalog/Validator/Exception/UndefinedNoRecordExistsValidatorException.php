<?php
namespace Catalog\Validator\Exception;


class UndefinedNoRecordExistsValidatorException extends \Exception
    implements ExceptionInterface
{

} 