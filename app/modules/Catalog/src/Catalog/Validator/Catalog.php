<?php
namespace Catalog\Validator;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\StringLength;
use Zend\Filter\StripTags;
use Zend\Filter\StringTrim;
use Zend\Filter\ToInt;
use Catalog\Mapper\Catalog\CatalogsInterface;

class Catalog extends InputFilter
{
    use ValidatorTrait;

    public function init()
    {
        $params      = $this -> getParams();
        $options     = $this -> getModuleOptions();

        $containerId = (int) $params -> get('parent_id');
        if ($containerId === 0) {
            $containerId = null;
        }

        $title = new Input('title');
        $title
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max'      => $options -> getCatalogTitleMaxLength()
            ]))
            -> attach(new NoRecordExists([
                'mapper'       => $this -> getMapper(),
                'key'          => 'title',
                'container_id' => $containerId,
                'exclude'      => $params -> get('id'),
                'messages'     => [
                    NoRecordExists::ERROR_RECORD_FOUND => 'Catalog with this name already exists'
                ]
            ]));

        $title
            -> getFilterChain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        $parentId = new Input('parent_id');
        $parentId
            -> setRequired(true)
            -> getFilterchain()
            -> attach(new ToInt());

        $this
            -> add($title)
            -> add($parentId);
    } // init()


    public function setMapper($mapper)
    {
        if (!$mapper instanceof CatalogsInterface) {
            throw new Exception\InvalidArgumentException(sprintf('Invalid mapper type. %s expected', CatalogsInterface::class));
        }
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()

}