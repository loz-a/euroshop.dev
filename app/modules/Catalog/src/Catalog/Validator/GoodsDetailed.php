<?php
namespace Catalog\Validator;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\StringLength;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\Filter\ToInt;
use Catalog\Mapper\Goods\DetailedInterface;


class GoodsDetailed extends InputFilter
{
    use ValidatorTrait;

    public function init()
    {
        $this
            -> add($this -> getTitleInput())
            -> add($this -> getValueInput())
            -> add($this -> getGoodsIdInput());
    } // init()


    /**
     * @return Input
     */
    protected function getTitleInput()
    {
        $params = $this -> getParams();
        $title  = new Input('title');

        $title
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this -> getModuleOptions() -> getGoodsDetailedTitleMaxLength()
            ]))
            -> attach(new NoRecordExists([
                'mapper'       => $this -> getMapper(),
                'key'          => 'title',
                'container_id' => $params -> get('goods_id'),
                'exclude'      => $params -> get('id'),
                'messages'     => [
                    NoRecordExists::ERROR_RECORD_FOUND => 'Record with this title already exists'
                ]
            ]));

        $title
            -> getFilterChain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $title;
    } // getTitleInput()


    protected function getValueInput()
    {
        $value = new Input('value');

        $value
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new StringLength([
                'encoding' => 'UTF-8',
                'max' => $this -> getModuleOptions() -> getGoodsDetailedValuemaxLength()
            ]));

        $value
            -> getFilterChain()
            -> attach(new StringTrim())
            -> attach(new StripTags());

        return $value;
    } // getValueInput()


    protected function getGoodsIdInput()
    {
        $goodsId = new Input('goods_id');
        $goodsId -> getFilterChain() -> attach(new ToInt());
        return $goodsId;
    } // getGoodsIdInput()


    public function setMapper($mapper)
    {
        if (!$mapper instanceof DetailedInterface) {
            throw new Exception\InvalidArgumentException(sprintf('Invalid mapper type. %s expected', DetailedInterface::class));
        }

        $this -> mapper = $mapper;
        return $this;
    } // setMapper()

}