<?php
namespace Catalog\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements CatalogInterface, GoodsInterface, GoodsDetailedInterface
{
    protected $catalogTitleMaxLength = 255;

    public function setCatalogTitleMaxLength($maxLength)
    {
        $this -> catalogTitleMaxLength = $maxLength;
    }

    public function getCatalogTitleMaxLength()
    {
        return $this -> catalogTitleMaxLength;
    }


    protected $goodsTitleMaxLength = 255;

    public function setGoodsTitleMaxLength($maxLength)
    {
        $this -> goodsTitleMaxLength = $maxLength;
    }

    public function getGoodsTitleMaxLength()
    {
        return $this -> goodsTitleMaxLength;
    }


    protected $goodsProducerMaxLength = 255;

    public function setGoodsProducerMaxLength($maxLength)
    {
        $this -> goodsProducerMaxLength = $maxLength;
    }

    public function getGoodsProducerMaxLength()
    {
        return $this -> goodsProducerMaxLength;
    }


    protected $goodsShortDescriptionMaxLength = 120;

    public function setGoodsShortDescriptionMaxLength($maxLength)
    {
        $this -> goodsShortDescriptionMaxLength = $maxLength;
    }

    public function getGoodsShortDescriptionMaxLength()
    {
        return $this -> goodsShortDescriptionMaxLength;
    }


    protected $goodsDetailedTitleMaxLength = 100;

    public function setGoodsDetailedTitleMaxLength($maxLength)
    {
        $this -> goodsDetailedTitleMaxLength = $maxLength;
    }

    public function getGoodsDetailedTitleMaxLength()
    {
        return $this -> goodsDetailedTitleMaxLength;
    }


    protected $goodsDetailedValueMaxLength = 255;

    public function setGoodsDetailedValueMaxLength($maxLength)
    {
        $this -> goodsDetailedValueMaxLength = $maxLength;
    }

    public function getGoodsDetailedValueMaxLength()
    {
        return $this -> goodsDetailedValueMaxLength;
    }


} 