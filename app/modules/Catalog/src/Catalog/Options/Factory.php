<?php
namespace Catalog\Options;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class Factory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container -> get('config');
        $options = isset($config['catalog_options']) ? $config['catalog_options'] : [];
        return new ModuleOptions($options);
    }
}