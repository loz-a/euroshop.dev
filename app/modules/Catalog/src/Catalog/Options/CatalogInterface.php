<?php
namespace Catalog\Options;


interface CatalogInterface
{
    public function setCatalogTitleMaxLength($maxLength);
    public function getCatalogTitleMaxLength();
}
