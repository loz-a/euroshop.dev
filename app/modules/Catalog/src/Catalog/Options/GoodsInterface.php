<?php
namespace Catalog\Options;

interface GoodsInterface
{
    public function setGoodsTitleMaxLength($maxLength);
    public function getGoodsTitleMaxLength();

    public function setGoodsProducerMaxLength($maxLength);
    public function getGoodsProducerMaxLength();

    public function setGoodsShortDescriptionMaxLength($maxLength);
    public function getGoodsShortDescriptionMaxLength();
}