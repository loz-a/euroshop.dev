<?php
namespace Catalog\Options;


interface GoodsDetailedInterface
{
    public function setGoodsDetailedTitleMaxLength($maxLength);
    public function getGoodsDetailedTitleMaxLength();

    public function setGoodsDetailedValueMaxLength($maxLength);
    public function getGoodsDetailedValueMaxLength();
}