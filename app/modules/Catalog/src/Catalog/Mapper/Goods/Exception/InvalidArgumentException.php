<?php
namespace Catalog\Mapper\Goods\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

}