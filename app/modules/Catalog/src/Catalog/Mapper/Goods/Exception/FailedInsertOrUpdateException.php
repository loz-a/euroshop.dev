<?php
namespace Catalog\Mapper\Goods\Exception;

class FailedInsertOrUpdateException extends \Exception
    implements ExceptionInterface
{

}