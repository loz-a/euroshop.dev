<?php
namespace Catalog\Mapper\Goods;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Sql\QueryBuilder\Where\SubqueryBuilder;
use Core\Db\Table\DbTableAwareInterface;
use Core\Db\Table\DbTableAwareTrait;
use Core\ImageStore\Mapper\MapperInterface;
use Catalog\Entity\Goods\Image;
use Catalog\Entity\Goods\ImageInterface;

class GoodsImageStore implements MapperInterface, DbTableAwareInterface
{
    use DbTableAwareTrait;

    /**
     * @param int $id
     * @return null|Image
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function get($id)
    {
        $table                = $this  -> getDbTable();
        $istoreTableName      = $table -> getTableName();
        $goodsImagesTableName = $table -> getRelatedTableName('goods_images');

        $qb = new QueryBuilder();
        $qb
            -> select([
                'istore.id',
                'istore.path',
                'medium' => 'medium_istore.path',
                'min' => 'min_istore.path',
                'istore.serialized_upload_data',
                'gimages.goods_id'
            ])
            -> from($istoreTableName, 'istore')
            -> join()
                -> with($istoreTableName, 'medium_istore')
                -> on('istore.id = medium_istore.parent_id') -> andEqual('medium_istore.alias', 'medium')
            -> join()
                -> with($istoreTableName, 'min_istore')
                -> on('istore.id = min_istore.parent_id') -> andEqual('min_istore.alias', 'min')
            -> join()
                -> with($goodsImagesTableName, 'gimages')
                -> on('istore.id = gimages.image_id') -> andEqual('istore.id', $id);

        $result = $table -> fetchOne($qb);

        if ($result) {
            return (new Image())
                -> setId($result['id'])
                -> setPath($result['path'])
                -> setUploadData($result['serialized_upload_data'])
                -> setGoodsId($result['goods_id'])
                -> setThumbs([
                    'medium' => $result['medium'],
                    'min'    => $result['min']
                ]);
        }
        return null;
    } // get()


    /**
     * @param $id
     * @return null|Image
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function getByGoods($id)
    {
        $table                = $this  -> getDbTable();
        $istoreTableName      = $table -> getTableName();
        $goodsImagesTableName = $table -> getRelatedTableName('goods_images');

        $qb = new QueryBuilder();
        $qb
            -> select([
                'istore.id',
                'istore.path',
                'medium' => 'medium_istore.path',
                'min' => 'min_istore.path',
                'istore.serialized_upload_data',
                'gimages.goods_id'
            ])
            -> from($istoreTableName, 'istore')
            -> join()
                -> with($istoreTableName, 'medium_istore')
                -> on('istore.id = medium_istore.parent_id') -> andEqual('medium_istore.alias', 'medium')
            -> join()
                -> with($istoreTableName, 'min_istore')
                -> on('istore.id = min_istore.parent_id') -> andEqual('min_istore.alias', 'min')
            -> join()
                -> with($goodsImagesTableName, 'gimages')
                -> on('istore.id = gimages.image_id') -> andEqual('gimages.goods_id', $id);

        return $table -> fetchAll($qb);
    } // getByGoods()


    /**
     * @param \Core\ImageStore\Entity\ImageInterface $image
     * @return Image|null
     * @throws Exception\FailedInsertOrUpdateException
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     * @throws \Core\Db\Table\Exception\InvalidConnectionException
     */
    public function set($image)
    {
        if (!$image instanceof ImageInterface) {
            throw new Exception\InvalidARgumentException('Invalid argument type. Instance of %s expected', ImageInterface::class);
        }

        $table      = $this  -> getDbTable();
        $connection = $table -> getConnection();
        $id         = null;

        $storeData = [
            'path'                   => $image -> getPath(),
            'parent_id'              => null,
            'serialized_upload_data' => serialize($image -> getUploadData())
        ];

        $connection -> beginTransaction();
        try {
            $table -> insert($storeData);

            $id     = $table -> getConnection() -> lastInsertId();
            $thumbs = $image -> getThumbs();

            if (count($thumbs)) {
                $childData = [];
                foreach ($thumbs as $alias => $path) {
                    $childData[] = [
                        'alias'     => $alias,
                        'path'      => $path,
                        'parent_id' => $id,
                        'serialized_upload_data' => $storeData['serialized_upload_data'],
                    ];
                }

                $table -> multiInsert($childData, array_keys($childData[0]));
            }

            $table -> insert([
                'goods_id' => $image -> getGoodsId(),
                'image_id' => $id
            ],
            $table -> getRelatedTableName('goods_images'));
        }
        catch(\Exception $e) {
            $connection -> rollBack();
            throw new Exception\FailedInsertOrUpdateException('Image can\'t insert or update');
        }
        $connection -> commit();

        return $this -> get($id);
    }


    /**
     * @param int $id
     * @return int
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function delete($id)
    {
        return $this -> getDbTable() -> delete($id);
    }

}