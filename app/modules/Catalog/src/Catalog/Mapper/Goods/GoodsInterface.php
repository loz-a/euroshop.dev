<?php
namespace Catalog\Mapper\Goods;

use Catalog\Entity\Goods\GoodsInterface as GoodsEntityInterface;
use Core\Db\Table\DbTableAwareInterface;
use Zend\Hydrator\HydratorAwareInterface;
use Catalog\Mapper\TitleSearcherInterface;

interface GoodsInterface extends TitleSearcherInterface, DbTableAwareInterface, HydratorAwareInterface
{
    public function getByPk($id);

    public function getByCatalogId($catalogId);

    public function getByCode($code, $hydrate = true);

    public function  searchByTitleOrCode($keyword);

    public function insert(GoodsEntityInterface $goods);

    public function update(GoodsEntityInterface $goods);

    public function delete($id);

    public function lookupByCatalogId($cid, $excludeId = null);
}