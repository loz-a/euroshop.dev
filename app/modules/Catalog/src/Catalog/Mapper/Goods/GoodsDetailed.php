<?php
namespace Catalog\Mapper\Goods;

use Catalog\Mapper\Goods\DetailedInterface;
use Catalog\Entity\Goods\DetailedInterface as GoodsDetailedEntityInterface;
use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareTrait;
use Core\Stdlib\Hydrator\HydratorAwareTrait;
use Catalog\Hydrator\GoodsDetailed as GoodsDetailedHydrator;

class GoodsDetailed implements DetailedInterface
{
    use DbTableAwareTrait, HydratorAwareTrait;

    public function getHydratorAlias()
    {
        return GoodsDetailedHydrator::class;
    }

    public function getByPk($id)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('id', $id);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);
        return $result ? $this -> getHydrator() -> hydrateGoodsDetailed($result) : null;
    } // getByPk()


    public function getByGoodsId($gid)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('goods_id', $gid);
        return $dbTable -> fetchAll($qb);
    } // getByGoodsId()


    public function insert(GoodsDetailedEntityInterface $goodsDetailed)
    {
        $table = $this -> getDbTable();

        $table -> insert([
            'title'    => $goodsDetailed -> getTitle(),
            'value'    => $goodsDetailed -> getValue(),
            'goods_id' => $goodsDetailed -> getGoodsId()
        ]);
        return $table -> getConnection() -> lastInsertId();
    }


    public function update(GoodsDetailedEntityInterface $goodsDetailed)
    {
        $table = $this -> getDbTable();

        $table -> update([
            'title'    => $goodsDetailed -> getTitle(),
            'value'    => $goodsDetailed -> getValue()
        ], $goodsDetailed -> getId());
    }


    public function delete($id)
    {
        return $this -> getDbTable() -> delete($id);
    }


    public function lookupByTitle($title, $containerId, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where() -> equal('title', $title) -> andEqual('goods_id', $containerId)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    }


    protected function select($dbTable)
    {
        $qb = new QueryBuilder();
        $qb -> select(['id', 'goods_id', 'title', 'value'])
            -> from($dbTable -> getTableName());
        return $qb;
    } // select()

}