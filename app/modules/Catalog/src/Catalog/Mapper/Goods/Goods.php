<?php
namespace Catalog\Mapper\Goods;

use Catalog\Entity\Goods\GoodsInterface as GoodsEntityInterface;
use Catalog\Hydrator\Goods as GoodsHydrator;
use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareTrait;
use Core\Stdlib\Hydrator\HydratorAwareTrait;

class Goods implements GoodsInterface
{
    use DbTableAwareTrait, HydratorAwareTrait;

    public function getHydratorAlias()
    {
        return GoodsHydrator::class;
    } // getHydratorAlias()


    public function getByPk($id, $hydrate = true)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('id', $id);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);
        if ($result) {
            return $hydrate ? $this -> getHydrator() -> hydrateGoods($result) : $result;            
        }
        return null;
    } // getByPk()


    public function getByCode($code, $hydrate = true)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('code', $code);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);
        if ($result) {
            return $hydrate ? $this -> getHydrator() -> hydrateGoods($result) : $result;
        }
        return null;
    } // getByCode()


    public function getByCatalogId($catalogId)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('catalog_id', $catalogId);
        return $dbTable -> fetchAll($qb);
    } // getByCatalogId()


    public function searchByTitleOrCode($keyword)
    {
        if (!$keyword) {
            return false;
        }

        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb
            -> select([
                'g.id', 'g.title', 'g.slug', 'description' => 'g.short_description', 'g.producer', 'g.quantity', 'g.price', 'image_min' => 'istore.path'
            ])
            -> distinct()
            -> from($table -> getTableName(), 'g')
            -> join()
            -> left($table -> getRelatedTableName('goods_images'), 'gi')
            -> on('g.id = gi.goods_id')
            -> join()
            -> left($table -> getRelatedTableName('image_store'), 'istore')
            -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'min')
            -> where() -> like('g.title', "%$keyword%") -> orEqual('g.code', $keyword)
            -> group() -> by('g.id');

        return $table -> fetchAll($qb);
    } // searchByTitleOrCode()


    public function getListByCatalogId($catalogId, array $filters = [])
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb
            -> select([
                'g.id', 'g.title', 'g.slug', 'g.code', 'description' => 'g.short_description', 'g.producer', 'g.quantity', 'g.price', 'image_min' => 'istore.path'
            ])
            -> from($table -> getTableName(), 'g')
            -> join()
                -> left($table -> getRelatedTableName('goods_images'), 'gi')
                -> on('g.id = gi.goods_id')
            -> join()
                -> left($table -> getRelatedTableName('image_store'), 'istore')
                -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'min')
            -> where('g.catalog_id', $catalogId)
            -> group() -> by('g.id');

        if (count($filters)) {
            $qb = $this -> handleFilters($qb, $filters);
        }
        else {
            $qb -> order() -> asc('g.title');
        }

        return $table -> fetchAll($qb);
    } // getListByCatalogId()


    public function getByIndexCarousel()
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb
            -> select([
                'g.id', 'g.title', 'g.slug', 'description' => 'g.short_description', 'g.price', 'image_medium' => 'istore.path'
            ])
            -> from($table -> getTableName(), 'g')
            -> join()
                -> left($table -> getRelatedTableName('goods_images'), 'gi')
                -> on('g.id = gi.goods_id')
            -> join()
                -> left($table -> getRelatedTableName('image_store'), 'istore')
                -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'medium')
            -> where('g.is_index_carousel', 1)
            -> group() -> by('g.id');

        return $table -> fetchAll($qb);
    } // getByIndexCarousel()


    public function getBestsellers()
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb
            -> select([
                'g.id', 'g.title', 'g.code', 'g.slug', 'description' => 'g.short_description', 'g.producer', 'g.quantity', 'g.price', 'image_min' => 'istore.path'
            ])
            -> from($table -> getTableName(), 'g')
            -> join()
                -> left($table -> getRelatedTableName('goods_images'), 'gi')
                -> on('g.id = gi.goods_id')
            -> join()
                -> left($table -> getRelatedTableName('image_store'), 'istore')
                -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'min')
            -> where('g.is_top', 1)
            -> group() -> by('g.id');

        return $table -> fetchAll($qb);
    } // getBestsellers()


    public function getLastAdded($limit = 10)
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb
            -> select([
                'g.id', 'g.title', 'g.slug', 'g.code', 'description' => 'g.short_description', 'g.producer', 'g.quantity', 'g.price', 'image_min' => 'istore.path'
            ])
            -> from($table -> getTableName(), 'g')
            -> join()
                -> left($table -> getRelatedTableName('goods_images'), 'gi')
                -> on('g.id = gi.goods_id')
            -> join()
                -> left($table -> getRelatedTableName('image_store'), 'istore')
                -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'min')
            -> group() -> by('g.id')
            -> limit($limit)
            -> order() -> desc('g.created');;

        return $table -> fetchAll($qb);
    } // getLastAdded()


    public function insert(GoodsEntityInterface $goods)
    {
        $table = $this -> getDbTable();

        $table -> insert([
            'title'       => $goods -> getTitle(),
            'code'        => $goods -> getCode(),
            'slug'        => $goods -> getSlug(),
            'description' => $goods -> getDescription(),
            'short_description' => $goods -> getShortDescription(),
            'producer'    => $goods -> getProducer(),
            'price'       => $goods -> getPrice(),
            'quantity'    => $goods -> getQuantity(),
            'catalog_id'  => $goods -> getCatalogId(),
            'created'     => $goods -> getCreated(),
            'updated'     => $goods -> getUpdated(),
            'is_top'      => $goods -> getIsTop(),
            'is_index_carousel'=> $goods -> getIsIndexCarousel()
        ]);
        return $table -> getConnection() -> lastInsertId();
    } // insert()


    public function update(GoodsEntityInterface $goods)
    {
        $table = $this -> getDbTable();

        $table -> update([
            'title'       => $goods -> getTitle(),
            'description' => $goods -> getDescription(),
            'short_description' => $goods -> getShortDescription(),
            'producer'    => $goods -> getProducer(),
            'price'       => $goods -> getPrice(),
            'quantity'    => $goods -> getQuantity(),
            'updated'     => $goods -> getUpdated(),
            'is_top'      => $goods -> getIsTop(),
            'is_index_carousel'=> $goods -> getIsIndexCarousel()
        ], $goods -> getId());
    } // update()


    public function delete($id)
    {
        return $this -> getDbTable() -> delete($id);
    } // delete()


    public function lookupByTitle($title, $containerId, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where() -> equal('title', $title) -> andEqual('catalog_id', $containerId)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    } // lookupByTitle


    public function lookupByCatalogId($cid, $excludeId = null)
    {
        $table = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($table -> getTableName())
            -> where() -> equal('catalog_id', $cid)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $table -> fetchOne($qb);
    } // lookupByCatalogId()


    public function getCountBySlugSubstring($substr)
    {
        $table = $this -> dbTable;
        $qb = new QueryBuilder();

        $qb -> select('code')
            -> from($table -> getTableName())
            -> where() -> like('slug', "$substr%")
            -> order() -> desc('code')
            -> limit(1);

        $result = $table -> fetchOne($qb);
        if (!$result) {
            return 1;
        }

        return intval(substr($result['code'], 3)) + 1;
    } // getCountBySlugSubstring()
    

    protected function select($dbTable)
    {
        $qb = new QueryBuilder();
        $qb -> select([
            'id',
            'title',
            'code',
            'slug',
            'description',
            'short_description',
            'producer',
            'price',
            'quantity',
            'catalog_id',
            'created',
            'updated',
            'is_top',
            'is_index_carousel'
        ])
        -> from($dbTable -> getTableName(), 'g')
        -> order() -> asc('g.title');

        return $qb;
    } // select()


    protected function handleFilters($qb, $filters)
    {
        if (isset($filters['availability'])) {
            switch ($filters['availability']) {
                case 'available'  : $qb -> where() -> andGt('g.quantity', 0); break;
                case 'unavailable': $qb -> where() -> andLt('g.quantity', 1); break;
            }
        }

        if (isset($filters['sortBy'])) {
            switch ($filters['sortBy']) {
                case 'title-asc' : $qb -> order() -> asc('g.title'); break;
                case 'title-desc': $qb -> order() -> desc('g.title'); break;
                case 'price-asc' : $qb -> order() -> asc('g.price'); break;
                case 'price-desc': $qb -> order() -> desc('g.price'); break;
            }
        }

        if (isset($filters['price-from']) and is_numeric($filters['price-from'])) {
            $qb -> where() -> andGte('g.price', $filters['price-from']);
        }

        if (isset($filters['price-until']) and is_numeric($filters['price-until'])) {
            $qb -> where() -> andLte('g.price', $filters['price-until']);
        }

        return $qb;
    } // handleFilters()

}