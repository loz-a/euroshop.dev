<?php
namespace Catalog\Mapper\Goods;

use Catalog\Entity\Goods\DetailedInterface as GoodsDetailedEntityInterface;
use Catalog\Mapper\TitleSearcherInterface;
use Core\Db\Table\DbTableAwareInterface;
use Zend\Hydrator\HydratorAwareInterface;

interface DetailedInterface extends TitleSearcherInterface, DbTableAwareInterface, HydratorAwareInterface
{
    public function getByPk($id);

    public function getByGoodsId($gid);

    public function insert(GoodsDetailedEntityInterface $goodsDetailed);

    public function update(GoodsDetailedEntityInterface $goodsDetailed);

    public function delete($id);
}