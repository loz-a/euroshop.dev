<?php
namespace Catalog\Mapper;


interface TitleSearcherInterface
{
    public function lookupByTitle($title, $containerId, $excludeId = null);
}