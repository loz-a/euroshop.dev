<?php
namespace Catalog\Mapper\Catalog;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareTrait;
use Core\Stdlib\Hydrator\HydratorAwareTrait;
use Catalog\Entity\Catalog\CatalogInterface;
use Catalog\Hydrator\Catalog as CatalogHydrator;

class Catalogs implements CatalogsInterface
{
    use DbTableAwareTrait, HydratorAwareTrait;

    public function getHydratorAlias()
    {
        return CatalogHydrator::class;
    } // getHydratorAlias()

    /**
     * @param $id
     * @return null|CatalogHydrator
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     * @throws \Core\Stdlib\Hydrator\Exception\UndefinedHydratorException
     */
    public function getByPk($id)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);
        $qb -> where('id', $id);
        $qb -> limit(1);

        $result = $dbTable -> fetchOne($qb);

        return $result ? $this -> getHydrator() -> hydrateCatalog($result) : null;
    } // getByPk()


    public function getCatalogWithChildrenById($id)
    {
        $dbTable = $this -> getDbTable();
        $qb = new QueryBuilder();
        $qb -> select(['id', 'parent_id', 'title', 'slug', 'created', 'updated', 'path'])
            -> from($dbTable -> getRelatedTableName('catalogs_view'))
            -> where()
                -> equal('id', $id) -> orEqual('parent_id', $id);

        return $dbTable -> fetchAll($qb);
    } // getBySlug()


    /**
     * @param $pid
     * @return array
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function getByPid($pid)
    {
        $dbTable = $this -> getDbTable();
        $qb = $this -> select($dbTable);

        if ($pid === 0 or is_null($pid)) {
            $qb -> where() -> isNull('parent_id');
        } else {
            $qb -> where('parent_id', $pid);
        }

        return $dbTable -> fetchAll($qb);
    } // getByPid()


    /**
     * @param CatalogInterface $catalog
     * @return string
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     * @throws \Core\Db\Table\Exception\InvalidConnectionException
     */
    public function insert(CatalogInterface $catalog)
    {
        $table = $this -> getDbTable();

        $table -> insert([
            'parent_id' => $catalog -> getParentId(),
            'title'     => $catalog -> getTitle(),
            'slug'      => $catalog -> getSlug(),
            'created'   => $catalog -> getCreated(),
            'updated'   => $catalog -> getUpdated()
        ]);

        return $table -> getConnection() -> lastInsertId();
    }

    public function update(CatalogInterface $catalog)
    {
        $table = $this -> getDbtable();

        $table -> update([
            'title'     => $catalog -> getTitle(),
            'updated'   => $catalog -> getUpdated()
        ], $catalog -> getId());

        return $catalog -> getId();
    }

    public function delete($id)
    {
        return $this -> getDbTable() -> delete($id);
    }


    public function lookupByParentId($pid, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where('parent_id', $pid)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    } // lookupByParentId()


    public function lookupByTitle($title, $containerId, $excludeId = null)
    {
        $dbTable = $this -> dbTable;

        $qb = new QueryBuilder();
        $qb -> select('id')
            -> from($dbTable -> getTableName())
            -> where() -> equal('title', $title) -> andEqual('parent_id', $containerId)
            -> limit(1);

        if ($excludeId) {
            $qb -> where() -> andNotEqual('id', $excludeId);
        }

        return (bool) $dbTable -> fetchOne($qb);
    } // lookupByParentId()


    /**
     * @param \Core\Db\Table\Table $dbTable
     * @return QueryBuilder
     */
    protected function select($dbTable)
    {
        $sqb = new QueryBuilder();
        $sqb
            -> select() -> count('id')
            -> from($dbTable -> getTableName(), 'subcat')
            -> where() -> predicate('subcat.parent_id=cat.id');

        $qb = new QueryBuilder();
        $qb -> select([
            'id', 'parent_id', 'title', 'slug', 'created', 'updated', 'children_count' => $sqb
        ])
        -> from($dbTable -> getTableName(), 'cat');

        return $qb;
    } // select()
}