<?php
namespace Catalog\Mapper\Catalog\Exception;

class FailedInsertOrUpdateException extends \Exception
    implements ExceptionInterface
{

}