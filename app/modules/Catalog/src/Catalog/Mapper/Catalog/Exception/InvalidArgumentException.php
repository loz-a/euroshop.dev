<?php
namespace Catalog\Mapper\Catalog\Exception;

class InvalidArgumentException extends \InvalidArgumentException
    implements ExceptionInterface
{

}