<?php
namespace Catalog\Mapper\Catalog;

use Catalog\Entity\Catalog\CatalogInterface;
use Core\Db\Table\DbTableAwareInterface;
use Zend\Hydrator\HydratorAwareInterface;
use Catalog\Mapper\TitleSearcherInterface;

interface CatalogsInterface extends TitleSearcherInterface, DbTableAwareInterface, HydratorAwareInterface
{
    public function getByPk($id);

    public function getByPid($pid);

    public function insert(CatalogInterface $catalog);

    public function update(CatalogInterface $catalog);

    public function delete($id);

    public function lookupByParentId($pid, $excludeId = null);

}