<?php
namespace Catalog\Mapper\Catalog;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareInterface;
use Core\Db\Table\DbTableAwareTrait;
use Core\ImageStore\Mapper\MapperInterface;
use Catalog\Entity\Catalog\Image;
use Catalog\Entity\Catalog\ImageInterface;

class CatalogsImageStore implements MapperInterface, DbTableAwareInterface
{
    use DbTableAwareTrait;

    /**
     * @param int $id
     * @return null|Image
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function get($id)
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb -> select('id', 'path', 'parent_id', 'serialized_upload_data')
            -> from($table -> getTableName())
            -> where('id', $id)
            -> limit(1);

        $result = $table -> fetchOne($qb);

        if ($result) {
            $entity = (new Image())
                -> setId($result['id'])
                -> setPath($result['path'])
                -> setParentId($result['parent_id'])
                -> setUploadData($result['serialized_upload_data']);

            $result = $entity;
        }

        return $result;
    } // get()


    /**
     * @param $id
     * @return null|Image
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function getByCatalog($id)
    {
        $table = $this -> getDbTable();
        $qb = $this -> select($table);
        $qb -> lastJoin() -> andEqual('cimages.catalog_id', $id);
        $qb -> limit(1);

        $result = $table -> fetchOne($qb);

        if ($result) {
            $entity = (new Image())
                -> setId($result['id'])
                -> setPath($result['path'])
                -> setParentId($result['parent_id'])
                -> setUploadData($result['serialized_upload_data'])
                -> setCatalogId($result['catalog_id']);

            $result = $entity;
        }

        return $result;
    } // getByCatalog()


    /**
     * @param ImageInterface $image
     * @return $this|ImageInterface|mixed
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     * @throws \Core\Db\Table\Exception\InvalidConnectionException
     */
    public function set($image)
    {
        if (!$image instanceof ImageInterface) {
            throw new Exception\InvalidARgumentException('Invalid argument type. Instance of %s expected', ImageInterface::class);
        }

        $table      = $this  -> getDbTable();
        $connection = $table -> getConnection();
        $id         = $image -> getId();

        $storeData = [
            'path'                   => $image -> getPath(),
            'parent_id'              => $image -> getParentId(),
            'serialized_upload_data' => serialize($image -> getUploadData())
        ];


        $connection -> beginTransaction();
        try {
            if ($id) {
                $table -> update($storeData, $id);
            }
            else {
                $table -> insert($storeData);

                $id = $table -> getConnection() -> lastInsertId();

                $table -> insert([
                    'catalog_id' => $image -> getCatalogId(),
                    'image_id'   => $id
                ],
                $table -> getRelatedTableName('catalogs_images'));
            }
        }
        catch(\Exception $e) {
            $connection -> rollBack();
            throw new Exception\FailedInsertOrUpdateException('Image can\'t insert or update');
        }
        $connection -> commit();

        return $this -> get($id);
    }


    /**
     * @param int $id
     * @return int
     * @throws \Core\Db\Table\Exception\DbTableUndefinedException
     */
    public function delete($id)
    {
        return $this -> getDbTable() -> delete($id);
    }


    /**
     * @param \Core\Db\Table\Table $dbTable
     * @return QueryBuilder
     */
    protected function select($dbTable)
    {
        $qb = new QueryBuilder();
        $qb
            -> select([
                'istore.id', 'istore.path', 'istore.parent_id', 'istore.serialized_upload_data', 'cimages.catalog_id'
            ])
            -> from($dbTable -> getTableName(), 'istore')
            -> join()
                -> with($dbTable -> getRelatedTableName('catalogs_images'), 'cimages')
                -> on('istore.id = cimages.image_id');

        return $qb;
    } // select()

}