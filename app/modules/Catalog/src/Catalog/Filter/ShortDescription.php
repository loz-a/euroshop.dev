<?php
namespace Catalog\Filter;

use Zend\Filter\AbstractFilter;

class ShortDescription extends AbstractFilter
{
    const MAX_SHORT_DESCRIPTION_LENGTH = 85;

    public function filter($value)
    {
        if (strlen($value) > self::MAX_SHORT_DESCRIPTION_LENGTH) {
            $value = substr($value, 0, self::MAX_SHORT_DESCRIPTION_LENGTH);

            $lastDonPos = strrpos($value, '.');
            if (false !== $lastDonPos) {
                $value = substr($value, 0, $lastDonPos + 1);
            }

            // fix error SQLSTATE[HY000]: General error: 1366 Incorrect string value: '\xD0'
            $value = mb_check_encoding($value, 'UTF-8') ? $value : utf8_encode($value);
        }
        return $value;
    }
}