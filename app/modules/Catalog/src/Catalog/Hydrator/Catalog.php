<?php
namespace Catalog\Hydrator;

use Zend\Hydrator\ClassMethods;
use Catalog\Entity\Catalog\Catalog as CatalogEntity;

class Catalog extends ClassMethods
{
    public function hydrateCatalog(array $data)
    {
        return parent::hydrate($data, new CatalogEntity());
    } // hydrateCatalog()
}