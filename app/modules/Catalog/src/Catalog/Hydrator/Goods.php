<?php
namespace Catalog\Hydrator;

use Zend\Hydrator\ClassMethods;
use Catalog\Entity\Goods\Goods as GoodsEntity;

class Goods extends ClassMethods
{
    public function hydrateGoods(array $data)
    {
        return parent::hydrate($data, new GoodsEntity());
    } // hydrateGoods()
}