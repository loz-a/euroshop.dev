<?php
namespace Catalog\Hydrator;

use Zend\Hydrator\ClassMethods;
use Catalog\Entity\Goods\Detailed as GoodsDetailedEntity;

class GoodsDetailed extends ClassMethods
{
    public function hydrateGoodsDetailed(array $data)
    {
        return parent::hydrate($data, new GoodsDetailedEntity());
    } // hydrateGoods()
}