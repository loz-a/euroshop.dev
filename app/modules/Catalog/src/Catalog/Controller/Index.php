<?php
namespace Catalog\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Catalog\Service\Catalog as CatalogService;

class Index extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;
    /**
     * @annotations.template catalog/index/index
     * @annotations.layout   admin/layout/layout
     * @annotations.loginRequired
     */
    public function indexAction()
    {
        return [
            'catalogs' => $this -> get(CatalogService::class) -> getRootCatalogs()
        ];
    } // index()
}