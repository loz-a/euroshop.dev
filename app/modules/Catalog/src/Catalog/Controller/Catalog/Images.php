<?php
namespace Catalog\Controller\Catalog;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Catalog\Service\CatalogUpload as CatalogUploadService;
use Catalog\Validator\Upload\UploadCatalogImage as UploadCatalogImageValidator;
use Core\View\Model\JsonErrorModel;
use Core\View\Model\JsonModel;
use Zend\Http\Response;

class Images extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function uploadAction()
    {
        $request  = $this -> get('request');
        $response = $this -> get('response');

        $post = array_merge_recursive(
            $request -> getPost()  -> toArray(),
            $request -> getFiles() -> toArray()
        );

        $validator = $this -> get(UploadCatalogImageValidator::class) -> setData($post);

        if (!$validator -> isValid()) {
            $response -> setStatusCode(Response::STATUS_CODE_400);
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        $result = $this -> get(CatalogUploadService::class) -> add($validator -> getValues());
        return (new JsonModel()) -> fromArray($result);
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getByCatalogAction()
    {
        $catalogId     = $this -> get('route_match') -> getParam('id');
        $uploadService = $this -> get(CatalogUploadService::class);

        $result =  $uploadService -> getByCatalog($catalogId);
        return (new JsonModel()) -> fromArray($result);
    } // getByCatalogAction()


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function deleteAction()
    {
        $imageId     = $this -> get('route_match') -> getParam('id');
        $uploadService = $this -> get(CatalogUploadService::class);

        $result = $uploadService -> delete($imageId);
        return (new JsonModel()) -> fromArray($result);
    } // deleteAction()

}