<?php
namespace Catalog\Controller\Catalog;

use Catalog\Mapper\Catalog\Catalogs;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\App\Controller\Restful\AbstractRestfulController;
use Catalog\Service\Catalog as CatalogService;
use Catalog\Validator\CatalogDelete as CatalogDeleteValidator;
use Catalog\Validator\Catalog as CatalogValidator;
use Core\View\Model\JsonErrorModel;

class Catalog extends AbstractRestfulController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getList()
    {
        $pid = $this -> get('route_match') -> getParam('pid');
        return $this -> get(CatalogService::class) -> getCatalogChildren($pid);
    } // getList()

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function create($data)
    {
        $validator = $this -> get(CatalogValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        return $this -> get(CatalogService::class) -> create($validator -> getValues());
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function update($id, $data)
    {
        $validator = $this -> get(CatalogValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        $result = $this -> get(CatalogService::class) -> update($data, $id);
        if (false === $result) {
            $result = (new JsonErrorModel()) -> addMessage('message', 'No catalog found');
        }
        return $result;
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function deleteById($id)
    {
        $validator = $this -> get(CatalogDeleteValidator::class);
        $isValid = $validator -> isValid($id);

        if ($isValid) {
            $result = $this -> get(CatalogService::class) -> remove($id);

            if (false === $result) {
                $result = (new JsonErrorModel()) -> addMessage('message', 'No catalog found');
            }
            return $result;
        }

        return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getById($id)
    {
        return $this -> get(CatalogService::class) -> get($id);
    }

}