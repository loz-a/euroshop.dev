<?php
namespace Catalog\Controller\Goods;

use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\App\Controller\Restful\AbstractRestfulController;
use Catalog\Service\GoodsDetailed as GoodsDetailedService;
use Core\View\Model\JsonErrorModel;
use Catalog\Validator\GoodsDetailed as GoodsDetailedValidator;

class Detailed extends AbstractRestfulController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getList()
    {
        $goodsId = $this -> get('route_match') -> getParam('gid');
        return $this -> get(GoodsDetailedService::class) -> getByGoods($goodsId);
    } // getList()


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getById($id)
    {
        $id = $this -> get('route_match') -> getParam('id');
        return $this -> get(GoodsDetailedService::class) -> get($id);
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function create($data)
    {
        $validator = $this -> get(GoodsDetailedValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        return $this -> get(GoodsDetailedService::class) -> create($validator -> getValues());
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function update($id, $data)
    {
        $validator = $this -> get(GoodsDetailedValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        $result = $this -> get(GoodsDetailedService::class) -> update($validator -> getValues(), $id);

        if (false === $result) {
            $result = (new JsonErrorModel()) -> addMessage('message', 'No goods detail found');
        }
        return $result;
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function deleteById($id)
    {
        $result = $this -> get(GoodsDetailedService::class) -> remove($id);

        if (false === $result) {
            $result = (new JsonErrorModel()) -> addMessage('message', 'No goods detail found');
        }

        return $result;
    }
}