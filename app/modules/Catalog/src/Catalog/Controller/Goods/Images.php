<?php
namespace Catalog\Controller\Goods;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Core\View\Model\JsonErrorModel;
use Core\View\Model\JsonModel;
use Zend\Http\Response;
use Catalog\Service\GoodsImages;
use Catalog\Validator\Upload\UploadGoodsImage as UploadGoodsImageValidator;

class Images extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function uploadAction()
    {
        $request  = $this -> get('request');
        $response = $this -> get('response');

        $post = array_merge_recursive(
            $request -> getPost()  -> toArray(),
            $request -> getFiles() -> toArray()
        );

        $validator = $this -> get(UploadGoodsImageValidator::class) -> setData($post);

        if (!$validator -> isValid()) {
            $response -> setStatusCode(Response::STATUS_CODE_400);
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        $result = $this -> get(GoodsImages::class) -> add($validator -> getValues());
        return (new JsonModel()) -> fromArray($result);
    } // uploadAction()


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getByGoodsAction()
    {
        $goodsId = $this -> get('route_match') -> getParam('id');
        $service = $this -> get(GoodsImages::class);

        $result =  $service -> getByGoods($goodsId);
        return (new JsonModel()) -> fromArray($result);
    } // getByGoodsAction()


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function deleteAction()
    {
        $imageId       = $this -> get('route_match') -> getParam('id');
        $uploadService = $this -> get(GoodsImages::class);

        $result = $uploadService -> delete($imageId);
        return (new JsonModel()) -> fromArray($result);
    } // deleteAction()

}