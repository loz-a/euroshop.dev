<?php
namespace Catalog\Controller\Goods;

use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\App\Controller\Restful\AbstractRestfulController;
use Catalog\Service\Goods as GoodsService;
use Catalog\Validator\Goods as GoodsValidator;
use Core\View\Model\JsonErrorModel;


class Goods extends AbstractRestfulController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getList()
    {
        $catalogId = $this -> get('route_match') -> getParam('cid');
        return $this -> get(GoodsService::class) -> getGoodsByCatalog($catalogId);
    } // getList()


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function create($data)
    {
        $validator = $this -> get(GoodsValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        return $this -> get(GoodsService::class) -> create($validator -> getValues());
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function update($id, $data)
    {
        $validator = $this -> get(GoodsValidator::class) -> setData($data);

        if (!$validator -> isValid()) {
            return (new JsonErrorModel()) -> setMessages($validator -> getMessages());
        }

        $goods = $this -> get(GoodsService::class);
        $result = $goods -> update($validator -> getValues(), $id);
        if (false === $result) {
            $result = (new JsonErrorModel()) -> addMessage('message', 'No goods found');
        }

        return $result;
    }


    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function deleteById($id)
    {
        $result = $this -> get(GoodsService::class) -> remove($id);

        if (false === $result) {
            $result = (new JsonErrorModel()) -> addMessage('message', 'No goods found');
        }

        return $result;
    }

    /**
     * @annotations.jsonResponse
     * @annotations.loginRequired
     */
    public function getById($id)
    {
        $id = $this -> get('route_match') -> getParam('id');
        return $this -> get(GoodsService::class) -> get($id);
    }

}