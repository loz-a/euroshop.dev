<?php
namespace Catalog\Entity\Goods;

class Detailed implements DetailedInterface
{
    protected $id;

    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    } // setId()

    public function getId()
    {
        return $this -> id;
    } // getId()


    protected $title;

    public function setTitle($title)
    {
        $this -> title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this -> title;
    }


    protected $value;

    public function setValue($value)
    {
        $this -> value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this -> value;
    }


    protected $gid;

    public function setGoodsId($gid)
    {
        $this -> gid = $gid;
        return $this;
    }

    public function getGoodsId()
    {
        return $this -> gid;
    }


}