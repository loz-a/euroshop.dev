<?php
namespace Catalog\Entity\Goods;

use Core\ImageStore\Entity\Image as BaseImage;

class Image extends BaseImage
    implements ImageInterface
{
    /**
     * @var int
     */
    protected $gid;

    /**
     * @param int $gid
     * @return $this
     */
    public function setGoodsId($gid)
    {
        $this -> gid = $gid;
        return $this;
    }

    /**
     * @return int
     */
    public function getGoodsId()
    {
        return $this -> gid;
    }
}