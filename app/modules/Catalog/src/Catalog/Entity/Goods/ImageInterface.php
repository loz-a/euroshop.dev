<?php
namespace Catalog\Entity\Goods;

use Core\ImageStore\Entity\ImageInterface as BaseImageInterface;

interface ImageInterface extends BaseImageInterface
{
    public function setGoodsId($gid);

    public function getGoodsId();
}