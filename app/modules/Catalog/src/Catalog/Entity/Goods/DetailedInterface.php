<?php
namespace Catalog\Entity\Goods;

interface DetailedInterface
{
    public function setId($id);

    public function getId();

    public function setTitle($title);

    public function getTitle();

    public function setValue($value);

    public function getValue();

    public function setGoodsId($gid);

    public function getGoodsId();
}