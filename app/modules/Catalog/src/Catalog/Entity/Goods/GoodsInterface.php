<?php
namespace Catalog\Entity\Goods;

interface GoodsInterface
{
    public function setId($id);
    public function getId();

    public function setTitle($title);
    public function getTitle();

    public function setCode($code);
    public function getCode();

    public function setSlug($slug);
    public function getSlug();

    public function setDescription($description);
    public function getDescription();

    public function setShortDescription($shortDescription);
    public function getShortDescription();

    public function setProducer($producer);
    public function getProducer();

    public function setPrice($price);
    public function getPrice();

    public function setQuantity($quantity);
    public function getQuantity();

    public function setCatalogId($catalogId);
    public function getCatalogId();

    public function setCreated($created);
    public function getCreated();

    public function setUpdated($updated);
    public function getUpdated();

    public function setIsTop($isTop);
    public function getIsTop();

    public function setIsIndexCarousel($isIndexCarousel);
    public function getIsIndexCarousel();
}