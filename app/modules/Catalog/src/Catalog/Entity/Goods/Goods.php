<?php
namespace Catalog\Entity\Goods;


class Goods implements GoodsInterface
{
    protected $id;

    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    }

    public function getId()
    {
        return $this -> id;
    }


    protected $title;

    public function setTitle($title)
    {
        $this -> title = $title;
        return $this;
    }

    public function getTitle()
    {
        return $this -> title;
    }


    protected $code;

    public function setCode($code)
    {
        $this -> code = $code;
        return $this;
    } // setCode()

    public function getCode()
    {
        return $this -> code;
    } // getCode()


    protected $slug;

    public function setSlug($slug)
    {
        $this -> slug = $slug;
        return $this;
    }

    public function getSlug()
    {
        return $this -> slug;
    }


    protected $description;

    public function setDescription($description)
    {
        $this -> description = $description;
        return $this;
    }

    public function getDescription()
    {
        return $this -> description;
    }


    protected $shortDescription;

    public function setShortDescription($shortDescription)
    {
        $this -> shortDescription = $shortDescription;
        return $this;
    }

    public function getShortDescription()
    {
        return $this -> shortDescription;
    }



    protected $producer;

    public function setProducer($producer)
    {
        $this -> producer = $producer;
        return $this;
    }

    public function getProducer()
    {
        return $this -> producer;
    }


    protected $price;

    public function setPrice($price)
    {
        $this -> price = $price;
        return $this;
    }

    public function getPrice()
    {
        return $this -> price;
    }


    protected $quantity;

    public function setQuantity($quantity)
    {
        $this -> quantity = $quantity;
        return $this;
    }

    public function getQuantity()
    {
        return $this -> quantity;
    }


    protected $catalogId;

    public function setCatalogId($catalogId)
    {
        $this -> catalogId = $catalogId;
        return $this;
    }

    public function getCatalogId()
    {
        return $this -> catalogId;
    }


    protected $created;

    public function setCreated($created)
    {
        $this -> created = $created;
        return $this;
    }

    public function getCreated()
    {
        return $this -> created;
    }


    protected $updated;

    public function setUpdated($updated)
    {
        $this -> updated = $updated;
        return $this;
    }

    public function getUpdated()
    {
        return $this -> updated;
    }

    protected $isTop;

    public function setIsTop($isTop)
    {
        $this -> isTop = $isTop;
        return $this;
    } // setTop()

    public function getIsTop()
    {
        return $this -> isTop;
    } // getIsTop()


    protected $isIndexCarousel;

    public function setIsIndexCarousel($isIndexCarousel)
    {
        $this -> isIndexCarousel = $isIndexCarousel;
        return $this;
    } // setIsIndexCarousel()

    public function getIsIndexCarousel()
    {
        return $this -> isIndexCarousel;
    } // getIsIndexCarousel()

}