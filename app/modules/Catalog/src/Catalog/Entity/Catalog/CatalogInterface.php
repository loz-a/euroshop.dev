<?php
namespace Catalog\Entity\Catalog;


interface CatalogInterface
{
    public function setId($id);
    public function getId();

    public function setParentId($parentId);
    public function getParentId();

    public function setTitle($title);
    public function getTitle();

    public function setSlug($slug);
    public function getSlug();

    public function setCreated($created);
    public function getCreated();

    public function setUpdated($updated);
    public function getUpdated();

    public function setChildrenCount($count);
    public function getChildrenCount();
}