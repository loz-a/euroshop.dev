<?php
namespace Catalog\Entity\Catalog;


class Catalog implements CatalogInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this -> id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this -> id;
    }


    /**
     * @var int
     */
    protected $parentId = 0;

    /**
     * @param int $parentId
     * @return $this
     */
    public function setParentId($parentId)
    {
        $this -> parentId = $parentId;
        return $this;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this -> parentId;
    }


    /**
     * @var string
     */
    protected $title;

    /**
     * @param $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this -> title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this -> title;
    }


    /**
     * @var string
     */
    protected $slug;

    /**
     * @param $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this -> slug = $slug;
        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this -> slug;
    }


    /**
     * @var int
     */
    protected $created;

    /**
     * @param $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this -> created = $created;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreated()
    {
        return $this -> created;
    }


    /**
     * @var int
     */
    protected $updated;

    /**
     * @param $updated
     * @return $this
     */
    public function setUpdated($updated)
    {
        $this -> updated = $updated;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdated()
    {
        return $this -> updated;
    }


    /**
     * @var int
     */
    protected $childrenCount;

    /**
     * @param int $count
     * @return $this
     */
    public function setChildrenCount($count)
    {
        $this -> childrenCount = $count;
        return $this;
    } // setChildrenCount()

    /**
     * @return int
     */
    public function getChildrenCount()
    {
        return $this -> childrenCount;
    } // getChildrenCount()

}