<?php
namespace Catalog\Entity\Catalog;

use Core\ImageStore\Entity\Image as BaseImage;

class Image extends BaseImage
    implements ImageInterface
{
    /**
     * @var int
     */
    protected $cid;

    /**
     * @param int $cid
     * @return $this
     */
    public function setCatalogId($cid)
    {
        $this -> cid = $cid;
        return $this;
    }

    /**
     * @return int
     */
    public function getCatalogId()
    {
        return $this -> cid;
    }
}