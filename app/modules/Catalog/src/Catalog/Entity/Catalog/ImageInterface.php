<?php
namespace Catalog\Entity\Catalog;

use Core\ImageStore\Entity\ImageInterface as BaseImageInterface;

interface ImageInterface extends BaseImageInterface
{
    public function setCatalogId($cid);

    public function getCatalogId();
}