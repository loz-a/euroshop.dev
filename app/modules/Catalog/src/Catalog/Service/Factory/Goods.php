<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Service\Goods as GoodsService;
use Catalog\Mapper\Goods\Goods as GoodsMapper;

class Goods implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new GoodsService();
        $service -> setMapper($container -> get(GoodsMapper::class));
        return $service;
    }

}