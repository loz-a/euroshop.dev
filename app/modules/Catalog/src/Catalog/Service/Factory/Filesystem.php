<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Core\ImageStore\Filesystem\Filesystem as ImageStoreFilesystem;
use Core\ImageStore\Filesystem\Adapter\FlySystemAdapter;

class Filesystem implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {

        $flySystemAdapter = new FlySystemAdapter([
            'adapter' => 'League\Flysystem\Adapter\Local',
            'options' => [
                $container -> get('request') -> getServer() -> get('DOCUMENT_ROOT')
            ]
        ]);

        $service = new ImageStoreFilesystem($flySystemAdapter);

        return $service;
    }

}