<?php
namespace Catalog\Service\Factory\Exception;

class UndefinedConfigException extends \Exception
    implements ExceptionInterface
{

}