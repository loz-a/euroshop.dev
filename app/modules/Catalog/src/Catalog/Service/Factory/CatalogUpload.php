<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Service\CatalogUpload as UploadService;
use Catalog\Mapper\Catalog\CatalogsImageStore as ImageStoreMapper;
use Core\ImageStore\Uploader\Uploader;
use Core\ImageStore\Store;

class CatalogUpload implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config  = $container -> get('config');

        if (!isset($config -> image_store)
            or !isset($config -> image_store -> catalog))
        {
            throw new Exception\UndefinedConfigException('Image store catalog config is undefined');
        }

        $config       = $config -> image_store -> catalog;
        $filesystem   = $container -> get(Filesystem::class);
        $imageManager = $container -> get(InterventionImageManager::class);

        $uploader = new Uploader($filesystem, $imageManager, $config -> uploader -> catalogs -> toArray());

        $store = new Store();
        $store
            -> setUploader($uploader)
            -> setMapper($container -> get(ImageStoreMapper::class));

        $service = new UploadService();
        $service -> setImageStore($store);

        return $service;
    }

}