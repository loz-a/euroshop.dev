<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Intervention\Image\ImageManager;

class InterventionImageManager implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container -> get('config');

        if (!isset($config -> image_store)
            or !isset($config -> image_store -> catalog))
        {
            throw new Exception\UndefinedConfigException('Image store catalog config is undefined');
        }
        $config = $config -> image_store -> catalog;

        $driverName = isset($config -> image_manager_driver)
                    ? $config -> image_manager_driver : 'imagick';

        return new ImageManager(['driver' => $driverName]);
    }
}