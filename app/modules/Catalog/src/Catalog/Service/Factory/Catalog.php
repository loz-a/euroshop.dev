<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Service\Catalog as CatalogService;
use Catalog\Mapper\Catalog\Catalogs as CatalogMapper;

class Catalog implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new CatalogService();
        $service -> setMapper($container -> get(CatalogMapper::class));
        return $service;
    }

} 