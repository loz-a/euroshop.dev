<?php
namespace Catalog\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Catalog\Service\GoodsDetailed as GoodsDetailedService;
use Catalog\Mapper\Goods\GoodsDetailed as GoodsDetailedMapper;

class GoodsDetailed implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new GoodsDetailedService();
        $service -> setMapper($container -> get(GoodsDetailedMapper::class));
        return $service;
    }

}