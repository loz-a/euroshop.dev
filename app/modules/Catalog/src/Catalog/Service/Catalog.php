<?php
namespace Catalog\Service;

use Catalog\Mapper\Catalog\CatalogsInterface as CatalogsInterface;
use Core\Stdlib\Filter\Translit;

class Catalog
{
    /**
     * @var CatalogsInterface
     */
    protected $mapper;

    /**
     * @var \Zend\Filter\AbstractFilter
     */
    protected $translitFilter;


    public function getRootCatalogs()
    {
        return $this -> getMapper() -> getByPid(0);
    } // getAll()


    public function getCatalogChildren($pid)
    {
        return $this -> getMapper() -> getByPid($pid);
    } // getCatalogChildren()


    public function getCatalogWithChildren($id)
    {
        $result = $this -> getMapper() -> getCatalogWithChildrenById($id);

        if ($result) {
            $catalog  = null;
            $children = [];
            foreach ($result as $item) {
                if ($item['id'] == $id) {
                    $catalog = $item;
                } else {
                    $children[] = $item;
                }
            }
            $catalog['children'] = $children;
            return $catalog;
        }

        return $result;
    } // getCatalogWithChildren()


    /**
     * @param int $id
     * @return \Catalog\Hydrator\Catalog|null
     * @throws Exception\UndefinedCatalogsMapperException
     */
    public function get($id)
    {
        $mapper   = $this -> getMapper();
        $hydrator = $mapper -> getHydrator();

        return $hydrator -> extract($mapper -> getByPk($id));
    } // get()


    public function create(array $data)
    {
        $data['slug']      = $this -> getTranslitFilter() -> filter($data['title']);
        $data['created']   = round(microtime(true) * 1000);
        $data['updated']   = $data['created'];

        if ($data['parent_id'] === 0) {
            $data['parent_id'] = null;
        }

        $mapper   = $this     -> getMapper();
        $hydrator = $mapper   -> getHydrator();
        $entity   = $hydrator -> hydrateCatalog($data);

        $insertedId = $mapper -> insert($entity);

        return $hydrator -> extract($mapper -> getByPk($insertedId));
    } // create()


    public function update(array $data, $id)
    {
        $data['updated'] = round(microtime(true) * 1000);

        $mapper     = $this   -> getMapper();
        $hydrator   = $mapper -> getHydrator();
        $catalog = $mapper -> getByPk($id);

        if (null === $catalog) {
            return false;
        }

        $catalog = $hydrator -> hydrate($data, $catalog);
        $mapper -> update($catalog);
        return $hydrator -> extract($mapper -> getByPk($id));
    } // update()


    public function remove($id)
    {
        $mapper  = $this -> getMapper();
        $catalog = $mapper -> getByPk($id);

        if (!$catalog) {
            return false;
        }

        $mapper -> delete($id);
        return $mapper -> getHydrator() -> extract($catalog);
    } // remove()


    /**
     * @return \Zend\Filter\AbstractFilter
     */
    public function getTranslitFilter()
    {
        if (null === $this -> translitFilter) {
            $this -> translitFilter = (new Translit())
                -> filterOnlyLetters()
                -> setLowerCase()
                -> setSpaceReplacer('-');
        }
        return $this -> translitFilter;
    } // getTranlateFilter()


    /**
     * @param CatalogsInterface $mapper
     * @return $this
     */
    public function setMapper(CatalogsInterface $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()

    /**
     * @return CatalogsMapper
     * @throws Exception\UndefinedCatalogsMapperException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedCatalogsMapperException('Catalogs mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()
}