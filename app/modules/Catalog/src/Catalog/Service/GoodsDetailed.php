<?php
namespace Catalog\Service;

use Catalog\Mapper\Goods\DetailedInterface as GoodsDetailedMapperInterface;

class GoodsDetailed
{
    /**
     * @var GoodsDetailedMapperInterface
     */
    protected $mapper;

    public function getByGoods($gid)
    {
        return $this -> getMapper() -> getByGoodsId($gid);
    } // getByGoods()


    public function get($id)
    {
        $mapper   = $this -> getMapper();
        $hydrator = $mapper -> getHydrator();

        return $hydrator -> extract($mapper -> getByPk($id));
    } // get()


    public function create(array $data)
    {
        $mapper   = $this -> getMapper();
        $table    = $mapper -> getdbTable();
        $hydrator = $mapper -> getHydrator();
        $entity   = $hydrator -> hydrateGoodsDetailed($data);

        $table -> getConnection() -> beginTransaction();
        try {
            $insertedId = $mapper -> insert($entity);

            $table -> update(
                [
                    'updated' => round(microtime(true) * 1000)
                ],
                $entity -> getGoodsId(),
                $table -> getRelatedTableName('goods')
            );

        } catch(\Exception $e) {
            $table -> getConnection() -> rollBack();
            throw new Exception\FailedInsertException('Goods can\'t updated');
        }
        $table -> getConnection() -> commit();

        return $hydrator -> extract($mapper -> getByPk($insertedId));
    } // create()


    public function update(array $data, $id)
    {
        $mapper   = $this   -> getMapper();
        $table    = $mapper -> getdbTable();
        $hydrator = $mapper -> getHydrator();
        $entity   = $mapper -> getByPk($id);

        if (null === $entity) {
            return false;
        }

        $entity = $hydrator -> hydrate($data, $entity);

        $table -> getConnection() -> beginTransaction();
        try {
            $mapper -> update($entity);

            $table -> update([
                    'updated' => round(microtime(true) * 1000)
                ],
                $entity -> getGoodsId(),
                $table -> getRelatedTableName('goods')
            );

        } catch(\Exception $e) {
            $table -> getConnection() -> rollBack();
            throw new Exception\FailedUpdateException('Goods can\'t updated');
        }
        $table -> getConnection() -> commit();
        return $hydrator -> extract($mapper -> getByPk($id));
    } // update()


    public function remove($id)
    {
        $mapper = $this -> getMapper();
        $goodsAttribute = $mapper -> getByPk($id);

        if (!$goodsAttribute) {
            return false;
        }

        $mapper -> delete($id);
        return $mapper -> getHydrator() -> extract($goodsAttribute);
    } // remove()


    /**
     * @param GoodsDetailedMapperInterface $mapper
     * @return $this
     */
    public function setMapper(GoodsDetailedMapperInterface $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return GoodsDetailedMapperInterface
     * @throws Exception\UndefinedGoodsDetailedMapperException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedGoodsDetailedMapperException('GoodsDetailed mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()

}