<?php
namespace Catalog\Service;

use Catalog\Service\Exception\UndefinedImageStoreException;
use Core\ImageStore\StoreInterface;
use Catalog\Entity\Catalog\Image;
use Zend\Hydrator\ClassMethods as Hydrator;

class CatalogUpload
{
    /**
     * @var StoreInterface
     */
    protected $imageStore;

    /**
     * @param $id
     * @return array
     * @throws UndefinedImageStoreException
     */
    public function getByCatalog($id)
    {
        $entity = $this -> getImageStore() -> getMapper() -> getByCatalog($id);
        return $this -> extractEntity($entity);
    } // getByCatalog()


    /**
     * @param $data
     * @return array
     * @throws UndefinedImageStoreException
     */
    public function add($data)
    {
        $imageStore = $this -> getImageStore();
        $entity     = $imageStore -> getMapper() -> getByCatalog($data['catalog_id']);

        if ($entity) {
            $imageStore -> getUploader() -> delete($entity -> getPath());
        }
        else {
            $entity = new Image();
            $entity -> setCatalogId($data['catalog_id']);
        }

        $entity -> setUploadData($data['image']);

        $callback = function($path, $uploader) use ($data) {
            $pi = pathinfo($path);
            $newPath = sprintf('%s/%s-%s.%s', $pi['dirname'], intval($data['catalog_id']), time(), $pi['extension']);
            $uploader -> getFilesystem() -> rename($path, $newPath);
            return $newPath;
        };

        $entity = $imageStore -> put($entity, $callback);

        return $this -> extractEntity($entity);
    } // add()


    public function delete($imageId)
    {
        $entity = $this -> getImageStore() -> getMapper() -> get($imageId);

        if ($entity) {
            $this -> getImageStore() -> delete($entity);
        }

        return $this -> extractEntity($entity);
    } // delete()


    /**
     * @param StoreInterface $store
     * @return $this
     */
    public function setImageStore(StoreInterface $store)
    {
        $this -> imageStore = $store;
        return $this;
    } // setImagesStore()


    /**
     * @return StoreInterface
     * @throws UndefinedImageStoreException
     */
    public function getImageStore()
    {
        if (null === $this -> imageStore) {
            throw new UndefinedImageStoreException('ImageStore is undefined');
        }
        return $this -> imageStore;
    } // getImageStore()


    protected function extractEntity($entity)
    {
        $extracted = [];

        if ($entity) {
            $extracted = (new Hydrator()) -> extract($entity);
            unset($extracted['upload_data']);
        }
        return $extracted;
    } // extractEntity()

}