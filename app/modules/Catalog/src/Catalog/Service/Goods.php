<?php
namespace Catalog\Service;

use Catalog\Mapper\Goods\GoodsInterface as GoodsMapperInterface;
use Core\Stdlib\Filter\Translit;
use Catalog\Filter\ShortDescription;
use Zend\Paginator\Adapter\ArrayAdapter;
use \Zend\Paginator\Paginator;

class Goods
{
    /**
     * @var GoodsMapperInterface
     */
    protected $mapper;

    /**
     * @var \Zend\Filter\AbstractFilter
     */
    protected $translitFilter;

    /**
     * @var\Zend\Filter\Abstract
     */
    protected $shortDescriptionFilter;


    /**
     * @param $id
     * @return array
     * @throws Exception\UndefinedGoodsMapperException
     */
    public function get($id)
    {
        $mapper   = $this -> getMapper();
        $hydrator = $mapper -> getHydrator();

        return $hydrator -> extract($mapper -> getByPk($id));
    } // getGoods()


    public function getGoodsByCatalog($catalogId)
    {
        return $this -> getMapper() -> getByCatalogId($catalogId);
    } // getGoodsByCatalog()


    public function getPagingListByCatalogId($catalogId, array $filters = [])
    {
        $goods     = $this -> getMapper() -> getListByCatalogId($catalogId, $filters);
        $paginator = new Paginator(new ArrayAdapter($goods));

        $itemCountPerPage = 12;
        if (isset($filters['perPage'])) {
            $perPage = (int) $filters['perPage'];

            if ($perPage > 0) {
                $itemCountPerPage = $filters['perPage'];
            }
        }
        $paginator -> setItemCountPerPage($itemCountPerPage);

        return $paginator;
    } // getPagingListByCatalogId()


    public function getSearchResult($keyword)
    {
        $keyword = trim($keyword);
        return $this -> getMapper() -> searchByTitleOrCode($keyword);
    } // getSearchResult()


    public function create(array $data)
    {
        $data['slug']    = $this -> getTranslitFilter() -> filter($data['title']);

        $data['short_description'] = (empty($data['short_description']))
            ? $this -> getShortDescriptionFilter() -> filter($data['description']) : $data['short_description'];

        $data['created'] = round(microtime(true) * 1000);
        $data['updated'] = $data['created'];
        $data['code']    = $this -> generateCode($data['slug']);

        $mapper   = $this     -> getMapper();
        $hydrator = $mapper   -> getHydrator();
        $entity   = $hydrator -> hydrateGoods($data);

        $insertedId = $mapper -> insert($entity);

        return $hydrator -> extract($mapper -> getByPk($insertedId));
    } // create()


    public function update(array $data, $id)
    {
        $data['shortDescription'] = isset($data['short_description']) ? $data['short_description']
                                        : $this -> getShortDescriptionFilter() -> filter($data['description']);

        $data['update'] = round(microtime(true) * 1000);
        $data['price']  = (float) $data['price'];

        $mapper   = $this   -> getMapper();
        $hydrator = $mapper -> getHydrator();
        $entity   = $mapper -> getByPk($id);

        if (null === $entity) {
            return false;
        }

        $entity = $hydrator -> hydrate($data, $entity);
        $mapper -> update($entity);
        return $hydrator -> extract($mapper -> getByPk($id));
    } // update()


    public function remove($id)
    {
        $mapper  = $this -> getMapper();
        $entity = $mapper -> getByPk($id);

        if (!$entity) {
            return false;
        }

        $mapper -> delete($id);
        return $mapper -> getHydrator() -> extract($entity);
    } // remove()


    /**
     * @return \Zend\Filter\AbstractFilter
     */
    public function getTranslitFilter()
    {
        if (null === $this -> translitFilter) {
            $this -> translitFilter = (new Translit())
                -> filterOnlyLetters()
                -> setLowerCase()
                -> setSpaceReplacer('-');
        }
        return $this -> translitFilter;
    } // getTranlateFilter()


    /**
     * @return ShortDescription
     */
    public function getShortDescriptionFilter()
    {
        if (null === $this -> shortDescriptionFilter) {
            $this -> shortDescriptionFilter = new ShortDescription();
        }
        return $this -> shortDescriptionFilter;
    } // getShortDescriptionFilter()


    /**
     * @param GoodsMapperInterface $mapper
     * @return $this
     */
    public function setMapper(GoodsMapperInterface $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return GoodsMapperInterface
     * @throws Exception\UndefinedGoodsMapperException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\UndefinedGoodsMapperException('Goods mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()


    protected function generateCode($slug)
    {
        $prefix  = substr($slug, 0, 2);
        $count   = $this -> getMapper() -> getCountBySlugSubstring($prefix);
        return sprintf('%s-%05d', $prefix, $count);
    } // generateCode()

}