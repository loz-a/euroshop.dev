<?php
namespace PriceList\Mapper;

use Core\Db\Table\DbTableAwareInterface;

interface GoodsInterface extends DbTableAwareInterface
{
    public function fetchAll();
}