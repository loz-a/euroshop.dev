<?php
namespace PriceList\Mapper;

use Core\Db\Sql\QueryBuilder;
use Core\Db\Table\DbTableAwareTrait;
use PDO;

class Goods implements GoodsInterface
{
    use DbTableAwareTrait;

    public function fetchAll()
    {
        $table = $this -> getDbTable();
        $qb    = new QueryBuilder();

        $qb -> select(['g.code', 'catalog' => 'c.title', 'g.title', 'g.short_description', 'g.producer', 'g.price'])
            -> from($table -> getTableName(), 'g')
            -> join()
                -> with($table -> getRelatedTableName('catalogs'), 'c')
                -> on('g.catalog_id = c.id')
            -> order()
                -> asc('catalog')
                -> asc('g.producer')
                -> asc('g.title');

        return $table -> fetchAll($qb, null, PDO::FETCH_NUM);
    } // fetchAll()



//    public function getListByCatalogId($catalogId, array $filters = [])
//    {
//        $table = $this -> getDbTable();
//        $qb    = new QueryBuilder();
//
//        $qb
//            -> select([
//                'g.id', 'g.title', 'g.slug', 'description' => 'g.short_description', 'g.producer', 'g.quantity', 'g.price', 'image_min' => 'istore.path'
//            ])
//            -> from($table -> getTableName(), 'g')
//            -> join()
//                -> left($table -> getRelatedTableName('goods_images'), 'gi')
//                -> on('g.id = gi.goods_id')
//            -> join()
//                -> left($table -> getRelatedTableName('image_store'), 'istore')
//                -> on('gi.image_id = istore.parent_id') -> andEqual('istore.alias', 'min')
//            -> where('g.catalog_id', $catalogId)
//            -> group() -> by('g.id');
//
//        if (count($filters)) {
//            $qb = $this -> handleFilters($qb, $filters);
//        }
//        else {
//            $qb -> order() -> asc('g.title');
//        }
//
//        return $table -> fetchAll($qb);
//    } // getListByCatalogId()

}