<?php
namespace PriceList\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use PriceList\Service\PriceList;
use Zend\Http\Headers;
use Zend\Http\Response\Stream as ResponseStream;

class Index extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template price-list/index/index
     * @annotations.layout   index/layout/layout
     */
    public function indexAction()
    {
        return [];
    } // index()


    public function downloadExcelAction()
    {
        header('Content-Type: application/vnd.ms-excel');
        header(sprintf('Content-Disposition: attachment;filename="prices_%s.xls"', date('d_m_Y')));
        header('Cache-Control: max-age=0');

        $this -> get(PriceList::class) -> generateExcelDocument();
        exit;
    } // downloadExcelAction()


    public function downloadPdfAction()
    {
        header('Content-type:Application/pdf');
        header(sprintf('Content-Disposition: attachment;filename="prices_%s.pdf"', date('d_m_Y')));
        header('Cache-Control: max-age=0');

        $this -> get(PriceList::class) -> generatePdfDocument();
        exit;
    } // downloadPdfAction()
}