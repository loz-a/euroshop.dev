<?php
namespace PriceList\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use PriceList\Service\PriceList as PriceListService;
use PriceList\Mapper\Goods as GoodsMapper;

class PriceList implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new PriceListService();
        $service -> setMapper($container -> get(GoodsMapper::class));
        return $service;
    }
}