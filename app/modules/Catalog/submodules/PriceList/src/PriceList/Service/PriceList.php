<?php
namespace PriceList\Service;

use PriceList\Mapper\Goods as GoodsMapper;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_PageSetup;
use PHPExcel_STYLE_ALIGNMENT;
use PHPExcel_STYLE_FILL;
use PHPExcel_Style_Border;
use PHPExcel_Settings;

class PriceList
{
    /**
     * @var GoodsMapper
     */
    protected $mapper;

    public function generateExcelDocument()
    {
        $mapper = $this -> getMapper();
        $goods = $mapper -> fetchAll();

        $objPHPExcel = new PHPExcel();

        $objPHPExcel -> getProperties()
                    ->setTitle("Price List")
                    ->setSubject("Price List")
                    ->setDescription("")
                    ->setKeywords("");

        $objPHPExcel -> getDefaultStyle() -> getFont()
            -> setName('Arial')
            -> setSize(8);

        array_unshift($goods, ['Код товара', 'Каталог', 'Назва', 'Опис', 'Виробник', 'Ціна']);

        $activeSheet = $objPHPExcel -> getActiveSheet();

        $activeSheet -> fromArray($goods);

        $activeSheet -> getPageSetup()
            -> setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT)
            -> setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $activeSheet -> getPageMargins()
            -> setTop(1)
            -> setRight(0.75)
            -> setLeft(0.75)
            -> setBottom(1);

        $activeSheet -> setTitle('Прайс-лист');
        $activeSheet -> getRowDimension('1') -> setRowHeight(16);

        $activeSheet -> getColumnDimension('A') -> setWidth(14);
        $activeSheet -> getColumnDimension('B') -> setWidth(14);
        $activeSheet -> getColumnDimension('C') -> setWidth(20);
        $activeSheet -> getColumnDimension('D') -> setWidth(40);
        $activeSheet -> getColumnDimension('E') -> setWidth(14);
        $activeSheet -> getColumnDimension('F') -> setWidth(10);


        $activeSheet -> getStyle('A1:F1') -> applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER
            ],
            'fill' => [
                'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
                'color' => [
                    'rgb' => 'DEDEDE'
                ]

            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => [
                        'rgb'=>'999999'
                    ]
                ]
            ]
        ]);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    } // generateExcelDocument()


    public function generatePdfDocument()
    {
        $mapper = $this -> getMapper();
        $goods = $mapper -> fetchAll();

        $rendererName = PHPExcel_Settings::PDF_RENDERER_TCPDF;
        $rendererLibraryPath = getcwd() . '/vendor/tecnickcom/tcpdf';

        $objPHPExcel = new PHPExcel();

        $objPHPExcel -> getProperties()
                     ->setTitle("Price List")
                     ->setSubject("Price List")
                     ->setDescription("")
                     ->setKeywords("");

        $objPHPExcel -> getDefaultStyle() -> getFont()
                     -> setName('Arial')
                     -> setSize(8);

        array_unshift($goods, ['Код товара', 'Каталог', 'Назва', 'Опис', 'Виробник', 'Ціна']);

        $activeSheet = $objPHPExcel -> getActiveSheet();

        $activeSheet -> fromArray($goods);

        $activeSheet -> getPageSetup()
                     -> setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT)
                     -> setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

        $activeSheet -> getPageMargins()
                     -> setTop(1)
                     -> setRight(0.75)
                     -> setLeft(0.75)
                     -> setBottom(1);

        $activeSheet -> setTitle('Прайс-лист');
        $activeSheet -> setShowGridLines(false);
        $activeSheet -> getRowDimension('1') -> setRowHeight(16);

        $activeSheet -> getColumnDimension('A') -> setWidth(14);
        $activeSheet -> getColumnDimension('B') -> setWidth(14);
        $activeSheet -> getColumnDimension('C') -> setWidth(20);
        $activeSheet -> getColumnDimension('D') -> setWidth(40);
        $activeSheet -> getColumnDimension('E') -> setWidth(14);
        $activeSheet -> getColumnDimension('F') -> setWidth(10);


        $activeSheet -> getStyle('A1:F1') -> applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 10
            ],
            'alignment' => [
                'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
                'vertical'   => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER
            ],
            'fill' => [
                'type' => PHPExcel_STYLE_FILL::FILL_SOLID,
                'color' => [
                    'rgb' => 'DEDEDE'
                ]

            ],
        ]);

        if (!PHPExcel_Settings::setPdfRenderer(
            $rendererName,
            $rendererLibraryPath
        )) {
            throw new \Exception('Invalid pdf library');
        }


        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
        $objWriter->save('php://output');
    } // generatePdfDocument()


    public function setMapper(GoodsMapper $mapper)
    {
        $this -> mapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return GoodsMapper
     * @throws Exception\MapperUndefinedException
     */
    public function getMapper()
    {
        if (null === $this -> mapper) {
            throw new Exception\MapperUndefinedException('Mapper is undefined');
        }
        return $this -> mapper;
    } // getMapper()
}