<?php
return [

    'routes' => [
        'price_list' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/price-list',
                'defaults' => [
                    'controller' => PriceList\Controller\Index::class,
                    'action'     => 'index'
                ]
            ],
            'may_terminate' => true,
            'child_routes'  => [
                'download_excel' => [
                    'type'    => 'Literal',
                    'options' => [
                        'route'    => '/download-excel',
                        'defaults' => [
                            'action' => 'downloadExcel'
                        ]
                    ]
                ],
                'download_pdf' => [
                    'type'    => 'Literal',
                    'options' => [
                        'route'    => '/download-pdf',
                        'defaults' => [
                            'action' => 'downloadPdf'
                        ]
                    ]
                ]
            ]
        ]
    ],

    'db' => [
        'tables_names' => [
            'price_list_goods' => [
                'goods'    => 'catalog_goods',
                'catalogs' => 'catalog_catalogs'
            ],
        ]
    ],

    'view' => [
        'template_path_stack' => [
            __DIR__ . '/templates'
        ],
        'template_map' => [
            'price-list/index/index' => __DIR__ . '/templates/price-list/index/index.phtml',
        ]
    ],

    'service_manager' => [
        'invokables' => [
            PriceList\Controller\Index::class => PriceList\Controller\Index::class,
            PriceList\Mapper\Goods::class     => PriceList\Mapper\Goods::class,
        ],
        'factories' => [
            PriceList\Service\PriceList::class => PriceList\Service\Factory\PriceList::class
        ]
    ]

];