<?php
namespace Auth\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Core\App\Controller\Action\FlashMessengerTrait;
use Core\App\Controller\Action\RedirectTrait;
use Auth\Core\Result as AuthResult;

class Login extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait,
        AnnotationsManagerAwareTrait,
        RedirectTrait,
        FlashMessengerTrait;

    /**
     * @annotations.template  auth/login/index
     * @annotations.request str login, str password
     */
    public function indexAction($login = '', $password = '')
    {
        if ($this -> get('request') -> isPost()) {

            $login = trim($login);
            $result = $this -> get('auth.service.user') -> login($login, $password);

            if ($result -> isValid()) {
                $this -> messageSuccess('Now you are logging');
                $this -> redirect($this -> getApplication() -> getRoutesManager() -> urlFor('home'));
            }
            else {
                $this -> messageError(
                    AuthResult::FAILURE_TIMEOUT === $result -> getCode()
                        ? $result -> getFirstMessage()
                        : 'Authorization error. Please check login or/and password'
                );

                sleep(1); // small bruteforce shield
            }
        }
        return array();
    } // indexAction()


    /**
     * @annotations.loginRequired
     */
    public function logoutAction()
    {
        $this -> get('auth.service.user') -> logout();
        $this -> messageInfo('Logout successful');
        $this -> redirect($this -> getApplication() -> getRoutesManager() -> urlFor('home'));
    } // logoutAction()
}
