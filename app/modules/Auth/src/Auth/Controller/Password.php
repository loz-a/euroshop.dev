<?php
namespace Auth\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\Action\RedirectTrait;
use Core\App\Controller\Action\FlashMessengerTrait;
use Core\App\Controller\AbstractController;

class Password extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait,
        AnnotationsManagerAwareTrait,
        RedirectTrait,
        FlashMessengerTrait;

    /**
     * @annotations.template  auth/password/index
     * @annotations.layout    admin/layout/layout
     * @annotations.loginRequired
     */
    public function indexAction()
    {
        return [];
    } // indexAction()


    /**
     * @annotations.template  auth/password/index
     * @annotations.layout    admin/layout/layout
     * @annotations.loginRequired
     */
    public function changeAction()
    {
        $post        = $this -> get('request') -> getPost() -> toArray();
        $userService = $this -> get('auth.service.user');
        $validator   = $this -> get('auth.validator.change_password') -> setData($post);

        if ($validator -> isValid()) {
            $result = $userService -> changePassword($validator -> getValue('new_password'));

            if ($result) {
                $this -> messageSuccess('Password was successfully changed');
                $userService -> logout();

                $this -> redirect(url_for('home'));
            }
        }

        return [
            'messages' => $validator -> getMessages()
        ];
    } // changeAction()
}
