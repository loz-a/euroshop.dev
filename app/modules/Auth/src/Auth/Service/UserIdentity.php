<?php
namespace Auth\Service;

use Auth\Mapper\Users as UsersMapper;
use Zend\Authentication\AuthenticationServiceInterface;

class UserIdentity
{
    /**
     * @var AuthenticationServiceInterface
     */
    protected $authService;

    /**
     * @var UsersMapper
     */
    protected $userMapper;

    /**
     * @var \stdClass
     */
    protected $user;

    /**
     * @return \stdClass
     */
    public function getIdentity()
    {
        $user = $this -> user;
        $auth = $this -> getAuthService();

        if (!$auth -> hasIdentity()) {
            $this -> user = null;
            return null;
        }

        $identity = $auth -> getIdentity();
        if (!$user or $user -> id != $identity) {
            $user = $this -> getMapper() -> findUserById($identity);

            if (!$user) {
                return null;
            }

            $this -> user = new \stdClass();
            $this -> user -> id    = $user -> getId();
            $this -> user -> login = $user -> getLogin();
            $this -> user -> salt  = $user -> getSalt();
        }
        return $this -> user;
    } // getIdentity()


    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this -> getIdentity() -> {$name};
    } // __get()


    /**
     * @return bool
     */
    public function hasIdentity()
    {
        return $this -> getAuthService() -> hasIdentity();
    } // hasIdentity()


    /**
     * @param AuthenticationServiceInterface $authService
     * @return $this
     */
    public function setAuthService(AuthenticationServiceInterface $authService)
    {
        $this -> authService = $authService;
        return $this;
    } // setAuthService()

    /**
     * @return AuthenticationServiceInterface
     * @throws Exception\InvalidAuthServiceException
     */
    public function getAuthService()
    {
        if (!$this -> authService) {
            throw new Exception\InvalidAuthServiceException('Auth service is undefined');
        }
        return $this -> authService;
    } // getAuthService()


    /**
     * @param UsersMapper $mapper
     * @return $this
     */
    public function setMapper(UsersMapper $mapper)
    {
        $this -> userMapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return UsersMapper
     * @throws Exception\InvalidMapperException
     */
    protected function getMapper()
    {
        if (!$this -> userMapper) {
            throw new Exception\InvalidMapperException('User mapper is undefined');
        }
        return $this -> userMapper;
    } // getMapper()

} // UserIdentity
