<?php
namespace Auth\Service\Exception;

class UserIdentityUndefinedException extends \Exception
{}