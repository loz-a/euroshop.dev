<?php
namespace Auth\Service\Exception;

class AuthenticationServiceUndefinedException extends \Exception
{}