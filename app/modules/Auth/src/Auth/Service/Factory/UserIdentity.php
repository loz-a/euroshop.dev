<?php
namespace Auth\Service\Factory;

use Interop\Container\ContainerInterface;
use Auth\Service\UserIdentity as UserIdentityService;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserIdentity implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userIdentity = new UserIdentityService();
        $userIdentity
            -> setAuthService($container -> get('auth'))
            -> setMapper($container -> get('mapper.users'));

        return $userIdentity;
    } // createService()
} 