<?php
namespace Auth\Service\Factory;

use Interop\Container\ContainerInterface;
use Auth\Service\User as UserService;
use Zend\ServiceManager\Factory\FactoryInterface;

class User implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options      = $container -> get('auth.options');
        $mapper       = $container -> get('mapper.users');
        $userIdentity = $container -> get('auth.service.user_identity');
        $authService  = $container -> get('auth');

        $service = new UserService($options);
        $service
            -> setMapper($mapper)
            -> setUserIdentity($userIdentity)
            -> setAuthService($authService);

        return $service;
    } // createService()
}