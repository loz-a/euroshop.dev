<?php
namespace Auth\Service;

use Auth\Service\Exception\UserIdentityUndefinedException;
use Auth\Options\ModuleOptions as AuthOptions;
use Auth\Mapper\Users as UsersMapper;
use Zend\Crypt\Password\Bcrypt;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Math\Rand;

class User
{
    /**
     * @var AuthOptions
     */
    protected $authOptions;

    /**
     * @var AuthenticationServiceInterface
     */
    protected $authService;

    /**
     * @var UsersMapper
     */
    protected $userMapper;

    /**
     * @var UserIdentity
     */
    protected $userIdentity;


    public function __construct(AuthOptions $options)
    {
        $this -> authOptions = $options;
    } // __construct()


    public function login($login, $password)
    {
        $auth = $this -> getAuthService();
        $auth
            -> getAdapter()
            -> setIdentity($login)
            -> setCredential($password);

        return $auth -> authenticate();
    } // login()


    public function logout()
    {
        $this -> getAuthService() -> clearIdentity();
    } // logout()


    public function changePassword($newPassword)
    {
        $userIdentity = $this -> getUserIdentity();
        $mapper       = $this -> getMapper();

        if (!$userIdentity -> hasIdentity()) {
            return false;
        }

        $identity = $userIdentity -> getIdentity();
        $cost     = $this -> authOptions -> getPasswordCost();
        $salt     = Rand::getString($this -> authOptions -> getSaltSize());

        $bcrypt = new Bcrypt();
        $bcrypt -> setCost($cost);
        $bcrypt -> setSalt($salt);

        $cryptPassword = $bcrypt -> create($newPassword);

        return (bool) $mapper
            -> update([
                'password' => $cryptPassword,
                'salt'     => $salt
            ], $identity -> id);
    } // changePassword()


    /**
     * @param UsersMapper $mapper
     * @return $this
     */
    public function setMapper(UsersMapper $mapper)
    {
        $this -> userMapper = $mapper;
        return $this;
    } // setMapper()


    /**
     * @return UsersMapper
     * @throws Exception\InvalidMapperException
     */
    public function getMapper()
    {
        if (!$this -> userMapper) {
            throw new Exception\InvalidMapperException('User mapper is undefined');
        }
        return $this -> userMapper;
    } // getMapper()


    public function setUserIdentity(UserIdentity $userIdentity)
    {
        $this -> userIdentity = $userIdentity;
        return $this;
    } // setUserIdentity()


    public function getUserIdentity()
    {
        if (null === $this -> userIdentity) {
            throw new UserIdentityUndefinedException('UserIdentity is undefined');
        }

        return $this -> userIdentity;
    } // getUserIdentity()


    /**
     * @param AuthenticationServiceInterface $authServie
     * @return $this
     */
    public function setAuthService(AuthenticationServiceInterface $authServie)
    {
        $this -> authService = $authServie;
        return $this;
    } // setAuthService()


    /**
     * @return AuthenticationServiceInterface
     * @throws Exception\AuthenticationServiceUndefinedException
     */
    public function getAuthService()
    {
        if (null === $this -> authService) {
            throw new Exception\AuthenticationServiceUndefinedException('Authentication service is undefined');
        }

        return $this -> authService;
    } // getAuthService()

}