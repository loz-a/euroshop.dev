<?php
namespace Auth\Entity;

class User
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $login;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $salt;

    /**
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this -> id = (int) $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this -> id;
    }


    /**
     * @param $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this -> login = $login;
        return $this;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this -> login;
    }


    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this -> password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this -> password;
    }


    public function setSalt($salt)
    {
        $this -> salt = $salt;
        return $this;
    } // setSalt()


    public function getSalt()
    {
        return $this -> salt;
    } // getSalt()

} // User