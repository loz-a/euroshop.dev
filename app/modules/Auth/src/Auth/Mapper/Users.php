<?php
namespace Auth\Mapper;

use Auth\Entity\User;
use Core\Db\Table\DbTableAwareInterface;
use Core\Db\Table\DbTableAwareTrait;
use Core\Stdlib\Hydrator\HydratorAwareTrait;
use Zend\Hydrator\HydratorAwareInterface;

class Users implements DbTableAwareInterface, HydratorAwareInterface
{
    use DbTableAwareTrait, HydratorAwareTrait;

    protected function dbTableInit()
    {
        $this -> getDbTable() -> setTableName('users');
    } // dbTableInit()


    public function findUserById($identity)
    {
        $result = $this -> getDbTable() -> getByPk($identity);

        if ($result) {
            return $this -> getHydrator() -> hydrate($result, new User());
        }
        return null;
    } // findUserById()


    public function findUserByLogin($identity)
    {
        $result =  $this -> getDbTable() -> findOneBylogin($identity);

        if ($result) {
            return $this -> getHydrator() -> hydrate($result, new User());
        }
        return null;
    } // findOneByLogin()


    public function update($data, $id)
    {
        return $this -> getDbTable() -> update($data, $id);
    } // update()


} // Users
