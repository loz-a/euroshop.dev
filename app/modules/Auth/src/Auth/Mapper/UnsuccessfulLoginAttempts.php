<?php
namespace Auth\Mapper;

use Core\Db\Table\DbTableAwareInterface;
use Core\Db\Table\DbTableAwareTrait;

class UnsuccessfulLoginAttempts implements DbTableAwareInterface
{
    use DbTableAwareTrait;

    protected function DbTableInit()
    {
        $this -> getDbTable() -> setTableName('unsuccessful_login_attempts');
    } // DbTableInit()

    /**
     * @param $id
     * @return array|\Generator
     */
    public function findById($id)
    {
        return $this -> getDbTable() -> getByPk($id);
    } // findById()


    /**
     * @param array $data
     * @param $id
     * @return \PDOStatement
     */
    public function update(array $data, $id)
    {
        return $this -> getDbTable() -> update($data, $id);
    } // update()


    /**
     * @param array $data
     * @return \PDOStatement
     */
    public function insert(array $data)
    {
        return $this -> getDbTable() -> insert($data);
    } // insert()

} // UnsuccessfulLoginAttempts