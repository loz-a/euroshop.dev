<?php
namespace Auth\Options;

interface AuthInterface
{
    public function setUnsuccessfulLoginCount($count);
    public function getUnsuccessfulLoginCount();

    public function setUnsuccessfulLoginPauseDuration($duration);
    public function getUnsuccessfulLoginPauseDuration();

    public function setPasswordCost($cost);
    public function getPasswordCost();

    public function setSaltSize($size);
    public function getSaltSize();
}