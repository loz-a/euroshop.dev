<?php
namespace Auth\Options;

use Zend\Crypt\Password\Bcrypt;
use Zend\Stdlib\AbstractOptions;

class ModuleOptions extends AbstractOptions
    implements AuthInterface, PasswordInterface
{
    protected $unsuccessfulLoginCount = 3;

    public function setUnsuccessfulLoginCount($count)
    {
        $this -> unsuccessfulLoginCount = $count;
    }

    public function getUnsuccessfulLoginCount()
    {
        return $this -> unsuccessfulLoginCount;
    }


    protected $unsuccessfulLoginPauseDuration = 30;

    public function setUnsuccessfulLoginPauseDuration($duration)
    {
        $this -> unsuccessfulLoginPauseDuration = $duration;
    }

    public function getUnsuccessfulLoginPauseDuration()
    {
        return $this -> unsuccessfulLoginPauseDuration;
    }


    protected $passwordCost = 14;

    public function setPasswordCost($cost)
    {
        $this -> passwordCost = $cost;
    }

    public function getPasswordCost()
    {
        return $this -> passwordCost;
    }


    protected $saltSize = Bcrypt::MIN_SALT_SIZE;

    public function setSaltSize($size)
    {
        $this -> saltSize = $size;
    }

    public function getSaltSize()
    {
        return $this -> saltSize;
    }


    protected $passwordMinLength = 8;

    public function setPasswordMinLength($minLength)
    {
        $this -> passwordMinLength = $minLength;
    }

    public function getPasswordMinLength()
    {
        return $this -> passwordMinLength;
    }


}