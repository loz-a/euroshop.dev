<?php
namespace Auth\Core\Adapter;

use Zend\EventManager\Event as BaseEvent;

class Event extends BaseEvent
{
    const IDENTITY_KEY = 'identity';
    const MESSAGES_KEY = 'messages';
    const CODE_KEY     = 'code';

    protected $identity;
    protected $messages;
    protected $code;

    public function getIdentity()
    {
        return $this -> identity;
    } // getIdentity()


    public function setIdentity($identity = null)
    {
        if ( null === $identity ) {
            $this
                -> setCode()
                -> setMessages();
        }
        $this -> identity = $identity;
        $this -> setParam(self::IDENTITY_KEY, $identity);
        return $this;
    } // setIdentity()


    public function getCode()
    {
        return $this -> code;
    } // getCode()


    public function setCode($code = null)
    {
        $this -> code = $code;
        $this -> setParam(self::CODE_KEY, $code);
        return $this;
    } // setCode()


    public function getMessages()
    {
        return $this -> messages;
    } // getMessage()


    public function setMessages(array $messages = array())
    {
        $this -> messages = $messages;
        $this -> setParam(self::MESSAGES_KEY, $messages);
        return $this;
    } // setMessages()


    public function setMessage($message)
    {
        if (is_array($this -> messages)) {
            $this -> messages[] = $message;
        }
        else {
            $this -> messages = array($message);
        }

        $this -> setParam(self::MESSAGES_KEY, $this -> messages);
        return $this;
    } // setMessage()

} // Event
