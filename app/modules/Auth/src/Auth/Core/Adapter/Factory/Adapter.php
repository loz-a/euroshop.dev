<?php
namespace Auth\Core\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Auth\Core\Adapter\Adapter as AuthAdapter;
use Zend\ServiceManager\Factory\FactoryInterface;


class Adapter implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $adapter = new AuthAdapter();
        $adapter -> getEventManager() -> attach($container -> get('auth.listener'));
        return $adapter;
    } // create()
} 