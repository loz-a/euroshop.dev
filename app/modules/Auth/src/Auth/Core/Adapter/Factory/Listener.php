<?php
namespace Auth\Core\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Auth\Core\Adapter\Listener as AuthListener;
use Zend\ServiceManager\Factory\FactoryInterface;

class Listener implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $options = $container -> get('auth.options') -> toArray();

        $usersMapper          = $container -> get('mapper.users');
        $unsLogAttemptsMapper = $container -> get('mapper.unsuccessful_login_attempts');

        return new AuthListener($options, $usersMapper, $unsLogAttemptsMapper);
    } // create()
} 