<?php
namespace Auth\Core\Validator\Exception;

class RuntimeException extends \RuntimeException implements
    ExceptionInterface
{
}
