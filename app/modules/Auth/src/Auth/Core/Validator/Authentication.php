<?php
namespace Auth\Core\Validator;

use Traversable;
use Zend\Authentication\Adapter\ValidatableAdapterInterface;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;

/**
 * Authentication Validator
 */
class Authentication extends AbstractValidator
{
    /**
     * Error codes
     * @const string
     */
    const CREDENTIAL_INVALID = 'credentialInvalid';


    /**
     * Error Messages
     * @var array
     */
    protected $messageTemplates = [
        self::CREDENTIAL_INVALID => 'Invalid password',
    ];

    /**
     * Authentication Adapter
     * @var ValidatableAdapterInterface
     */
    protected $adapter;

    /**
     * Identity (or field)
     * @var string
     */
    protected $identity;

    /**
     * Credential (or field)
     * @var string
     */
    protected $credential;

    /**
     * Sets validator options
     *
     * @param mixed $options
     */
    public function __construct($options = null)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }

        if (is_array($options)) {
            if (array_key_exists('adapter', $options)) {
                $this -> setAdapter($options['adapter']);
            }
            if (array_key_exists('identity', $options)) {
                $this -> setIdentity($options['identity']);
            }
            if (array_key_exists('credential', $options)) {
                $this -> setCredential($options['credential']);
            }
        }
        parent::__construct($options);
    }

    /**
     * Get Adapter
     *
     * @return ValidatableAdapterInterface
     */
    public function getAdapter()
    {
        return $this -> adapter;
    }

    /**
     * Set Adapter
     *
     * @param  ValidatableAdapterInterface $adapter
     * @return Authentication
     */
    public function setAdapter(ValidatableAdapterInterface $adapter)
    {
        $this -> adapter = $adapter;
        return $this;
    }

    /**
     * Get Identity
     *
     * @return mixed
     */
    public function getIdentity()
    {
        return $this -> identity;
    }

    /**
     * Set Identity
     *
     * @param  mixed          $identity
     * @return Authentication
     */
    public function setIdentity($identity)
    {
        $this -> identity = $identity;
        return $this;
    }


    /**
     * Get Credential
     *
     * @return mixed
     */
    public function getCredential()
    {
        return $this -> credential;
    }

    /**
     * Set Credential
     *
     * @param  mixed          $credential
     * @return Authentication
     */
    public function setCredential($credential)
    {
        $this -> credential = $credential;
        return $this;
    }


    /**
     * Is Valid
     *
     * @param  mixed $value
     * @param  array $context
     * @return bool
     */
    public function isValid($value = null, $context = null)
    {
        if ($value !== null) {
            $this -> setCredential($value);
        }

        if (($context !== null) && array_key_exists($this -> identity, $context)) {
            $identity = $context[$this -> identity];
        } else {
            $identity = $this -> identity;
        }
        if (!$this -> identity) {
            throw new Exception\RuntimeException('Identity must be set prior to validation');
        }

        if (($context !== null) && array_key_exists($this -> credential, $context)) {
            $credential = $context[$this -> credential];
        } else {
            $credential = $this -> credential;
        }

        if (!$this->adapter) {
            throw new Exception\RuntimeException('Adapter must be set prior to validation');
        }
        $this -> adapter -> setIdentity($identity);
        $this -> adapter -> setCredential($credential);

        $result = $this -> adapter -> authenticate() -> isValid();

        if (!$result) {
            $this -> error(self::CREDENTIAL_INVALID);
            return false;
        }
        return true;
    }
}
