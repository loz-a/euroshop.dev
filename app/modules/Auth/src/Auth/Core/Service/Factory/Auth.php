<?php
namespace Auth\Core\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\Authentication\AuthenticationService;

class Auth implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $adapter = $container -> get('auth.adapter');
        $service = new AuthenticationService();
        $service -> setAdapter($adapter);
        return $service;
    } // createService()
} 