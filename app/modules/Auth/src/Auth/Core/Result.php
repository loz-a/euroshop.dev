<?php
namespace Auth\Core;

use Zend\Authentication\Result as BaseResult;

class Result extends BaseResult
{
    const FAILURE_TIMEOUT = -5;

    public function __construct($code, $identity, array $messages = array())
    {
        $code = (int) $code;

        if ( $code !== self::FAILURE_TIMEOUT ) {
            parent::__construct($code, $identity, $messages);
        }

        $this->code     = $code;
        $this->identity = $identity;
        $this->messages = $messages;
    }


    /**
     * @return mixed|null
     */
    public function getFirstMessage()
    {
        if (sizeof($this -> messages)) {
            return current($this -> messages);
        }
        return null;
    } // getFirstMessage()

} // Result
