<?php
namespace Auth\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\I18n\Translator\Translator as ZendI18nTranslator;
use Zend\Mvc\I18n\Translator as ZendTranslator;

class Translator implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $translator = new ZendI18nTranslator();

        $translator -> addTranslationFile(
            'phparray',
            sprintf('%s/app/modules/Auth/resources/languages/uk.php', getcwd()),
            'default',
            'uk_UA'
        );

        return new ZendTranslator($translator);
    }
}