<?php
namespace Auth\Validator\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Auth\Validator\ChangePassword as ChangePasswordValidator;

class ChangePassword implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return (new ChangePasswordValidator(
            $container -> get('auth.options'),
            $container -> get('auth.service.user_identity'),
            $container -> get('auth'),
            $container -> get('auth.validator.translator')
        ));
    }
}