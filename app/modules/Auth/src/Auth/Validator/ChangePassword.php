<?php
namespace Auth\Validator;

use Auth\Options\ModuleOptions as AuthModuleOptions;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Input;
use Zend\Validator\AbstractValidator;
use Zend\Validator\StringLength;
use Zend\Validator\Identical;
use Auth\Core\Validator\Authentication;
use Auth\Service\UserIdentity;
use Zend\Authentication\AuthenticationService;
use Zend\Mvc\I18n\Translator;

class ChangePassword extends InputFilter
{
    /**
     * @var AuthModuleOptions
     */
    protected $options;

    /**
     * @var UserIdentity
     */
    protected $userIdentity;

    /**
     * @var AuthenticationService
     */
    protected $auth;

    public function __construct(
        AuthModuleOptions $options,
        UserIdentity $userIdentity,
        AuthenticationService $auth,
        Translator $translator
    ){
        $this -> options      = $options;
        $this -> userIdentity = $userIdentity;
        $this -> auth         = $auth;

        //TODO
        AbstractValidator::setDefaultTranslator($translator);
    } // __construct()


    public function init()
    {
        $this
            -> add($this -> password())
            -> add($this -> newPassword())
            -> add($this -> confirm());
    } // init()


    protected function password()
    {
        $auth         = $this -> auth;
        $userIdentity = $this -> userIdentity;

        $password = new Input('password');
        $password
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new Authentication([
                'adapter'  => $auth -> getAdapter(),
                'identity' => $userIdentity -> getIdentity() -> login
            ]));

        return $password;
    } // password()


    protected function newPassword()
    {
        $newPassword = new Input('new_password');
        $newPassword
            -> setRequired(true)
            -> getValidatorchain()
            -> attach(new StringLength($this -> options -> getPasswordMinLength()));

        return $newPassword;
    } // newPassword()


    protected function confirm()
    {
        $confirm = new Input('confirm');
        $confirm
            -> setRequired(true)
            -> getValidatorChain()
            -> attach(new Identical([
                'token' => 'new_password',
                'messages'     => [
                    Identical::NOT_SAME => 'Passwords do not match'
                ]
            ]));

        return $confirm;
    } // confirm()

}