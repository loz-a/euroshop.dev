<?php
return [
    "Value is required and can't be empty" => "Значення є обов'язковим для заповнення і не може бути пустим",
    "The input is less than %min% characters long" => "Значення не може бути меньше за %min% символів",
    "Invalid password" => "Пароль не вірний",
    "Passwords do not match" => "Паролі не співпадають",
];