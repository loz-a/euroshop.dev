<?php
return array(

    'routes' => array(
        'login' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/login',
                'defaults' => array(
                    'controller' => 'auth.controller.login',
                    'action'     => 'index'
                )
            )
        ),

        'logout' => array(
            'type'    => 'Literal',
            'options' => array(
                'route'    => '/logout',
                'defaults' => array(
                    'controller' => 'auth.controller.login',
                    'action'     => 'logout'
                )
            )
        ),

        'change_password' => array(
            'type'    => 'Literal',
            'options' => array(
                'route' => '/change-password',
                'defaults' => array(
                    'controller' => 'auth.controller.password'
                )
            ),
            'may_terminate' => false,
            'child_routes'  => array(
                'get' => array(
                    'type'    => 'Method',
                    'options' => array(
                        'verb'     => 'get',
                        'defaults' => array(
                            'action' => 'index'
                        )
                    )
                ),
                'post' => array(
                    'type'    => 'Method',
                    'options' => array(
                        'verb'     => 'post',
                        'defaults' => array(
                            'action' => 'change'
                        )
                    )
                )
            )
        )
    ),

    'view' => array(
        'template_path_stack' => array(
            __DIR__ . '/templates'
        ),
        'template_map' => array(
            'auth/login/index'        => __DIR__ . '/templates/auth/login/index.phtml',
            'index/layout/navigation' => __DIR__ . '/templates/index/layout/navigation.phtml',
        )
    ),

    'service_manager' => array(
        'invokables' => array(
            'auth.controller.login'              => Auth\Controller\Login::class,
            'auth.controller.password'           => Auth\Controller\Password::class,
            'mapper.users'                       => Auth\Mapper\Users::class,
            'mapper.unsuccessful_login_attempts' => Auth\Mapper\UnsuccessfulLoginAttempts::class,

            'Auth.Annotations.LoginRequired' => Auth\Annotations\LoginRequired::class
        ),

        'factories' => array(
            'auth'                       => Auth\Core\Service\Factory\Auth::class,
            'auth.adapter'               => Auth\Core\Adapter\Factory\Adapter::class,
            'auth.listener'              => Auth\Core\Adapter\Factory\Listener::class,
            'auth.service.user_identity' => Auth\Service\Factory\UserIdentity::class,
            'auth.service.user'          => Auth\Service\Factory\User::class,
            'auth.options'               => Auth\Options\Factory::class,

            'auth.validator.change_password' => Auth\Validator\Factory\ChangePassword::class,
            'auth.validator.translator' => Auth\Validator\Factory\Translator::class
        ),

        'aliases' => array(
            'annotations.loginRequired' => 'Auth.Annotations.LoginRequired'
        )
    ),

    'admin_navigation' => [
        [
            'title'      => 'Адміністратор',
            'icon.class' => 'fa fa-user',
            'order' => 1,
            'child_items' => [
                [
                    'title'      => 'Змінити пароль',
                    'icon.class' => 'fa fa-key',
                    'matched.route.name' => 'change_password/get',
                    'order' => 1,
                ]
            ]
        ]
    ]

);