<?php
return [

    'routes' => [
        'admin' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/admin',
                'defaults' => [
                    'controller' => 'admin.controller.index',
                    'action'     => 'index'
                ]
            ]
        ],
    ],

    'view' => [
        'template_path_stack' => [
            __DIR__ . '/templates'
        ],
        'template_map' => [
            'admin/index/index'       => __DIR__ . '/templates/admin/index/index.phtml',
            'admin/index/layout'      => __DIR__ . '/templates/admin/layout/layout.phtml',
            'admin/widget/navigation' => __DIR__ . '/templates/admin/widget/navigation.phtml'
        ],
        'widgets_service_manager' => [
            'invokables' => [
                'admin.navigation'     => 'Admin\Widget\Navigation',
                'admin.flashMessages' => 'Admin\Widget\FlashMessages'
            ]
        ]
    ],

    'service_manager' => [
        'invokables' => [
            'admin.controller.index' => 'Admin\Controller\Index'
        ]
    ]

];