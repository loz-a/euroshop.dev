<?php
namespace Admin\Widget;

use Core\App as Application;
use Core\Stdlib\MinPriorityQueue;
use Core\View\Widget\AbstractWidget;
use Zend\Config\Config;

class Navigation extends AbstractWidget
{
    public function __invoke(Application $app)
    {
        $sm       = $app -> getServiceManager();
        $config   = $sm -> get('config') -> admin_navigation;
        $matchedRoutedName = $sm -> get('route_match') -> getMatchedRouteName();
        $routesManager     = $app -> getRoutesManager();

        $items = $this -> prepareItems($config);
        $renderedItems = $this -> renderItems($items, $matchedRoutedName, $routesManager);

        return $sm
            -> get('view')
            -> getRenderer()
            -> render('admin/widget/navigation', compact('renderedItems'));
    }


    protected function prepareItems($config)
    {
        $config = ($config instanceof Config) ? $config -> toArray() : $config;
        $queue  = new MinPriorityQueue();
        $queue -> setExtractFlags(MinPriorityQueue::EXTR_DATA);

        $temp = null;

        foreach ($config as $item) {
            $temp['title'] = $item['title'];
            $temp['icon.class'] = isset($item['icon.class']) ? $item['icon.class'] : '';

            if (isset($item['matched.route.name'])) {
                $temp['matched.route.name'] = $item['matched.route.name'];
            }

            if (isset($item['child_items'])) {
                $temp['child_items'] = $this -> prepareItems($item['child_items']);
            }

            $order = $item['order'];
            $queue -> insert($temp, $order);
        }
        return $queue;
    } // prepareItems()


    protected function renderItems($items, $matchedRouteName, $routesManager)
    {
        $result = [];
        foreach($items as $item) {
            $iconClass = isset($item['icon.class']) ? $item['icon.class'] : null;
            $isActive  = isset($item['matched.route.name']) && $item['matched.route.name'] === $matchedRouteName;
            $route     = isset($item['matched.route.name']) ? $routesManager -> urlFor($item['matched.route.name']) : '#';
            $sublist   = isset($item['child_items']) ? $this -> renderItems($item['child_items'], $matchedRouteName, $routesManager) : '';

            $result[] = $this -> listItem(
                $this -> link(
                    $this -> itemContent($item['title'], $iconClass),
                    $route,
                    $isActive
                ),
                $sublist
            );
        }

        return join(PHP_EOL, $result);
    } // generate()


    protected function listItem($link, $sublist = '')
    {
        if ($sublist) {
            $sublist = sprintf('%s<ul>%s</ul>', PHP_EOL, $sublist);
        }
        return sprintf('<li>%s%s</li>', $link, $sublist);
    } // listItem()


    protected function link($itemContent, $route = null, $isActive = false)
    {
        $active = $isActive ? ' class="active"' : '';
        return sprintf('<a href="%s"%s>%s</a>', $route, $active, $itemContent);
    } // link\()


    protected function itemContent($title, $iconClass = null)
    {
        $result = '';
        if ($iconClass) {
            $result = sprintf('<i class="%s"></i>', $iconClass);
        }
        return ltrim(sprintf('%s %s', $result, $title));
    } // itemContent()
}