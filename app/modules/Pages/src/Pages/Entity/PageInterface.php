<?php
namespace Pages\Entity;

interface PageInterface
{
    public function setAlias($alias);
    public function getAlias();

    public function setAuthor($author);
    public function getAuthor();


    /**
     * @param ContentInterface $content
     * @return $this
     */
    public function setContent(ContentInterface $content);

    /**
     * @return ContentInterface
     */
    public function getContent();


    public function setCreated($created);
    public function getCreated();

    public function setDescription($description);
    public function getDescription();

    public function setId($id);
    public function getId();

    public function setTranslationId($id);
    public function getTranslationId();

    public function setSlug($slug);
    public function getSlug();

    public function setStatus($status);
    public function getStatus();

    public function setTitle($title);
    public function getTitle();

    public function setUpdated($updated);
    public function getUpdated();
} 