<?php
namespace Pages\Hydrator;

use Zend\Hydrator\Aggregate\AggregateHydrator;
use Zend\Hydrator\ClassMethods;
use Pages\Entity\Page as PageEntity;
use Pages\Entity\Content as ContentEntity;

class Page extends AggregateHydrator
{
    public function __construct()
    {
        $this -> add(new ClassMethods());
        $this -> add(new Content());
    } // __construct()


    public function hydrate(array $data, $object)
    {
        if (array_key_exists('content', $data)) {
            unset($data['content']);
        }
        return parent::hydrate($data, $object);
    } // hydrate()


    public function hydratePage(array $data)
    {
        return $this -> hydrate($data, new PageEntity());
    } // hydratePage()


    public function extract($object)
    {
        $data = parent::extract($object);

        if ($data['content'] instanceof ContentEntity) {
            $data['preparsed_content'] = $data['content'] -> getRawText();
            $data['parsed_content']    = $data['content'] -> getParsedText();
        }

        return $data;
    } // extract()
} 