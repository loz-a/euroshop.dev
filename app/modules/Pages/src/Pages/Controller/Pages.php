<?php
namespace Pages\Controller;

use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\App\Controller\Restful\AbstractRestfulController;

class Pages extends AbstractRestfulController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.jsonResponse
     */
    public function create($data)
    {
        $validator = $this -> get('pages.validator');

        $validator -> setData($data);
        $isValid = $validator -> isValid();
        if ($isValid) {
            return $this -> get('pages.service') -> create($validator -> getValues());
        }

        return array(
            'data'   => $data,
            'messages' => $validator -> getMessages()
        );
    }


    /**
     * @annotations.jsonResponse
     */
    public function deleteById($id)
    {
        return $this -> get('pages.service') -> remove($id);
    }


    /**
     * @annotations.jsonResponse
     */
    public function getById($id)
    {
        return array('id' => $id);
    }


    /**
     * @annotations.jsonResponse
     */
    public function getPartialList($start, $end)
    {
        $params = $this
            -> getApplication()
            -> getParams()
            -> filter(function($value, $key) {
                return strpos($key, 'limit(') !== 0;
            });

        $pagesService = $this -> get('pages.service');
        $pages = $pagesService -> getPages($end - $start, $start, $params);
        $total = $pagesService -> getPagesTotal();



        return array(
            'total' => $total,
            'items' => $pages
        );
    } // getPartialList()


    /**
     * @annotations.jsonResponse
     */
    public function update($id, $data)
    {
        $pagesService = $this -> get('pages.service');

        $result = $pagesService -> update($data, $id);
        return $result;
    }


} 