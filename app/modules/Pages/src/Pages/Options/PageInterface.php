<?php
namespace Pages\Options;


interface PageInterface
{
    public function setAliasMaxLength($maxLength);
    public function getAliasMaxLength();

    public function setTitleMaxLength($maxLength);
    public function getTitleMaxLength();

    public function setDescriptionMaxLength($maxLength);
    public function getDescriptionMaxLength();
}
