<?php
return [

    'routes' => [
        'home' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/',
                'defaults' => [
                    'controller' => Index\Controller\Index::class,
                    'action'     => 'index'
                ]
            ]
        ],
        'product' => [
            'type'    => 'Regex',
            'options' => [
                'regex' => '/product/(?<id>[0-9]+)-(?<slug>[a-zA-Z0-9_-]+)',
                'spec'  => '/product/%id%-%slug%',
                'defaults' => [
                    'controller' => Index\Controller\Product::class,
                    'action'     => 'index'
                ]
            ]
        ],
        'product_search' => [
            'type'    => 'Literal',
            'options' => [
                'route'    => '/search',
                'defaults' => [
                    'controller' => Index\Controller\ProductSearch::class
                ]
            ],
            'may_terminate' => false,
            'child_routes' => [
                'get' => [
                    'type'    => 'Method',
                    'options' => [
                        'verb'     => 'get',
                        'defaults' => [
                            'action' => 'search'
                        ]
                    ]
                ],
                'post' => [
                    'type'    => 'Method',
                    'options' => [
                        'verb'    => 'post',
                        'defaults' => [
                            'action' => 'advancedSearch'
                        ]
                    ]
                ],
                'not_found' => [
                    'type'    => 'Literal',
                    'options' => [
                        'route'    => '/not-found',
                        'defaults' => [
                            'action' => 'notFound'
                        ]
                    ]
                ]
            ]
        ],
        'catalog' => [
            'type'    => 'Segment',
            'options' => [
                'route'       => '/catalog/:id{-}-:slug[/page/:page]',
                'constraints' => [
                    'id'     => '[0-9]+',
                    'slug' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    'page' => '\d+'
                ],
                'defaults' => [
                    'controller' => Index\Controller\Catalog::class,
                    'action'     => 'index',
                    'page'       => 1
                ]
            ],
            'may_terminate' => true,
            'child_routes' => [
                'filter' => [
                    'type'    => 'Literal',
                    'options' => [
                        'route'    => '/filter',
                        'defaults' => [
                            'action' => 'filter'
                        ]
                    ]
                ]
            ]
        ]
    ],

    'view' => [
        'template_path_stack' => [
            __DIR__ . '/templates'
        ],
        'template_map' => [
            'index/index/index'       => __DIR__ . '/templates/index/index/index.phtml',
            'index/product/index'     => __DIR__ . '/templates/index/product/index.phtml',
            'index/product-search/not-found' => __DIR__ . '/templates/index/product-search/not-found.phtml',
            'index/catalog/index'     => __DIR__ . '/templates/index/catalog/index.phtml',
            'index/widget/pagination' => __DIR__ . '/templates/index/widget/pagination.phtml',

            'index/layout/aside'      => __DIR__ . '/templates/index/layout/aside.phtml',
            'index/layout/footer'     => __DIR__ . '/templates/index/layout/footer.phtml',
            'index/layout/header'     => __DIR__ . '/templates/index/layout/header.phtml',
            'index/layout/layout'     => __DIR__ . '/templates/index/layout/layout.phtml',
        ],
        'widgets_service_manager' => [
            'invokables' => [
                'goods.pagination' => Index\Widget\PaginationControl::class
            ]
        ]
    ],

    'service_manager' => [
        'invokables' => [
            Index\Controller\Index::class         => Index\Controller\Index::class,
            Index\Controller\Product::class       => Index\Controller\Product::class,
            Index\Controller\ProductSearch::class => Index\Controller\ProductSearch::class,
            Index\Controller\Catalog::class       => Index\Controller\Catalog::class,
        ]
    ],

];