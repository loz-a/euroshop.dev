<?php
namespace Index\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Core\App\Exception\PageNotFoundException;

use Catalog\Service\Catalog as CatalogService;
use Catalog\Service\Goods as GoodsService;

class Catalog extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template index/catalog/index
     * @annotations.layout   index/layout/layout
     */
    public function indexAction()
    {
        $params  = $this -> get('route_match') -> getParams();
        $catalog = $this -> get(CatalogService::class) -> getCatalogWithChildren($params['id']);

        if (!$catalog) {
            throw new PageNotFoundException('Page not found');
        }

        $goods   = $this -> get(GoodsService::class) -> getPagingListByCatalogId($catalog['id']);
        $goods -> setCurrentPageNumber($params['page']);

        return compact('catalog', 'goods');
    } // index()


    /**
     * @annotations.template index/catalog/index
     * @annotations.layout   index/layout/layout
     */
    public function filterAction()
    {
        $params      = $this -> get('route_match') -> getParams();
        $queryParams = $this -> get('request') -> getQuery() -> toArray();

        $catalog = $this -> get(CatalogService::class) -> getCatalogWithChildren($params['id']);

        if (!$catalog) {
            throw new PageNotFoundException('Page not found');
        }

        $goods   = $this -> get(GoodsService::class) -> getPagingListByCatalogId($catalog['id'], $queryParams);
        $goods -> setCurrentPageNumber($params['page']);

        return compact('catalog', 'goods', 'queryParams');
    } // filterAction()

}