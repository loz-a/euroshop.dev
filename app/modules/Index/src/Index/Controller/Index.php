<?php
namespace Index\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;

class Index extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template index/index/index
     * @annotations.layout   index/layout/layout
     */
    public function indexAction()
    {
        return [];
    } // index()
}