<?php
namespace Index\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Catalog\Mapper\Goods\Goods as GoodsMapper;
use Catalog\Mapper\Goods\GoodsDetailed as GoodsDetailedMapper;
use Catalog\Mapper\Goods\GoodsImageStore as GoodsImagesMapper;
use Core\App\Exception\PageNotFoundException;

class Product extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait;

    /**
     * @annotations.template index/product/index
     * @annotations.layout   index/layout/layout
     * @annotations.routeMatch int id
     */
    public function indexAction($id)
    {
        $goods = $this -> get(GoodsMapper::class) -> getByPk($id, false);

        if (!$goods) {
            throw new PageNotFoundException('Page not found');
        }

        $goodsDetailed = $this -> get(GoodsDetailedMapper::class) -> getByGoodsId($id);
        $goodsImages   = $this -> get(GoodsImagesMapper::class) -> getByGoods($id);

        return compact('goods', 'goodsDetailed', 'goodsImages');
    } // index()

}