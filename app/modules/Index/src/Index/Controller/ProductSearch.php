<?php
namespace Index\Controller;

use Core\App\AppAwareInterface;
use Core\App\AppAwareTrait;
use Core\Annotations\AnnotationsManagerAwareInterface;
use Core\Annotations\AnnotationsManagerAwareTrait;
use Core\App\Controller\AbstractController;
use Catalog\Service\Goods as GoodsService;
use Core\App\Controller\Action\RedirectTrait;

class ProductSearch extends AbstractController
    implements AppAwareInterface, AnnotationsManagerAwareInterface
{
    use AppAwareTrait, AnnotationsManagerAwareTrait, RedirectTrait;

    /**
     * @annotations.template index/product-search/search
     * @annotations.layout   index/layout/layout
     * @annotations.request str keyword
     */
    public function searchAction($keyword)
    {

        $goods = $this -> get(GoodsService::class) -> getSearchResult($keyword);

        if (!$goods) {
            $this -> redirect(url_for('product_search/not_found', [], ['query' => compact('keyword')]));
        }

        return compact('goods');
    } // searchAction()


    public function advancedSearchAction()
    {

    } // advancedSearchAction()


    /**
     * @annotations.template index/product-search/not-found
     * @annotations.layout   index/layout/layout
     * @annotations.request str keyword
     */
    public function notFoundAction($keyword)
    {
        return compact('keyword');
    } // notFoundAction()

}