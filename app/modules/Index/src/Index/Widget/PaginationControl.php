<?php
namespace Index\Widget;

use Core\App as Application;
use Core\View\Widget\AbstractWidget;
use Zend\Paginator\Paginator;

class PaginationControl extends AbstractWidget
{
    public function __invoke(
        Application $app,
        Paginator $paginator,
        $partial,
        $params = null
    ) {
        $scrollingStyle = 'Sliding';
        $sm = $app -> getServiceManager();

        $pages = get_object_vars($paginator -> getPages($scrollingStyle));
        if ($params !== null) {
            $pages = array_merge($pages, (array) $params);
        }

        return $sm -> get('view') -> getRenderer() -> render($partial, $pages);
    } // __invoke()

}