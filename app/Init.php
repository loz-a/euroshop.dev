<?php
namespace App;

use Core\App as Application;

class Init
{
    public function __invoke(Application $app)
    {
        $response = $app -> getServiceManager() -> get('response');
        $em       = $app -> getEventManager();

        $em -> attach('404', function() use ($response) {
            $content = file_get_contents(__DIR__ . '/modules/Index/templates/index/error/404.phtml');

            $response -> setStatusCode(404);
            $response -> setContent($content);
        });


        $em -> attach('500', function() use ($response) {
            $content = file_get_contents(__DIR__ . '/modules/Index/templates/index/error/500.phtml');

            $response -> setStatusCode(500);
            $response -> setContent($content);
        });


        $em -> attach('begin', function() {
            //TODO set locale in config
            \Locale::setDefault('uk_UA');
        });

    } // __invoke()
} 