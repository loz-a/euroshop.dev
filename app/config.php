<?php
return array(

    'modules' => array(
        'Index'     => __DIR__ . '/modules/Index',
        'Admin'     => __DIR__ . '/modules/Admin',
        'Auth'      => __DIR__ . '/modules/Auth',
        'Catalog'   => __DIR__ . '/modules/Catalog',
        'PriceList' => __DIR__ . '/modules/Catalog/submodules/PriceList'
    ),

    'db' => array(
        'dsn' => 'mysql:host=localhost;dbname=euroshop;charset=utf8',
        'user' => 'root',
        'password' => 'AM23naRmysql'
    ),

    'auth' => array(
        'password_cost' => 14,
        'unsuccessful_login_count' => 3,
        'unsuccessful_login_pause_duration' => 30
    ),

    'service_manager' => array(
        'invokables' => array(
            'view.map_resolver'        => Core\View\Resolver\TemplateMapResolver::class,
            'view.path_stack_resolver' => Core\View\Resolver\TemplatePathStack::class,
            'view_model.view'          => Core\View\Model\ViewModel::class,
            'view_model.json'          => Core\View\Model\JsonModel::class,

            'Core.Annotations.Action.RouteMatch' => Core\Annotations\Action\RouteMatch::class,
            'Core.Annotations.Action.Request'    => Core\Annotations\Action\Request::class,
            'Core.View.Annotations.Layout'       => Core\View\Annotations\Layout::class,
            'Core.View.Annotations.Template'     => Core\View\Annotations\Template::class,
            'Core.View.Annotations.JsonResponse' => Core\View\Annotations\JsonResponse::class,

            'flashMessenger' => Zend\Mvc\Controller\Plugin\FlashMessenger::class,
        ),

        'aliases' => array(
            'annotations.routeMatch'   => 'Core.Annotations.Action.RouteMatch',
            'annotations.request'       => 'Core.Annotations.Action.Request',
            'annotations.layout'        => 'Core.View.Annotations.Layout',
            'annotations.template'      => 'Core.View.Annotations.Template',
            'annotations.jsonResponse' => 'Core.View.Annotations.JsonResponse'
        ),

        'factories' => array(
            'view.json_renderer' => Core\View\Renderer\Factory\JsonRenderer::class,
            'view.php_renderer'  => Core\View\Renderer\Factory\PhpRenderer::class,
            'view'               => Core\App\View\Factory::class,
            'db.connection'      => Core\App\Db\ConnectionFactory::class
        ),

        'initializers' => array(
            Core\Db\Table\Initializer::class,
            Core\Stdlib\InputFilterInitializer::class
        )
    )

);